﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace Transversal.Security
{
    public class Common
    {
        #region Public Methods

        public static string SHA256Hash(string input)
        {
            var result = string.Empty;
            using (var provider = new SHA256CryptoServiceProvider())
            {
                byte[] inputBytes = Encoding.UTF8.GetBytes(input);
                byte[] hashedBytes = provider.ComputeHash(inputBytes);

                var output = new StringBuilder();

                for (int i = 0; i < hashedBytes.Length; i++)
                    output.Append(hashedBytes[i].ToString("x2").ToLower());

                result = output.ToString();
            }

            return result;
        }

        public static string GenerarPasswd(int limit = 16)
        {
            char[] Chars = new char[] {
                            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
                            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
                        };
            string RandomPass = string.Empty;
            Random Random = new Random();

            for (byte a = 0; a < limit; a++)
            {
                RandomPass += Chars[Random.Next(0, 61)];
            };
            return RandomPass;
        }

        #endregion
    }
}
