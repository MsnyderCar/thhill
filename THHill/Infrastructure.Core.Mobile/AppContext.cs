﻿using Microsoft.EntityFrameworkCore;
using System;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Infrastructure.Core.Mobile
{
    public class AppContext : DbContext
    {
        #region Members

        protected bool IsAndroid { get; set; }
        protected bool Clear { get; set; }
        protected string DbPath { get; set; }
        protected static string DbName = "EFDb.db3";
        
        #endregion

        #region Constructor

        public AppContext()
        {
            IsAndroid = true;
            Clear = false;
            Database.EnsureCreated();
        }

        public AppContext(DbContextOptions options) : base(options)
        {
            Clear = false;
            GetDbPath();
        }

        public AppContext(bool isAndroid)
        {
            IsAndroid = isAndroid;
            Clear = false;
            GetDbPath();
        }

        public AppContext(bool isAndroid, bool clear)
        {
            IsAndroid = isAndroid;
            Clear = clear;
            GetDbPath();
        }

        #endregion

        #region Private Methods

        protected void GetDbPath()
        {
            if (!IsAndroid) SQLitePCL.Batteries_V2.Init();

            DbPath = IsAndroid
                ? Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), DbName)
                : Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "..", "Library", DbName);

            if (Clear && File.Exists(DbPath))
            {
                File.Delete(DbPath);
            }
                
            Database.EnsureCreatedAsync();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            try
            {
                optionsBuilder.UseSqlite($"Filename={DbPath}");
                base.OnConfiguring(optionsBuilder);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region Mapeado mediante Fluent para entidades

            var theList = Assembly.GetAssembly(this.GetType()).GetTypes()
                 .Where(x => x.Name.EndsWith("Mapping", StringComparison.Ordinal))
                 .ToList();

            foreach (var item in theList)
            {
                Console.WriteLine(item.Name);
                modelBuilder.ApplyConfiguration((dynamic)Activator.CreateInstance(item));
            }

            base.OnModelCreating(modelBuilder);

            #endregion
        }

        #endregion
    }
}