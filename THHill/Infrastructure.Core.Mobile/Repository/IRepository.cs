﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Infrastructure.Core.Mobile.Repository
{
    public interface IRepository<TEntity> where TEntity : class
    {
        TEntity GetById<TKey>(TKey id);
        List<TEntity> GetAll();
        IQueryable<TEntity> GetAll(List<string> includes, int internalQuery = 0);
        List<TEntity> GetByFilter(Expression<Func<TEntity, bool>> filter);
        IQueryable<TEntity> GetByFilter(Expression<Func<TEntity, bool>> filter, List<string> includes, int internalQuery = 0);
        List<TEntity> GetByFilter(Expression<Func<TEntity, bool>> filter, List<KeyValuePair<string, bool>> orderBy, int pageSize, int pageNumber, out int rowsCount);
        List<TEntity> GetByFilter(Expression<Func<TEntity, bool>> filter, List<string> includes, List<KeyValuePair<string, bool>> orderBy, int pageSize, int pageNumber, out int rowsCount);
        bool Exist(Expression<Func<TEntity, bool>> filter);

        TEntity Insert(TEntity entity);
        TEntity Update(TEntity entity);
        bool Delete(TEntity entity);
        bool DeleteById<TKey>(TKey id);
    }
}
