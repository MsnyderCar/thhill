﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using Transversal.Common.Utilities;

namespace Infrastructure.Core.Mobile.Repository
{
    public class Respository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        #region Members

        private AppContext Context;

        private DbSet<TEntity> _entities;

        protected DbSet<TEntity> Entities
        {
            get
            {
                if (_entities == null)
                {
                    _entities = Context.Set<TEntity>();
                }
                return _entities;
            }
        }

        #endregion

        #region Constructor

        public Respository(AppContext context)
        {
            Context = context;
        }

        #endregion

        #region IRepository

        #region Select

        public TEntity GetById<TKey>(TKey id)
        {
            var entity = Task.Run(async () => await Entities.FindAsync(id)).Result;
            if(entity != null) Context.Entry(entity).State = EntityState.Detached;
            return entity;
        }

        public List<TEntity> GetAll()
        {
            return Task.Run(async () => await Entities.ToListAsync()).Result;
        }

        public IQueryable<TEntity> GetAll(List<string> includes, int internalQuery = 0)
        {
            var result = Task.Run(async () => await GetAllII(includes).ToListAsync()).Result;
            return internalQuery == 0 ? FixIncludes(result, includes).AsQueryable() : result.AsQueryable();
        }

        public List<TEntity> GetByFilter(Expression<Func<TEntity, bool>> filter)
        {
            return Task.Run(async () => await GetAllI().Where(filter).ToListAsync()).Result;
        }

        public IQueryable<TEntity> GetByFilter(Expression<Func<TEntity, bool>> filter, List<string> includes, int internalQuery = 0)
        {
            var result = Task.Run(async () => await GetAllII(includes).Where(filter).ToListAsync()).Result;
            return internalQuery == 0 ? FixIncludes(result, includes).AsQueryable() : result.AsQueryable();
        }

        public List<TEntity> GetByFilter(Expression<Func<TEntity, bool>> filter, List<KeyValuePair<string, bool>> orderBy, int pageSize, int pageNumber, out int rowsCount)
        {
            var query = GetAllI().Where(filter);
            rowsCount = Task.Run(async () => await query.ToListAsync()).Result.Count;
            return Task.Run(async () => await GetPaged(query, orderBy, pageSize, pageNumber).ToListAsync()).Result;
        }

        public List<TEntity> GetByFilter(Expression<Func<TEntity, bool>> filter, List<string> includes, List<KeyValuePair<string, bool>> orderBy, int pageSize, int pageNumber, out int rowsCount)
        {
            var query = GetAllII(includes).Where(filter);
            rowsCount = Task.Run(async () => await query.ToListAsync()).Result.Count;

            return FixIncludes(Task.Run(async () => await GetPaged(query, orderBy, pageSize, pageNumber).ToListAsync()).Result, includes);
        }

        public bool Exist(Expression<Func<TEntity, bool>> filter)
        {
            return Task.Run(async () => await Entities.AnyAsync(filter)).Result;
        }

        #endregion

        #region Insert

        public TEntity Insert(TEntity entity)
        {
            return Task.Run(async () => await InsertAsync(entity)).Result;
        }

        #endregion

        #region Update

        public TEntity Update(TEntity entity)
        {
            return Task.Run(async () => await UpdateAsync(entity)).Result;
        }

        #endregion

        #region Delete

        public bool Delete(TEntity entity)
        {
            return Task.Run(async () => await DeleteAsync(entity)).Result;
        }

        public bool DeleteById<TKey>(TKey id)
        {
            return Delete(GetById(id));
        }

        #endregion

        #endregion

        #region Private Methods

        private void ValidateEntity(TEntity entity)
        {
            if (entity == null)
            {
                throw new Exception("entidad");
            }
        }

        private IQueryable<TEntity> GetAllI()
        {
            var entities = Entities.AsNoTracking().AsQueryable<TEntity>();

            entities.ForEachAsync(entity => {
                Context.Entry(entity).State = EntityState.Detached;
            });
            
            return entities;
        }

        private IQueryable<TEntity> GetAllII(List<string> includes)
        {
            var query = GetAllI();
            foreach (var include in includes)
            {
                query = query.Include(include);
            }
            return query;
        }

        private IQueryable<TEntity> GetPaged(IQueryable<TEntity> Query, List<KeyValuePair<string, bool>> orderBy, int pageSize, int pageNumber)
        {
            Expression methodCallExpression = Query.Expression;

            foreach (KeyValuePair<string, bool> order in orderBy)
            {
                ParameterExpression parameter = Expression.Parameter(Query.ElementType, "");

                MemberExpression property = null;

                if (order.Key.Contains("."))
                {
                    string[] parts = order.Key.Split('.');
                    foreach (string part in parts)
                    {
                        if (property == null) property = Expression.Property(parameter, part);
                        else property = Expression.Property(property, part);
                    }
                }
                else property = Expression.Property(parameter, order.Key);

                LambdaExpression lambda = Expression.Lambda(property, parameter);

                methodCallExpression = Expression.Call(
                    typeof(Queryable),
                    !order.Value ? "OrderBy" : "OrderByDescending",
                    new Type[] { Query.ElementType, property.Type },
                    Query.Expression,
                    Expression.Quote(lambda)
                );

            }

            Query = Query.Provider.CreateQuery<TEntity>(methodCallExpression);

            return Query.Skip(pageSize * pageNumber).Take(pageSize).Select(p => p);
        }

        private async Task<TEntity> InsertAsync(TEntity entity)
        {
            ValidateEntity(entity);
            await Context.Set<TEntity>().AddAsync(entity);
            await Context.SaveChangesAsync();
            return entity;
        }

        private async Task<TEntity> UpdateAsync(TEntity entity)
        {
            if (Objects.GetProperty(entity, "UpdatedDate") != null)
                Objects.SetValue(entity, "UpdatedDate", DateTime.Now);
            ValidateEntity(entity);
            Entities.Update(entity);
            await Context.SaveChangesAsync();
            return entity;
        }

        private async Task<bool> DeleteAsync(TEntity entity)
        {
            ValidateEntity(entity);
            Entities.Remove(entity);
            await Context.SaveChangesAsync();
            return true;
        }

        private void FixIncludesRecursive(List<object> elements, List<string> includes)
        {
            if (elements.Count > 0)
            {
                try
                {
                    var baseProps = Objects.GetPropertiesByType(elements.FirstOrDefault().GetType());

                    var propsInternal = new List<KeyValuePair<string, List<string>>>();

                    includes.ForEach(inc => {
                        PropertyInfo prop = null;
                        inc.Split('.').ToList().ForEach(part => {
                            prop = prop == null ? baseProps.Where(p => p.Name.Equals(part)).FirstOrDefault() : Objects.GetPropertiesByType(prop.PropertyType.Name.Contains("Collection") ? prop.PropertyType.GetTypeInfo().GenericTypeArguments[0] : prop.PropertyType).Where(p => p.Name.Equals(part)).FirstOrDefault();
                        });
                        propsInternal.Add(new KeyValuePair<string, List<string>>(inc, prop.PropertyType.Name.Contains("Collection") ? new List<string>() : Objects.GetPropertiesByType(prop.PropertyType).Where(p => !p.PropertyType.Namespace.Equals("System")).Select(p => p.Name).ToList()));
                    });

                    var propsDirect = baseProps.Where(p => !includes.Any(v => v.Equals(p.Name)) && !p.PropertyType.Namespace.Equals("System")).Select(p => p.Name).ToList();

                    foreach (var element in elements)
                    {
                        foreach (var prop in propsDirect)
                        {
                            if (element != null) Objects.SetValueBasic(element, prop, null);
                        }

                        foreach (var prop in propsInternal)
                        {
                            if (prop.Value.Count > 0)
                            {
                                object member = null;

                                var parts = prop.Key.Split('.');
                                int i = 0;
                                while (i < parts.Length)
                                {
                                    member = Objects.GetValue(member == null ? element : member, parts[i]);
                                    if (member == null) break;
                                    if (member.GetType().Name.Contains("HashSet"))
                                    {
                                        FixIncludesRecursive(
                                            ((IEnumerable)member).Cast<object>().ToList(),
                                            includes
                                                .Where(inc => inc.StartsWith($"{string.Join(".", parts.Take(i + 1))}."))
                                                .Select(inc => string.Join(".", inc.Split('.').Skip(i + 1)))
                                                .ToList()
                                            );

                                        break;
                                    }
                                    i++;
                                }

                                if (i == parts.Length)
                                {
                                    prop.Value.ForEach(value => {
                                        if (member != null && !propsInternal.Any(p => p.Key.Equals($"{prop.Key}.{value}"))) Objects.SetValueBasic(member, value, null);
                                    });
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        private List<TEntity> FixIncludes(List<TEntity> elements, List<string> includes)
        {
            try
            {
                FixIncludesRecursive(elements.Cast<object>().ToList(), includes);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return elements;
        }

        #endregion
    }
}
