﻿using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Data.Mobile
{
    public class Main : Core.Mobile.AppContext
    {
        #region Constructor

        public Main() : base() { }

        public Main(DbContextOptions options) : base(options) { }

        public Main(bool isAndroid) : base(isAndroid) { }

        public Main(bool isAndroid, bool clear) : base(isAndroid, clear) { }

        #endregion
    }

    public class Dummy {
        public string DummyField { get; set; }
    }
}
