﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Transversal.Common.Entities.DataBase.Mobile;

namespace Infrastructure.Data.Mobile.Mappings
{
    public class RecordStatesMapping : IEntityTypeConfiguration<RecordState>
    {
        public void Configure(EntityTypeBuilder<RecordState> builder)
        {
            builder.ToTable("RecordStates", "Mobile");

            builder.HasKey(hk => hk.Id);
            builder.Property(p => p.Id).ValueGeneratedNever();

            builder.Property(p => p.Name).HasMaxLength(32);
        }
    }

    public class DataTypesMapping : IEntityTypeConfiguration<DataType>
    {
        public void Configure(EntityTypeBuilder<DataType> builder)
        {
            builder.ToTable("DataTypes", "Mobile");

            builder.HasKey(hk => hk.Id);
            builder.Property(p => p.Id).ValueGeneratedNever();

            builder.Property(p => p.Name).HasMaxLength(32);
            builder.Property(p => p.NameSpace).HasMaxLength(128);
        }
    }

    public class DataMapping : IEntityTypeConfiguration<Transversal.Common.Entities.DataBase.Mobile.Data>
    {
        public void Configure(EntityTypeBuilder<Transversal.Common.Entities.DataBase.Mobile.Data> builder)
        {
            builder.ToTable("Data", "Mobile");

            builder.HasKey(hk => hk.Id);
            builder.Property(p => p.Id).ValueGeneratedOnAdd();

            builder.HasOne<RecordState>(hr => hr.RecordState)
                .WithMany(wm => wm.Data)
                .HasForeignKey(hf => hf.RecordStateId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne<DataType>(hr => hr.DataType)
                .WithMany(wm => wm.Data)
                .HasForeignKey(hf => hf.DataTypeId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }

    public class MediaMapping : IEntityTypeConfiguration<Media>
    {
        public void Configure(EntityTypeBuilder<Media> builder)
        {
            builder.ToTable("Media", "Mobile");

            builder.HasKey(hk => hk.Id);
            builder.Property(p => p.Id).ValueGeneratedOnAdd();

            builder.Property(p => p.Name).HasMaxLength(1024);
            builder.Property(p => p.Description).HasMaxLength(1024);

            builder.HasOne<RecordState>(hr => hr.RecordState)
                .WithMany(wm => wm.Media)
                .HasForeignKey(hf => hf.RecordStateId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne<Transversal.Common.Entities.DataBase.Mobile.Data>(hr => hr.Data)
                .WithMany(wm => wm.MediaFiles)
                .HasForeignKey(hf => hf.DataId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
