﻿using Infrastructure.Data.Mobile;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace Infrastructure.Data.Data
{
    public class DbInitializer
    {
        public DbInitializer(Main context)
        {
            var migrations = context.Database.GetAppliedMigrations().ToList().Count;

            if (migrations == 0)
            {
                context.Database.Migrate();
                context.Database.EnsureCreated();
            }

            using (var ct = context.Database.BeginTransaction())
            {
                try
                {
                    ct.Commit();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    ct.Rollback();
                }
            }
        }
    }
}
