﻿using Infrastructure.Core.Mobile.UnityOfWork;
using Infrastructure.CrossCutting.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using Transversal.Common.Entities.Api;
using Transversal.Common.Entities.DataBase.Authentication;
using Transversal.Common.Entities.DataBase.Mobile;
using Transversal.Common.Entities.Models;

namespace Domain.Services.Mobile
{
    public class Authentication : BaseService
    {
        #region Constructor

        public Authentication(string webApiBaseUrl, bool isAndroid, bool serialize = false) : base(webApiBaseUrl, isAndroid, serialize) { }

        public Authentication(string webApiBaseUrl, IUnityOfWork uow, bool isAndroid, bool serialize = false) : base(webApiBaseUrl, uow, isAndroid, serialize) { }

        public Authentication(string webApiBaseUrl, bool clear, bool isAndroid, bool serialize = false) : base(webApiBaseUrl, clear, isAndroid, serialize) { }

        #endregion

        #region Authentication

        public void CheckDatabase(Service service)
        {
            var core = new Core(WebApiBaseUrl, IsAndroid);
            
            if (core.GetDataTypes().Count == 0)
            {
                ((List<Combo>)service.Execute("Core", "GetCombo", "RecordState", string.Empty)).ForEach(c => {
                    core.SaveRecordState(new RecordState { Id = Convert.ToInt32(c.Id), Name = c.Value });
                });

                ((List<Combo>)service.Execute("Core", "GetCombo", "DataType", string.Empty)).ForEach(c => {
                    var parts = c.Value.Split(',');
                    core.SaveDataType(new DataType { Id = Convert.ToInt32(c.Id), Name = parts[0], NameSpace = parts[1] });
                });

                var listCombos = new List<KeyValuePair<string, string>> {
                    new KeyValuePair<string, string>("Operators", "Operator"),
                    new KeyValuePair<string, string>("OilWells", "OilWell"),
                    new KeyValuePair<string, string>("MonitoringTypes", "MonitoringType"),
                    new KeyValuePair<string, string>("InspectionCategories", "InspectionCategory"),
                    new KeyValuePair<string, string>("WorkOrderStates", "WorkOrderState"),
                    new KeyValuePair<string, string>("ToolTypes", "ToolType"),
                    new KeyValuePair<string, string>("Conditions", "Condition"),
                    new KeyValuePair<string, string>("DiscrepancyTypes", "DiscrepancyType"),
                    new KeyValuePair<string, string>("DiscrepancyResults", "DiscrepancyResult"),
                    new KeyValuePair<string, string>("DiscrepancyStates", "DiscrepancyState"),
                    new KeyValuePair<string, string>("DiscrepancyRisks", "DiscrepancyRisk")
                };

                listCombos.ForEach(c => {
                    var combo = (List<Combo>)service.Execute("Core", "GetCombo", c.Value, string.Empty);
                    core.SaveCombo(new ComboEntity { Name = c.Key, Values = combo });
                });
            }
        }

        public ErrorContainer Connect(string login, string passwd, string ip)
        {
            var result = new ErrorContainer();

            try
            {
                var repo = new Intermediate.Intermediate<User>(UOW, Transversal.Common.Enumerators.Mobile.DataTypes.User.GetHashCode());

                var user = repo.GetByFilter(t =>
                    (t.Active)
                    && (t.Email == login)
                ).FirstOrDefault().Value;

                if(user == null)
                {
                    using (Service service = new Service(WebApiBaseUrl, true))
                    {
                        if (service.Connect(login, passwd, ip))
                        {
                            CheckDatabase(service);

                            var tmpUser = (User)service.Execute("Authentication", "GetUserByEmail", login);
                            user = SaveUser(tmpUser);

                            var tmpPassword = (Password)service.Execute("Authentication", "GetPasswordByUserId", user.Id);
                            tmpPassword = SavePassword(tmpPassword);
                        }
                    }
                }

                if (user != null)
                {
                    if (!ValidatePassword(user, passwd)) { result.Message.Add("Usuario o Contraseña invalidos."); }
                }
                else {
                    result.Message.Add("Usuario o Contraseña invalidos.");
                }
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
                result.Message.Add("Error del sistema.");
            }

            return result;
        }

        bool ValidatePassword(User user, string passwd)
        {
            bool result = true;

            try
            {
                var repo = new Intermediate.Intermediate<Password>(UOW, Transversal.Common.Enumerators.Mobile.DataTypes.Password.GetHashCode());

                var password = repo.GetByFilter(t =>
                    (t.Active)
                    && (t.UserId == user.Id)
                ).FirstOrDefault().Value;

                if (passwd == null || !password.Value.Equals(Transversal.Security.Common.SHA256Hash($"{password.Salt}{passwd}")))
                {
                    throw new Exception(""); // Contraseña invalida
                }
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
                result = false;
            }

            return result;
        }

        public RecordState SaveRecordState(RecordState recordState)
        {
            RecordState result = recordState;

            try
            {
                var repo = UOW.Repository<RecordState>();

                repo.Insert(recordState);
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
            }

            return result;
        }

        public DataType SaveDataType(DataType dataType)
        {
            DataType result = dataType;

            try
            {
                var repo = UOW.Repository<DataType>();

                repo.Insert(dataType);
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
            }

            return result;
        }

        public User SaveUser(User user)
        {
            User result = null;

            try
            {
                var inter = new Intermediate.Intermediate<User>(UOW, Transversal.Common.Enumerators.Mobile.DataTypes.User.GetHashCode());
                result = inter.Save(user).Value;
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
                result = null;
            }

            return result;
        }

        public User GetUserByEmail(string login)
        {
            User result = null;

            try
            {
                var repo = new Intermediate.Intermediate<User>(UOW, Transversal.Common.Enumerators.Mobile.DataTypes.User.GetHashCode());
                result = repo.GetByFilter(t => ( t.Email == login )).FirstOrDefault().Value;
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
            }

            return result;
        }

        public Password SavePassword(Password password)
        {
            Password result = password;

            try
            {
                var inter = new Intermediate.Intermediate<Password>(UOW, Transversal.Common.Enumerators.Mobile.DataTypes.Password.GetHashCode());
                inter.Save(password);
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
            }

            return result;
        }

        #endregion
    }
}