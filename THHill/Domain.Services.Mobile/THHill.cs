﻿using Infrastructure.Core.Mobile.UnityOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using Transversal.Common.Entities.DataBase.Mobile;
using Transversal.Common.Entities.DataBase.THHill;

namespace Domain.Services.Mobile
{
    public class THHill : BaseService
    {
        #region Contants

        const int MaxIterations = 5;

        #endregion

        #region Constructor

        public THHill(string webApiBaseUrl, bool isAndroid, bool serialize = false) : base(webApiBaseUrl, isAndroid, serialize) { }

        public THHill(string webApiBaseUrl, IUnityOfWork uow, bool isAndroid, bool serialize = false) : base(webApiBaseUrl, uow, isAndroid, serialize) { }

        public THHill(string webApiBaseUrl, bool clear, bool isAndroid, bool serialize = false) : base(webApiBaseUrl, clear, isAndroid, serialize) { }

        #endregion

        #region WorkOrder

        public List<KeyValuePair<Guid, WorkOrder>> GetWorkOrders(Guid userId)
        {
            var response = new List<KeyValuePair<Guid, WorkOrder>>();

            try
            {
                var repo = new Intermediate.Intermediate<WorkOrder>(UOW, Transversal.Common.Enumerators.Mobile.DataTypes.WorkOrder.GetHashCode());
                response = repo.GetByFilter(f => f.UserId == userId).ToList();
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
                response = new List<KeyValuePair<Guid, WorkOrder>>();
            }

            return response;
        }

        public KeyValuePair<Guid, WorkOrder> SaveWorkOrder(Guid dataId, WorkOrder workOrder, List<Media> mediaFiles)
        {
            var response = new KeyValuePair<Guid, WorkOrder>(Guid.Empty, null);

            UOW.BeginTransaction();

            try
            {
                var inter = new Intermediate.Intermediate<WorkOrder>(UOW, Transversal.Common.Enumerators.Mobile.DataTypes.WorkOrder.GetHashCode());
                var core = new Core(WebApiBaseUrl, UOW, IsAndroid);

                response = inter.Save(workOrder, dataId);

                mediaFiles.ForEach(mf => {
                    mf.DataId = response.Key;
                    core.SaveMedia(mf);
                });

                UOW.CommitTransaction();
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
                response = new KeyValuePair<Guid, WorkOrder>(Guid.Empty, null);
                UOW.RollbackTransaction();
            }
            
            return response;
        }

        #endregion
    }
}
