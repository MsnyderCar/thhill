﻿using Infrastructure.Core.Mobile.UnityOfWork;
using Infrastructure.CrossCutting.Internal;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Transversal.Common.Entities.DataBase;
using Transversal.Common.Entities.DataBase.Mobile;
using Transversal.Common.Entities.DataBase.THHill;
using Transversal.Common.Entities.Models;
using static Transversal.Common.Enumerators.Mobile;

namespace Domain.Services.Mobile
{
    public class Core : BaseService
    {
        #region Constructor

        public Core(string webApiBaseUrl, bool isAndroid, bool serialize = false) : base(webApiBaseUrl, isAndroid, serialize) { }

        public Core(string webApiBaseUrl, IUnityOfWork uow, bool isAndroid, bool serialize = false) : base(webApiBaseUrl, uow, isAndroid, serialize) { }

        public Core(string webApiBaseUrl, bool clear, bool isAndroid, bool serialize = false) : base(webApiBaseUrl, clear, isAndroid, serialize) { }

        #endregion

        #region General

        public object GetNewEntity(string entity)
        {
            object response = null;

            try
            {
                response = (dynamic)Activator.CreateInstance(Assembly.GetAssembly(typeof(BaseEntity)).GetTypes()
                    .Where(x => x.Name.Equals(entity))
                    .FirstOrDefault());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                response = null;
            }

            return response;
        }

        public List<Combo> GetCombo(string entity)
        {
            var response = new List<Combo>();

            try
            {
                var inter = new Intermediate.Intermediate<ComboEntity>(UOW, Transversal.Common.Enumerators.Mobile.DataTypes.ComboEntity.GetHashCode());
                var combo = inter.GetByFilter(f => f.Name.Equals(entity)).FirstOrDefault().Value;
                if(combo != null) response = combo.Values;
            }
            catch (Exception ex)
            {
                WriteLog(MethodBase.GetCurrentMethod(), ex);
                response = new List<Combo>();
            }

            return response.OrderBy(o => o.Value).ToList();
        }

        public ComboEntity SaveCombo(ComboEntity comboEntity)
        {
            ComboEntity result = comboEntity;

            try
            {
                var inter = new Intermediate.Intermediate<ComboEntity>(UOW, Transversal.Common.Enumerators.Mobile.DataTypes.ComboEntity.GetHashCode());
                inter.Save(comboEntity);
            }
            catch (Exception ex)
            {
                WriteLog(MethodBase.GetCurrentMethod(), ex);
            }

            return result;
        }

        #endregion

        #region Core

        public RecordState SaveRecordState(RecordState recordState)
        {
            RecordState result = null;

            try
            {
                var repo = UOW.Repository<RecordState>();

                if (repo.GetById(recordState.Id) == null)
                {
                    result = repo.Insert(recordState);
                }
                else
                {
                    repo.Update(recordState);
                    result = recordState;
                }
            }
            catch (Exception ex)
            {
                WriteLog(MethodBase.GetCurrentMethod(), ex);
            }

            return result;
        }

        public DataType SaveDataType(DataType dataType)
        {
            DataType result = null;

            try
            {
                var repo = UOW.Repository<DataType>();

                if (repo.GetById(dataType.Id) == null)
                {
                    result = repo.Insert(dataType);
                }
                else
                {
                    repo.Update(dataType);
                    result = dataType;
                }
            }
            catch (Exception ex)
            {
                WriteLog(MethodBase.GetCurrentMethod(), ex);
            }

            return result;
        }

        public List<DataType> GetDataTypes()
        {
            var result = new List<DataType>();

            try
            {
                result = UOW.Repository<DataType>().GetAll();
            }
            catch (Exception ex)
            {
                WriteLog(MethodBase.GetCurrentMethod(), ex);
                result = new List<DataType>();
            }

            return result;
        }

        #endregion

        #region Media

        public Media GetMedia(Guid id)
        {
            Media response = null;

            try
            {
                response = UOW.Repository<Media>().GetByFilter(t => (t.InternalId == id)).FirstOrDefault();
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
                response = null;
            }

            return response;
        }

        public Media SaveMedia(Media media)
        {
            Media response = null;

            try
            {
                var repo = UOW.Repository<Media>();

                var tempMedia = GetMedia(media.InternalId);
                if (tempMedia != null) media.Id = tempMedia.Id;

                if (media.Id == Guid.Empty)
                    repo.Insert(media);
                else
                    repo.Update(media);
            }
            catch (Exception ex)
            {
                WriteLog(MethodBase.GetCurrentMethod(), ex);
                response = null;
            }

            return response;
        }

        public bool DeleteMedia(Guid id)
        {
            var response = false;

            try
            {
                UOW.Repository<Media>().DeleteById(id);
                response = true;
            }
            catch (Exception ex)
            {
                WriteLog(MethodBase.GetCurrentMethod(), ex);
                response = false;
            }

            return response;
        }

        public List<Media> GetMediaNotSync()
        {
            List<Media> response = null;

            try
            {
                response = UOW.Repository<Media>().GetByFilter(t =>
                    (t.Active)
                    && (t.RecordStateId == RecordStates.Local.GetHashCode())
                    );
            }
            catch (Exception ex)
            {
                WriteLog(MethodBase.GetCurrentMethod(), ex);
                response = null;
            }

            return response;
        }

        #endregion

        #region Media

        public Data SaveData(Data data)
        {
            Data response = null;

            try
            {
                var repo = UOW.Repository<Data>();

                if (data.Id == Guid.Empty)
                    repo.Insert(data);
                else
                    repo.Update(data);
            }
            catch (Exception ex)
            {
                WriteLog(MethodBase.GetCurrentMethod(), ex);
                response = null;
            }

            return response;
        }

        public List<Data> GetDataNotSync()
        {
            List<Data> response = null;

            try
            {
                response = UOW.Repository<Data>().GetByFilter(t =>
                    (t.Active)
                    && (t.RecordStateId == RecordStates.Local.GetHashCode())
                    );
            }
            catch (Exception ex)
            {
                WriteLog(MethodBase.GetCurrentMethod(), ex);
                response = null;
            }

            return response;
        }

        #endregion

        public int SyncData(int temp)
        {
            var repoData = UOW.Repository<Data>();
            var repoMedia = UOW.Repository<Media>();

            UOW.BeginTransaction();

            try
            {
                var wabuParts = WebApiBaseUrl.Split(';');

                using (Service service = new Service(wabuParts[0], true))
                {
                    if (service.Connect(wabuParts[1], wabuParts[2], "0.0.0.0"))
                    {
                        repoData.GetByFilter(t => (t.RecordStateId == RecordStates.Local.GetHashCode()) && (t.DataTypeId == DataTypes.WorkOrder.GetHashCode())).ForEach(data =>
                        {
                            var workOrderString = Encoding.UTF8.GetString(data.Content);
                            repoMedia.GetByFilter(t => (t.RecordStateId == RecordStates.Local.GetHashCode()) && (t.DataId == data.Id)).ForEach(media =>
                            {
                                var internalId = ProcessFile(service, media);
                                workOrderString = workOrderString.Replace(media.InternalId.ToString(), internalId.ToString());
                                media.InternalId = internalId;
                                media.RecordStateId = RecordStates.Server.GetHashCode();
                                repoMedia.Update(media);
                            });
                            data.Content = Encoding.UTF8.GetBytes(workOrderString);

                            var tmpWorkOrder = (WorkOrder)service.Execute("THHill", "SaveWorkOrder", JsonConvert.DeserializeObject<WorkOrder>(Encoding.UTF8.GetString(data.Content)));

                            workOrderString = JsonConvert.SerializeObject(tmpWorkOrder);

                            data.Content = Encoding.UTF8.GetBytes(workOrderString);
                            data.RecordStateId = RecordStates.Server.GetHashCode();
                            data.ServerId = tmpWorkOrder.Id;

                            repoData.Update(data);
                        });
                    }
                    else throw new Exception("No se puede conectar al servicio");
                }

                UOW.CommitTransaction();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                UOW.RollbackTransaction();
            }
            
            return 0;
        }

        private Guid ProcessFile(Service service, Media media)
        {

            var response = media.Id;

            var TransferBufferSize = 16384;

            byte[] buffer = new byte[TransferBufferSize];
            int read = 0;

            try
            {
                var stream = new MemoryStream(media.Content);
                var size = stream.Length;
                var partsQuantity = (double)size / (double)TransferBufferSize;
                var parts = Convert.ToInt32(partsQuantity - Math.Truncate(partsQuantity) > 0 ? partsQuantity + 1 : partsQuantity);

                var sf = new StreamFile
                {
                    StoredFileName = media.Name,
                    Part = 0,
                    Parts = parts
                };
                
                while (sf.Part < sf.Parts)
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        stream.Seek(sf.Part * TransferBufferSize, SeekOrigin.Begin);

                        if ((read = stream.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            ms.Write(buffer, 0, read);
                        }

                        sf.Data = Convert.ToBase64String(ms.ToArray());

                        var pivot = 0;
                        var musContinue = true;

                        while (musContinue && pivot < 5)
                        {
                            try
                            {
                                var tempId = (Guid)service.Execute("Core", "UploadFilePart", sf);

                                if (tempId == Guid.Empty)
                                    throw new Exception("Can't Upload file part");
                                else
                                {
                                    var tempId2 = new Guid(Path.GetFileNameWithoutExtension(sf.StoredFileName));
                                    if (tempId != tempId2) sf.StoredFileName = sf.StoredFileName.Replace(tempId2.ToString(), tempId.ToString());
                                     response = tempId;
                                    musContinue = false;
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                            pivot++;
                        }
                    }

                    sf.Part++;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return response;
        }
    }
}
