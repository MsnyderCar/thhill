﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Infrastructure.Core.Mobile.Repository;
using Infrastructure.Core.Mobile.UnityOfWork;
using Newtonsoft.Json;
using Transversal.Common.Entities.DataBase.Mobile;
using Transversal.Common.Utilities;
using static Transversal.Common.Enumerators.Mobile;

namespace Domain.Services.Mobile.Intermediate
{
    public class Intermediate<TEntity> : IIntermediate<TEntity> where TEntity : class
    {
        #region Members

        private IUnityOfWork UOW { get; set; }
        private int DataTypeId { get; set; }
        private IRepository<Data> Repo { get; set; }

        #endregion

        #region Constructor

        public Intermediate(IUnityOfWork unityOfWork, int dataTypeId)
        {
            UOW = unityOfWork;
            DataTypeId = dataTypeId;
            Repo = UOW.Repository<Data>();
        }

        #endregion

        #region IIntermediate

        public KeyValuePair<Guid, TEntity> GetById(Guid id)
        {
            return Cast(Repo.GetById(id));
        }

        public List<KeyValuePair<Guid, TEntity>> GetAll()
        {
            return Repo.GetByFilter(f => f.DataTypeId == DataTypeId).Select(d => Cast(d)).ToList();
        }

        public List<KeyValuePair<Guid, TEntity>> GetByFilter(Expression<Func<TEntity, bool>> filter)
        {
            var elements = GetAll();
            var es = elements.Select(e => e.Value).AsQueryable().Where(filter).ToList();
            return elements.Where(e => es.Contains(e.Value)).ToList();
        }

        public KeyValuePair<Guid, TEntity> Save(TEntity entity, Guid? id = null)
        {
            Data ent = null;

            if (id.HasValue) ent = Repo.GetById(id.Value);

            if (ent == null) ent = NewData(id);

            var content = JsonConvert.SerializeObject(entity);
            ent.Content = Encoding.UTF8.GetBytes(content);

            ent.RecordStateId = RecordStates.Local.GetHashCode();

            try
            {
                if (!id.HasValue)
                    Repo.Insert(ent);
                else
                    Repo.Update(ent);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            

            return new KeyValuePair<Guid, TEntity>(ent.Id, entity);
        }

        #endregion

        #region Private

        private KeyValuePair<Guid, TEntity> Cast(Data data)
        {
            return new KeyValuePair<Guid, TEntity>(data.Id, JsonConvert.DeserializeObject<TEntity>(Encoding.UTF8.GetString(data.Content)));
        }

        private Data NewData(Guid? id = null)
        {
            return new Data
            {
                Id = Guid.Empty,
                DataTypeId = DataTypeId
            };
        }

        #endregion
    }
}
