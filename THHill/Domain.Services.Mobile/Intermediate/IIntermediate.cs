﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Domain.Services.Mobile.Intermediate
{
    public interface IIntermediate<TEntity> where TEntity : class
    {
        KeyValuePair<Guid, TEntity> GetById(Guid id);
        List<KeyValuePair<Guid, TEntity>> GetAll();
        List<KeyValuePair<Guid, TEntity>> GetByFilter(Expression<Func<TEntity, bool>> filter);
        KeyValuePair<Guid, TEntity> Save(TEntity entity, Guid? id = null);
    }
}
