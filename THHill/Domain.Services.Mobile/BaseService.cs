﻿using Infrastructure.Core.Mobile.UnityOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Transversal.Common.Entities.Api;
using Transversal.Common.Entities.Models;
using Transversal.Common.Utilities;

namespace Domain.Services.Mobile
{
    public class BaseService
    {
        #region Constants

        protected const string BasePath = "wwwroot/repository/templates/";

        #endregion

        #region Members

        protected readonly string WebApiBaseUrl;
        protected readonly string UserName;
        protected readonly string Password;
        
        protected readonly IUnityOfWork UOW;
        protected readonly bool IsAndroid;
        protected readonly bool Serialize;

        #endregion

        #region Constructor

        public BaseService(string webApiBaseUrl, bool isAndroid, bool serialize = false)
        {
            WebApiBaseUrl = webApiBaseUrl;
            IsAndroid = isAndroid;
            UOW = new UnityOfWork(isAndroid);
            Serialize = serialize;
        }
        
        public BaseService(string webApiBaseUrl, IUnityOfWork uow, bool isAndroid, bool serialize = false)
        {
            WebApiBaseUrl = webApiBaseUrl;
            IsAndroid = isAndroid;
            UOW = uow;
            Serialize = serialize;
        }

        public BaseService(string webApiBaseUrl, bool clear, bool isAndroid, bool serialize = false)
        {
            WebApiBaseUrl = webApiBaseUrl;
            IsAndroid = isAndroid;
            UOW = new UnityOfWork(isAndroid, clear);
            Serialize = serialize;
        }

        #endregion

        #region Methods

        public Packet Execute(Packet request)
        {
            Packet response = new Packet
            {
                Service = request.Service,
                Action = request.Action
            };

            object result = ExecuteAction(request);

            if (result != null)
            {
                var assembly = string.Empty;

                if (result.GetType().Name.Contains("List") && result.GetType().GenericTypeArguments.Length == 1 && result.GetType().GenericTypeArguments[0].Name.Equals("Object"))
                {
                    foreach (var part in (List<object>)result)
                    {
                        response.RawData.Add(JsonRawData.Serialize(part, Serialize));
                    }
                }
                else
                {
                    try
                    {
                        // Revisar cuando result sea nulo
                        response.RawData.Add(JsonRawData.Serialize(result, Serialize));
                    }
                    catch (Exception ex)
                    {
                        
                        WriteLog(MethodBase.GetCurrentMethod(),ex);
                    }
                }
            }

            return response;
        }

        #endregion

        #region Private Methods

        protected string WriteLog(MethodBase methodBase, Exception ex)
        {
            var date = DateTime.Now;
            Console.WriteLine($"{date.ToString("yyyy-MM-dd HH:mm:ss")}|{methodBase}|Exception:{ex.ToString()}");
            return ex.Message;
        }

        protected object ExecuteAction(Packet packet)
        {
            MethodInfo mi = GetType().GetMethod(packet.Action);
            if (mi == null)
                throw new Exception("La acción solicitada no existe en el servicio");

            var rawdata = new List<object>();

            try
            {
                foreach (var r in packet.RawData)
                {
                    rawdata.Add(
                        r.Assembly.EndsWith("Guid") ? new Guid(r.Data.ToString()) : (
                            Serialize ? JsonRawData.Deserialize(r) : r.Data
                        )
                    );
                }
            }
            catch (Exception ex)
            {
                WriteLog(MethodBase.GetCurrentMethod(), ex);
            }

            return mi.Invoke(this, rawdata.ToArray());
        }

        protected void FilterProcessor(object filterObj, List<Filter> filters)
        {
            foreach (var filter in filters)
            {
                FilterSeekAndSet(filterObj, filter);
            }
        }

        private void FilterSeekAndSet(object filterObj, Filter filter)
        {
            var parts = filter.Member.Split('.');
            if (parts.Length == 1) Objects.SetValue(filterObj, filter.Member, filter.Value);
            else
            {
                var property = Activator.CreateInstance(Objects.GetProperty(filterObj, parts[0]).PropertyType);
                Objects.SetValue(filterObj, parts[0], property);
                filter.Member = string.Join(".", parts.Skip(1));
                FilterSeekAndSet(property, filter);
            }
        }

        #endregion
    }
}
