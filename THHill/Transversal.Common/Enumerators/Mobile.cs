﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Transversal.Common.Enumerators
{
    public class Mobile
    {
        public enum RecordStates
        {
            [System.ComponentModel.Description("Local")]
            Local = 1,
            [System.ComponentModel.Description("Server")]
            Server = 2
        }

        public enum DataTypes
        {
            [System.ComponentModel.Description("User")]
            User = 1,
            [System.ComponentModel.Description("Password")]
            Password = 2,
            [System.ComponentModel.Description("ComboEntity")]
            ComboEntity = 3,
            [System.ComponentModel.Description("WorkOrder")]
            WorkOrder = 4

        }
    }
}
