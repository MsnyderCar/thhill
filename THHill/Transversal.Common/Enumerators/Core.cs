﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Transversal.Common.Enumerators
{
    public class Core
    {
        public enum ParameterTypes
        {
            [System.ComponentModel.Description("Bool")]
            Bool = 1,
            [System.ComponentModel.Description("Int")]
            Int = 2,
            [System.ComponentModel.Description("String")]
            String = 3
        }
    }
}
