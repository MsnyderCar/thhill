﻿namespace Transversal.Common.Enumerators
{
    public class Authentication
    {
        public enum UserType
        {
            [System.ComponentModel.Description("Administrator")]
            Administrator = 1,
            [System.ComponentModel.Description("Technician")]
            Technician = 2
        }
    }
}