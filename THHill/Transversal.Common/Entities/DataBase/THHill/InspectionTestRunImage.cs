﻿using System;
using Transversal.Common.Entities.DataBase.Authentication;

namespace Transversal.Common.Entities.DataBase.THHill
{
    public class InspectionTestRunImage : BaseEntity
    {
        #region Members

        public Guid InspectionToolId { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        
        #endregion

        #region Relations

        public virtual InspectionTool InspectionTool { get; set; }

        #endregion
    }
}
