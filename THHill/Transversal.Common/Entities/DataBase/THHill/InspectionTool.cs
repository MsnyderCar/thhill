﻿using System;
using System.Collections.Generic;
using Transversal.Common.Entities.DataBase.Authentication;

namespace Transversal.Common.Entities.DataBase.THHill
{
    public class InspectionTool : BaseEntity
    {
        #region Members

        public Guid WorkOrderId { get; set; }
        public int ToolTypeId { get; set; }
        
        public string Name { get; set; }
        public string Number { get; set; }
        public string PartNumber { get; set; }
        public string Remarks { get; set; }

        public int AdvancePercent { get; set; }
        public string Image { get; set; }

        // Assembly

        public int AssemblyPercent { get; set; }
        public string SubAssemblySerial { get; set; }
        public string SubAssemblyDescription { get; set; }
        public int SubAssemblyConditionId { get; set; }
        public string SubAssemblyComments { get; set; }

        // Test Run

        public int TestRunPercent { get; set; }
        public bool TestRunPass { get; set; }
        public string TestRunDescription { get; set; }

        #endregion

        #region Relations

        public virtual WorkOrder WorkOrder { get; set; }
        public virtual ToolType ToolType { get; set; }
        public virtual Condition SubAssemblyCondition { get; set; }

        public virtual ICollection<InspectionComponent> InspectionComponents { get; set; }
        public virtual ICollection<InspectionToolDiscrepancy> InspectionToolDiscrepancies { get; set; }
        public virtual ICollection<InspectionSubAssemblyImage> InspectionSubAssemblyImages { get; set; }
        public virtual ICollection<InspectionTestRunImage> InspectionTestRunImages { get; set; }
        public virtual ICollection<InspectionTestRunDiscrepancy> InspectionTestRunDiscrepancies { get; set; }

        #endregion
    }
}
