﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Transversal.Common.Entities.DataBase.THHill
{
    public class DiscrepancyState
    {
        #region Members

        public int Id { get; set; }
        public string Name { get; set; }

        #endregion

        #region Relations

        public virtual ICollection<InspectionToolDiscrepancy> InspectionToolDiscrepancies { get; set; }
        public virtual ICollection<InspectionComponentDiscrepancy> InspectionComponentDiscrepancies { get; set; }
        public virtual ICollection<InspectionTestRunDiscrepancy> InspectionTestRunDiscrepancies { get; set; }
        
        #endregion
    }
}
