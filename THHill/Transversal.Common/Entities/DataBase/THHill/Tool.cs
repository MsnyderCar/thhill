﻿using System.Collections.Generic;

namespace Transversal.Common.Entities.DataBase.THHill
{
    public class Tool : BaseEntity
    {
        #region Members

        public int ToolTypeId { get; set; }

        public string Name { get; set; }
        public string Number { get; set; }
        public string PartNumber { get; set; }
        public string Image { get; set; }
        public string Remarks { get; set; }

        #endregion

        #region Relations

        public virtual ToolType ToolType { get; set; }

        public virtual ICollection<Component> Components { get; set; }
        
        #endregion
    }
}
