﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Transversal.Common.Entities.DataBase.THHill
{
    public class InspectionToolDiscrepancy : BaseEntity
    {
        #region Members

        public Guid InspectionToolId { get; set; }
        public int DiscrepancyTypeId { get; set; }
        public string StandardProcedure { get; set; }
        public string Clause { get; set; }
        public string Detail { get; set; }
        public string CorrectiveAction { get; set; }
        public string InspectionCompany { get; set; }
        public int DiscrepancyResultId { get; set; }
        public int DiscrepancyStateId { get; set; }
        public int DiscrepancyRiskId { get; set; }
        public string Evidence { get; set; }

        #endregion

        #region Relations

        public virtual InspectionTool InspectionTool { get; set; }
        public virtual DiscrepancyType DiscrepancyType { get; set; }
        public virtual DiscrepancyResult DiscrepancyResult { get; set; }
        public virtual DiscrepancyState DiscrepancyState { get; set; }
        public virtual DiscrepancyRisk DiscrepancyRisk { get; set; }

        #endregion
    }
}
