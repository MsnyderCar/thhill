﻿using System.Collections.Generic;

namespace Transversal.Common.Entities.DataBase.THHill
{
    public class ToolType
    {
        #region Members

        public int Id { get; set; }
        public string Name { get; set; }

        #endregion

        #region Relations

        public virtual ICollection<Tool> Tools { get; set; }
        public virtual ICollection<InspectionTool> InspectionTools { get; set; }

        #endregion
    }
}
