﻿using System;
using Transversal.Common.Entities.DataBase.Authentication;

namespace Transversal.Common.Entities.DataBase.THHill
{
    public class InspectionImage : BaseEntity
    {
        #region Members

        public Guid WorkOrderId { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        
        #endregion

        #region Relations

        public virtual WorkOrder WorkOrder { get; set; }

        #endregion
    }
}
