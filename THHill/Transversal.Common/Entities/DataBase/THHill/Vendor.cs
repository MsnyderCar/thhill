﻿using System.Collections.Generic;

namespace Transversal.Common.Entities.DataBase.THHill
{
    public class Vendor : BaseEntity
    {
        #region Members

        public string Name { get; set; }
        
        #endregion

        #region Relations

        public virtual ICollection<WorkOrder> WorkOrders { get; set; }

        #endregion
    }
}
