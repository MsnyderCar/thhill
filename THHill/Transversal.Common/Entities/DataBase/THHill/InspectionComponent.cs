﻿using System;
using System.Collections.Generic;

namespace Transversal.Common.Entities.DataBase.THHill
{
    public class InspectionComponent : BaseEntity
    {
        #region Members

        public Guid InspectionToolId { get; set; }
        
        public int ConditionId { get; set; }

        public string Name { get; set; }
        public string Number { get; set; }
        public string Image { get; set; }
        public string Remarks { get; set; }

        public string Description { get; set; }

        #endregion

        #region Relations

        public virtual InspectionTool InspectionTool { get; set; }
        public virtual Condition Condition { get; set; }
        
        public virtual ICollection<InspectionComponentDiscrepancy> InspectionComponentDiscrepancies { get; set; }
        
        #endregion
    }
}
