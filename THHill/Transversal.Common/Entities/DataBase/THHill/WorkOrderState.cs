﻿using System.Collections.Generic;

namespace Transversal.Common.Entities.DataBase.THHill
{
    public class WorkOrderState
    {
        #region Members

        public int Id { get; set; }
        public string Name { get; set; }

        #endregion

        #region Relations

        public virtual ICollection<WorkOrder> WorkOrders { get; set; }

        #endregion
    }
}
