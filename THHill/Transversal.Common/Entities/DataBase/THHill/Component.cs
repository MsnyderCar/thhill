﻿using System;
using System.Collections.Generic;

namespace Transversal.Common.Entities.DataBase.THHill
{
    public class Component : BaseEntity
    {
        #region Members

        public Guid ToolId { get; set; }
        public string Name { get; set; }
        public string Number { get; set; }
        public string Image { get; set; }
        public string Remarks { get; set; }

        #endregion

        #region Relations

        public virtual Tool Tool { get; set; }

        #endregion
    }
}
