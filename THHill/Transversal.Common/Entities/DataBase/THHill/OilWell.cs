﻿using System.Collections.Generic;

namespace Transversal.Common.Entities.DataBase.THHill
{
    public class OilWell : BaseEntity
    {
        #region Members

        public string Name { get; set; }
        public string Description { get; set; }

        #endregion

        #region Relations

        public virtual ICollection<WorkOrder> WorkOrders { get; set; }

        #endregion
    }
}
