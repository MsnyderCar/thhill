﻿using System.Collections.Generic;

namespace Transversal.Common.Entities.DataBase.THHill
{
    public class Condition
    {
        #region Members

        public int Id { get; set; }
        public string Name { get; set; }

        #endregion

        #region Relations

        public virtual ICollection<InspectionComponent> InspectionComponents { get; set; }
        public virtual ICollection<InspectionTool> InspectionToolSubAssemblies { get; set; }

        #endregion
    }
}
