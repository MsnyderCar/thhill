﻿using System;
using System.Collections.Generic;
using Transversal.Common.Entities.DataBase.Authentication;

namespace Transversal.Common.Entities.DataBase.THHill
{
    public class WorkOrder : BaseEntity
    {
        #region Members

        #region Cabecera

        public Guid UserId { get; set; }
        public Guid OperatorId { get; set; }
        public string Drill { get; set; }
        public string ProjectManager { get; set; }
        public Guid OilWellId { get; set; }
        public int Consecutive { get; set; }
        public string BeginDate { get; set; }
        public string EndDate { get; set; }
        public int Assignment { get; set; }
        public string LocationWork { get; set; }
        public Guid MonitoringTypeId { get; set; }
        public Guid InspectionCategoryId { get; set; }
        public string CustomerSpecification { get; set; }
        public string ManualsAndProcedures { get; set; }
        public string ProviderRepresentative { get; set; }
        public string InformationToClient { get; set; }
        public Guid? VendorId { get; set; }

        #endregion

        #region Inspeccion

        public string InspectionCompany { get; set; }
        public string InspectionToolDate { get; set; }
        public string Inspector { get; set; }
        public string TrainingCertification { get; set; }
        public string ParticleConcentration { get; set; }
        public string ParticleType { get; set; }
        public double BlackLightIntensity { get; set; }
        public bool PenetratingLiquid { get; set; }
        public string PenetratingLiquidDate { get; set; }
        public double WhiteLightIntensity { get; set; }
        public bool Developer { get; set; } // Revelador
        public string DeveloperDate { get; set; }
        public string InspectionInfo { get; set; }

        #endregion

        public int WorkOrderStateId { get; set; }

        #endregion

        #region Relations

        public virtual User User { get; set; }
        public virtual Operator Operator { get; set; }
        public virtual OilWell OilWell { get; set; }
        public virtual MonitoringType MonitoringType { get; set; }
        public virtual InspectionCategory InspectionCategory { get; set; }
        public virtual Vendor Vendor { get; set; }
        public virtual WorkOrderState WorkOrderState { get; set; }

        public virtual ICollection<InspectionImage> InspectionImages { get; set; }
        public virtual ICollection<InspectionTool> InspectionTools { get; set; }

        #endregion
    }
}
