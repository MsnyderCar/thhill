﻿using System.Collections.Generic;

namespace Transversal.Common.Entities.DataBase.THHill
{
    public class Operator : BaseEntity
    {
        #region Members

        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }

        #endregion

        #region Relations

        public virtual ICollection<WorkOrder> WorkOrders { get; set; }

        #endregion
    }
}
