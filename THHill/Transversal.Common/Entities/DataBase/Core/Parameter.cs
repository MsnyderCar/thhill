﻿namespace Transversal.Common.Entities.DataBase.Core
{
    public class Parameter : BaseEntity
    {
        #region Members

        public int ParameterTypeId { get; set; }
        public string Code { get; set; }
        public string Categories { get; set; }
        public string Description { get; set; }
        public string Value { get; set; }

        #endregion

        #region Relations

        public virtual ParameterType ParameterType { get; set; }

        #endregion
    }
}
