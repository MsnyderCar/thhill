﻿using System.Collections.Generic;

namespace Transversal.Common.Entities.DataBase.Core
{
    public class ParameterType
    {
        #region Members

        public int Id { get; set; }
        public string Name { get; set; }

        #endregion

        #region Relations

        public virtual ICollection<Parameter> Parameters { get; set; }

        #endregion
    }
}
