﻿using System;

namespace Transversal.Common.Entities.DataBase.Authentication
{
    public class Permission : BaseEntity
    {
        #region Members

        public int UserTypeId { get; set; }
        public Guid ModuleId { get; set; }

        #endregion

        #region Relations

        public virtual UserType UserType { get; set; }
        public virtual Module Module { get; set; }

        #endregion
    }
}
