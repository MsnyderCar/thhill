﻿using System;

namespace Transversal.Common.Entities.DataBase.Authentication
{
    public class TokenDB : BaseEntity
    {
        #region Members

        public Guid UserId { get; set; }
        public string AccessToken { get; set; }
        public int ValidityPeriod { get; set; }
        public string RefreshToken { get; set; }
        public string Ip { get; set; }

        #endregion

        #region Relations

        public virtual User User { get; set; }

        #endregion
    }
}
