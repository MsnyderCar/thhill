﻿using System.Collections.Generic;

namespace Transversal.Common.Entities.DataBase.Authentication
{
    public class UserType
    {
        #region Members

        public int Id { get; set; }
        public string Name { get; set; }

        #endregion

        #region Relations

        public virtual ICollection<User> Users { get; set; }
        public virtual ICollection<Permission> Permissions { get; set; }

        #endregion
    }
}
