﻿using System.Collections.Generic;

namespace Transversal.Common.Entities.DataBase.Authentication
{
    public class Module : BaseEntity
    {
        #region Members

        public string Name { get; set; }
        public int Order { get; set; }
        public string Icon { get; set; }
        public string Controller { get; set; }
        public bool CanCreate { get; set; }
        public bool CanUpdate { get; set; }
        public bool CanDelete { get; set; }
        public bool CanSave { get; set; }

        #endregion

        #region Relations

        public virtual ICollection<Permission> Permissions { get; set; }

        #endregion
    }
}
