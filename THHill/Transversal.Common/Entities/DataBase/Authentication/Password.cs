﻿using System;

namespace Transversal.Common.Entities.DataBase.Authentication
{
    public class Password : BaseEntity
    {
        #region Members

        public Guid UserId { get; set; }
        public string Value { get; set; }
        public string Salt { get; set; }

        #endregion

        #region Relations

        public virtual User User { get; set; }

        #endregion
    }
}
