﻿using System;
using System.Collections.Generic;
using Transversal.Common.Entities.DataBase.THHill;

namespace Transversal.Common.Entities.DataBase.Authentication
{
    public class User : BaseEntity
    {
        #region Members

        public string Email { get; set; }
        public int UserTypeId { get; set; }
        public string Document { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get { return $"{FirstName} {LastName} "; } }
        public string Address { get; set; }
        public Guid? AuthToken { get; set; }
        public DateTime? AuthTokenDate { get; set; }

        #endregion

        #region Relations

        public virtual UserType UserType { get; set; }
        
        public virtual ICollection<Password> Passwords { get; set; }
        public virtual ICollection<TokenDB> Tokens { get; set; }
        public virtual ICollection<WorkOrder> WorkOrders { get; set; }

        #endregion
    }
}
