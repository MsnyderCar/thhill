﻿using System;

namespace Transversal.Common.Entities.DataBase
{
    public class BaseEntity
    {
        #region Members

        public Guid Id { get; set; }
        public DateTime CreatedDate { get; set; } = DateTime.Now;
        public DateTime? UpdatedDate { get; set; }
        public bool Active { get; set; }

        #endregion

        #region Constructor

        public BaseEntity()
        {
            Id = Guid.Empty;
            CreatedDate = DateTime.Now;
            UpdatedDate = CreatedDate;
            Active = true;
        }

        #endregion
    }
}
