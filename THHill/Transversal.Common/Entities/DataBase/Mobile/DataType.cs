﻿using System;
using System.Collections.Generic;
using System.Text;


namespace Transversal.Common.Entities.DataBase.Mobile
{
    public class DataType
    {
        #region Members

        public int Id { get; set; }
        public string Name { get; set; }
        public string NameSpace { get; set; }

        #endregion

        #region Relations

        public virtual ICollection<Data> Data { get; set; }
        
        #endregion
    }
}
