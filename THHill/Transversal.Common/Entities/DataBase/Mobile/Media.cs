﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Transversal.Common.Entities.DataBase.Mobile
{
    public class Media : BaseEntityMobile
    {
        #region Members

        public Guid InternalId { get; set; }
        public Guid DataId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public byte[] Content { get; set; }

        #endregion

        #region Relations

        public virtual Data Data { get; set; }

        #endregion
    }
}
