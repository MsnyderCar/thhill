﻿using System;

namespace Transversal.Common.Entities.DataBase.Mobile
{
    public class BaseEntityMobile
    {
        #region Members

        public Guid Id { get; set; }
        public Guid ServerId { get; set; }
        public Guid UserId { get; set; }
        public int RecordStateId { get; set; }
        public DateTime CreatedDate { get; set; } = DateTime.Now;
        public DateTime? UpdatedDate { get; set; }
        public bool Active { get; set; }

        #endregion

        #region Relations

        public virtual RecordState RecordState { get; set; }
        
        #endregion

        #region Constructor

        public BaseEntityMobile()
        {
            Id = Guid.Empty;
            UserId = Guid.Empty;
            RecordStateId = Enumerators.Mobile.RecordStates.Local.GetHashCode();
            CreatedDate = DateTime.Now;
            UpdatedDate = CreatedDate;
            Active = true;
        }

        #endregion
    }
}
