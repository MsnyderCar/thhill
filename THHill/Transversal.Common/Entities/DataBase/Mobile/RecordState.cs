﻿using System;
using System.Collections.Generic;
using System.Text;


namespace Transversal.Common.Entities.DataBase.Mobile
{
    public class RecordState
    {
        #region Members

        public int Id { get; set; }
        public string Name { get; set; }

        #endregion

        #region Relations

        public virtual ICollection<Data> Data { get; set; }
        public virtual ICollection<Media> Media { get; set; }

        #endregion
    }
}
