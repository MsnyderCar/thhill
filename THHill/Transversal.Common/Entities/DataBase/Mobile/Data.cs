﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Transversal.Common.Entities.DataBase.Mobile
{
    public class Data : BaseEntityMobile
    {
        #region Members

        public int DataTypeId { get; set; }
        public byte[] Content { get; set; }

        #endregion

        #region Relations

        public virtual DataType DataType { get; set; }

        public virtual ICollection<Media> MediaFiles { get; set; }

        #endregion
    }
}
