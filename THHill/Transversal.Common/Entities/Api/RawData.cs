﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Transversal.Common.Entities.Api
{
    public class RawData
    {
        #region Members

        public string Assembly { get; set; }
        public object Data { get; set; }

        #endregion
    }
}
