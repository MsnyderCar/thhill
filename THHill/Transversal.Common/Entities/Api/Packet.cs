﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Transversal.Common.Entities.Api
{
    public class Packet : Base
    {
        #region Members

        public string Service { get; set; }
        public string Action { get; set; }
        public int PID { get; set; }
        public List<RawData> RawData { get; set; }

        #endregion

        #region

        public Packet()
        {
            RawData = new List<RawData>();
        }

        #endregion
    }
}
