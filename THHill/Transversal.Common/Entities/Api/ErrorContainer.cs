﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Transversal.Common.Entities.Api
{
    public class ErrorContainer
    {
        #region Members

        public Guid ExecutionId { get; set; }

        public bool InError { get { return Message.Count > 0;  } }

        public string Code { get; set; }

        public List<string> Message { get; set; }

        #endregion

        #region Constructor

        public ErrorContainer()
        {
            ExecutionId = Guid.Empty;
            Code = string.Empty;
            Message = new List<string>();
        }

        #endregion
    }
}
