﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Transversal.Common.Entities.Api
{
    public class Base
    {
        #region Members

        public int StatusCode { get; set; }
        public List<string> StatusMessage { get; set; }

        #endregion
    }
}
