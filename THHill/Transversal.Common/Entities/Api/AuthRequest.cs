﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Transversal.Common.Entities.Api
{
    public class AuthRequest
    {
        #region Members

        public string Type { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string RefreshToken { get; set; }
        public string AuthToken { get; set; }
        public string Ip { get; set; }

        #endregion
    }
}
