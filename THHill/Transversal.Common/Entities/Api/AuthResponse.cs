﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Transversal.Common.Entities.Api
{
    public class AuthResponse : Base
    {
        #region Members

        public string AnotherData { get; set; }
        public Token Token { get; set; }

        #endregion
    }
}
