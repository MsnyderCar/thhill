﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Transversal.Common.Entities.Api
{
    public class TransferFilePart
    {
        #region Members

        public int Part { get; set; }
        public int Parts { get; set; }
        public string FileName { get; set; }
        public string NewFileName { get; set; }
        public string Data { get; set; }
        public int PartLength { get; set; }
        public string Path { get; set; }

        #endregion
    }
}
