﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Transversal.Common.Entities.Api
{
    public class Token
    {
        #region Members

        public Guid AccountId { get; set; }
        public DateTime Create { get; set; }
        public string Access { get; set; }
        public int Period { get; set; }
        public string Refresh { get; set; }
        public string Ip { get; set; }

        #endregion
    }
}
