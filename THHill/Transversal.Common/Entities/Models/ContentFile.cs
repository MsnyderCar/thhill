﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Transversal.Common.Entities.Models
{
    public class ContentFile
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string State { get; set; }
        public string Path { get; set; }
    }
}
