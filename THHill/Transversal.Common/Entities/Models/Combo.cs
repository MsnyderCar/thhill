﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Transversal.Common.Entities.Models
{
    public class Combo
    {
        public object Id { get; set; }
        public string Value { get; set; }
    }
}
