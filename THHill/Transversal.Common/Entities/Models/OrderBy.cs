﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Transversal.Common.Entities.Models
{
    public class OrderBy
    {
        #region Members

        public string Field { get; set; }
        public bool Direction { get; set; }

        #endregion
    }
}
