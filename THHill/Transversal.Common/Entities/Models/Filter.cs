﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Transversal.Common.Entities.Models
{
    public class Filter
    {
        #region Members

        public string Member { get; set; }
        public string Value { get; set; }

        #endregion
    }
}
