﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Transversal.Common.Entities.Models
{
    public class Module
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Controller { get; set; }
        public object MainForm { get; set; }
        public object Columns { get; set; }
        public object Filters { get; set; }
        public string Value { get; set; }
        public bool CanCreate { get; set; }
        public bool CanUpdate { get; set; }
        public bool CanDelete { get; set; }
    }
}
