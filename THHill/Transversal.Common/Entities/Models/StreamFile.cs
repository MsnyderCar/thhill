﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Transversal.Common.Entities.Models
{
    public class StreamFile
    {
        public string StoredFileName { get; set; }
        public int Part { get; set; }
        public int Parts { get; set; }
        public string Data { get; set; }
    }
}
