﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Transversal.Common.Entities.Models
{
    public class ComboEntity
    {
        public string Name { get; set; }
        public List<Combo> Values { get; set; }
    }
}
