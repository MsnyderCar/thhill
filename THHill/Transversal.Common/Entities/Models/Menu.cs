﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Transversal.Common.Entities.Models
{
    public class Menu
    {
        public Guid Id { get; set; }
        public int Order { get; set; }
        public string Icon { get; set; }
        public string Title { get; set; }
        public List<Menu> Items { get; set; }
    }
}
