﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Transversal.Common.Entities.Models
{
    public class ComboFilter
    {
        public string Entity { get; set; }
        public string Value { get; set; }
    }
}
