﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Transversal.Common.Entities.Models
{
    public class RequestFile
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int Part { get; set; }
        public int Parts { get; set; }
        public int TransferBufferSize { get; set; }
    }
}
