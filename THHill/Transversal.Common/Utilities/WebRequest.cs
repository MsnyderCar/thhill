﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Transversal.Common.Utilities
{
    public class WebRequest
    {
        #region Members

        private const string ContentType = "Application/JSON; charset=utf-8";

        private const string Method = "POST";

        private JsonSerializerSettings jsonss = new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore, PreserveReferencesHandling = PreserveReferencesHandling.Objects };

        private volatile bool FinishRequest;

        private string Domain { get; set; }

        private string AccessToken { get; set; }

        private bool IsMobile { get; set; }

        #endregion

        public WebRequest(string domain, string accessToken, bool isMobile)
        {
            Domain = domain;
            AccessToken = accessToken;
            IsMobile = isMobile;
        }

        public object ExecRequest(string action, object rawdata, Type targetType)
        {
            object response = null;

            if (IsMobile)
            {
                var task = Task.Run<object>(async () => await ExecRequestMobile(action, rawdata, targetType));
                response = task.Result;
            }
            else response = ExecRequestNonMobile(action, rawdata, targetType);

            return response;
        }

        private object ExecRequestNonMobile(string action, object rawdata, Type targetType)
        {
            object response = null;

            var request = System.Net.WebRequest.Create($"{Domain}/{action}") as HttpWebRequest;
            request.Method = Method;
            request.ContentType = ContentType;
            if (!string.IsNullOrEmpty(AccessToken)) request.Headers.Add("Authorization", $"Bearer {AccessToken}");

            try
            {
                byte[] bytes = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(rawdata, jsonss));

                request.ContentLength = bytes.Length;

                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(bytes, 0, bytes.Length);
                }

                FinishRequest = false;

                request.BeginGetResponse((x) =>
                {
                    try
                    {
                        var res = (HttpWebResponse)request.EndGetResponse(x);
                        response = JsonConvert.DeserializeObject(new StreamReader(res.GetResponseStream()).ReadToEnd(), targetType);

                    }
                    catch (WebException ex)
                    {
                        Console.WriteLine("\nException raised!");
                        Console.WriteLine("\nMessage:{0}", ex.Message);
                        Console.WriteLine("\nStatus:{0}", ex.Status);
                    }
                    finally
                    {
                        FinishRequest = true;
                    }
                }, null);

                while (!FinishRequest) Thread.Sleep(500);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return response;
        }

        private async Task<object> ExecRequestMobile(string action, object rawdata, Type targetType)
        {
            object response = null;

            try
            {
                var requestString = JsonConvert.SerializeObject(rawdata, jsonss);
                var content = new StringContent(requestString, Encoding.UTF8, "application/json");

                using (HttpClient client = GetClient(Domain))
                {
                    //var client = GetClient(Domain);

                    if (!string.IsNullOrEmpty(AccessToken)) client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", AccessToken);

                    var responseTmp = await client.PostAsync($"api/{action}", content);
                    var result = await responseTmp.Content.ReadAsStringAsync();

                    if (responseTmp.IsSuccessStatusCode)
                    {
                        response = ((JObject)JsonConvert.DeserializeObject<object>(result)).ToObject(targetType);
                    }
                }

                    
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return response;
        }

        private HttpClient GetClient(string urlBase)
        {
            var handler = new HttpClientHandler();
            handler.ClientCertificateOptions = ClientCertificateOption.Manual;
            handler.ServerCertificateCustomValidationCallback =
                (httpRequestMessage, cert, cetChain, policyErrors) =>
                {
                    return true;
                };

            var client = new HttpClient(handler);
            client.BaseAddress = new Uri(urlBase);

            return client;
        }
    }
}
