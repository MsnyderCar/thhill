﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Transversal.Common.Entities.Api;

namespace Transversal.Common.Utilities
{
    public class JsonRawData
    {
        #region Public Methods

        public static RawData Serialize(object request, bool serialize)
        {
            var assembly = string.Empty;

            if (request.GetType().Name.Contains("List") && request.GetType().GenericTypeArguments.Length == 1)
            {
                assembly = $"{request.GetType().GenericTypeArguments[0].FullName} List";
            }
            else assembly = $"{request.GetType().FullName}";

            return new RawData { Assembly = assembly, Data = serialize ? JsonConvert.SerializeObject(request, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore, PreserveReferencesHandling = PreserveReferencesHandling.Objects }) : request };
        }

        public static object Deserialize(RawData request)
        {
            var isList = request.Assembly.EndsWith("List");
            if (isList) request.Assembly = request.Assembly.Replace(" List", "");

            var targetType = Assembly.Load("Transversal.Common").GetTypes().Where(t => t.FullName.Equals(request.Assembly)).FirstOrDefault();
            if (targetType == null) targetType = Type.GetType(request.Assembly);

            if (request.Assembly.Equals("System.String") && !request.Data.ToString().StartsWith("\""))
                request.Data = $"\"{request.Data.ToString()}\"";

            return JsonConvert.DeserializeObject(request.Data.ToString(), isList ? typeof(List<>).MakeGenericType(targetType) : targetType);
        }

        public static string GuidReplacer(string jsonContent)
        {
            return JsonConvert.SerializeObject(GuidReplacerProcess((JObject)JsonConvert.DeserializeObject(jsonContent)), Formatting.Indented);
        }

        private static JObject GuidReplacerProcess(JObject obj)
        {
            foreach (var prop in obj.Properties())
            {
                if (prop.Name.Equals("id")) prop.Value = Guid.NewGuid().ToString().ToLower();
                else if (prop.Value.Type == JTokenType.Object) prop.Value = GuidReplacerProcess((JObject)prop.Value);
                else if (prop.Value.Type == JTokenType.Array) prop.Value.ToList().ForEach(element => { element = GuidReplacerProcess((JObject)element); });
            }

            return obj;
        }


        #endregion
    }
}
