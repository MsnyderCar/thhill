﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Transversal.Common.Utilities
{
    public class Loader
    {
        #region Public Methods

        public static object GetObject(string assemblyInfo, string tipoInterface, params object[] args)
        {
            return Loader.BuildObject(assemblyInfo, tipoInterface, args);
        }

        public static bool MyInterfaceFilter(Type typeObj, object criteriaObj)
        {
            return typeObj.ToString() == criteriaObj.ToString();
        }

        #endregion

        #region Private Methods

        static object BuildObject(string assemblyInfo, string tipoInterface, params object[] args)
        {
            object customObject = null;
            customObject = Loader.BuildObject(assemblyInfo, args);
            return customObject;
        }

        static object BuildObject(string assemblyInfo, params object[] args)
        {
            Type type = Loader.GetType(assemblyInfo);

            object response = null;

            try
            {
                response = Activator.CreateInstance(type, args);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"{ex.Message}{ex.StackTrace}");
            }

            return response;
        }

        static Type GetType(string assemblyInfo)
        {
            string[] data = assemblyInfo.Split(new char[]
            {
                ';'
            });
            string assemblyName = data[0];
            string typeName = data[1];

            string path = AppDomain.CurrentDomain.BaseDirectory;
            bool flag2 = path.EndsWith("\\");
            if (flag2)
            {
                path = path.Remove(path.Length - 1, 1);
            }

            var ass = Assembly.LoadFrom(Path.Combine(path, assemblyName));

            return ass.GetTypes().Where(t => t.FullName.StartsWith(typeName)).FirstOrDefault();
        }

        #endregion
    }
}
