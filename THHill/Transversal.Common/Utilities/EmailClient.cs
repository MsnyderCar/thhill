﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace Transversal.Common.Utilities
{
    public class EmailClient
    {
        #region Members

        protected SmtpClient Client;
        protected MailAddress User;

        #endregion

        #region Constructor

        public EmailClient(string host, int port, bool eSsl, string user, string passwd)
        {
            Client = new SmtpClient();
            Client.Timeout = 60000;

            User = new MailAddress(user);

            Client.Host = host;

            Client.Port = port;

            Client.EnableSsl = eSsl;

            Client.Credentials = new NetworkCredential(
                User.Address,
                passwd,
                User.Host
            );
        }

        #endregion

        #region Methods

        public void Send(string to, string subject, string content)
        {
            MailMessage msg = new MailMessage();

            msg.To.Add(new MailAddress(to));
            msg.From = User;
            msg.Subject = subject;
            msg.Body = content;
            msg.IsBodyHtml = true;
            msg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(msg.Body, Encoding.Default, "text/html"));

            Client.Send(msg);
        }

        #endregion
    }
}
