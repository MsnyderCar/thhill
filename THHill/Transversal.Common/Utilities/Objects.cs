﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using Transversal.Common.Entities.Models;

namespace Transversal.Common.Utilities
{
    public class Objects
    {
        #region Public Methods

        public static List<string> GetProperties(object target)
        {
            return (new List<PropertyInfo>(target.GetType().GetProperties())).Select(p => p.Name).ToList();
        }

        public static List<PropertyInfo> GetPropertiesByType(Type type)
        {
            return new List<PropertyInfo>(type.GetProperties());
        }

        public static void SetValueBasic(object target, string property, object value)
        {
            try
            {
                Type type = target.GetType();
                PropertyInfo prop = type.GetProperty(property);
                prop.SetValue(target, value, null);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public static bool SetValue(object target, string property, object value)
        {
            try
            {
                var parts = property.Split('.');

                if (parts.Length > 1)
                {
                    return SetValue(GetValue(target, parts[0]), string.Join(".",parts.Skip(1).ToArray()), value);
                }
                else
                {
                    Type type = target.GetType();
                    PropertyInfo prop = type.GetProperty(property);

                    object newValue = null;
                    string propType = GetTypeName(prop.PropertyType).ToLower();
                    string valueType = value.GetType().Name.ToLower();

                    if (propType != valueType && valueType == "string")
                    {
                        switch (GetTypeName(prop.PropertyType).ToLower())
                        {
                            case "int": case "int32": newValue = Convert.ToInt32(value); break;
                            case "decimal": newValue = Convert.ToDecimal(value); break;
                            case "string": newValue = (string)value; break;
                            case "datetime":
                                string dateString = (string)value;
                                DateTime newDate = DateTime.Now;
                                if (dateString.Length > 0)
                                {
                                    if (!DateTime.TryParseExact(dateString, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out newDate))
                                        if (!DateTime.TryParseExact(dateString, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out newDate))
                                            if (!DateTime.TryParseExact(dateString, "dd/MM/yyyy H:mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out newDate))
                                                newDate = DateTime.Now;
                                }
                                newValue = newDate;
                                break;
                            case "guid": newValue = new Guid(value.ToString()); break;
                        }
                    }
                    else newValue = value;

                    prop.SetValue(target, newValue, null);

                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public static object GetValue(object target, string property)
        {
            object Valor = null;

            try
            {
                Type type = target.GetType();
                PropertyInfo prop = type.GetProperty(property);
                Valor = prop.GetValue(target);
            }
            catch
            {
                Valor = string.Empty;
            }

            return Valor;
        }

        public static PropertyInfo GetProperty(object target, string property)
        {
            PropertyInfo Property = null;

            try
            {
                Type type = target.GetType();
                return type.GetProperty(property);
            }
            catch
            {

            }

            return Property;
        }

        public static byte[] ObjectToByteArray(object obj)
        {
            if (obj == null)
                return null;

            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream ms = new MemoryStream();
            bf.Serialize(ms, obj);

            return ms.ToArray();
        }

        public static object ByteArrayToObject(byte[] arrBytes)
        {
            MemoryStream memStream = new MemoryStream();
            BinaryFormatter binForm = new BinaryFormatter();
            memStream.Write(arrBytes, 0, arrBytes.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            object response = null;
            try
            {
                response = (object)binForm.Deserialize(memStream);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                response = new List<object>();
            }

            return response;
        }

        public static string GetTypeName(Type type)
        {
            var nullableType = Nullable.GetUnderlyingType(type);

            bool isNullableType = nullableType != null;

            if (isNullableType)
                return nullableType.Name;
            else
                return type.Name;
        }

        public static object GetFilter(object target, List<Filter> filters)
        {
            foreach (var filter in filters.OrderBy(f => f.Member))
            {

                if (filter.Member.Contains("."))
                {
                    SetValueRecursive(target, filter.Member, filter.Value);
                }
                else
                {
                    SetValue(
                        target,
                        filter.Member,
                        filter.Value
                    );
                }
            }

            return target;
        }

        static object SetValueRecursive(object target, string property, object value)
        {
            object response = null;

            if (property.Contains("."))
            {
                var parts = property.Split('.').ToList();
                var memberTarget = GetValue(target, parts[0]);
                if (parts.Count > 1)
                {
                    memberTarget = SetValueRecursive(memberTarget, string.Join(".", parts.Skip(1).ToArray()), value);
                }
                else
                {
                    SetValue(
                        memberTarget,
                        parts[0],
                        value
                    );
                }
                response = memberTarget;
            }

            return response;
        }

        public static object CloneObject(object target)
        {
            return ByteArrayToObject(ObjectToByteArray(target));
        }

        public static byte[] ReadFully(Stream input)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                input.CopyTo(ms);
                return ms.ToArray();
            }
        }

        #endregion
    }
}
