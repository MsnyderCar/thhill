﻿using Infrastructure.Core.UnityOfWork;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using Transversal.Common.Entities.DataBase.Authentication;
using Transversal.Common.Entities.DataBase.Core;
using Transversal.Common.Entities.DataBase.THHill;
using Transversal.Common.Entities.Models;
using Transversal.Common.Utilities;

namespace Domain.Services
{
    public class THHill : BaseService
    {
        #region Contants

        const int MaxIterations = 5;

        #endregion

        #region Constructor

        public THHill(IUnityOfWork unityOfWork) : base(unityOfWork) { }

        public THHill(IUnityOfWork unityOfWork, bool serialize) : base(unityOfWork, serialize) { }

        #endregion

        #region Technician

        public KeyValuePair<List<User>, int> GetTechniciansPaged(List<Filter> filters, List<KeyValuePair<string, bool>> orderBy, int pageSize, int pageNumber)
        {
            var response = new KeyValuePair<List<User>, int>(new List<User>(), 0);

            int total = 0;

            try
            {
                var repo = UOW.Repository<User>();

                var filterObj = new User();

                foreach (var filter in filters)
                {
                    Objects.SetValue(
                        filterObj,
                        filter.Member,
                        filter.Value
                    );
                }

                var res = repo.GetByFilter(
                    t => (
                        (t.Active)
                        && (t.UserTypeId == Transversal.Common.Enumerators.Authentication.UserType.Technician.GetHashCode())
                    ),
                    new List<string>(),
                    orderBy,
                    pageSize,
                    pageNumber,
                    out total
                ).ToList();

                response = new KeyValuePair<List<User>, int>(res, total);
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
            }

            return response;
        }

        public User GetTechnicianById(Guid id)
        {
            var response = new User();

            try
            {
                var repo = UOW.Repository<User>();

                response = repo.GetByFilter(
                    t => (
                        (id != Guid.Empty ? t.Id == id : true)
                    )
                ).ToList().FirstOrDefault();
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
                response = null;
            }

            return response;
        }

        public User SaveTechnician(User user)
        {
            User response = null;

            UOW.BeginTransaction();

            try
            {
                var isNew = user.Id == Guid.Empty;

                response = new Authentication(UOW).SaveUser(user);
                
                UOW.CommitTransaction();
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
                response = null;
                UOW.RollbackTransaction();
            }

            return response;
        }

        public bool DeleteTechnician(Guid id)
        {
            var result = true;

            try
            {
                var repo = UOW.Repository<User>();

                var tool = repo.GetById(id);

                if (tool != null)
                {
                    tool.Active = false;
                    repo.Update(tool);
                }
                else throw new Exception($"The Technician with id {id} doesn't exist");
            }
            catch (Exception ex)
            {
                result = false;
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
            }

            return result;
        }

        #endregion

        #region Tool

        public List<Tool> GetTools(long userId)
        {
            var response = new List<Tool>();

            try
            {
                var repo = UOW.Repository<Tool>();

                var res = repo.GetByFilter(
                    t => (
                        (t.Active)
                    ),
                    new List<string>{
                        "ToolType"
                    }
                ).ToList();

                response = res.ToList();
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
            }

            return response;
        }

        public KeyValuePair<List<Tool>, int> GetToolsPaged(List<Filter> filters, List<KeyValuePair<string, bool>> orderBy, int pageSize, int pageNumber)
        {
            var response = new KeyValuePair<List<Tool>, int>(new List<Tool>(), 0);

            int total = 0;

            try
            {
                var repo = UOW.Repository<Tool>();

                var filterObj = new Tool();

                foreach (var filter in filters)
                {
                    Objects.SetValue(
                        filterObj,
                        filter.Member,
                        filter.Value
                    );
                }

                var res = repo.GetByFilter(
                    t => (
                        t.Active
                    ),
                    new List<string>{
                        "ToolType"
                    },
                    orderBy,
                    pageSize,
                    pageNumber,
                    out total
                ).ToList();

                response = new KeyValuePair<List<Tool>, int>(res, total);
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
            }

            return response;
        }

        public Tool GetToolById(Guid id)
        {
            var response = new Tool();

            try
            {
                var repo = UOW.Repository<Tool>();
                response = repo.GetByFilter(t => t.Id == id, new List<string> { "Components" }).FirstOrDefault();
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
                response = null;
            }

            return response;
        }

        public Tool SaveTool(Tool tool)
        {
            var path = "repository/core/";

            Tool response = null;

            var fromTransaction = UOW.IsOnTransaction();

            if (!fromTransaction) UOW.BeginTransaction();

            try
            {
                var isNew = true;

                var repo = UOW.Repository<Tool>();
                var repoComponent = UOW.Repository<Component>();

                var components = new List<Component>();
                if (tool.Components != null) components.AddRange(tool.Components);
                tool.Components = null;

                tool.Image = tool.Image != null ? ProcessMetadataImage(tool.Image, path) : tool.Image;

                if (tool.Id == Guid.Empty)
                {
                    response = repo.Insert(tool);
                }
                else
                {
                    repo.Update(tool);
                    response = tool;
                    isNew = false;
                }

                #region Components

                if (!isNew)
                {
                    repoComponent.GetByFilter(storedComponent => storedComponent.ToolId == tool.Id).ToList().ForEach(storedComponent => {
                        if (!components.Exists(component => component.Id == storedComponent.Id)) repoComponent.DeleteById(storedComponent.Id);
                    });
                }

                components.ForEach(component => {
                    component.ToolId = tool.Id;
                    component.Image = component.Image != null ? ProcessMetadataImage(component.Image, path) : component.Image;
                    if (component.Id == Guid.Empty) repoComponent.Insert(component);
                    else repoComponent.Update(component);
                });

                #endregion

                if (!fromTransaction) UOW.CommitTransaction();
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
                response = null;
                if (!fromTransaction) UOW.RollbackTransaction();
                else throw ex;
            }

            return response;
        }

        public Component SaveComponent(Component component)
        {
            Component response = null;

            var path = "repository/core/";

            var fromTransaction = UOW.IsOnTransaction();

            if (!fromTransaction) UOW.BeginTransaction();

            try
            {
                var repo = UOW.Repository<Component>();

                component.Image = component.Image != null ? ProcessMetadataImage(component.Image, path) : component.Image;

                if (component.Id == Guid.Empty) repo.Insert(component);
                else repo.Update(component);
                
                if (!fromTransaction) UOW.CommitTransaction();
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
                response = null;
                if (!fromTransaction) UOW.RollbackTransaction();
                else throw ex;
            }

            return response;
        }

        public bool DeleteTool(Guid id)
        {
            var result = true;

            try
            {
                var repo = UOW.Repository<Tool>();

                var tool = repo.GetById(id);

                if (tool != null)
                {
                    tool.Active = false;
                    repo.Update(tool);
                }
                else throw new Exception($"The Tool with id {id} doesn't exist");
            }
            catch (Exception ex)
            {
                result = false;
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
            }
            
            return result;
        }

        #endregion

        #region Operator

        public KeyValuePair<List<Operator>, int> GetOperatorsPaged(List<Filter> filters, List<KeyValuePair<string, bool>> orderBy, int pageSize, int pageNumber)
        {
            var response = new KeyValuePair<List<Operator>, int>(new List<Operator>(), 0);

            int total = 0;

            try
            {
                var repo = UOW.Repository<Operator>();

                var filterObj = new Operator();

                foreach (var filter in filters)
                {
                    Objects.SetValue(
                        filterObj,
                        filter.Member,
                        filter.Value
                    );
                }

                var res = repo.GetByFilter(
                    t => (
                        t.Active
                    ),
                    new List<string>(),
                    orderBy,
                    pageSize,
                    pageNumber,
                    out total
                ).ToList();

                response = new KeyValuePair<List<Operator>, int>(res, total);
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
            }

            return response;
        }

        public Operator GetOperatorById(Guid id)
        {
            var response = new Operator();

            try
            {
                var repo = UOW.Repository<Operator>();

                response = repo.GetByFilter(
                    t => (
                        (id != Guid.Empty ? t.Id == id : true)
                    )
                ).ToList().FirstOrDefault();
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
                response = null;
            }

            return response;
        }

        public Operator SaveOperator(Operator operatorE)
        {
            Operator response = null;

            UOW.BeginTransaction();

            try
            {
                var repo = UOW.Repository<Operator>();

                if (operatorE.Id == Guid.Empty) response = repo.Insert(operatorE);
                else
                {
                    repo.Update(operatorE);
                    response = operatorE;
                }

                UOW.CommitTransaction();
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
                response = null;
                UOW.RollbackTransaction();
            }

            return response;
        }

        public bool DeleteOperator(Guid id)
        {
            var result = true;

            try
            {
                var repo = UOW.Repository<Operator>();

                var tool = repo.GetById(id);

                if (tool != null)
                {
                    tool.Active = false;
                    repo.Update(tool);
                }
                else throw new Exception($"The Operator with id {id} doesn't exist");
            }
            catch (Exception ex)
            {
                result = false;
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
            }

            return result;
        }

        #endregion

        #region OilWell

        public KeyValuePair<List<OilWell>, int> GetOilWellsPaged(List<Filter> filters, List<KeyValuePair<string, bool>> orderBy, int pageSize, int pageNumber)
        {
            var response = new KeyValuePair<List<OilWell>, int>(new List<OilWell>(), 0);

            int total = 0;

            try
            {
                var repo = UOW.Repository<OilWell>();

                var filterObj = new OilWell();

                foreach (var filter in filters)
                {
                    Objects.SetValue(
                        filterObj,
                        filter.Member,
                        filter.Value
                    );
                }

                var res = repo.GetByFilter(
                    t => (
                        t.Active
                    ),
                    new List<string>(),
                    orderBy,
                    pageSize,
                    pageNumber,
                    out total
                ).ToList();

                response = new KeyValuePair<List<OilWell>, int>(res, total);
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
            }

            return response;
        }

        public OilWell GetOilWellById(Guid id)
        {
            var response = new OilWell();

            try
            {
                var repo = UOW.Repository<OilWell>();

                response = repo.GetByFilter(
                    t => (
                        (id != Guid.Empty ? t.Id == id : true)
                    )
                ).ToList().FirstOrDefault();
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
                response = null;
            }

            return response;
        }

        public OilWell SaveOilWell(OilWell oilWell)
        {
            OilWell response = null;

            UOW.BeginTransaction();

            try
            {
                var repo = UOW.Repository<OilWell>();

                if (oilWell.Id == Guid.Empty) response = repo.Insert(oilWell);
                else
                {
                    repo.Update(oilWell);
                    response = oilWell;
                }

                UOW.CommitTransaction();
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
                response = null;
                UOW.RollbackTransaction();
            }

            return response;
        }

        public bool DeleteOilWell(Guid id)
        {
            var result = true;

            try
            {
                var repo = UOW.Repository<OilWell>();

                var tool = repo.GetById(id);

                if (tool != null)
                {
                    tool.Active = false;
                    repo.Update(tool);
                }
                else throw new Exception($"The OilWell with id {id} doesn't exist");
            }
            catch (Exception ex)
            {
                result = false;
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
            }

            return result;
        }

        #endregion

        #region MonitoringType

        public KeyValuePair<List<MonitoringType>, int> GetMonitoringTypesPaged(List<Filter> filters, List<KeyValuePair<string, bool>> orderBy, int pageSize, int pageNumber)
        {
            var response = new KeyValuePair<List<MonitoringType>, int>(new List<MonitoringType>(), 0);

            int total = 0;

            try
            {
                var repo = UOW.Repository<MonitoringType>();

                var filterObj = new MonitoringType();

                foreach (var filter in filters)
                {
                    Objects.SetValue(
                        filterObj,
                        filter.Member,
                        filter.Value
                    );
                }

                var res = repo.GetByFilter(
                    t => (
                        t.Active
                    ),
                    new List<string>(),
                    orderBy,
                    pageSize,
                    pageNumber,
                    out total
                ).ToList();

                response = new KeyValuePair<List<MonitoringType>, int>(res, total);
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
            }

            return response;
        }

        public MonitoringType GetMonitoringTypeById(Guid id)
        {
            var response = new MonitoringType();

            try
            {
                var repo = UOW.Repository<MonitoringType>();

                response = repo.GetByFilter(
                    t => (
                        (id != Guid.Empty ? t.Id == id : true)
                    )
                ).ToList().FirstOrDefault();
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
                response = null;
            }

            return response;
        }

        public MonitoringType SaveMonitoringType(MonitoringType monitoringType)
        {
            MonitoringType response = null;

            UOW.BeginTransaction();

            try
            {
                var repo = UOW.Repository<MonitoringType>();

                if (monitoringType.Id == Guid.Empty) response = repo.Insert(monitoringType);
                else
                {
                    repo.Update(monitoringType);
                    response = monitoringType;
                }

                UOW.CommitTransaction();
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
                response = null;
                UOW.RollbackTransaction();
            }

            return response;
        }

        public bool DeleteMonitoringType(Guid id)
        {
            var result = true;

            try
            {
                var repo = UOW.Repository<MonitoringType>();

                var tool = repo.GetById(id);

                if (tool != null)
                {
                    tool.Active = false;
                    repo.Update(tool);
                }
                else throw new Exception($"The Monitoring Type with id {id} doesn't exist");
            }
            catch (Exception ex)
            {
                result = false;
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
            }

            return result;
        }

        #endregion

        #region InspectionCategory

        public KeyValuePair<List<InspectionCategory>, int> GetInspectionCategoriesPaged(List<Filter> filters, List<KeyValuePair<string, bool>> orderBy, int pageSize, int pageNumber)
        {
            var response = new KeyValuePair<List<InspectionCategory>, int>(new List<InspectionCategory>(), 0);

            int total = 0;

            try
            {
                var repo = UOW.Repository<InspectionCategory>();

                var filterObj = new InspectionCategory();

                foreach (var filter in filters)
                {
                    Objects.SetValue(
                        filterObj,
                        filter.Member,
                        filter.Value
                    );
                }

                var res = repo.GetByFilter(
                    t => (
                        t.Active
                    ),
                    new List<string>(),
                    orderBy,
                    pageSize,
                    pageNumber,
                    out total
                ).ToList();

                response = new KeyValuePair<List<InspectionCategory>, int>(res, total);
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
            }

            return response;
        }

        public InspectionCategory GetInspectionCategoryById(Guid id)
        {
            var response = new InspectionCategory();

            try
            {
                var repo = UOW.Repository<InspectionCategory>();

                response = repo.GetByFilter(
                    t => (
                        (id != Guid.Empty ? t.Id == id : true)
                    )
                ).ToList().FirstOrDefault();
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
                response = null;
            }

            return response;
        }

        public InspectionCategory SaveInspectionCategory(InspectionCategory inspectionCategory)
        {
            InspectionCategory response = null;

            UOW.BeginTransaction();

            try
            {
                var repo = UOW.Repository<InspectionCategory>();

                if (inspectionCategory.Id == Guid.Empty) response = repo.Insert(inspectionCategory);
                else
                {
                    repo.Update(inspectionCategory);
                    response = inspectionCategory;
                }

                UOW.CommitTransaction();
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
                response = null;
                UOW.RollbackTransaction();
            }

            return response;
        }

        public bool DeleteInspectionCategory(Guid id)
        {
            var result = true;

            try
            {
                var repo = UOW.Repository<InspectionCategory>();

                var tool = repo.GetById(id);

                if (tool != null)
                {
                    tool.Active = false;
                    repo.Update(tool);
                }
                else throw new Exception($"The Inspection Category with id {id} doesn't exist");
            }
            catch (Exception ex)
            {
                result = false;
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
            }

            return result;
        }

        #endregion

        #region WorkOrder

        public List<WorkOrder> GetWorkOrders(Guid userId)
        {
            var response = new List<WorkOrder>();

            try
            {
                var repo = UOW.Repository<WorkOrder>();

                var res = repo.GetByFilter(
                    t => (
                        (t.Active)
                        && (t.UserId == userId)
                    ),
                    new List<string>{
                        "WorkOrderState",
                        "Operator",
                        "OilWell"
                    }
                ).ToList();

                response = res.ToList();
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
            }

            return response;
        }

        public KeyValuePair<List<WorkOrder>, int> GetWorkOrdersPaged(List<Filter> filters, List<KeyValuePair<string, bool>> orderBy, int pageSize, int pageNumber)
        {
            var response = new KeyValuePair<List<WorkOrder>, int>(new List<WorkOrder>(), 0);

            int total = 0;

            try
            {
                var repo = UOW.Repository<WorkOrder>();

                var filterObj = new WorkOrder();

                foreach (var filter in filters)
                {
                    Objects.SetValue(
                        filterObj,
                        filter.Member,
                        filter.Value
                    );
                }

                var res = repo.GetByFilter(
                    t => (
                        t.Active
                    ),
                    new List<string> {
                        "User",
                        "Operator"
                    },
                    orderBy,
                    pageSize,
                    pageNumber,
                    out total
                ).ToList();

                response = new KeyValuePair<List<WorkOrder>, int>(res, total);
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
            }

            return response;
        }

        public WorkOrder GetWorkOrderById(Guid id)
        {
            var response = new WorkOrder();

            try
            {
                var repo = UOW.Repository<WorkOrder>();

                response = repo.GetByFilter(
                    t => (
                        (id != Guid.Empty ? t.Id == id : true)
                    ),
                    new List<string> {
                        "Operator",
                        "OilWell",
                        "MonitoringType",
                        "InspectionCategory",
                        "Vendor",
                        "WorkOrderState",

                        "InspectionImages",
                        "InspectionTools",
                        "InspectionTools.ToolType",
                        "InspectionTools.SubAssemblyCondition",

                        "InspectionTools.InspectionToolDiscrepancies",
                        "InspectionTools.InspectionToolDiscrepancies.DiscrepancyType",
                        "InspectionTools.InspectionToolDiscrepancies.DiscrepancyResult",
                        "InspectionTools.InspectionToolDiscrepancies.DiscrepancyState",
                        "InspectionTools.InspectionToolDiscrepancies.DiscrepancyRisk",

                        "InspectionTools.InspectionComponents",
                        "InspectionTools.InspectionComponents.Condition",
                        "InspectionTools.InspectionComponents.InspectionComponentDiscrepancies",
                        "InspectionTools.InspectionComponents.InspectionComponentDiscrepancies.DiscrepancyType",
                        "InspectionTools.InspectionComponents.InspectionComponentDiscrepancies.DiscrepancyResult",
                        "InspectionTools.InspectionComponents.InspectionComponentDiscrepancies.DiscrepancyState",
                        "InspectionTools.InspectionComponents.InspectionComponentDiscrepancies.DiscrepancyRisk",

                        "InspectionTools.InspectionSubAssemblyImages",
                        "InspectionTools.InspectionTestRunImages",

                        "InspectionTools.InspectionTestRunDiscrepancies",
                        "InspectionTools.InspectionTestRunDiscrepancies.DiscrepancyType",
                        "InspectionTools.InspectionTestRunDiscrepancies.DiscrepancyResult",
                        "InspectionTools.InspectionTestRunDiscrepancies.DiscrepancyState",
                        "InspectionTools.InspectionTestRunDiscrepancies.DiscrepancyRisk"
                    }
                ).ToList().FirstOrDefault();
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
                response = null;
            }

            return response;
        }

        public WorkOrder SaveWorkOrder(WorkOrder workOrder)
        {
            var path = "repository/core";

            WorkOrder response = workOrder;

            UOW.BeginTransaction();

            var repo = UOW.Repository<WorkOrder>();

            try
            {
                var isNew = true;

                var repoInspectionImage = UOW.Repository<InspectionImage>();
                var repoInspectionTool = UOW.Repository<InspectionTool>();
                var repoInspectionComponent = UOW.Repository<InspectionComponent>();
                var repoInspectionComponentDiscrepancy = UOW.Repository<InspectionComponentDiscrepancy>();

                var repoInspectionToolDiscrepancy = UOW.Repository<InspectionToolDiscrepancy>();
                var repoInspectionSubAssemblyImage = UOW.Repository<InspectionSubAssemblyImage>();
                var repoInspectionTestRunImage = UOW.Repository<InspectionTestRunImage>();
                var repoInspectionTestRunDiscrepancy = UOW.Repository<InspectionTestRunDiscrepancy>();

                var repoParameter = UOW.Repository<Parameter>();

                var inspectionImages = new List<InspectionImage>();
                if (workOrder.InspectionImages != null) inspectionImages.AddRange(workOrder.InspectionImages);
                workOrder.InspectionImages = null;

                var inspectionTools = new List<InspectionTool>();
                if (workOrder.InspectionTools != null) inspectionTools.AddRange(workOrder.InspectionTools);
                workOrder.InspectionTools = null;

                workOrder.User = null;
                workOrder.Operator = null;
                workOrder.OilWell = null;
                workOrder.MonitoringType = null;
                workOrder.InspectionCategory = null;
                workOrder.Vendor = null;
                workOrder.WorkOrderState = null;

                if (workOrder.Id == Guid.Empty)
                {
                    var paramConsecutive = repoParameter.GetByFilter(f => f.Code.Equals("Consecutive") && f.Categories.Equals("Business")).FirstOrDefault();
                    paramConsecutive.Value = (Convert.ToInt32(paramConsecutive.Value) + 1).ToString();
                    repoParameter.Update(paramConsecutive);
                    workOrder.Consecutive = Convert.ToInt32(paramConsecutive.Value);
                    response = repo.Insert(workOrder);
                }
                else
                {
                    repo.Update(workOrder);
                    response = workOrder;
                    isNew = false;
                }

                #region InspectionImage

                if (!isNew)
                {
                    repoInspectionImage.GetByFilter(storedInspectionImage => storedInspectionImage.WorkOrderId == workOrder.Id).ToList().ForEach(storedInspectionImage => {
                        if (!inspectionImages.Exists(inspectionImage => inspectionImage.Id == storedInspectionImage.Id)) repoInspectionImage.DeleteById(storedInspectionImage.Id);
                    });
                }

                inspectionImages.ForEach(inspectionImage => {
                    inspectionImage.WorkOrderId = workOrder.Id;
                    inspectionImage.Image = inspectionImage.Image != null ? ProcessMetadataImage(inspectionImage.Image, path) : inspectionImage.Image;
                    if (inspectionImage.Id == Guid.Empty) repoInspectionImage.Insert(inspectionImage);
                    else repoInspectionImage.Update(inspectionImage);
                });

                #endregion

                #region InspectionTool

                if (!isNew)
                {
                    repoInspectionTool.GetByFilter(storedInspectionTool => storedInspectionTool.WorkOrderId == workOrder.Id).ToList().ForEach(storedInspectionTool => {
                        if (!inspectionTools.Exists(inspectionTool => inspectionTool.Id == storedInspectionTool.Id))
                        {
                            repoInspectionComponent.GetByFilter(storedInspectionComponent => storedInspectionComponent.InspectionToolId == storedInspectionTool.Id).ToList().ForEach(storedInspectionComponent => {
                                repoInspectionComponentDiscrepancy.GetByFilter(storedInspectionComponentDiscrepancy => storedInspectionComponentDiscrepancy.InspectionComponentId == storedInspectionComponent.Id).ToList().ForEach(storedInspectionComponentDiscrepancy => {
                                    repoInspectionComponentDiscrepancy.DeleteById(storedInspectionComponentDiscrepancy.Id);
                                });
                                repoInspectionComponent.DeleteById(storedInspectionComponent.Id);
                            });

                            repoInspectionToolDiscrepancy.GetByFilter(storedInspectionToolDiscrepancy => storedInspectionToolDiscrepancy.InspectionToolId == storedInspectionTool.Id).ToList().ForEach(storedInspectionToolDiscrepancy => {
                                repoInspectionToolDiscrepancy.DeleteById(storedInspectionToolDiscrepancy.Id);
                            });

                            repoInspectionSubAssemblyImage.GetByFilter(storedInspectionSubAssemblyImage => storedInspectionSubAssemblyImage.InspectionToolId == storedInspectionTool.Id).ToList().ForEach(storedInspectionSubAssemblyImage => {
                                repoInspectionSubAssemblyImage.DeleteById(storedInspectionSubAssemblyImage.Id);
                            });

                            repoInspectionTestRunImage.GetByFilter(storedInspectionTestRunImage => storedInspectionTestRunImage.InspectionToolId == storedInspectionTool.Id).ToList().ForEach(storedInspectionTestRunImage => {
                                repoInspectionTestRunImage.DeleteById(storedInspectionTestRunImage.Id);
                            });

                            repoInspectionTestRunDiscrepancy.GetByFilter(storedInspectionTestRunDiscrepancy => storedInspectionTestRunDiscrepancy.InspectionToolId == storedInspectionTool.Id).ToList().ForEach(storedInspectionTestRunDiscrepancy => {
                                repoInspectionTestRunDiscrepancy.DeleteById(storedInspectionTestRunDiscrepancy.Id);
                            });

                            repoInspectionTool.DeleteById(storedInspectionTool.Id);
                        }
                    });
                }

                inspectionTools.ForEach(inspectionTool => {
                    var isNewInspectionTool = true;
                    inspectionTool.WorkOrderId = workOrder.Id;

                    inspectionTool.WorkOrder = null;

                    if (inspectionTool.ToolType != null && inspectionTool.ToolType.Id > 0) inspectionTool.ToolTypeId = inspectionTool.ToolType.Id;
                    inspectionTool.ToolType = null;

                    if (inspectionTool.SubAssemblyCondition != null && inspectionTool.SubAssemblyCondition.Id > 0) inspectionTool.SubAssemblyConditionId = inspectionTool.SubAssemblyCondition.Id;
                    inspectionTool.SubAssemblyCondition = null;

                    var inspectionComponents = new List<InspectionComponent>();
                    if (inspectionTool.InspectionComponents != null) inspectionComponents.AddRange(inspectionTool.InspectionComponents);
                    inspectionTool.InspectionComponents = null;

                    var inspectionToolDiscrepancies = new List<InspectionToolDiscrepancy>();
                    if (inspectionTool.InspectionToolDiscrepancies != null) inspectionToolDiscrepancies.AddRange(inspectionTool.InspectionToolDiscrepancies);
                    inspectionTool.InspectionToolDiscrepancies = null;

                    var inspectionSubAssemblyImages = new List<InspectionSubAssemblyImage>();
                    if (inspectionTool.InspectionSubAssemblyImages != null) inspectionSubAssemblyImages.AddRange(inspectionTool.InspectionSubAssemblyImages);
                    inspectionTool.InspectionSubAssemblyImages = null;

                    var inspectionTestRunImages = new List<InspectionTestRunImage>();
                    if (inspectionTool.InspectionTestRunImages != null) inspectionTestRunImages.AddRange(inspectionTool.InspectionTestRunImages);
                    inspectionTool.InspectionTestRunImages = null;

                    var inspectionTestRunDiscrepancies = new List<InspectionTestRunDiscrepancy>();
                    if (inspectionTool.InspectionTestRunDiscrepancies != null) inspectionTestRunDiscrepancies.AddRange(inspectionTool.InspectionTestRunDiscrepancies);
                    inspectionTool.InspectionTestRunDiscrepancies = null;

                    inspectionTool.Image = inspectionTool.Image != null ? ProcessMetadataImage(inspectionTool.Image, path) : inspectionTool.Image;

                    if (inspectionTool.Id == Guid.Empty) repoInspectionTool.Insert(inspectionTool);
                    else
                    {
                        repoInspectionTool.Update(inspectionTool);
                        isNewInspectionTool = false;
                    }

                    if (!isNewInspectionTool)
                    {
                        repoInspectionComponent.GetByFilter(storedInspectionComponent => storedInspectionComponent.InspectionToolId == inspectionTool.Id).ToList().ForEach(storedInspectionComponent => {
                            if (!inspectionComponents.Exists(inspectionComponent => inspectionComponent.Id == storedInspectionComponent.Id))
                                repoInspectionComponent.DeleteById(storedInspectionComponent.Id);
                        });

                        repoInspectionToolDiscrepancy.GetByFilter(storedInspectionToolDiscrepancy => storedInspectionToolDiscrepancy.InspectionToolId == inspectionTool.Id).ToList().ForEach(storedInspectionToolDiscrepancy => {
                            if (!inspectionToolDiscrepancies.Exists(inspectionComponentDiscrepancy => inspectionComponentDiscrepancy.Id == storedInspectionToolDiscrepancy.Id))
                                repoInspectionToolDiscrepancy.DeleteById(storedInspectionToolDiscrepancy.Id);
                        });

                        repoInspectionSubAssemblyImage.GetByFilter(storedInspectionSubAssemblyImage => storedInspectionSubAssemblyImage.InspectionToolId == inspectionTool.Id).ToList().ForEach(storedInspectionSubAssemblyImage => {
                            if (!inspectionSubAssemblyImages.Exists(inspectionSubAssemblyImage => inspectionSubAssemblyImage.Id == storedInspectionSubAssemblyImage.Id))
                                repoInspectionSubAssemblyImage.DeleteById(storedInspectionSubAssemblyImage.Id);
                        });

                        repoInspectionTestRunImage.GetByFilter(storedInspectionTestRunImage => storedInspectionTestRunImage.InspectionToolId == inspectionTool.Id).ToList().ForEach(storedInspectionTestRunImage => {
                            if (!inspectionTestRunImages.Exists(inspectionTestRunImage => inspectionTestRunImage.Id == storedInspectionTestRunImage.Id))
                                repoInspectionTestRunImage.DeleteById(storedInspectionTestRunImage.Id);
                        });

                        repoInspectionTestRunDiscrepancy.GetByFilter(storedInspectionTestRunDiscrepancy => storedInspectionTestRunDiscrepancy.InspectionToolId == inspectionTool.Id).ToList().ForEach(storedInspectionTestRunDiscrepancy => {
                            if (!inspectionTestRunDiscrepancies.Exists(inspectionTestRunDiscrepancy => inspectionTestRunDiscrepancy.Id == storedInspectionTestRunDiscrepancy.Id))
                                repoInspectionTestRunDiscrepancy.DeleteById(storedInspectionTestRunDiscrepancy.Id);
                        });
                    }

                    inspectionComponents.ForEach(inspectionComponent => {
                        var isNewInspectionComponent = true;

                        inspectionComponent.InspectionToolId = inspectionTool.Id;

                        inspectionComponent.InspectionTool = null;

                        if (inspectionComponent.Condition != null && inspectionComponent.Condition.Id > 0) inspectionComponent.ConditionId = inspectionComponent.Condition.Id;
                        inspectionComponent.Condition = null;

                        var inspectionComponentDiscrepancies = new List<InspectionComponentDiscrepancy>();
                        if (inspectionComponent.InspectionComponentDiscrepancies != null) inspectionComponentDiscrepancies.AddRange(inspectionComponent.InspectionComponentDiscrepancies);
                        inspectionComponent.InspectionComponentDiscrepancies = null;

                        inspectionComponent.Image = inspectionComponent.Image != null ? ProcessMetadataImage(inspectionComponent.Image, path) : inspectionComponent.Image;

                        if (inspectionComponent.Id == Guid.Empty) repoInspectionComponent.Insert(inspectionComponent);
                        else
                        {
                            repoInspectionComponent.Update(inspectionComponent);
                            isNewInspectionComponent = false;
                        }

                        if (!isNewInspectionComponent)
                        {
                            repoInspectionComponentDiscrepancy.GetByFilter(storedInspectionComponentDiscrepancy => storedInspectionComponentDiscrepancy.InspectionComponentId == inspectionComponent.Id).ToList().ForEach(storedInspectionComponentDiscrepancy => {
                                if (!inspectionComponents.Exists(inspectionComponentDiscrepancy => inspectionComponentDiscrepancy.Id == storedInspectionComponentDiscrepancy.Id))
                                    repoInspectionComponentDiscrepancy.DeleteById(storedInspectionComponentDiscrepancy.Id);
                            });
                        }

                        inspectionComponentDiscrepancies.ForEach(inspectionComponentDiscrepancy => {
                            inspectionComponentDiscrepancy.InspectionComponentId = inspectionComponent.Id;

                            inspectionComponentDiscrepancy.InspectionComponent = null;

                            if (inspectionComponentDiscrepancy.DiscrepancyType != null && inspectionComponentDiscrepancy.DiscrepancyType.Id > 0) inspectionComponentDiscrepancy.DiscrepancyTypeId = inspectionComponentDiscrepancy.DiscrepancyType.Id;
                            inspectionComponentDiscrepancy.DiscrepancyType = null;

                            if (inspectionComponentDiscrepancy.DiscrepancyResult != null && inspectionComponentDiscrepancy.DiscrepancyResult.Id > 0) inspectionComponentDiscrepancy.DiscrepancyResultId = inspectionComponentDiscrepancy.DiscrepancyResult.Id;
                            inspectionComponentDiscrepancy.DiscrepancyResult = null;

                            if (inspectionComponentDiscrepancy.DiscrepancyState != null && inspectionComponentDiscrepancy.DiscrepancyState.Id > 0) inspectionComponentDiscrepancy.DiscrepancyStateId = inspectionComponentDiscrepancy.DiscrepancyState.Id;
                            inspectionComponentDiscrepancy.DiscrepancyState = null;

                            if (inspectionComponentDiscrepancy.DiscrepancyRisk != null && inspectionComponentDiscrepancy.DiscrepancyRisk.Id > 0) inspectionComponentDiscrepancy.DiscrepancyRiskId = inspectionComponentDiscrepancy.DiscrepancyRisk.Id;
                            inspectionComponentDiscrepancy.DiscrepancyRisk = null;

                            inspectionComponentDiscrepancy.Evidence = inspectionComponentDiscrepancy.Evidence != null ? ProcessMetadataImage(inspectionComponentDiscrepancy.Evidence, path) : inspectionComponentDiscrepancy.Evidence;

                            if (inspectionComponentDiscrepancy.Id == Guid.Empty) repoInspectionComponentDiscrepancy.Insert(inspectionComponentDiscrepancy);
                            else repoInspectionComponentDiscrepancy.Update(inspectionComponentDiscrepancy);
                        });
                    });

                    inspectionToolDiscrepancies.ForEach(inspectionToolDiscrepancy => {
                        inspectionToolDiscrepancy.InspectionToolId = inspectionTool.Id;

                        inspectionToolDiscrepancy.InspectionTool = null;

                        if (inspectionToolDiscrepancy.DiscrepancyType != null && inspectionToolDiscrepancy.DiscrepancyType.Id > 0) inspectionToolDiscrepancy.DiscrepancyTypeId = inspectionToolDiscrepancy.DiscrepancyType.Id;
                        inspectionToolDiscrepancy.DiscrepancyType = null;

                        if (inspectionToolDiscrepancy.DiscrepancyResult != null && inspectionToolDiscrepancy.DiscrepancyResult.Id > 0) inspectionToolDiscrepancy.DiscrepancyResultId = inspectionToolDiscrepancy.DiscrepancyResult.Id;
                        inspectionToolDiscrepancy.DiscrepancyResult = null;

                        if (inspectionToolDiscrepancy.DiscrepancyState != null && inspectionToolDiscrepancy.DiscrepancyState.Id > 0) inspectionToolDiscrepancy.DiscrepancyStateId = inspectionToolDiscrepancy.DiscrepancyState.Id;
                        inspectionToolDiscrepancy.DiscrepancyState = null;

                        if (inspectionToolDiscrepancy.DiscrepancyRisk != null && inspectionToolDiscrepancy.DiscrepancyRisk.Id > 0) inspectionToolDiscrepancy.DiscrepancyRiskId = inspectionToolDiscrepancy.DiscrepancyRisk.Id;
                        inspectionToolDiscrepancy.DiscrepancyRisk = null;

                        inspectionToolDiscrepancy.Evidence = inspectionToolDiscrepancy.Evidence != null ? ProcessMetadataImage(inspectionToolDiscrepancy.Evidence, path) : inspectionToolDiscrepancy.Evidence;

                        if (inspectionToolDiscrepancy.Id == Guid.Empty) repoInspectionToolDiscrepancy.Insert(inspectionToolDiscrepancy);
                        else repoInspectionToolDiscrepancy.Update(inspectionToolDiscrepancy);
                    });

                    inspectionSubAssemblyImages.ForEach(inspectionSubAssemblyImage => {
                        inspectionSubAssemblyImage.InspectionToolId = inspectionTool.Id;

                        inspectionSubAssemblyImage.Image = inspectionSubAssemblyImage.Image != null ? ProcessMetadataImage(inspectionSubAssemblyImage.Image, path) : inspectionSubAssemblyImage.Image;

                        if (inspectionSubAssemblyImage.Id == Guid.Empty) repoInspectionSubAssemblyImage.Insert(inspectionSubAssemblyImage);
                        else repoInspectionSubAssemblyImage.Update(inspectionSubAssemblyImage);
                    });

                    inspectionTestRunImages.ForEach(inspectionTestRunImage => {
                        inspectionTestRunImage.InspectionToolId = inspectionTool.Id;

                        inspectionTestRunImage.Image = inspectionTestRunImage.Image != null ? ProcessMetadataImage(inspectionTestRunImage.Image, path) : inspectionTestRunImage.Image;

                        if (inspectionTestRunImage.Id == Guid.Empty) repoInspectionTestRunImage.Insert(inspectionTestRunImage);
                        else repoInspectionTestRunImage.Update(inspectionTestRunImage);
                    });

                    inspectionTestRunDiscrepancies.ForEach(inspectionTestRunDiscrepancy => {
                        inspectionTestRunDiscrepancy.InspectionToolId = inspectionTool.Id;

                        inspectionTestRunDiscrepancy.InspectionTool = null;

                        if (inspectionTestRunDiscrepancy.DiscrepancyType != null && inspectionTestRunDiscrepancy.DiscrepancyType.Id > 0) inspectionTestRunDiscrepancy.DiscrepancyTypeId = inspectionTestRunDiscrepancy.DiscrepancyType.Id;
                        inspectionTestRunDiscrepancy.DiscrepancyType = null;

                        if (inspectionTestRunDiscrepancy.DiscrepancyResult != null && inspectionTestRunDiscrepancy.DiscrepancyResult.Id > 0) inspectionTestRunDiscrepancy.DiscrepancyResultId = inspectionTestRunDiscrepancy.DiscrepancyResult.Id;
                        inspectionTestRunDiscrepancy.DiscrepancyResult = null;

                        if (inspectionTestRunDiscrepancy.DiscrepancyState != null && inspectionTestRunDiscrepancy.DiscrepancyState.Id > 0) inspectionTestRunDiscrepancy.DiscrepancyStateId = inspectionTestRunDiscrepancy.DiscrepancyState.Id;
                        inspectionTestRunDiscrepancy.DiscrepancyState = null;

                        if (inspectionTestRunDiscrepancy.DiscrepancyRisk != null && inspectionTestRunDiscrepancy.DiscrepancyRisk.Id > 0) inspectionTestRunDiscrepancy.DiscrepancyRiskId = inspectionTestRunDiscrepancy.DiscrepancyRisk.Id;
                        inspectionTestRunDiscrepancy.DiscrepancyRisk = null;

                        inspectionTestRunDiscrepancy.Evidence = inspectionTestRunDiscrepancy.Evidence != null ? ProcessMetadataImage(inspectionTestRunDiscrepancy.Evidence, path) : inspectionTestRunDiscrepancy.Evidence;

                        if (inspectionTestRunDiscrepancy.Id == Guid.Empty) repoInspectionTestRunDiscrepancy.Insert(inspectionTestRunDiscrepancy);
                        else repoInspectionTestRunDiscrepancy.Update(inspectionTestRunDiscrepancy);
                    });
                });

                #endregion

                UOW.CommitTransaction();
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
                response = null;
                UOW.RollbackTransaction();
            }

            if(response.InspectionTools != null) response.InspectionTools.ToList().ForEach(it => {
                it.WorkOrder = null;
                if(it.InspectionComponents != null) it.InspectionComponents.ToList().ForEach(ic => {
                    ic.InspectionTool = null;
                    if(ic.InspectionComponentDiscrepancies != null) ic.InspectionComponentDiscrepancies.ToList().ForEach(icd => {
                        icd.InspectionComponent = null;
                    });
                });
                if (it.InspectionToolDiscrepancies != null) it.InspectionToolDiscrepancies.ToList().ForEach(ic => {
                    ic.InspectionTool = null;
                });
                if(it.InspectionSubAssemblyImages != null) it.InspectionSubAssemblyImages.ToList().ForEach(ic => {
                    ic.InspectionTool = null;
                });
                if (it.InspectionTestRunImages != null) it.InspectionTestRunImages.ToList().ForEach(ic => {
                    ic.InspectionTool = null;
                });
                if (it.InspectionTestRunDiscrepancies != null) it.InspectionTestRunDiscrepancies.ToList().ForEach(ic => {
                    ic.InspectionTool = null;
                });
            });
            if (response.InspectionImages != null) response.InspectionImages.ToList().ForEach(ii => {
                ii.WorkOrder = null;
            });

            /*return repo.FixEntity(response, new List<string> {
                "InspectionTools",
                "InspectionTools.InspectionComponents",
                "InspectionTools.InspectionComponents.InspectionComponentDiscrepancies",
                "InspectionTools.InspectionToolDiscrepancies",
                "InspectionTools.InspectionSubAssemblyImages",
                "InspectionTools.InspectionTestRunImages",
                "InspectionTools.InspectionTestRunDiscrepancies",
                "InspectionImages"
            });*/

            return response;
        }

        public bool DeleteWorkOrder(int id)
        {
            var result = true;

            try
            {
                var repo = UOW.Repository<WorkOrder>();

                var tool = repo.GetById(id);

                if (tool != null)
                {
                    tool.Active = false;
                    repo.Update(tool);
                }
                else throw new Exception($"The WorkOrder with id {id} doesn't exist");
            }
            catch (Exception ex)
            {
                result = false;
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
            }

            return result;
        }

        #endregion

        #region Private Methods

        private string ProcessMetadataImage(string image, string path) {
            var file = JsonConvert.DeserializeObject<ContentFile>(image);
            file.Path = path;
            file.State = "Stored";
            return JsonConvert.SerializeObject(file);
        }

        #endregion
    }
}
