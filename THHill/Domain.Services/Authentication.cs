﻿using Infrastructure.Core.UnityOfWork;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Transversal.Common.Entities.Api;
using Transversal.Common.Entities.DataBase.Authentication;
using Transversal.Common.Entities.DataBase.Core;
using Transversal.Common.Entities.Models;
using Transversal.Common.Utilities;

namespace Domain.Services
{
    public class Authentication : BaseService
    {
        #region Constructor

        public Authentication(IUnityOfWork unityOfWork) : base(unityOfWork) { }

        public Authentication(IUnityOfWork unityOfWork, bool serialize) : base(unityOfWork, serialize) { }

        #endregion

        #region Authentication

        public string ValidateAuthToken(string token)
        {
            var result = string.Empty;

            try
            {
                var repoUser = UOW.Repository<User>();

                var authTokenValidityPeriod = UOW.Repository<Parameter>().GetByFilter(t => t.Code == "AuthTokenValidityPeriod").FirstOrDefault().Value;

                var authToken = new Guid(token);

                var user = repoUser.GetByFilter(f => (f.AuthToken == authToken)).ToList().FirstOrDefault();

                if (user != null && user.Id != Guid.Empty && user.AuthTokenDate.Value.AddHours(Convert.ToInt32(authTokenValidityPeriod)) > DateTime.Now)
                {
                    result = $"{user.FirstName} {user.LastName};{user.Email}";
                }
            }
            catch (Exception ex)
            {
                //System.Reflection.MethodBase.GetCurrentMethod()
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
            }

            return result;
        }

        public ErrorContainer CreatePassword(string username, string password, string token)
        {
            ErrorContainer result = new ErrorContainer();

            try
            {
                UOW.BeginTransaction();

                try
                {
                    var authToken = new Guid(token);
                    var repoUser = UOW.Repository<User>();
                    var repoPassword = UOW.Repository<Password>();
                    
                    var authTokenValidityPeriod = UOW.Repository<Parameter>().GetByFilter(t => t.Code == "AuthTokenValidityPeriod").FirstOrDefault().Value;

                    var user = repoUser.GetByFilter(f => (f.Email == username) && (f.AuthToken == authToken) && (f.Active)).ToList().FirstOrDefault();

                    if (user != null && user.Id != Guid.Empty && user.AuthTokenDate.Value.AddHours(Convert.ToInt32(authTokenValidityPeriod)) > DateTime.Now)
                    {
                        var tempSalt = Transversal.Security.Common.GenerarPasswd(4);
                        
                        var passwordTmp = new Password
                        {
                            UserId = user.Id,
                            Salt = tempSalt,
                            Value = Transversal.Security.Common.SHA256Hash(string.Concat(tempSalt, password))
                        };

                        repoPassword.GetByFilter(f => (f.UserId == user.Id) && (f.Active == true)).ToList().ForEach(p => { p.Active = false; repoPassword.Update(p); });

                        repoPassword.Insert(passwordTmp);

                        user.AuthToken = null;
                        user.AuthTokenDate = null;

                        repoUser.Update(user);
                    }
                    else
                    {
                        result.Code = "Error01";
                    }
                }
                catch (Exception ex)
                {
                    WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
                    result.Code = "SystemError";
                }
                finally
                {
                    if (UOW.IsOnTransaction())
                    {
                        if (!result.InError) UOW.CommitTransaction();
                        else UOW.RollbackTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
                result.Code = "SystemError";
            }

            if (result.InError && string.IsNullOrEmpty(result.Code))
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), new Exception(string.Join(", ", result.Message)));
                result.Code = "SystemError";
            }

            return result;
        }

        public ErrorContainer Forgot(string username)
        {
            var repo = UOW.Repository<User>();

            var user = repo.GetByFilter(f => (f.Email == username) && (f.Active)).ToList().FirstOrDefault();
            user.AuthToken = Guid.NewGuid();
            user.AuthTokenDate = DateTime.Now;
            repo.Update(user);

            var dictionary = new Dictionary<string, string>();

            dictionary.Add("[Nombre]", $"{user.FirstName} {user.LastName}");
            dictionary.Add("[Email]", user.Email);
            var repoParameter = UOW.Repository<Parameter>();
            dictionary.Add("[Url]", repoParameter.GetByFilter(t => t.Code == "Url").FirstOrDefault().Value);
            dictionary.Add("[Token]", user.AuthToken.Value.ToString());

            return SendAuthNotification(dictionary, user.Email, "THHill - RECORDAR CONTRASEÑA", File.ReadAllText($"{BasePath}/auth/forgot.html"));
        }

        public ErrorContainer Change(User user)
        {
            var repo = UOW.Repository<User>();

            user = repo.GetByFilter(f => (f.Email == user.Email) && (f.Active)).ToList().FirstOrDefault();
            user.AuthToken = Guid.NewGuid();
            user.AuthTokenDate = DateTime.Now;
            repo.Update(user);

            var dictionary = new Dictionary<string, string>();

            dictionary.Add("[Nombre]", $"{user.FirstName} {user.LastName}");
            dictionary.Add("[Email]", user.Email);
            var repoParameter = UOW.Repository<Parameter>();
            dictionary.Add("[Url]", repoParameter.GetByFilter(t => t.Code == "Url").FirstOrDefault().Value);
            dictionary.Add("[Token]", user.AuthToken.Value.ToString());

            return SendAuthNotification(dictionary, user.Email, "THHill - CAMBIAR CONTRASEÑA", File.ReadAllText($"{BasePath}/auth/change.html"));
        }

        public TokenDB Connect(string login, string passwd, string ip)
        {
            var response = new TokenDB();

            string message = string.Empty;
            bool error = false;

            try
            {
                var repo = UOW.Repository<User>();

                //Recuperando la informacion del Usuario
                var UserList = repo.GetByFilter(t =>
                    (t.Active)
                    && (t.Email == login)
                );

                if (UserList != null && UserList.Count() == 1)
                {
                    var user = UserList.FirstOrDefault();

                    if (user != null)
                    {
                        if (user.Active)
                        {
                            //Validando sesiones activas para el usuario
                            var tokens = GetTokens(new TokenDB { UserId = user.Id });

                            if (tokens.Count == 0 || tokens.Count == 1)
                            {
                                //Validando contraseña del usuario

                                if (ValidatePassword(user, passwd))
                                {
                                    //Usuario validado correctamente. Creando o Actualizando Token
                                    if (tokens.Count == 0) response = AddToken(user.Id, ip);
                                    else
                                    {
                                        response = Refresh(tokens[0].RefreshToken, ip);
                                        message = $"Usuario {login} Reconectado.";
                                    }
                                }
                                else { error = true; message = "Usuario o Contraseña invalidos."; }
                            }
                            else { error = true; message = "Mas de una Conexion."; }
                        }
                        else { error = true; message = "Inactivo."; }
                    }
                    else { error = true; message = "Usuario o Contraseña invalidos."; }
                }
                else {
                    error = true; message = "Usuario o Contraseña invalidos.";
                    if (repo.GetAll().Count() == 0)
                    {
                        UOW.Initialize();
                    }
                }
            }
            catch (Exception ex)
            {
                UOW.Initialize();
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
                error = true; message = "Error del sistema.";
            }

            if (error)
            {
                throw new Exception(message);
            }

            return response;
        }

        public TokenDB Refresh(string refreshToken, string ipClient)
        {
            var response = new TokenDB();

            string message = string.Empty;
            bool error = false;

            try
            {
                var token = GetTokens(new TokenDB { RefreshToken = refreshToken }).FirstOrDefault();

                if (token != null && token.Id != Guid.Empty)
                {
                    UOW.BeginTransaction();

                    try
                    {
                        if (!DeleteToken(token)) { error = true; message = "No se puede eliminar token anterior."; }

                        if (!error)
                        {
                            // Type 'Transversal.Common.Entities.DataBase.Authentication.TokenDB' in Assembly 'Transversal.Common, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null' is not marked as serializable.
                            //response = (TokenDB)Objects.CloneObject(AddToken(token.UserId, ipClient));
                            response = AddToken(token.UserId, ipClient);
                            if (response != null) UOW.CommitTransaction();
                        }
                    }
                    catch (Exception ex)
                    {
                        UOW.RollbackTransaction();
                        throw ex;
                    }
                }
                else { error = true; message = "Token no existe."; }
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
                error = true; message = "Error del sistema.";
            }

            if (error)
            {
                if (UOW.IsOnTransaction()) UOW.RollbackTransaction();
                throw new Exception(message);
            }

            return response;
        }

        public bool Disconnect(string accessToken, string ipClient)
        {
            string message = string.Empty;
            bool error = false;

            try
            {
                var token = GetTokens(new TokenDB { AccessToken = accessToken, Ip = ipClient }).FirstOrDefault();

                if (token != null && token.Id != Guid.Empty)
                {
                    if (!DeleteToken(token)) { error = true; message = "No se puede eliminar token."; }
                }
                else { error = true; message = "Token no existe"; }
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
                error = true; message = "Error del sistema.";
            }

            if (error)
            {
                throw new Exception(message);
            }

            return !error;
        }

        public bool HasPermissions(string accessToken, string service, string action)
        {
            return true;
        }

        public Menu GetMenu(Guid userId)
        {
            var response = new Menu { Items = new List<Menu>() };

            try
            {
                var user = UOW.Repository<User>().GetByFilter(t => 
                    (t.Id == userId)
                    , new List<string>
                    {
                        "UserType",
                        "UserType.Permissions",
                        "UserType.Permissions.Module"
                    }
                ).FirstOrDefault();

                user.UserType.Permissions.ToList().ForEach(p => {
                    response.Items.Add(new Menu {
                        Id = p.Module.Id,
                        Order = p.Module.Order,
                        Icon = p.Module.Icon,
                        Title = p.Module.Name,
                        Items = new List<Menu>()
                    });
                });
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
            }

            return response;
        }

        public Transversal.Common.Entities.DataBase.Authentication.Module GetModule(Guid id)
        {
            var response = new Transversal.Common.Entities.DataBase.Authentication.Module();

            try
            {
                var repo = UOW.Repository<Transversal.Common.Entities.DataBase.Authentication.Module>();
                response = repo.GetById(id);
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
            }

            return response;
        }

        #region Crud methods

        public KeyValuePair<List<User>, int> GetUserssPaged(List<Filter> filters, List<KeyValuePair<string, bool>> orderBy, int pageSize, int pageNumber)
        {
            var response = new KeyValuePair<List<User>, int>(new List<User>(), 0);

            int total = 0;

            try
            {
                var repo = UOW.Repository<User>();

                var filterObj = new User();

                foreach (var filter in filters)
                {
                    Objects.SetValue(
                        filterObj,
                        filter.Member,
                        filter.Value
                    );
                }

                var res = repo.GetByFilter(
                    t => (
                        (t.Active == true)
                        && (!string.IsNullOrEmpty(filterObj.Email) ? t.Email.Contains(filterObj.Email) : true)
                        && (!string.IsNullOrEmpty(filterObj.FirstName) ? t.FirstName.Contains(filterObj.FirstName) : true)
                        && (!string.IsNullOrEmpty(filterObj.LastName) ? t.LastName.Contains(filterObj.LastName) : true)
                    ),
                    new List<string>{
                        "UserType"
                    },
                    orderBy,
                    pageSize,
                    pageNumber,
                    out total
                ).ToList();

                response = new KeyValuePair<List<User>, int>(res, total);
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
            }

            return response;
        }

        public User GetUserById(Guid userId)
        {
            var result = new User();

            try
            {
                result = UOW.Repository<User>().GetById(userId);
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
            }

            return result;
        }

        public User GetUserByEmail(string email)
        {
            var result = new User();

            try
            {
                result = UOW.Repository<User>().GetByFilter(t => t.Email == email).FirstOrDefault();
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
            }

            return result;
        }

        public User SaveUser(User user)
        {
            User result = null;

            var inError = false;

            var isOnTransaction = UOW.IsOnTransaction();

            if(!isOnTransaction) UOW.BeginTransaction();

            try
            {
                var repo = UOW.Repository<User>();

                if (repo.GetByFilter(f => (f.Active == true) && (f.Email == user.Email) && (f.Id != user.Id)).FirstOrDefault() != null) throw new Exception($"Ya existe un usuario con el email {user.Email}");

                if (user.Id == Guid.Empty)
                {
                    user.AuthToken = Guid.NewGuid();
                    user.AuthTokenDate = DateTime.Now;

                    result = repo.Insert(user);

                    var dictionary = new Dictionary<string, string>();

                    dictionary.Add("[Nombre]", $"{user.FirstName} {user.LastName}");
                    dictionary.Add("[Email]", user.Email);
                    var repoParameter = UOW.Repository<Parameter>();
                    dictionary.Add("[Url]", repoParameter.GetByFilter(t => t.Code == "Url").FirstOrDefault().Value);
                    dictionary.Add("[Token]", user.AuthToken.Value.ToString());

                    if (SendAuthNotification(dictionary, user.Email, "THHill - CREACION DE USUARIO", File.ReadAllText($"{BasePath}/auth/create.html")).InError) throw new Exception("No se pudo almacenar el usuario");
                }
                else
                {
                    repo.Update(user);
                    result = user;
                }
            }
            catch (Exception ex)
            {
                inError = true;
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
            }
            finally
            {
                if (!isOnTransaction && UOW.IsOnTransaction())
                {
                    if (!inError) UOW.CommitTransaction();
                    else UOW.RollbackTransaction();
                }
            }

            return result;
        }

        public ErrorContainer DeleteUser(int id)
        {
            ErrorContainer result = new ErrorContainer();

            UOW.BeginTransaction();

            try
            {
                var repo = UOW.Repository<User>();

                var user = repo.GetById(id);

                if (user != null)
                {
                    var repoPassword = UOW.Repository<Password>();
                    repoPassword.GetByFilter(f => f.UserId == user.Id).ToList().ForEach(p => { repoPassword.DeleteById(p.Id); });

                    user.Active = false;
                    repo.Update(user);
                }
                else throw new Exception("El usuario no existe");
            }
            catch (Exception ex)
            {
                result.Message.Add(WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex));
            }
            finally
            {
                if (UOW.IsOnTransaction())
                {
                    if (!result.InError) UOW.CommitTransaction();
                    else UOW.RollbackTransaction();
                }
            }

            return result;
        }

        public Password GetPasswordByUserId(Guid userId)
        {
            var result = new Password();

            try
            {
                result = UOW.Repository<Password>().GetByFilter(f => f.UserId == userId && f.Active).FirstOrDefault();
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
            }

            return result;
        }

        #endregion

        #endregion

        #region Private

        bool ValidatePassword(User user, string passwd)
        {
            bool result = true;

            try
            {
                var repo = UOW.Repository<Password>();

                var password = repo.GetByFilter(t =>
                    (t.Active)
                    && (t.UserId == user.Id)
                ).FirstOrDefault();

                if (passwd == null || !password.Value.Equals(Transversal.Security.Common.SHA256Hash($"{password.Salt}{passwd}")))
                {
                    throw new Exception(""); // Contraseña invalida
                }
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
                result = false;
            }

            return result;
        }

        #endregion

        #region Token

        List<TokenDB> GetTokens(TokenDB token)
        {
            var response = new List<TokenDB>();

            try
            {
                var repo = UOW.Repository<TokenDB>();

                response = repo.GetByFilter(t =>
                    (t.Active)
                    && (token.UserId != Guid.Empty ? t.UserId == token.UserId : true)
                    && (!string.IsNullOrEmpty(token.AccessToken) ? t.AccessToken == token.AccessToken : true)
                    && (!string.IsNullOrEmpty(token.RefreshToken) ? t.RefreshToken == token.RefreshToken : true)
                    && (!string.IsNullOrEmpty(token.Ip) ? t.Ip == token.Ip : true)
                ).ToList();
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
                response = null;
            }

            return response;
        }

        TokenDB AddToken(Guid accountId, string Ip)
        {
            TokenDB response = null;

            int ValidityPeriod = 5;

            try
            {
                var repo = UOW.Repository<TokenDB>();

                var coreService = new Core(UOW);

                var param = coreService.GetParametersByFilter(new Parameter { Code = "TokenValidityPeriod" }).FirstOrDefault();
                if (param != null && param.Id != Guid.Empty) ValidityPeriod = Convert.ToInt32(param.Value);

                response = repo.GetByFilter(t =>
                    (t.Active)
                    && (t.UserId == accountId)
                //&& (t.Ip == Ip)
                ).FirstOrDefault();

                if (response == null || response.Id == Guid.Empty)
                {
                    var tokenTmp = new TokenDB
                    {
                        UserId = accountId,
                        AccessToken = Guid.NewGuid().ToString(),
                        ValidityPeriod = ValidityPeriod,
                        RefreshToken = Guid.NewGuid().ToString(),
                        Ip = Ip
                    };
                    //WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), new Exception(JsonConvert.SerializeObject(tokenTmp, Formatting.None)));
                    response = repo.Insert(tokenTmp);
                }

                repo.Update(response);
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
                response = null;
            }

            return response;
        }

        bool UpdateToken(TokenDB token)
        {
            bool result = true;

            try
            {
                var repo = UOW.Repository<TokenDB>();
                repo.Update(token);
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
                result = false;
            }

            return result;
        }

        bool DeleteToken(TokenDB token)
        {
            bool result = true;

            try
            {
                var repo = UOW.Repository<TokenDB>();
                repo.DeleteById(token.Id);
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
                result = false;
            }

            return result;
        }

        #endregion
    }
}
