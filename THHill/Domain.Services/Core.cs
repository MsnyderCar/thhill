﻿using Infrastructure.Core.UnityOfWork;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Transversal.Common.Entities.Api;
using Transversal.Common.Entities.DataBase;
using Transversal.Common.Entities.DataBase.Authentication;
using Transversal.Common.Entities.DataBase.Core;
using Transversal.Common.Entities.DataBase.Mobile;
using Transversal.Common.Entities.DataBase.THHill;
using Transversal.Common.Entities.Models;
using Transversal.Common.Utilities;

namespace Domain.Services
{
    public class Core : BaseService
    {
        #region Constructor

        public Core(IUnityOfWork unityOfWork) : base(unityOfWork) { }

        public Core(IUnityOfWork unityOfWork, bool serialize) : base(unityOfWork, serialize) { }

        #endregion

        public string GetNewGuid()
        {
            return Guid.NewGuid().ToString();
        }

        public object GetNewEntity(string entity)
        {
            object response = null;

            try
            {
                if (entity.Equals("Tool"))
                {
                    response = new Tool { Components = new List<Component>() };
                }
                else if (entity.Equals("InspectionTool"))
                {
                    response = new InspectionTool { InspectionComponents = new List<InspectionComponent>() };
                }
                else
                    response = (dynamic)Activator.CreateInstance(Assembly.GetAssembly(typeof(BaseEntity)).GetTypes()
                     .Where(x => x.Name.Equals(entity))
                     .FirstOrDefault());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                response = null;
            }

            return response;
        }

        public List<Combo> GetCombo(string entity, string value)
        {
            List<Combo> response = null;

            try
            {
                switch (entity)
                {
                    #region Mobile
                    case "RecordState":
                        response = UOW.Repository<RecordState>().GetAll().Select(s => new Combo { Id = s.Id, Value = s.Name }).ToList();
                        break;
                    case "DataType":
                        response = UOW.Repository<DataType>().GetAll().Select(s => new Combo { Id = s.Id, Value = $"{s.Name},{s.NameSpace}" }).ToList();
                        break;
                    #endregion
                    #region Core
                    case "ParameterType":
                        response = UOW.Repository<ParameterType>().GetAll().Select(s => new Combo { Id = s.Id, Value = s.Name }).ToList();
                        break;
                    #endregion
                    #region Authentication
                    case "UserType":
                        response = UOW.Repository<UserType>().GetAll().Select(s => new Combo { Id = s.Id, Value = s.Name }).ToList();
                        break;
                    case "User":
                        response = UOW.Repository<User>().GetByFilter(f => (f.Active == true)).Select(s => new Combo { Id = s.Id, Value = $"{s.Email} - {s.FirstName} {s.LastName}" }).ToList();
                        break;
                    #endregion
                    #region THHill
                    case "ToolType":
                        response = UOW.Repository<ToolType>().GetAll().Select(s => new Combo { Id = s.Id, Value = s.Name }).ToList();
                        break;
                    case "Tool":
                        response = UOW.Repository<Tool>().GetByFilter(f => (f.ToolTypeId == Convert.ToInt32(value))).Select(s => new Combo { Id = s.Id, Value = s.Name }).ToList();
                        break;
                    case "Operator":
                        response = UOW.Repository<Operator>().GetByFilter(f => (f.Active == true)).Select(s => new Combo { Id = s.Id, Value = s.Name }).ToList();
                        break;
                    case "OilWell":
                        response = UOW.Repository<OilWell>().GetByFilter(f => (f.Active == true)).Select(s => new Combo { Id = s.Id, Value = s.Name }).ToList();
                        break;
                    case "MonitoringType":
                        response = UOW.Repository<MonitoringType>().GetByFilter(f => (f.Active == true)).Select(s => new Combo { Id = s.Id, Value = s.Name }).ToList();
                        break;
                    case "InspectionCategory":
                        response = UOW.Repository<InspectionCategory>().GetByFilter(f => (f.Active == true)).Select(s => new Combo { Id = s.Id, Value = s.Name }).ToList();
                        break;
                    case "WorkOrderState":
                        response = UOW.Repository<WorkOrderState>().GetAll().Select(s => new Combo { Id = s.Id, Value = s.Name }).ToList();
                        break;
                    case "Condition":
                        response = UOW.Repository<Condition>().GetAll().Select(s => new Combo { Id = s.Id, Value = s.Name }).ToList();
                        break;
                    case "Vendor":
                        response = UOW.Repository<Vendor>().GetAll().Select(s => new Combo { Id = s.Id, Value = s.Name }).ToList();
                        break;
                    case "DiscrepancyType":
                        response = UOW.Repository<DiscrepancyType>().GetAll().Select(s => new Combo { Id = s.Id, Value = s.Name }).ToList();
                        break;
                    case "DiscrepancyResult":
                        response = UOW.Repository<DiscrepancyResult>().GetAll().Select(s => new Combo { Id = s.Id, Value = s.Name }).ToList();
                        break;
                    case "DiscrepancyState":
                        response = UOW.Repository<DiscrepancyState>().GetAll().Select(s => new Combo { Id = s.Id, Value = s.Name }).ToList();
                        break;
                    case "DiscrepancyRisk":
                        response = UOW.Repository<DiscrepancyRisk>().GetAll().Select(s => new Combo { Id = s.Id, Value = s.Name }).ToList();
                        break;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                response = null;
            }

            return response.OrderBy(o => o.Value).ToList();
        }

        #region Parameter

        public List<Parameter> GetParametersByFilter(Parameter filter)
        {
            List<Parameter> response = null;

            try
            {
                var repo = UOW.Repository<Parameter>();
                response = repo.GetByFilter(f =>
                    (!string.IsNullOrEmpty(filter.Code) ? f.Code == filter.Code : true)
                    && (!string.IsNullOrEmpty(filter.Categories) ? f.Categories.Contains(filter.Categories) : true)
                ).ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                response = null;
            }

            return response;
        }

        public KeyValuePair<List<Parameter>, int> GetParametersPaged(List<Filter> filters, List<KeyValuePair<string, bool>> orderBy, int pageSize, int pageNumber)
        {
            var response = new KeyValuePair<List<Parameter>, int>(new List<Parameter>(), 0);

            int total = 0;

            try
            {
                var repo = UOW.Repository<Parameter>();

                var filterObj = new Parameter();

                foreach (var filter in filters)
                {
                    Objects.SetValue(
                        filterObj,
                        filter.Member,
                        filter.Value
                    );
                }

                var res = repo.GetByFilter(
                    t => (
                        (!string.IsNullOrEmpty(filterObj.Code) ? t.Description.Contains(filterObj.Code) : true)
                        && (!string.IsNullOrEmpty(filterObj.Description) ? t.Description.Contains(filterObj.Description) : true)
                    ),
                    new List<string>{
                        "ParameterType"
                    },
                    orderBy,
                    pageSize,
                    pageNumber,
                    out total
                ).ToList();

                response = new KeyValuePair<List<Parameter>, int>(res, total);
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
            }

            return response;
        }

        public Parameter GetParameterById(Guid id)
        {
            var response = new Parameter();

            try
            {
                var repo = UOW.Repository<Parameter>();

                response = repo.GetByFilter(
                    t => (
                        id != Guid.Empty ? t.Id == id : true
                    ),
                    new List<string>{
                        "ParameterType"
                    }
                ).ToList().FirstOrDefault();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                response = null;
            }

            return response;
        }

        public Parameter SaveParameter(Parameter parameter)
        {
            Parameter response = null;

            try
            {
                var repo = UOW.Repository<Parameter>();

                if (parameter.Id == Guid.Empty)
                    response = repo.Insert(parameter);
                else
                {
                    repo.Update(parameter);
                    response = parameter;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                response = null;
            }

            return response;
        }

        #endregion

        #region Transfer Files

        public Guid UploadFilePart(StreamFile streamFile)
        {
            var response = Guid.Empty;

            try
            {
                //validatePermissions(ControllerAction.Delete); 

                var tempId = new Guid(Path.GetFileNameWithoutExtension(streamFile.StoredFileName));

                var directory = "wwwroot/repository/core/";
                
                if (streamFile.Part == 0 && Directory.GetFiles(directory, $"{tempId}*").Length > 0)
                {
                    do
                    {
                        tempId = Guid.NewGuid();
                    } while (Directory.GetFiles(directory, $"{tempId}*").Length > 0);
                }

                var baseFile = $"{directory}{tempId}{Path.GetExtension(streamFile.StoredFileName)}";
                var tempFile = $"{baseFile}.temp";

                var data = Convert.FromBase64String(streamFile.Data);

                if (File.Exists(tempFile) && streamFile.Part == 0)
                {
                    File.Delete(tempFile);
                }
                
                using (var streamF = new FileStream(tempFile, streamFile.Part != 0 ? FileMode.Append : FileMode.Create))
                {
                    streamF.Write(data, 0, data.Length);
                }

                if (streamFile.Part == streamFile.Parts - 1)
                {
                    File.Move(tempFile, baseFile);

                    // Transport to amazon
                    Console.WriteLine("Transport to amazon");
                }

                response = tempId;
            }
            catch (Exception ex)
            {
                response = Guid.Empty;
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
            }

            return response;
        }

        public bool PutFile(TransferFilePart tfp)
        {
            var response = false;

            try
            {
                var parameters = GetParametersByFilter(new Parameter { Categories = "Folders" });

                var rootStorageFolder = parameters.FirstOrDefault(t => t.Code == "RootStorageFolder").Value;

                var workFolder = Path.Combine(rootStorageFolder, tfp.Path);

                ExistFolder(workFolder, true);

                string tempFile = Path.Combine(workFolder, $"{tfp.FileName}.tmp");

                if (File.Exists(tempFile) && tfp.Part == 0)
                    File.Delete(tempFile);

                using (var stream = new FileStream(tempFile, tfp.Part != 0 ? FileMode.Append : FileMode.Create))
                {
                    stream.Write(Convert.FromBase64String(tfp.Data), 0, tfp.Data.Length);
                }

                if (tfp.Part == tfp.Parts - 1)
                {
                    var finalName = Path.Combine(workFolder, tfp.NewFileName);
                    if (File.Exists(finalName)) File.Delete(finalName);
                    File.Move(tempFile, finalName);
                }

                response = true;
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
            }

            return response;
        }

        public TransferFilePart GetFile(TransferFilePart tfp)
        {
            TransferFilePart response = null;

            try
            {
                var parameters = GetParametersByFilter(new Parameter { Categories = "Folders" });

                var rootStorageFolder = parameters.FirstOrDefault(t => t.Code == "RootStorageFolder").Value;

                var transferBufferSize = Convert.ToInt32(parameters.FirstOrDefault(t => t.Code == "TransferBufferSize").Value);

                var workFolder = Path.Combine(rootStorageFolder, tfp.Path);

                var FileProcess = Path.Combine(workFolder, tfp.FileName);

                if (!File.Exists(FileProcess)) throw new Exception("EL ARCHIVO SOLICITADO NO EXISTE");

                FileInfo file = new FileInfo(FileProcess); ;

                int Parts = Convert.ToInt32(Convert.ToDouble(file.Length) / Convert.ToDouble(transferBufferSize));

                Double PartsTemp = Convert.ToDouble(file.Length) / Convert.ToDouble(transferBufferSize);

                Parts = (PartsTemp - Convert.ToDouble(Parts) > 0) ? Parts + 1 : Parts;

                using (Stream source = File.OpenRead(FileProcess))
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        byte[] buffer = new byte[transferBufferSize];

                        int read = 0;

                        source.Seek(tfp.Part * transferBufferSize, SeekOrigin.Begin);

                        if ((read = source.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            ms.Write(buffer, 0, read);
                        }

                        response = new TransferFilePart
                        {
                            Part = tfp.Part,
                            Parts = Parts,
                            FileName = Path.GetFileName(FileProcess),
                            NewFileName = string.Empty,
                            Data = Convert.ToBase64String(ms.ToArray()),
                            PartLength = read,
                            Path = string.Empty
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
            }

            return response;
        }

        public bool ExistFile(TransferFilePart tfp)
        {
            var response = false;

            try
            {
                var parameters = GetParametersByFilter(new Parameter { Categories = "Folders" });

                var rootStorageFolder = parameters.FirstOrDefault(t => t.Code == "RootStorageFolder").Value;

                var workFolder = Path.Combine(rootStorageFolder, tfp.Path);

                if (File.Exists(Path.Combine(workFolder, tfp.FileName))) response = true;
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
            }

            return response;
        }

        public bool DeleteFile(TransferFilePart tfp)
        {
            var response = false;

            try
            {
                var parameters = GetParametersByFilter(new Parameter { Categories = "Folders" });

                var rootStorageFolder = parameters.FirstOrDefault(t => t.Code == "RootStorageFolder").Value;

                var workFolder = Path.Combine(rootStorageFolder, tfp.Path);

                string workFolderFile = Path.Combine(workFolder, tfp.FileName);

                if (File.Exists(workFolderFile))
                {
                    File.Delete(workFolderFile);
                    response = true;
                }
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
            }

            return response;
        }

        bool ExistFolder(string path, bool create)
        {
            var response = false;

            try
            {
                response = Directory.Exists(path);

                if (!response && create)
                {
                    var pathParts = path.Split('\\');
                    if (pathParts.Length > 1)
                    {
                        var basePath = pathParts[0];
                        foreach (var pathPart in pathParts.Skip(1))
                        {
                            basePath = Path.Combine(basePath, pathPart);
                            if (!Directory.Exists(basePath))
                            {
                                Directory.CreateDirectory(basePath);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
            }

            return response;
        }

        #region Hidden

        #endregion

        #endregion
    }
}
