﻿using Infrastructure.Core.UnityOfWork;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Transversal.Common.Entities.Api;
using Transversal.Common.Entities.DataBase.Core;
using Transversal.Common.Entities.Models;
using Transversal.Common.Utilities;

namespace Domain.Services
{
    public class BaseService
    {
        #region Constants

        protected const string BasePath = "wwwroot/repository/templates/";

        #endregion

        #region Members

        protected readonly IUnityOfWork UOW;
        protected readonly bool Serialize;

        #endregion

        #region Constructor

        public BaseService(IUnityOfWork uow)
        {
            UOW = uow;
            Serialize = true;
        }

        public BaseService(IUnityOfWork uow, bool serialize)
        {
            UOW = uow;
            Serialize = serialize;
        }

        #endregion

        #region Methods

        public Packet Execute(Packet request)
        {
            Packet response = new Packet
            {
                Service = request.Service,
                Action = request.Action
            };

            object result = ExecuteAction(request);

            if (result != null)
            {
                var assembly = string.Empty;

                if (result.GetType().Name.Contains("List") && result.GetType().GenericTypeArguments.Length == 1 && result.GetType().GenericTypeArguments[0].Name.Equals("Object"))
                {
                    foreach (var part in (List<object>)result)
                    {
                        response.RawData.Add(JsonRawData.Serialize(part, Serialize));
                    }
                }
                else
                {
                    try
                    {
                        // Revisar cuando result sea nulo
                        response.RawData.Add(JsonRawData.Serialize(result, Serialize));
                    }
                    catch (Exception ex)
                    {
                        
                        WriteLog(MethodBase.GetCurrentMethod(),ex);
                    }
                }
            }

            return response;
        }

        #endregion

        #region Private Methods

        protected ErrorContainer SendAuthNotification(Dictionary<string, string> dictionary, string to, string subject, string template)
        {
            ErrorContainer result = new ErrorContainer();

            try
            {
                try
                {
                    foreach (var item in dictionary)
                    {
                        template = template.Replace(item.Key, item.Value);
                    }

                    var repoParameter = UOW.Repository<Parameter>();

                    var serverConfig = repoParameter.GetByFilter(t => t.Code == "ServerConfig").FirstOrDefault().Value.Split(';');

                    new EmailClient(serverConfig[0], Convert.ToInt32(serverConfig[1]), serverConfig[2].Equals("ssl"), serverConfig[3], serverConfig[4]).Send(to, subject, template);
                }
                catch (Exception ex)
                {
                    WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
                    result.Code = "SystemError";
                }
            }
            catch (Exception ex)
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), ex);
                result.Code = "SystemError";
            }

            if (result.InError && string.IsNullOrEmpty(result.Code))
            {
                WriteLog(System.Reflection.MethodBase.GetCurrentMethod(), new Exception(string.Join(", ", result.Message)));
                result.Code = "SystemError";
            }

            return result;
        }

        protected string WriteLog(MethodBase methodBase, Exception ex)
        {
            var date = DateTime.Now;
            using (StreamWriter sw = File.AppendText($"log_{date.ToString("yyyyMMdd")}.txt"))
            {
                sw.WriteLine($"{date.ToString("yyyy-MM-dd HH:mm:ss")}|{methodBase}|Exception:{ex.ToString()}");
            }
            return ex.Message;
        }

        protected object ExecuteAction(Packet packet)
        {
            MethodInfo mi = GetType().GetMethod(packet.Action);
            if (mi == null)
                throw new Exception("La acción solicitada no existe en el servicio");

            var rawdata = new List<object>();

            try
            {
                foreach (var r in packet.RawData)
                {
                    rawdata.Add(
                        r.Assembly.EndsWith("Guid") ? new Guid(r.Data.ToString()) : (
                            Serialize ? JsonRawData.Deserialize(r) : r.Data
                        )
                    );
                }
            }
            catch (Exception ex)
            {
                WriteLog(MethodBase.GetCurrentMethod(), ex);
            }

            return mi.Invoke(this, rawdata.ToArray());
        }

        protected void FilterProcessor(object filterObj, List<Filter> filters)
        {
            foreach (var filter in filters)
            {
                FilterSeekAndSet(filterObj, filter);
            }
        }

        private void FilterSeekAndSet(object filterObj, Filter filter)
        {
            var parts = filter.Member.Split(".");
            if (parts.Length == 1) Objects.SetValue(filterObj, filter.Member, filter.Value);
            else
            {
                var property = Activator.CreateInstance(Objects.GetProperty(filterObj, parts[0]).PropertyType);
                Objects.SetValue(filterObj, parts[0], property);
                filter.Member = string.Join('.', parts.Skip(1));
                FilterSeekAndSet(property, filter);
            }
        }

        #endregion
    }
}
