﻿using System;
using System.Collections.Generic;
using System.Text;
using Transversal.Common.Entities.Api;

namespace DistributedServices.Controller.Api
{
    public interface IService
    {
        bool Connect(string username, string password, string ip);
        bool Disconnect();
        object Execute(string service, string action, bool isConnected, params object[] args);
        ErrorContainer GetError(Guid executionId);
    }
}
