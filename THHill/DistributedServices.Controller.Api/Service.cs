﻿using DistributedServices.Controller.DataAccess.Mobile;
using Infrastructure.Core.Mobile.UnityOfWork;
using Infrastructure.Data.Mobile;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Common.Entities.Api;
using Transversal.Common.Entities.DataBase;
using Transversal.Common.Entities.DataBase.Authentication;
using Transversal.Common.Entities.DataBase.Mobile;
using Transversal.Common.Entities.Models;
using Transversal.Common.Utilities;
using Xamarin.Essentials;

namespace DistributedServices.Controller.Api
{
    public class Service : IService
    {
        #region Members

        GenericController GenericController { get; set; }
        private string UserName;
        private string Ip;
        public Token Token { get; private set; }
        public string WebApiBaseUrl { get; private set; }
        private bool IsMobile;

        public bool Error
        {
            get
            {
                var error = ErrorContainer.Where(e => e.ExecutionId == Guid.Empty).FirstOrDefault();
                return error.InError;
            }
        }

        public string Message
        {
            get
            {
                var error = ErrorContainer.Where(e => e.ExecutionId == Guid.Empty).FirstOrDefault();
                return string.Join(", ", error.Message);
            }
        }

        private List<ErrorContainer> ErrorContainer;

        static object IsUpdateToken = new object();

        #endregion

        #region Constructor

        public Service(string webApiBaseUrl, bool isMobile, bool isAndroid, Main main, Token token = null)
        {
            WebApiBaseUrl = webApiBaseUrl;
            IsMobile = isMobile;
            //GenericController = new GenericController(isAndroid, main);
            Token = token;
            ErrorContainer = new List<ErrorContainer>
            {
                new ErrorContainer()
            };
        }

        #endregion

        #region IService Methods

        public bool Connect(string username, string password, string ip)
        {
            var result = false;
            var isConnected = IsConnected();

            UserName = username;
            Ip = ip;

            var authRequest = new AuthRequest { Type = "login", Username = username, Password = password, Ip = ip };

            result = ProcessAuth(GenericController.Auth(authRequest));


            if (!result && isConnected)
            {
                result = ProcessAuth((AuthResponse)(new WebRequest(WebApiBaseUrl, Token != null ? Token.Access : string.Empty, IsMobile).ExecRequest("auth", authRequest, typeof(AuthResponse))));
                if (result)
                {
                    if(((List<Combo>)Execute("Core", "GetCombo", false, "Operators")).Count == 0)
                    {
                        ((List<Combo>)Execute("Core", "GetCombo", "RecordState", string.Empty)).ForEach(c => {
                            var r = (RecordState)Execute("Core", "SaveRecordState", false, new RecordState { Id = Convert.ToInt32(c.Id), Name = c.Value });
                        });

                        ((List<Combo>)Execute("Core", "GetCombo", "DataType", string.Empty)).ForEach(c => {
                            var parts = c.Value.Split(',');
                            var r = (DataType)Execute("Core", "SaveDataType", false, new DataType { Id = Convert.ToInt32(c.Id), Name = parts[0], NameSpace = parts[1] });
                        });

                        var listCombos = new List<KeyValuePair<string, string>> {
                            new KeyValuePair<string, string>("Operators", "Operator"),
                            new KeyValuePair<string, string>("OilWells", "OilWell"),
                            new KeyValuePair<string, string>("MonitoringTypes", "MonitoringType"),
                            new KeyValuePair<string, string>("InspectionCategories", "InspectionCategory"),
                            new KeyValuePair<string, string>("WorkOrderStates", "WorkOrderState"),
                            new KeyValuePair<string, string>("ToolTypes", "ToolType"),
                            new KeyValuePair<string, string>("Conditions", "Condition"),
                            new KeyValuePair<string, string>("DiscrepancyTypes", "DiscrepancyType"),
                            new KeyValuePair<string, string>("DiscrepancyResults", "DiscrepancyResult"),
                            new KeyValuePair<string, string>("DiscrepancyStates", "DiscrepancyState"),
                            new KeyValuePair<string, string>("DiscrepancyRisks", "DiscrepancyRisk")
                        };

                        listCombos.ForEach(c => {
                            var combo = (List<Combo>)Execute("Core", "GetCombo", c.Value, string.Empty);
                            var r = (ComboEntity)Execute("Core", "SaveCombo", false, new ComboEntity { Name = c.Key, Values = combo });
                        });
                    }

                    var user = (User)Execute("Authentication", "GetUserByEmail", IsConnected(), username);
                    user = (User)Execute("Authentication", "SaveUser", false, user);

                    var passwd = (Password)Execute("Authentication", "GetPasswordByUserId", IsConnected(), user.Id);
                    passwd = (Password)Execute("Authentication", "SavePassword", false, passwd);
                }
            }
            else 
            {
                result = ProcessAuth(GenericController.Auth(authRequest));
            }

            return result;
        }

        public bool Disconnect()
        {
            var result = new AuthResponse();

            var authRequest = new AuthRequest { Type = "logout", Ip = Ip };

            return ProcessAuth(
                IsConnected()
                ? (AuthResponse)(new WebRequest(WebApiBaseUrl, Token != null ? Token.Access : string.Empty, IsMobile).ExecRequest("auth", authRequest, result.GetType()))
                : GenericController.Auth(authRequest)
                );
        }

        public object Execute(string service, string action, params object[] args)
        {
            return Execute(service, action, IsConnected(), args);
        }

        public object Execute(string service, string action, bool isConnected, params object[] args)
        {
            //isConnected = IsConnected();

            object response = null;

            var error = ErrorContainer.Where(e => e.ExecutionId == Guid.Empty).FirstOrDefault();

            if (!isConnected || (Token != null && Token.Access != Guid.Empty.ToString()))
            {
                if(isConnected) UpdateToken();

                if (!isConnected || (Token != null && Token.Access != Guid.Empty.ToString()))
                {
                    var responseExecute = ExecuteInternal(isConnected, service, action, args.Select(o => new RawData { Assembly = GetAssembly(o.GetType()), Data = o }).ToList());

                    if (responseExecute.StatusCode == 200)
                    {
                        error.Message = new List<string>();

                        if (responseExecute.RawData.Count == 1)
                            response = isConnected
                                ? JsonRawData.Deserialize(responseExecute.RawData[0])
                                : responseExecute.RawData[0].Data;
                        else
                            response = isConnected 
                                ? responseExecute.RawData.Select(r => JsonRawData.Deserialize(r)).ToList()
                                : responseExecute.RawData.Select(r => r.Data).ToList();
                    }
                    else
                    {
                        error.Message = responseExecute.StatusMessage;
                    }
                }
                else
                {
                    error.Message = new List<string> { "No se pudo actualizar la conexion" };
                }
            }
            else
            {
                error.Message = new List<string> { "No esta conectado" };
            }

            return response;
        }

        public ErrorContainer GetError(Guid executionId)
        {
            ErrorContainer response = null;

            var error = ErrorContainer.Where(e => e.ExecutionId == executionId).FirstOrDefault();

            if (error != null)
            {
                response = new ErrorContainer
                {
                    ExecutionId = error.ExecutionId,
                    Message = error.Message
                };

                ErrorContainer.Remove(error);
            }

            return response;
        }

        #endregion

        #region Private Methods

        private void SyncUpParametrics()
        {
            var recordStates = (List<Combo>)Execute("Core", "GetCombo", IsConnected(), "RecordState", string.Empty);
            recordStates.ForEach(rs => {
                //var rsTemp = (RecordState)Execute("Core", "SaveRecordState", false, new RecordState { Id = Convert.ToInt32(rs.Id.ToString()), Name = rs.Value });
            });

            var userTypes = (List<Combo>)Execute("Core", "GetCombo", IsConnected(), "UserType", string.Empty);
            userTypes.ForEach(ut => {
                var utTemp = (UserType)Execute("Authentication", "SaveUserType", false, new UserType { Id = Convert.ToInt32(ut.Id.ToString()), Name = ut.Value });
            });
        }

        private bool ProcessAuth(AuthResponse responseExecute)
        {
            var response = true;

            var error = ErrorContainer.Where(e => e.ExecutionId == Guid.Empty).FirstOrDefault();

            if (responseExecute.StatusCode == 200)
            {
                if (error == null) error = new ErrorContainer();
                error.Message = new List<string>();
                Token = responseExecute.Token;
                //if (!UserService) GenericController.AccessToken = Token.Access;
            }
            else
            {
                error.Message = responseExecute.StatusMessage;
                Token = new Token();
                response = false;
            }

            return response;
        }

        private void UpdateToken()
        {
            if (Token != null && Token.Create.AddSeconds(Token.Period) < DateTime.Now)
            {
                lock (IsUpdateToken)
                {
                    if (Token != null)
                    {
                        var authRequest = new AuthRequest { Type = "refresh", RefreshToken = Token.Refresh, Ip = Ip };
                        ProcessAuth((AuthResponse)(new WebRequest(WebApiBaseUrl, Token.Access, IsMobile).ExecRequest("auth", authRequest, typeof(AuthResponse))));
                    }
                }
            }
        }

        private Packet ExecuteInternal(bool isConnected, string service, string action, List<RawData> rawdata)
        {
            var packet = new Packet { Service = service, Action = action, RawData = rawdata };
            return isConnected
                ? (Packet)(new WebRequest(WebApiBaseUrl, Token != null ? Token.Access : string.Empty, IsMobile).ExecRequest("exec", packet, typeof(Packet)))
                : GenericController.Exec(packet);
        }

        private string GetAssembly(Type target)
        {
            var assembly = string.Empty;
            if (target.Name.Contains("List") && target.GenericTypeArguments.Length == 1)
            {
                assembly = $"{target.GenericTypeArguments[0].FullName} List";
            }
            else assembly = $"{target.FullName}";

            return assembly;
        }

        public bool IsConnected()
        {
            return true;
            //return Connectivity.NetworkAccess == NetworkAccess.Internet;
        }

        #endregion
    }
}
