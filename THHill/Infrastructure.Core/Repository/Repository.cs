﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Transversal.Common.Utilities;

namespace Infrastructure.Core.Repository
{
    public class Respository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        #region Members

        private AppContext Context;

        private DbSet<TEntity> _entities;

        protected DbSet<TEntity> Entities
        {
            get
            {
                if (_entities == null)
                {
                    _entities = Context.Set<TEntity>();
                }
                return _entities;
            }
        }

        #endregion

        #region Constructor

        public Respository(AppContext context)
        {
            this.Context = context;
        }

        #endregion

        #region IRepository

        #region Select

        public TEntity GetById<TKey>(TKey id)
        {
            var entity = Entities.Find(id);
            Context.Entry(entity).State = EntityState.Detached;
            return entity;
        }

        public IQueryable<TEntity> GetAll()
        {
            return Entities.AsQueryable<TEntity>().AsNoTracking();
        }

        public IQueryable<TEntity> GetAll(List<string> includes, int internalQuery = 0)
        {
            var query = GetAll();
            foreach (var include in includes)
            {
                query = query.Include(include);
            }
            return internalQuery == 0 ? FixIncludes(query.ToList(), includes).AsQueryable() : query;
        }

        public IQueryable<TEntity> GetByFilter(Expression<Func<TEntity, bool>> filter)
        {
            return GetAll().Where(filter);
        }

        public IQueryable<TEntity> GetByFilter(Expression<Func<TEntity, bool>> filter, List<string> includes, int internalQuery = 0)
        {
            var query = GetAll(includes, 1).Where(filter);
            return internalQuery == 0 ? FixIncludes(query.ToList(), includes).AsQueryable() : query;
        }

        public IQueryable<TEntity> GetByFilter(Expression<Func<TEntity, bool>> filter, List<KeyValuePair<string, bool>> orderBy, int pageSize, int pageNumber, out int rowsCount)
        {
            var query = GetByFilter(filter);

            rowsCount = query.Count();

            return GetPaged(query, orderBy, pageSize, pageNumber);
        }

        public IQueryable<TEntity> GetByFilter(Expression<Func<TEntity, bool>> filter, List<string> includes, List<KeyValuePair<string, bool>> orderBy, int pageSize, int pageNumber, out int rowsCount)
        {
            var query = GetByFilter(filter, includes, 1);

            rowsCount = query.Count();

            return FixIncludes(GetPaged(query, orderBy, pageSize, pageNumber).ToList(), includes).AsQueryable();
        }

        public bool Exist(Expression<Func<TEntity, bool>> filter)
        {
            return Entities.Any(filter);
        }

        #endregion

        #region Insert

        public TEntity Insert(TEntity entity)
        {
            ValidateEntity(entity);
            /*var prop = Objects.GetProperty(entity, "Id");
            if (prop != null && prop.PropertyType == typeof(Guid))
                Objects.SetValue(entity, "Id", Guid.NewGuid());*/
            Entities.Add(entity);
            Context.SaveChanges();
            return entity;
        }

        #endregion

        #region Update

        public void Update(TEntity entity)
        {
            if (Objects.GetProperty(entity, "UpdatedDate") != null)
                Objects.SetValue(entity, "UpdatedDate", DateTime.Now);

            ValidateEntity(entity);
            Entities.Update(entity);
            Context.SaveChanges();
        }

        #endregion

        #region Delete

        public void Delete(TEntity entity)
        {
            ValidateEntity(entity);
            Entities.Attach(entity);
            Entities.Remove(entity);
            Context.SaveChanges();
        }

        public void DeleteById<TKey>(TKey id)
        {
            Entities.Remove(Entities.Find(id));
            Context.SaveChanges();
        }

        #endregion

        #region Another Methods

        public TEntity FixEntity(TEntity entity, List<string> includes)
        {
            var elements = new List<TEntity> { entity };

            FixIncludesRecursive(elements.Cast<object>().ToList(), includes);

            return elements.FirstOrDefault();
        }

        #endregion

        #endregion

        #region Private Methods

        private IQueryable<TEntity> GetPaged(IQueryable<TEntity> Query, List<KeyValuePair<string, bool>> orderBy, int pageSize, int pageNumber)
        {
            Expression methodCallExpression = Query.Expression;

            foreach (KeyValuePair<string, bool> order in orderBy)
            {
                ParameterExpression parameter = Expression.Parameter(Query.ElementType, "");

                MemberExpression property = null;

                if (order.Key.Contains("."))
                {
                    string[] parts = order.Key.Split('.');
                    foreach (string part in parts)
                    {
                        if (property == null) property = Expression.Property(parameter, part);
                        else property = Expression.Property(property, part);
                    }
                }
                else property = Expression.Property(parameter, order.Key);

                LambdaExpression lambda = Expression.Lambda(property, parameter);

                methodCallExpression = Expression.Call(
                    typeof(Queryable),
                    !order.Value ? "OrderBy" : "OrderByDescending",
                    new Type[] { Query.ElementType, property.Type },
                    Query.Expression,
                    Expression.Quote(lambda)
                );

            }

            Query = Query.Provider.CreateQuery<TEntity>(methodCallExpression);

            return Query.Skip(pageSize * pageNumber).Take(pageSize).Select(p => p);
        }

        private void ValidateEntity(TEntity entity)
        {
            if (entity == null)
            {
                throw new Exception("entidad");
            }
        }

        private void ValidateEntity(IEnumerable<TEntity> entities)
        {
            if (entities == null)
            {
                throw new Exception("entidad");
            }
        }

        private void FixIncludesRecursive(List<object> elements, List<string> includes)
        {
            if (elements != null && elements.Count > 0)
            {
                try
                {
                    var baseProps = Objects.GetPropertiesByType(elements.FirstOrDefault().GetType());

                    var propsInternal = new List<KeyValuePair<string, List<string>>>();

                    includes.ForEach(inc => {
                        PropertyInfo prop = null;
                        inc.Split(".").ToList().ForEach(part => {
                            prop = prop == null ? baseProps.Where(p => p.Name.Equals(part)).FirstOrDefault() : Objects.GetPropertiesByType(prop.PropertyType.Name.Contains("Collection") ? prop.PropertyType.GetTypeInfo().GenericTypeArguments[0] : prop.PropertyType).Where(p => p.Name.Equals(part)).FirstOrDefault();
                        });
                        propsInternal.Add(new KeyValuePair<string, List<string>>(inc, prop.PropertyType.Name.Contains("Collection") ? new List<string>() : Objects.GetPropertiesByType(prop.PropertyType).Where(p => !p.PropertyType.Namespace.Equals("System")).Select(p => p.Name).ToList()));
                    });

                    var propsDirect = baseProps.Where(p => !includes.Any(v => v.Equals(p.Name)) && !p.PropertyType.Namespace.Equals("System")).Select(p => p.Name).ToList();

                    foreach (var element in elements)
                    {
                        foreach (var prop in propsDirect)
                        {
                            if (element != null) Objects.SetValueBasic(element, prop, null);
                        }

                        foreach (var prop in propsInternal)
                        {
                            if (prop.Value.Count > 0)
                            {
                                object member = null;

                                var parts = prop.Key.Split(".");
                                int i = 0;
                                while (i < parts.Length)
                                {
                                    member = Objects.GetValue(member == null ? element : member, parts[i]);
                                    if (member == null) break;
                                    if (member.GetType().Name.Contains("HashSet"))
                                    {
                                        FixIncludesRecursive(
                                            ((IEnumerable)member).Cast<object>().ToList(),
                                            includes
                                                .Where(inc => inc.StartsWith($"{string.Join(".", parts.Take(i + 1))}."))
                                                .Select(inc => string.Join(".", inc.Split(".").Skip(i + 1)))
                                                .ToList()
                                            );

                                        break;
                                    }
                                    i++;
                                }

                                if (i == parts.Length)
                                {
                                    prop.Value.ForEach(value => {
                                        if (member != null && !propsInternal.Any(p => p.Key.Equals($"{prop.Key}.{value}"))) Objects.SetValueBasic(member, value, null);
                                    });
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        private List<TEntity> FixIncludes(List<TEntity> elements, List<string> includes)
        {
            try
            {
                FixIncludesRecursive(elements.Cast<object>().ToList(), includes);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return elements;
        }

        #endregion

        #region IDisposable

        public void Dispose()
        {
            if (Context != null)
                Context.Dispose();
        }

        #endregion
    }
}
