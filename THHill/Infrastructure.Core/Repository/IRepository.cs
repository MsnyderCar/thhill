﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Infrastructure.Core.Repository
{
    public interface IRepository<TEntity> : IDisposable
    {
        #region Select

        TEntity GetById<TKey>(TKey id);

        IQueryable<TEntity> GetAll();

        IQueryable<TEntity> GetAll(List<string> includes, int internalQuery = 0);

        IQueryable<TEntity> GetByFilter(Expression<Func<TEntity, bool>> filter);

        IQueryable<TEntity> GetByFilter(Expression<Func<TEntity, bool>> filter, List<string> includes, int internalQuery = 0);

        IQueryable<TEntity> GetByFilter(Expression<Func<TEntity, bool>> filter, List<KeyValuePair<string, bool>> orderBy, int pageSize, int pageNumber, out int rowsCount);

        IQueryable<TEntity> GetByFilter(Expression<Func<TEntity, bool>> filter, List<string> includes, List<KeyValuePair<string, bool>> orderBy, int pageSize, int pageNumber, out int rowsCount);

        bool Exist(Expression<Func<TEntity, bool>> filter);

        #endregion

        #region Insert

        TEntity Insert(TEntity entity);

        #endregion

        #region Update

        void Update(TEntity entity);

        #endregion

        #region Delete

        void Delete(TEntity entity);

        void DeleteById<TKey>(TKey id);

        #endregion

        #region Another Methods

        TEntity FixEntity(TEntity entity, List<string> includes);

        #endregion
    }
}
