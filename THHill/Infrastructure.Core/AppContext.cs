﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Infrastructure.Core
{
    public class AppContext : DbContext
    {
        #region Members

        protected string ConnectionString { get; set; }

        #endregion

        #region Constructor

        public AppContext()
        {
            IConfigurationRoot Configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile(path: "appsettings.json", optional: false, reloadOnChange: true).Build();
            var appsettingsConnectionString = string.Empty;

            #if DEBUG
            appsettingsConnectionString = "DefaultConnectionLocal";
            #else
            appsettingsConnectionString = "DefaultConnectionHosting";
            #endif
            ConnectionString = Configuration.GetConnectionString(appsettingsConnectionString);
        }

        public AppContext(DbContextOptions options) : base(options)
        {
            Database.AutoTransactionsEnabled = false;
        }

        public AppContext(string connectionString)
        {
            ConnectionString = connectionString;
        }

        #endregion

        #region Private Methods

        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            try
            {
                builder.UseSqlServer(ConnectionString);
                base.OnConfiguring(builder);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region Mapeado mediante Fluent para entidades

            var theList = Assembly.GetAssembly(this.GetType()).GetTypes()
                 .Where(x => x.Name.EndsWith("Mapping"))
                 .ToList();

            foreach (var item in theList)
            {
                Console.WriteLine(item.Name);
                modelBuilder.ApplyConfiguration((dynamic)Activator.CreateInstance(item));
            }

            base.OnModelCreating(modelBuilder);

            #endregion
        }

        #endregion
    }
}
