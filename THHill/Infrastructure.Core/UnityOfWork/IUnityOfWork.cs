﻿using Microsoft.EntityFrameworkCore;
using System;

using Infrastructure.Core.Repository;

namespace Infrastructure.Core.UnityOfWork
{
    public interface IUnityOfWork : IDisposable
    {
        int DismissChanges();

        void UpdateState(object entity, EntityState newState);

        void Reload(object entity);

        IRepository<TEntity> Repository<TEntity>() where TEntity : class;

        bool IsOnTransaction();

        void BeginTransaction();

        void CommitTransaction();

        void RollbackTransaction();

        AppContext GetContext();

        void Initialize();
    }
}
