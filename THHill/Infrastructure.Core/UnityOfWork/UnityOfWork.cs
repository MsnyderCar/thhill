﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

using Infrastructure.Core.Repository;

namespace Infrastructure.Core.UnityOfWork
{
    public class UnityOfWork : IUnityOfWork
    {
        #region Members

        private string NamesSpace { get; set; }

        private string DllFile { get; set; }

        private string FullPath { get; set; }

        protected AppContext Context;

        protected IDbContextTransaction Transaction = null;

        private Dictionary<Type, object> _dict = new Dictionary<Type, object>();

        bool OnTransaction;

        #endregion

        #region Constructor

        public UnityOfWork(string connectionString)
        {
            try
            {
                NamesSpace = $"Infrastructure.Data";
                DllFile = $"{NamesSpace}.dll";
                FullPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, DllFile);
                Context = (AppContext)Activator.CreateInstance(Assembly.LoadFrom(FullPath).GetType($"{Path.GetFileNameWithoutExtension(DllFile)}.Main"), new object[] { connectionString });
                OnTransaction = false;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"WorkUnity error: {ex.Message} {ex.StackTrace}");
            }
        }

        #endregion

        #region IUnityOfWork

        public virtual int DismissChanges()
        {
            int i = 0;

            try
            {
                foreach (var entry in Context.ChangeTracker.Entries())
                {
                    i++;
                    if (entry.State == EntityState.Modified)
                    {
                        entry.CurrentValues.SetValues(entry.OriginalValues);
                        entry.State = EntityState.Unchanged;
                    }
                    else if (entry.State == EntityState.Deleted)
                        entry.State = EntityState.Unchanged;
                    else if (entry.State == EntityState.Added)
                        entry.State = EntityState.Detached;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"WorkUnity error: {ex.Message} {ex.StackTrace}");
            }
            
            return i;
        }

        public void UpdateState(object entity, EntityState newState)
        {
            Context.Entry(entity).State = newState;
        }

        public void Reload(object entity)
        {
            Context.Entry(entity).Reload();
        }

        public IRepository<TEntity> Repository<TEntity>() where TEntity : class
        {
            if (!_dict.ContainsKey(typeof(TEntity)))
                RegisterRepository(new Respository<TEntity>(Context));

            return _dict[typeof(TEntity)] as IRepository<TEntity>;
        }

        public bool IsOnTransaction()
        {
            return OnTransaction;
        }

        public void BeginTransaction()
        {
            if (!OnTransaction)
            {
                Transaction = Context.Database.BeginTransaction();
                OnTransaction = true;
            }
        }

        public void CommitTransaction()
        {
            try
            {
                Context.SaveChanges();
                Transaction.Commit();
            }
            finally
            {
                OnTransaction = false;
                if (Transaction != null)
                {
                    Transaction.Dispose();
                    Transaction = null;
                }
            }
        }

        public void RollbackTransaction()
        {
            try
            {
                DismissChanges();
                Transaction.Rollback();
            }
            finally
            {
                OnTransaction = false;
                if (Transaction != null)
                {
                    Transaction.Dispose();
                    Transaction = null;
                }
            }
        }

        public AppContext GetContext()
        {
            return Context;
        }

        public void Initialize()
        {
            Activator.CreateInstance(Assembly.LoadFrom(FullPath).GetType($"{Path.GetFileNameWithoutExtension(DllFile)}.Data.DbInitializer"), new object[] { Context });
        }

        /*public int spExecutionInt(string spName, List<KeyValuePair<String, object>> parameters)
        {
            string spNameAndParameters = $"{spName} {String.Join(", ", parameters.Select(p => $"@{p.Key}").ToArray())}";
            return Database.ExecuteSqlCommand(spNameAndParameters, parameters.Select(p => p.Value).ToArray());
        }*/

        #endregion

        #region Public Methods

        public void RegisterRepository<TEntity>(IRepository<TEntity> repository) where TEntity : class
        {
            if (!_dict.ContainsKey(typeof(TEntity)))
                _dict.Add(typeof(TEntity), repository);
        }

        #endregion

        #region IDisposable

        public void Dispose()
        {
            try
            {
                if (Transaction != null) Transaction.Dispose();
                Context.Dispose();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        #endregion
    }
}
