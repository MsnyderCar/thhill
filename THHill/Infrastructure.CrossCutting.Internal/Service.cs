﻿using System;
using System.Collections.Generic;
using System.Linq;
using Transversal.Common.Entities.Api;
using Transversal.Common.Utilities;

namespace Infrastructure.CrossCutting.Internal
{
    public class Service : IService, IDisposable
    {
        #region Members

        private string UserName;
        private string Ip;
        public Token Token { get; private set; }
        public string WebApiBaseUrl { get; private set; }
        private bool IsMobile;

        public bool Error
        {
            get
            {
                var error = ErrorContainer.Where(e => e.ExecutionId == Guid.Empty).FirstOrDefault();
                return error.InError;
            }
        }

        public string Message
        {
            get
            {
                var error = ErrorContainer.Where(e => e.ExecutionId == Guid.Empty).FirstOrDefault();
                return string.Join(", ", error.Message);
            }
        }

        private List<ErrorContainer> ErrorContainer;

        static object IsUpdateToken = new object();

        #endregion

        #region Constructor

        public Service(string webApiBaseUrl, bool isMobile, Token token = null)
        {
            WebApiBaseUrl = webApiBaseUrl;
            IsMobile = isMobile;
            Token = token;
            ErrorContainer = new List<ErrorContainer>
            {
                new ErrorContainer()
            };
        }

        #endregion

        #region IService Methods

        public bool Connect(string username, string password, string ip)
        {
            UserName = username;
            Ip = ip;

            return ProcessAuth((AuthResponse)(new WebRequest(WebApiBaseUrl, Token != null ? Token.Access : string.Empty, IsMobile).ExecRequest("auth", new AuthRequest { Type = "login", Username = username, Password = password, Ip = ip }, typeof(AuthResponse))));
        }

        public bool Disconnect()
        {
            return ProcessAuth((AuthResponse)(new WebRequest(WebApiBaseUrl, Token != null ? Token.Access : string.Empty, IsMobile).ExecRequest("auth", new AuthRequest { Type = "logout", Ip = Ip }, typeof(AuthResponse))));
        }

        public object Execute(string service, string action, params object[] args)
        {
            //isConnected = IsConnected();

            object response = null;

            var error = ErrorContainer.Where(e => e.ExecutionId == Guid.Empty).FirstOrDefault();

            if (Token != null && Token.Access != Guid.Empty.ToString())
            {
                UpdateToken();

                if (Token != null && Token.Access != Guid.Empty.ToString())
                {
                    var responseExecute = ExecuteInternal(service, action, args.Select(o => new RawData { Assembly = GetAssembly(o.GetType()), Data = o }).ToList());

                    if (responseExecute.StatusCode == 200)
                    {
                        error.Message = new List<string>();

                        if (responseExecute.RawData.Count == 1)
                            response = JsonRawData.Deserialize(responseExecute.RawData[0]);
                        else
                            response = responseExecute.RawData.Select(r => JsonRawData.Deserialize(r)).ToList();
                    }
                    else
                    {
                        error.Message = responseExecute.StatusMessage;
                    }
                }
                else
                {
                    error.Message = new List<string> { "No se pudo actualizar la conexion" };
                }
            }
            else
            {
                error.Message = new List<string> { "No esta conectado" };
            }

            return response;
        }

        public ErrorContainer GetError(Guid executionId)
        {
            ErrorContainer response = null;

            var error = ErrorContainer.Where(e => e.ExecutionId == executionId).FirstOrDefault();

            if (error != null)
            {
                response = new ErrorContainer
                {
                    ExecutionId = error.ExecutionId,
                    Message = error.Message
                };

                ErrorContainer.Remove(error);
            }

            return response;
        }

        #endregion

        #region Private Methods

        private bool ProcessAuth(AuthResponse responseExecute)
        {
            var response = true;

            var error = ErrorContainer.Where(e => e.ExecutionId == Guid.Empty).FirstOrDefault();

            if (responseExecute.StatusCode == 200)
            {
                if (error == null) error = new ErrorContainer();
                error.Message = new List<string>();
                Token = responseExecute.Token;
            }
            else
            {
                error.Message = responseExecute.StatusMessage;
                Token = null;
                response = false;
            }

            return response;
        }

        private void UpdateToken()
        {
            if (Token != null && Token.Create.AddSeconds(Token.Period) < DateTime.Now)
            {
                lock (IsUpdateToken)
                {
                    if (Token != null)
                    {
                        var authRequest = new AuthRequest { Type = "refresh", RefreshToken = Token.Refresh, Ip = Ip };
                        ProcessAuth((AuthResponse)(new WebRequest(WebApiBaseUrl, Token.Access, IsMobile).ExecRequest("auth", authRequest, typeof(AuthResponse))));
                    }
                }
            }
        }

        private Packet ExecuteInternal(string service, string action, List<RawData> rawdata)
        {
            return (Packet)(new WebRequest(WebApiBaseUrl, Token != null ? Token.Access : string.Empty, IsMobile).ExecRequest("exec", new Packet { Service = service, Action = action, RawData = rawdata }, typeof(Packet)));
        }

        private string GetAssembly(Type target)
        {
            var assembly = string.Empty;
            if (target.Name.Contains("List") && target.GenericTypeArguments.Length == 1)
            {
                assembly = $"{target.GenericTypeArguments[0].FullName} List";
            }
            else assembly = $"{target.FullName}";

            return assembly;
        }

        #endregion

        #region IDisposable

        public void Dispose()
        {
            if(Token != null)
            {
                Disconnect();
            }
        }

        #endregion
    }
}
