﻿using System;
using Transversal.Common.Entities.Api;

namespace Infrastructure.CrossCutting.Internal 
{ 
    public interface IService
    {
        bool Connect(string username, string password, string ip);
        bool Disconnect();
        object Execute(string service, string action, params object[] args);
        ErrorContainer GetError(Guid executionId);
    }
}
