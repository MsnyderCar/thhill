﻿using DistributedServices.Controller.DataAccess.Mobile;
using Prism.Commands;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using THHill.Prism.Helpers;
using THHill.Prism.Models;
using Transversal.Common.Entities.DataBase.Authentication;
using Transversal.Common.Entities.DataBase.THHill;
using Transversal.Common.Entities.Models;

namespace THHill.Prism.ViewModels.WorkOrders
{
    public class MainPageViewModel : BasicViewModelBase
    {
        #region Members

        private User _user;

        private List<KeyValuePair<string, List<Combo>>> _combos;

        private ObservableCollection<WorkOrderItemViewModel> _workOrders;

        #region Encapsulation

        public User User
        {
            get => _user;
            set => SetProperty(ref _user, value);
        }

        public List<KeyValuePair<string, List<Combo>>> Combos
        {
            get => _combos;
            set => SetProperty(ref _combos, value);
        }

        public ObservableCollection<WorkOrderItemViewModel> WorkOrders
        {
            get => _workOrders;
            set => SetProperty(ref _workOrders, value);
        }

        #endregion

        #region Delegates

        public DelegateCommand AddCommand { get; set; }

        #endregion

        #endregion

        #region Constructor

        public MainPageViewModel(INavigationService navigationService, RemoteManager remote) : base(navigationService, remote)
        {
            AddCommand = new DelegateCommand(Add);
        }

        #endregion

        #region Overrides

        public async override void OnNavigatedTo(INavigationParameters parameters)
        {
            try
            {
                IsRunning = true;

                WorkOrders = new ObservableCollection<WorkOrderItemViewModel>();

                await Task.Run(() => {
                    ((List<KeyValuePair<Guid, WorkOrder>>)Remote.Execute("THHill", "GetWorkOrders", User.Id)).ForEach(wop => {
                        var wo = wop.Value;
                        if (wo.OperatorId != Guid.Empty) wo.Operator = Combos.Where(c => c.Key.Equals("Operators")).FirstOrDefault().Value.Where(o => new Guid(o.Id.ToString()) == wo.OperatorId).Select(o => new Operator { Id = new Guid(o.Id.ToString()), Name = o.Value }).FirstOrDefault();
                        if (wo.OperatorId == Guid.Empty && wo.Operator != null) wo.OperatorId = wo.Operator.Id;

                        if (wo.OilWellId != Guid.Empty) wo.OilWell = Combos.Where(c => c.Key.Equals("OilWells")).FirstOrDefault().Value.Where(o => new Guid(o.Id.ToString()) == wo.OilWellId).Select(o => new OilWell { Id = new Guid(o.Id.ToString()), Name = o.Value }).FirstOrDefault();
                        if (wo.OilWellId == Guid.Empty && wo.OilWell != null) wo.OilWellId = wo.OilWell.Id;

                        if (wo.MonitoringTypeId != Guid.Empty) wo.MonitoringType = Combos.Where(c => c.Key.Equals("MonitoringTypes")).FirstOrDefault().Value.Where(o => new Guid(o.Id.ToString()) == wo.MonitoringTypeId).Select(o => new MonitoringType { Id = new Guid(o.Id.ToString()), Name = o.Value }).FirstOrDefault();
                        if (wo.MonitoringTypeId == Guid.Empty && wo.MonitoringType != null) wo.MonitoringTypeId = wo.MonitoringType.Id;

                        if (wo.InspectionCategoryId != Guid.Empty) wo.InspectionCategory = Combos.Where(c => c.Key.Equals("InspectionCategories")).FirstOrDefault().Value.Where(o => new Guid(o.Id.ToString()) == wo.InspectionCategoryId).Select(o => new InspectionCategory { Id = new Guid(o.Id.ToString()), Name = o.Value }).FirstOrDefault();
                        if (wo.InspectionCategoryId == Guid.Empty && wo.InspectionCategory != null) wo.InspectionCategoryId = wo.InspectionCategory.Id;

                        if (wo.WorkOrderStateId > 0) wo.WorkOrderState = Combos.Where(c => c.Key.Equals("WorkOrderStates")).FirstOrDefault().Value.Where(o => Convert.ToInt32(o.Id) == wo.WorkOrderStateId).Select(o => new WorkOrderState { Id = Convert.ToInt32(o.Id), Name = o.Value }).FirstOrDefault();
                        if (wo.WorkOrderStateId == 0 && wo.WorkOrderState != null) wo.WorkOrderStateId = wo.WorkOrderState.Id;

                        WorkOrders.Add(new WorkOrderItemViewModel(NavigationService, User, Combos, wop.Key, wo));
                    });
                });
            }
            finally
            {
                IsRunning = false;
            }
        }

        public override void Initialize(INavigationParameters parameters)
        {
            User = parameters.GetValue<User>("user");
            Combos = parameters.GetValue<List<KeyValuePair<string, List<Combo>>>>("combos");
            Title = $"ÓRDENES DE TRABAJO";
        }

        #endregion

        #region Methods

        private async void Add()
        {
            await NavigationService.NavigateAsync("WorkOrders.WorkOrderPage", new NavigationParameters {
                { "user", _user },
                { "combos", Combos },
                { "dataId", Guid.Empty },
                {
                    "workOrder", new WorkOrder {
                        Id = Guid.Empty,
                        UserId = User.Id,
                        OperatorId = Guid.Empty,
                        OilWellId = Guid.Empty,
                        MonitoringTypeId = Guid.Empty,
                        InspectionCategoryId = Guid.Empty,
                        WorkOrderStateId = 1,
                        InspectionImages = new List<InspectionImage>(),
                        InspectionTools = new List<InspectionTool>()
                    }
                },
                { "mediaFiles", new List<MediaFileModel>() }
            });
            return;
        }

        #endregion
    }
}
