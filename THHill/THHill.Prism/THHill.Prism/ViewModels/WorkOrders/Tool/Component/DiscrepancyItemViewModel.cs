﻿using Newtonsoft.Json;
using Prism.Commands;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using THHill.Prism.Models;
using Transversal.Common.Entities.DataBase.THHill;
using Transversal.Common.Entities.Models;
using Xamarin.Forms;

namespace THHill.Prism.ViewModels.WorkOrders.Tool.Component
{
    public class DiscrepancyItemViewModel
    {
        #region Members

        private readonly INavigationService _navigationService;
        private List<KeyValuePair<string, List<Combo>>> Combos { get; set; }
        public Guid GuidTemp { get; set; }
        public ImageSource ImageSource { get; set; }
        public InspectionComponentDiscrepancy Original { get; set; }
        public InspectionComponentDiscrepancy Copy { get; set; }

        #endregion

        #region Delegates

        public DelegateCommand SelectCommand { get; set; }

        #endregion

        #region Constructor

        public DiscrepancyItemViewModel(INavigationService navigationService, List<KeyValuePair<string, List<Combo>>> combos, Guid guidTemp, ImageSource imageSource, InspectionComponentDiscrepancy inspectionComponentDiscrepancy)
        {
            _navigationService = navigationService;
            Combos = combos;
            GuidTemp = guidTemp;
            Original = inspectionComponentDiscrepancy;
            Copy = JsonConvert.DeserializeObject<InspectionComponentDiscrepancy>(JsonConvert.SerializeObject(inspectionComponentDiscrepancy));
            ImageSource = imageSource;
            SelectCommand = new DelegateCommand(Select);
        }

        #endregion

        #region Methods

        private async void Select()
        {
            await _navigationService.NavigateAsync("WorkOrders.Tool.Component.DiscrepancyPage", new NavigationParameters
            {
                { "combos", Combos },
                { "guidTemp", GuidTemp },
                { "original", Original },
                { "copy", Copy },
                { "imageSource", ImageSource },
                { "mediaFile",  new MediaFileModel() },
                { "option", "Editar" }
            });
        }

        #endregion
    }
}
