﻿using DistributedServices.Controller.DataAccess.Mobile;
using Newtonsoft.Json;
using Prism.Commands;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using THHill.Prism.Helpers;
using THHill.Prism.Models;
using Transversal.Common.Entities.DataBase.THHill;

namespace THHill.Prism.ViewModels.WorkOrders.Tool.Component
{
    public class DiscrepanciesPageViewModel : ComponentViewModelBase
    {
        #region Members

        private ObservableCollection<DiscrepancyItemViewModel> _discrepancies;
        
        #region Encapsulation

        public ObservableCollection<DiscrepancyItemViewModel> Discrepancies
        {
            get => _discrepancies;
            set => SetProperty(ref _discrepancies, value);
        }

        #endregion

        #region Delegates

        public DelegateCommand AddCommand { get; set; }

        #endregion

        #endregion

        #region Constructor

        public DiscrepanciesPageViewModel(INavigationService navigationService, RemoteManager remote) : base(navigationService, remote)
        {
            AddCommand = new DelegateCommand(Add);
        }

        #endregion

        #region Overrides

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            if (parameters.ContainsKey("optionDiscrepancy"))
            {
                MediaFiles = UpdateMediaFiles(parameters, MediaFiles);

                var gt = parameters.GetValue<Guid>("guidTemp");
                                
                var icevm = Discrepancies.Where(ic => ic.GuidTemp == gt).FirstOrDefault();

                if (icevm != null) Copy.InspectionComponentDiscrepancies.Remove(icevm.Original);

                if (Copy.InspectionComponentDiscrepancies == null) Copy.InspectionComponentDiscrepancies = new List<InspectionComponentDiscrepancy>();

                Copy.InspectionComponentDiscrepancies.Add(parameters.GetValue<InspectionComponentDiscrepancy>("copy"));
            }

            Discrepancies = new ObservableCollection<DiscrepancyItemViewModel>();

            if (Copy.InspectionComponentDiscrepancies != null && Copy.InspectionComponentDiscrepancies.Count > 0)
            {
                Copy.InspectionComponentDiscrepancies.ToList().ForEach(icd => {
                    if (icd.DiscrepancyTypeId > 0) icd.DiscrepancyType = Combos.Where(c => c.Key.Equals("DiscrepancyTypes")).FirstOrDefault().Value.Where(o => Convert.ToInt32(o.Id) == icd.DiscrepancyTypeId).Select(o => new DiscrepancyType { Id = Convert.ToInt32(o.Id), Name = o.Value }).FirstOrDefault();
                    if (icd.DiscrepancyTypeId == 0 && icd.DiscrepancyType != null) icd.DiscrepancyTypeId = icd.DiscrepancyType.Id;

                    if (icd.DiscrepancyResultId > 0) icd.DiscrepancyResult = Combos.Where(c => c.Key.Equals("DiscrepancyResults")).FirstOrDefault().Value.Where(o => Convert.ToInt32(o.Id) == icd.DiscrepancyResultId).Select(o => new DiscrepancyResult { Id = Convert.ToInt32(o.Id), Name = o.Value }).FirstOrDefault();
                    if (icd.DiscrepancyResultId == 0 && icd.DiscrepancyResult != null) icd.DiscrepancyResultId = icd.DiscrepancyResult.Id;

                    if (icd.DiscrepancyStateId > 0) icd.DiscrepancyState = Combos.Where(c => c.Key.Equals("DiscrepancyStates")).FirstOrDefault().Value.Where(o => Convert.ToInt32(o.Id) == icd.DiscrepancyStateId).Select(o => new DiscrepancyState { Id = Convert.ToInt32(o.Id), Name = o.Value }).FirstOrDefault();
                    if (icd.DiscrepancyStateId == 0 && icd.DiscrepancyState != null) icd.DiscrepancyStateId = icd.DiscrepancyState.Id;

                    if (icd.DiscrepancyRiskId > 0) icd.DiscrepancyRisk = Combos.Where(c => c.Key.Equals("DiscrepancyRisks")).FirstOrDefault().Value.Where(o => Convert.ToInt32(o.Id) == icd.DiscrepancyRiskId).Select(o => new DiscrepancyRisk { Id = Convert.ToInt32(o.Id), Name = o.Value }).FirstOrDefault();
                    if (icd.DiscrepancyRiskId == 0 && icd.DiscrepancyRisk != null) icd.DiscrepancyRiskId = icd.DiscrepancyRisk.Id;

                    Discrepancies.Add(new DiscrepancyItemViewModel(
                        NavigationService,
                        Combos,
                        Guid.NewGuid(),
                        GetImageSource(MediaFiles, icd.Evidence),
                        icd
                        ));
                });
            }
        }

        #endregion

        #region Methods

        private async void Add()
        {
            var elem = new InspectionComponentDiscrepancy
            {
                DiscrepancyTypeId = 0,
                DiscrepancyResultId = 0,
                DiscrepancyStateId = 0,
                DiscrepancyRiskId = 0
            };

            await NavigationService.NavigateAsync("WorkOrders.Tool.Component.DiscrepancyPage", new NavigationParameters {
                { "combos", Combos },
                { "guidTemp", Guid.NewGuid() },
                { "original", elem },
                { "copy", JsonConvert.DeserializeObject<InspectionComponentDiscrepancy>(JsonConvert.SerializeObject(elem))},
                { "mediaFile",  new MediaFileModel() },
                { "option", "Agregar" }
            });
            return;
        }

        #endregion
    }
}
