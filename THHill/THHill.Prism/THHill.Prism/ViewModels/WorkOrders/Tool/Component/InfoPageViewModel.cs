﻿using DistributedServices.Controller.DataAccess.Mobile;
using Plugin.Media.Abstractions;
using Prism.Commands;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using THHill.Prism.Helpers;
using THHill.Prism.Models;
using Transversal.Common.Entities.Models;
using Transversal.Common.Utilities;
using Xamarin.Forms;

namespace THHill.Prism.ViewModels.WorkOrders.Tool.Component
{
    public class InfoPageViewModel : ComponentViewModelBase
    {
        #region Members

        private List<Combo> _conditions;
        private Combo _selectedCondition;

        private MediaFile _mediaFile;
        private ImageSource _imageSource;
        
        #region Encapsulation

        public List<Combo> Conditions
        {
            get => _conditions;
            set => SetProperty(ref _conditions, value);
        }
        public Combo SelectedCondition
        {
            get => _selectedCondition;
            set
            {
                SetProperty(ref _selectedCondition, value);
                Copy.ConditionId = value != null && value.Id != null && value.Id.ToString().Length > 0 ? Convert.ToInt32(value.Id.ToString()) : 0;
            }
        }
        public MediaFile MediaFile
        {
            get => _mediaFile;
            set => SetProperty(ref _mediaFile, value);
        }
        public ImageSource ImageSource
        {
            get => _imageSource != null ? _imageSource : "BotonFoto.png";
            set => SetProperty(ref _imageSource, value);
        }

        #endregion

        #region Delegates

        public DelegateCommand ChangeImageCommand { get; set; }

        #endregion

        #endregion

        #region Constructor

        public InfoPageViewModel(INavigationService navigationService, RemoteManager remote) : base(navigationService, remote)
        {
            ChangeImageCommand = new DelegateCommand(ChangeImage);

            MediaFiles = new List<MediaFileModel>();
        }

        #endregion

        #region Overrides

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            if (parameters.ContainsKey("option") && parameters.GetValue<string>("option").Equals("Editar")) ImageSource = parameters.GetValue<ImageSource>("imageSource");

            if (Conditions == null)
            {
                Conditions = new List<Combo>(Combos.Where(c => c.Key.Equals("Conditions")).FirstOrDefault().Value);
                if(Copy.ConditionId > 0) SelectedCondition = Conditions.Where(o => Convert.ToInt32(o.Id) == Copy.ConditionId).FirstOrDefault();
            }
        }

        #endregion

        #region Methods

        private async void ChangeImage()
        {
            var cim = new ChangeImageModel
            {
                MediaFileModel = MediaFileModel,
                MediaFile = MediaFile,
                ImageSource = ImageSource,
                Image = string.Empty
            };

            cim = await ChangeImageEvent(cim);

            MediaFileModel = cim.MediaFileModel;
            MediaFile = cim.MediaFile;
            ImageSource = cim.ImageSource;
            Objects.SetValueBasic(Copy, "Image", cim.Image);
        }

        #endregion
    }
}
