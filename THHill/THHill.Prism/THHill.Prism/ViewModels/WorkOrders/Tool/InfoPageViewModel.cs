﻿using DistributedServices.Controller.DataAccess.Mobile;
using Plugin.Media.Abstractions;
using Prism.Commands;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using THHill.Prism.Helpers;
using THHill.Prism.Models;
using Transversal.Common.Entities.Models;
using Transversal.Common.Utilities;
using Xamarin.Forms;

namespace THHill.Prism.ViewModels.WorkOrders.Tool
{
    public class InfoPageViewModel : ToolViewModelBase
    {
        #region Members

        private List<Combo> _toolTypes;
        private Combo _selectedToolType;

        private MediaFile _mediaFile;
        private ImageSource _imageSource;
        
        #region Encapsulation

        public List<Combo> ToolTypes
        {
            get => _toolTypes;
            set => SetProperty(ref _toolTypes, value);
        }
        public Combo SelectedToolType
        {
            get => _selectedToolType;
            set
            {
                SetProperty(ref _selectedToolType, value);
                Copy.ToolTypeId = value != null && value.Id != null && value.Id.ToString().Length > 0 ? Convert.ToInt32(value.Id.ToString()) : 0;
            }
        }
        public MediaFile MediaFile
        {
            get => _mediaFile;
            set => SetProperty(ref _mediaFile, value);
        }
        public ImageSource ImageSource
        {
            get => _imageSource != null ? _imageSource : "BotonFoto.png";
            set => SetProperty(ref _imageSource, value);
        }
        
        #endregion

        #region Delegates

        public DelegateCommand ChangeImageCommand { get; set; }
        public DelegateCommand GoToAssemblyCommand { get; set; }
        public DelegateCommand GoToTestRunCommand { get; set; }


        #endregion

        #endregion

        #region Constructor

        public InfoPageViewModel(INavigationService navigationService, RemoteManager remote) : base(navigationService, remote)
        {
            ChangeImageCommand = new DelegateCommand(ChangeImage);
            GoToAssemblyCommand = new DelegateCommand(GoToAssembly);
            GoToTestRunCommand = new DelegateCommand(GoToTestRun);

            MediaFiles = new List<MediaFileModel>();
        }

        #endregion

        #region Overrides

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            if (parameters.ContainsKey("option") && parameters.GetValue<string>("option").Equals("Editar")) ImageSource = parameters.GetValue<ImageSource>("imageSource");

            if (Copy.SubAssemblyConditionId > 0) Copy.SubAssemblyCondition = Combos.Where(c => c.Key.Equals("Conditions")).FirstOrDefault().Value.Where(o => Convert.ToInt32(o.Id) == Copy.SubAssemblyConditionId).Select(o => new Transversal.Common.Entities.DataBase.THHill.Condition { Id = Convert.ToInt32(o.Id), Name = o.Value }).FirstOrDefault();
            if (Copy.SubAssemblyConditionId == 0 && Copy.SubAssemblyCondition != null) Copy.SubAssemblyConditionId = Copy.SubAssemblyCondition.Id;

            if (ToolTypes == null)
            {
                ToolTypes = new List<Combo>(Combos.Where(c => c.Key.Equals("ToolTypes")).FirstOrDefault().Value);
                if (Copy.ToolTypeId > 0) SelectedToolType = ToolTypes.Where(o => Convert.ToInt32(o.Id) == Copy.ToolTypeId).FirstOrDefault();
            }
        }

        #endregion

        #region Methods

        private async void ChangeImage()
        {
            var cim = new ChangeImageModel {
                MediaFileModel = MediaFileModel,
                MediaFile = MediaFile,
                ImageSource = ImageSource,
                Image = Copy.Image
            };

            cim = await ChangeImageEvent(cim);

            MediaFileModel = cim.MediaFileModel;
            MediaFile = cim.MediaFile;
            ImageSource = cim.ImageSource;
            Objects.SetValueBasic(Copy, "Image", cim.Image);
        }


        private async void GoToAssembly()
        {
            await NavigationService.NavigateAsync("WorkOrders.Tool.Assembly.MainPage", new NavigationParameters {
                    { "combos", _combos },
                    { "inspectionTool", _copy },
                    { "mediaFiles", _mediaFiles }
                });

            return;
        }

        private async void GoToTestRun()
        {
            await NavigationService.NavigateAsync("WorkOrders.Tool.TestRun.MainPage", new NavigationParameters {
                    { "combos", _combos },
                    { "inspectionTool", _copy },
                    { "mediaFiles", _mediaFiles }
                });

            return;
        }

        #endregion
    }
}
