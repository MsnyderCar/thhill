﻿using DistributedServices.Controller.DataAccess.Mobile;
using Newtonsoft.Json;
using Prism.Commands;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using THHill.Prism.Helpers;
using THHill.Prism.Models;
using Transversal.Common.Entities.DataBase.THHill;

namespace THHill.Prism.ViewModels.WorkOrders.Tool
{
    public class ComponentsPageViewModel : ToolViewModelBase
    {
        #region Members

        private ObservableCollection<ComponentItemViewModel> _inspectionComponents;
        
        #region Encapsulation

        public ObservableCollection<ComponentItemViewModel> InspectionComponents
        {
            get => _inspectionComponents;
            set => SetProperty(ref _inspectionComponents, value);
        }

        #endregion

        #region Delegates

        public DelegateCommand AddCommand { get; set; }

        #endregion

        #endregion

        #region Constructor

        public ComponentsPageViewModel(INavigationService navigationService, RemoteManager remote) : base(navigationService, remote)
        {
            AddCommand = new DelegateCommand(Add);
        }

        #endregion

        #region Overrides

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            if (parameters.ContainsKey("optionComponent"))
            {
                MediaFiles = UpdateMediaFiles(parameters, MediaFiles);

                var gt = parameters.GetValue<Guid>("guidTemp");

                var icevm = InspectionComponents.Where(ic => ic.GuidTemp == gt).FirstOrDefault();

                if (icevm != null) Copy.InspectionComponents.Remove(icevm.Original);

                if (Copy.InspectionComponents == null) Copy.InspectionComponents = new List<InspectionComponent>();

                Copy.InspectionComponents.Add(parameters.GetValue<InspectionComponent>("copy"));
            }

            InspectionComponents = new ObservableCollection<ComponentItemViewModel>();

            if (Copy.InspectionComponents != null && Copy.InspectionComponents.Count > 0)
            {
                Copy.InspectionComponents.ToList().ForEach(ic => {
                    if (ic.ConditionId > 0) ic.Condition = Combos.Where(c => c.Key.Equals("Conditions")).FirstOrDefault().Value.Where(o => Convert.ToInt32(o.Id) == ic.ConditionId).Select(o => new Transversal.Common.Entities.DataBase.THHill.Condition { Id = Convert.ToInt32(o.Id), Name = o.Value }).FirstOrDefault();
                    if (ic.ConditionId == 0 && ic.Condition != null) ic.ConditionId = ic.Condition.Id;

                    InspectionComponents.Add(new ComponentItemViewModel(
                        NavigationService,
                        Combos,
                        Guid.NewGuid(),
                        new List<MediaFileModel>(MediaFiles),
                        GetImageSource(MediaFiles, ic.Image),
                        ic
                        ));
                });
            }
        }

        #endregion

        #region Methods

        private async void Add()
        {
            var elem = new InspectionComponent
            {
                ConditionId = 0,
                InspectionComponentDiscrepancies = new List<InspectionComponentDiscrepancy>()
            };

            await NavigationService.NavigateAsync("WorkOrders.Tool.Component.MainPage", new NavigationParameters {
                { "combos", Combos },
                { "guidTemp", Guid.NewGuid() },
                { "original", elem },
                { "copy", JsonConvert.DeserializeObject<InspectionComponent>(JsonConvert.SerializeObject(elem))},
                { "mediaFile",  new MediaFileModel() },
                { "mediaFiles", new List<MediaFileModel>(MediaFiles) },
                { "option", "Agregar" }
            });
            return;
        }

        #endregion
    }
}
