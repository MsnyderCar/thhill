﻿using DistributedServices.Controller.DataAccess.Mobile;
using Plugin.Media.Abstractions;
using Prism.Commands;
using Prism.Navigation;
using System;
using THHill.Prism.Helpers;
using THHill.Prism.Models;
using Transversal.Common.Entities.DataBase.THHill;
using Transversal.Common.Utilities;
using Xamarin.Forms;

namespace THHill.Prism.ViewModels.WorkOrders.Tool.TestRun
{
    public class ImagePageViewModel : ViewModelBase
    {
        #region Members

        private Guid _guidTemp;
        private InspectionTestRunImage _original;
        private InspectionTestRunImage _copy;
        private string _option;

        private MediaFile _mediaFile;
        private ImageSource _imageSource;
        protected MediaFileModel _mediaFileModel;

        #region Encapsulation

        public Guid GuidTemp
        {
            get => _guidTemp;
            set => SetProperty(ref _guidTemp, value);
        }

        public InspectionTestRunImage Original
        {
            get => _original;
            set => SetProperty(ref _original, value);
        }
        public InspectionTestRunImage Copy
        {
            get => _copy;
            set => SetProperty(ref _copy, value);
        }

        public MediaFile MediaFile
        {
            get => _mediaFile;
            set => SetProperty(ref _mediaFile, value);
        }
        public ImageSource ImageSource
        {
            get => _imageSource != null ? _imageSource : "BotonFoto.png";
            set => SetProperty(ref _imageSource, value);
        }
        protected MediaFileModel MediaFileModel
        {
            get => _mediaFileModel;
            set => SetProperty(ref _mediaFileModel, value);
        }

        public string Option
        {
            get => _option;
            set => SetProperty(ref _option, value);
        }

        #endregion

        #region Delegates

        public DelegateCommand ChangeImageCommand { get; set; }
        public DelegateCommand SaveCommand { get; set; }
        
        #endregion

        #endregion

        #region Constructor

        public ImagePageViewModel(INavigationService navigationService, RemoteManager remote) : base(navigationService, remote)
        {
            ChangeImageCommand = new DelegateCommand(ChangeImage);
            SaveCommand = new DelegateCommand(Save);
        }

        #endregion

        #region Overrides

        public override void Initialize(INavigationParameters parameters)
        {
            ImageSource = parameters.GetValue<ImageSource>("imageSource");

            GuidTemp = parameters.GetValue<Guid>("guidTemp");
            Original = parameters.GetValue<InspectionTestRunImage>("original");
            Copy = parameters.GetValue<InspectionTestRunImage>("copy");
            MediaFileModel = parameters.GetValue<MediaFileModel>("mediaFile");
            Option = parameters.GetValue<string>("option");
            Title = $"{Option} Imagen";
        }

        #endregion

        #region Methods

        private async void ChangeImage()
        {
            var cim = new ChangeImageModel
            {
                MediaFileModel = MediaFileModel,
                MediaFile = MediaFile,
                ImageSource = ImageSource,
                Image = string.Empty
            };

            cim = await ChangeImageEvent(cim);

            MediaFileModel = cim.MediaFileModel;
            MediaFile = cim.MediaFile;
            ImageSource = cim.ImageSource;
            Objects.SetValueBasic(Copy, "Image", cim.Image);
        }

        private async void Save()
        {
            await NavigationService.GoBackAsync(new NavigationParameters {
                { "guidTemp", GuidTemp },
                { "original", Original },
                { "copy", Copy },
                { "optionImage", Option },
                { "mediaFile", MediaFileModel }
            });
            return;
        }

        #endregion
    }
}
