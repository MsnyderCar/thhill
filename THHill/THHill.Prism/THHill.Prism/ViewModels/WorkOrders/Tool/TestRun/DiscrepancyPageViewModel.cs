﻿using DistributedServices.Controller.DataAccess.Mobile;
using Plugin.Media.Abstractions;
using Prism.Commands;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using THHill.Prism.Helpers;
using THHill.Prism.Models;
using Transversal.Common.Entities.DataBase.THHill;
using Transversal.Common.Entities.Models;
using Transversal.Common.Utilities;
using Xamarin.Forms;

namespace THHill.Prism.ViewModels.WorkOrders.Tool.TestRun
{
    public class DiscrepancyPageViewModel : ViewModelBase
    {
        #region Members

        private List<KeyValuePair<string, List<Combo>>> _combos;
        private Guid _guidTemp;
        private InspectionTestRunDiscrepancy _original;
        private InspectionTestRunDiscrepancy _copy;
        private string _option;

        private List<Combo> _discrepancyTypes;
        private Combo _selectedDiscrepancyType;

        private List<Combo> _discrepancyResults;
        private Combo _selectedDiscrepancyResult;

        private List<Combo> _discrepancyStates;
        private Combo _selectedDiscrepancyState;

        private List<Combo> _discrepancyRisks;
        private Combo _selectedDiscrepancyRisk;

        private MediaFile _mediaFile;
        private ImageSource _imageSource;
        protected MediaFileModel _mediaFileModel;

        #region Encapsulation

        public List<KeyValuePair<string, List<Combo>>> Combos
        {
            get => _combos;
            set => SetProperty(ref _combos, value);
        }
        public Guid GuidTemp
        {
            get => _guidTemp;
            set => SetProperty(ref _guidTemp, value);
        }
        public InspectionTestRunDiscrepancy Original
        {
            get => _original;
            set => SetProperty(ref _original, value);
        }
        public InspectionTestRunDiscrepancy Copy
        {
            get => _copy;
            set => SetProperty(ref _copy, value);
        }
        public string Option
        {
            get => _option;
            set => SetProperty(ref _option, value);
        }

        public List<Combo> DiscrepancyTypes
        {
            get => _discrepancyTypes;
            set => SetProperty(ref _discrepancyTypes, value);
        }
        public Combo SelectedDiscrepancyType
        {
            get => _selectedDiscrepancyType;
            set
            {
                SetProperty(ref _selectedDiscrepancyType, value);
                Copy.DiscrepancyTypeId = value != null && value.Id != null && value.Id.ToString().Length > 0 ? Convert.ToInt32(value.Id.ToString()) : 0;
            }
        }

        public List<Combo> DiscrepancyResults
        {
            get => _discrepancyResults;
            set => SetProperty(ref _discrepancyResults, value);
        }
        public Combo SelectedDiscrepancyResult
        {
            get => _selectedDiscrepancyResult;
            set
            {
                SetProperty(ref _selectedDiscrepancyResult, value);
                Copy.DiscrepancyResultId = value != null && value.Id != null && value.Id.ToString().Length > 0 ? Convert.ToInt32(value.Id.ToString()) : 0;
            }
        }

        public List<Combo> DiscrepancyStates
        {
            get => _discrepancyStates;
            set => SetProperty(ref _discrepancyStates, value);
        }
        public Combo SelectedDiscrepancyState
        {
            get => _selectedDiscrepancyState;
            set
            {
                SetProperty(ref _selectedDiscrepancyState, value);
                Copy.DiscrepancyStateId = value != null && value.Id != null && value.Id.ToString().Length > 0 ? Convert.ToInt32(value.Id.ToString()) : 0;
            }
        }

        public List<Combo> DiscrepancyRisks
        {
            get => _discrepancyRisks;
            set => SetProperty(ref _discrepancyRisks, value);
        }
        public Combo SelectedDiscrepancyRisk
        {
            get => _selectedDiscrepancyRisk;
            set
            {
                SetProperty(ref _selectedDiscrepancyRisk, value);
                Copy.DiscrepancyRiskId = value != null && value.Id != null && value.Id.ToString().Length > 0 ? Convert.ToInt32(value.Id.ToString()) : 0;
            }
        }

        public MediaFile MediaFile
        {
            get => _mediaFile;
            set => SetProperty(ref _mediaFile, value);
        }
        public ImageSource ImageSource
        {
            get => _imageSource != null ? _imageSource : "BotonFoto.png";
            set => SetProperty(ref _imageSource, value);
        }
        protected MediaFileModel MediaFileModel
        {
            get => _mediaFileModel;
            set => SetProperty(ref _mediaFileModel, value);
        }

        #endregion

        #region Delegates

        public DelegateCommand ChangeImageCommand { get; set; }
        public DelegateCommand SaveCommand { get; set; }
        
        #endregion

        #endregion

        #region Constructor

        public DiscrepancyPageViewModel(INavigationService navigationService, RemoteManager remote) : base(navigationService, remote)
        {
            ChangeImageCommand = new DelegateCommand(ChangeImage);
            SaveCommand = new DelegateCommand(Save);
        }

        #endregion

        #region Overrides

        public override void Initialize(INavigationParameters parameters)
        {
            ImageSource = parameters.GetValue<ImageSource>("imageSource");

            Combos = parameters.GetValue<List<KeyValuePair<string, List<Combo>>>>("combos");
            GuidTemp = parameters.GetValue<Guid>("guidTemp");
            Original = parameters.GetValue<InspectionTestRunDiscrepancy>("original");
            Copy = parameters.GetValue<InspectionTestRunDiscrepancy>("copy");
            MediaFileModel = parameters.GetValue<MediaFileModel>("mediaFile");
            Option = parameters.GetValue<string>("option");

            Title = $"{Option} Discrepancia";

            DiscrepancyTypes = new List<Combo>(Combos.Where(c => c.Key.Equals("DiscrepancyTypes")).FirstOrDefault().Value);
            if (Copy.DiscrepancyTypeId > 0) SelectedDiscrepancyType = DiscrepancyTypes.Where(o => Convert.ToInt32(o.Id) == Copy.DiscrepancyTypeId).FirstOrDefault();

            DiscrepancyResults = new List<Combo>(Combos.Where(c => c.Key.Equals("DiscrepancyResults")).FirstOrDefault().Value);
            if (Copy.DiscrepancyResultId > 0) SelectedDiscrepancyResult = DiscrepancyResults.Where(o => Convert.ToInt32(o.Id) == Copy.DiscrepancyResultId).FirstOrDefault();

            DiscrepancyStates = new List<Combo>(Combos.Where(c => c.Key.Equals("DiscrepancyStates")).FirstOrDefault().Value);
            if (Copy.DiscrepancyStateId > 0) SelectedDiscrepancyState = DiscrepancyStates.Where(o => Convert.ToInt32(o.Id) == Copy.DiscrepancyStateId).FirstOrDefault();

            DiscrepancyRisks = new List<Combo>(Combos.Where(c => c.Key.Equals("DiscrepancyRisks")).FirstOrDefault().Value);
            if (Copy.DiscrepancyRiskId > 0) SelectedDiscrepancyRisk = DiscrepancyRisks.Where(o => Convert.ToInt32(o.Id) == Copy.DiscrepancyRiskId).FirstOrDefault();
        }

        #endregion

        #region Methods

        private async void ChangeImage()
        {
            var cim = new ChangeImageModel
            {
                MediaFileModel = MediaFileModel,
                MediaFile = MediaFile,
                ImageSource = ImageSource,
                Image = string.Empty
            };

            cim = await ChangeImageEvent(cim);

            MediaFileModel = cim.MediaFileModel;
            MediaFile = cim.MediaFile;
            ImageSource = cim.ImageSource;
            Objects.SetValueBasic(Copy, "Evidence", cim.Image);
        }

        private async void Save()
        {
            var errors = new List<string>();

            try
            {
                await Task.Run(() => {
                    if (Copy.DiscrepancyTypeId == 0)
                        errors.Add("Debe seleccionar un tipo.");
                    if (Copy.DiscrepancyResultId == 0)
                        errors.Add("Debe seleccionar un resultado.");
                    if (Copy.DiscrepancyStateId == 0)
                        errors.Add("Debe seleccionar un estado.");
                    if (Copy.DiscrepancyRiskId == 0)
                        errors.Add("Debe seleccionar un riesgo.");
                });
            }
            finally
            {
                if (errors.Count == 0)
                {
                    await NavigationService.GoBackAsync(new NavigationParameters {
                        { "guidTemp", GuidTemp },
                        { "original", Original },
                        { "copy", Copy },
                        { "optionDiscrepancy", Option },
                        { "mediaFile", MediaFileModel }
                    });
                }
                else
                {
                    await App.Current.MainPage.DisplayAlert("Error.", string.Join(" ", errors.ToArray()), "Accept");
                }
            }

            return;
        }

        #endregion
    }
}
