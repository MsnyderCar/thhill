﻿using DistributedServices.Controller.DataAccess.Mobile;
using Newtonsoft.Json;
using Prism.Commands;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using THHill.Prism.Helpers;
using THHill.Prism.Models;
using Transversal.Common.Entities.DataBase.THHill;

namespace THHill.Prism.ViewModels.WorkOrders.Tool.TestRun
{
    public class ImagesPageViewModel : TestRunViewModelBase
    {
        #region Members

        private ObservableCollection<ImageItemViewModel> _testRunImages;
        
        #region Encapsulation

        public ObservableCollection<ImageItemViewModel> TestRunImages
        {
            get => _testRunImages;
            set => SetProperty(ref _testRunImages, value);
        }

        #endregion

        #region Delegates

        public DelegateCommand AddCommand { get; set; }

        #endregion

        #endregion

        #region Constructor

        public ImagesPageViewModel(INavigationService navigationService, RemoteManager remote) : base(navigationService, remote)
        {
            AddCommand = new DelegateCommand(Add);
        }

        #endregion

        #region Overrides

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            if (parameters.ContainsKey("optionImage"))
            {
                MediaFiles = UpdateMediaFiles(parameters, MediaFiles);

                var gt = parameters.GetValue<Guid>("guidTemp");

                var iievm = TestRunImages.Where(ii => ii.GuidTemp == gt).FirstOrDefault();

                if (iievm != null) InspectionTool.InspectionTestRunImages.Remove(iievm.Original);

                if (InspectionTool.InspectionTestRunImages == null) InspectionTool.InspectionTestRunImages = new List<InspectionTestRunImage>();

                InspectionTool.InspectionTestRunImages.Add(parameters.GetValue<InspectionTestRunImage>("copy"));
            }

            TestRunImages = new ObservableCollection<ImageItemViewModel>();

            if (InspectionTool.InspectionTestRunImages != null && InspectionTool.InspectionTestRunImages.Count > 0)
            {
                InspectionTool.InspectionTestRunImages.ToList().ForEach(im => {
                    TestRunImages.Add(new ImageItemViewModel(
                        NavigationService,
                        Guid.NewGuid(),
                        GetImageSource(MediaFiles, im.Image),
                        im
                        ));
                });
            }
        }

        #endregion

        #region Methods

        private async void Add()
        {
            var elem = new InspectionTestRunImage();

            await NavigationService.NavigateAsync("WorkOrders.Tool.TestRun.ImagePage", new NavigationParameters {
                { "guidTemp", Guid.NewGuid() },
                { "original", elem },
                { "copy", JsonConvert.DeserializeObject<InspectionTestRunImage>(JsonConvert.SerializeObject(elem))},
                { "mediaFile",  new MediaFileModel() },
                { "option", "Agregar" }
            });
            return;
        }

        #endregion
    }
}
