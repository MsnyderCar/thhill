﻿using DistributedServices.Controller.DataAccess.Mobile;
using Prism.Navigation;
using THHill.Prism.Helpers;

namespace THHill.Prism.ViewModels.WorkOrders.Tool.TestRun
{
    public class MainPageViewModel : TestRunViewModelBase
    {
        #region Constructor

        public MainPageViewModel(INavigationService navigationService, RemoteManager remote) : base(navigationService, remote)
        {
            
        }

        #endregion
    }
}
