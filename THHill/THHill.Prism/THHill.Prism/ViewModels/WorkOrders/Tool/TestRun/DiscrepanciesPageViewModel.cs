﻿using DistributedServices.Controller.DataAccess.Mobile;
using Newtonsoft.Json;
using Prism.Commands;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using THHill.Prism.Helpers;
using THHill.Prism.Models;
using Transversal.Common.Entities.DataBase.THHill;

namespace THHill.Prism.ViewModels.WorkOrders.Tool.TestRun
{
    public class DiscrepanciesPageViewModel : TestRunViewModelBase
    {
        #region Members

        private ObservableCollection<DiscrepancyItemViewModel> _discrepancies;
        
        #region Encapsulation

        public ObservableCollection<DiscrepancyItemViewModel> Discrepancies
        {
            get => _discrepancies;
            set => SetProperty(ref _discrepancies, value);
        }

        #endregion

        #region Delegates

        public DelegateCommand AddCommand { get; set; }

        #endregion

        #endregion

        #region Constructor

        public DiscrepanciesPageViewModel(INavigationService navigationService, RemoteManager remote) : base(navigationService, remote)
        {
            AddCommand = new DelegateCommand(Add);
        }

        #endregion

        #region Overrides

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            if (parameters.ContainsKey("optionDiscrepancy"))
            {
                MediaFiles = UpdateMediaFiles(parameters, MediaFiles);

                var gt = parameters.GetValue<Guid>("guidTemp");

                var itevm = Discrepancies.Where(ii => ii.GuidTemp == gt).FirstOrDefault();

                if (itevm != null) InspectionTool.InspectionTestRunDiscrepancies.Remove(itevm.Original);

                if (InspectionTool.InspectionTestRunDiscrepancies == null) InspectionTool.InspectionTestRunDiscrepancies = new List<InspectionTestRunDiscrepancy>();

                InspectionTool.InspectionTestRunDiscrepancies.Add(parameters.GetValue<InspectionTestRunDiscrepancy>("copy"));
            }

            Discrepancies = new ObservableCollection<DiscrepancyItemViewModel>();

            if (InspectionTool.InspectionTestRunDiscrepancies != null && InspectionTool.InspectionTestRunDiscrepancies.Count > 0)
            {
                InspectionTool.InspectionTestRunDiscrepancies.ToList().ForEach(itd => {
                    if (itd.DiscrepancyTypeId > 0) itd.DiscrepancyType = Combos.Where(c => c.Key.Equals("DiscrepancyTypes")).FirstOrDefault().Value.Where(o => Convert.ToInt32(o.Id) == itd.DiscrepancyTypeId).Select(o => new DiscrepancyType { Id = Convert.ToInt32(o.Id), Name = o.Value }).FirstOrDefault();
                    if (itd.DiscrepancyTypeId == 0 && itd.DiscrepancyType != null) itd.DiscrepancyTypeId = itd.DiscrepancyType.Id;

                    if (itd.DiscrepancyResultId > 0) itd.DiscrepancyResult = Combos.Where(c => c.Key.Equals("DiscrepancyResults")).FirstOrDefault().Value.Where(o => Convert.ToInt32(o.Id) == itd.DiscrepancyResultId).Select(o => new DiscrepancyResult { Id = Convert.ToInt32(o.Id), Name = o.Value }).FirstOrDefault();
                    if (itd.DiscrepancyResultId == 0 && itd.DiscrepancyResult != null) itd.DiscrepancyResultId = itd.DiscrepancyResult.Id;

                    if (itd.DiscrepancyStateId > 0) itd.DiscrepancyState = Combos.Where(c => c.Key.Equals("DiscrepancyStates")).FirstOrDefault().Value.Where(o => Convert.ToInt32(o.Id) == itd.DiscrepancyStateId).Select(o => new DiscrepancyState { Id = Convert.ToInt32(o.Id), Name = o.Value }).FirstOrDefault();
                    if (itd.DiscrepancyStateId == 0 && itd.DiscrepancyState != null) itd.DiscrepancyStateId = itd.DiscrepancyState.Id;

                    if (itd.DiscrepancyRiskId > 0) itd.DiscrepancyRisk = Combos.Where(c => c.Key.Equals("DiscrepancyRisks")).FirstOrDefault().Value.Where(o => Convert.ToInt32(o.Id) == itd.DiscrepancyRiskId).Select(o => new DiscrepancyRisk { Id = Convert.ToInt32(o.Id), Name = o.Value }).FirstOrDefault();
                    if (itd.DiscrepancyRiskId == 0 && itd.DiscrepancyRisk != null) itd.DiscrepancyRiskId = itd.DiscrepancyRisk.Id;

                    Discrepancies.Add(new DiscrepancyItemViewModel(
                        NavigationService,
                        Combos,
                        Guid.NewGuid(),
                        GetImageSource(MediaFiles, itd.Evidence),
                        itd
                        ));
                });
            }
        }

        #endregion

        #region Methods

        private async void Add()
        {
            var elem = new InspectionTestRunDiscrepancy();

            await NavigationService.NavigateAsync("WorkOrders.Tool.TestRun.DiscrepancyPage", new NavigationParameters {
                { "combos", Combos },
                { "guidTemp", Guid.NewGuid() },
                { "original", elem },
                { "copy", JsonConvert.DeserializeObject<InspectionTestRunDiscrepancy>(JsonConvert.SerializeObject(elem))},
                { "mediaFile",  new MediaFileModel() },
                { "option", "Agregar" }
            });
            return;
        }

        #endregion
    }
}
