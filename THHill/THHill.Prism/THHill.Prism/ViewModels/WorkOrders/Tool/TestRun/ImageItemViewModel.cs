﻿using Newtonsoft.Json;
using Prism.Commands;
using Prism.Navigation;
using System;
using THHill.Prism.Models;
using Transversal.Common.Entities.DataBase.THHill;
using Xamarin.Forms;

namespace THHill.Prism.ViewModels.WorkOrders.Tool.TestRun
{
    public class ImageItemViewModel
    {
        #region Members

        private readonly INavigationService _navigationService;
        public Guid GuidTemp { get; set; }
        public ImageSource ImageSource { get; set; }
        public InspectionTestRunImage Original { get; set; }
        public InspectionTestRunImage Copy { get; set; }

        #endregion

        #region Delegates

        public DelegateCommand SelectCommand { get; set; }

        #endregion

        #region Constructor

        public ImageItemViewModel(INavigationService navigationService, Guid guidTemp, ImageSource imageSource, InspectionTestRunImage inspectionTestRunImage)
        {
            _navigationService = navigationService;
            GuidTemp = guidTemp;
            Original = inspectionTestRunImage;
            Copy = JsonConvert.DeserializeObject<InspectionTestRunImage>(JsonConvert.SerializeObject(inspectionTestRunImage));
            ImageSource = imageSource;
            SelectCommand = new DelegateCommand(Select);
        }

        #endregion

        #region Methods

        private async void Select()
        {
            await _navigationService.NavigateAsync("WorkOrders.Tool.TestRun.ImagePage", new NavigationParameters
            {
                { "guidTemp", GuidTemp },
                { "original", Original },
                { "copy", Copy },
                { "imageSource", ImageSource },
                { "mediaFile",  new MediaFileModel() },
                { "option", "Editar" }
            });
        }

        #endregion
    }
}
