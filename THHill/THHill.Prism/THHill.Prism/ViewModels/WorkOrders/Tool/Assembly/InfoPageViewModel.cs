﻿using DistributedServices.Controller.DataAccess.Mobile;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using THHill.Prism.Helpers;
using Transversal.Common.Entities.DataBase.THHill;
using Transversal.Common.Entities.Models;

namespace THHill.Prism.ViewModels.WorkOrders.Tool.Assembly
{
    public class InfoPageViewModel : AssemblyViewModelBase
    {
        #region Members

        private List<Combo> _conditions;
        private Combo _selectedCondition;

        #region Encapsulation

        public List<Combo> Conditions
        {
            get => _conditions;
            set => SetProperty(ref _conditions, value);
        }
        public Combo SelectedCondition
        {
            get => _selectedCondition;
            set
            {
                SetProperty(ref _selectedCondition, value);
                InspectionTool.SubAssemblyConditionId = value != null && value.Id != null && value.Id.ToString().Length > 0 ? Convert.ToInt32(value.Id.ToString()) : 0;
            }
        }

        #endregion

        #endregion

        #region Constructor

        public InfoPageViewModel(INavigationService navigationService, RemoteManager remote) : base(navigationService, remote)
        {
            
        }

        #endregion

        #region Overrides

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            if (Conditions == null)
            {
                Conditions = new List<Combo>(Combos.Where(c => c.Key.Equals("Conditions")).FirstOrDefault().Value);
                if (InspectionTool.SubAssemblyConditionId > 0) SelectedCondition = Conditions.Where(o => Convert.ToInt32(o.Id) == InspectionTool.SubAssemblyConditionId).FirstOrDefault();
            }
        }

        #endregion
    }
}
