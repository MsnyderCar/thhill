﻿using Newtonsoft.Json;
using Prism.Commands;
using Prism.Navigation;
using System;
using THHill.Prism.Models;
using Transversal.Common.Entities.DataBase.THHill;
using Xamarin.Forms;

namespace THHill.Prism.ViewModels.WorkOrders.Tool.Assembly
{
    public class ImageItemViewModel
    {
        #region Members

        private readonly INavigationService _navigationService;
        public Guid GuidTemp { get; set; }
        public ImageSource ImageSource { get; set; }
        public InspectionSubAssemblyImage Original { get; set; }
        public InspectionSubAssemblyImage Copy { get; set; }

        #endregion

        #region Delegates

        public DelegateCommand SelectCommand { get; set; }

        #endregion

        #region Constructor

        public ImageItemViewModel(INavigationService navigationService, Guid guidTemp, ImageSource imageSource, InspectionSubAssemblyImage inspectionSubAssemblyImage)
        {
            _navigationService = navigationService;
            GuidTemp = guidTemp;
            Original = inspectionSubAssemblyImage;
            Copy = JsonConvert.DeserializeObject<InspectionSubAssemblyImage>(JsonConvert.SerializeObject(inspectionSubAssemblyImage));
            ImageSource = imageSource;
            SelectCommand = new DelegateCommand(Select);
        }

        #endregion

        #region Methods

        private async void Select()
        {
            await _navigationService.NavigateAsync("WorkOrders.Tool.Assembly.ImagePage", new NavigationParameters
            {
                { "guidTemp", GuidTemp },
                { "original", Original },
                { "copy", Copy },
                { "imageSource", ImageSource },
                { "mediaFile",  new MediaFileModel() },
                { "option", "Editar" }
            });
        }

        #endregion
    }
}
