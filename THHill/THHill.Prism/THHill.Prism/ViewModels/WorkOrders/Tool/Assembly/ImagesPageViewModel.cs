﻿using DistributedServices.Controller.DataAccess.Mobile;
using Newtonsoft.Json;
using Prism.Commands;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using THHill.Prism.Helpers;
using THHill.Prism.Models;
using Transversal.Common.Entities.DataBase.THHill;

namespace THHill.Prism.ViewModels.WorkOrders.Tool.Assembly
{
    public class ImagesPageViewModel : AssemblyViewModelBase
    {
        #region Members

        private ObservableCollection<ImageItemViewModel> _assemblyImages;
        
        #region Encapsulation

        public ObservableCollection<ImageItemViewModel> AssemblyImages
        {
            get => _assemblyImages;
            set => SetProperty(ref _assemblyImages, value);
        }

        #endregion

        #region Delegates

        public DelegateCommand AddCommand { get; set; }

        #endregion

        #endregion

        #region Constructor

        public ImagesPageViewModel(INavigationService navigationService, RemoteManager remote) : base(navigationService, remote)
        {
            AddCommand = new DelegateCommand(Add);
        }

        #endregion

        #region Overrides

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            if (parameters.ContainsKey("optionImage"))
            {
                MediaFiles = UpdateMediaFiles(parameters, MediaFiles);

                var gt = parameters.GetValue<Guid>("guidTemp");

                var iievm = AssemblyImages.Where(ii => ii.GuidTemp == gt).FirstOrDefault();

                if (iievm != null) InspectionTool.InspectionSubAssemblyImages.Remove(iievm.Original);

                if (InspectionTool.InspectionSubAssemblyImages == null) InspectionTool.InspectionSubAssemblyImages = new List<InspectionSubAssemblyImage>();

                InspectionTool.InspectionSubAssemblyImages.Add(parameters.GetValue<InspectionSubAssemblyImage>("copy"));
            }

            AssemblyImages = new ObservableCollection<ImageItemViewModel>();

            if (InspectionTool.InspectionSubAssemblyImages != null && InspectionTool.InspectionSubAssemblyImages.Count > 0)
            {
                InspectionTool.InspectionSubAssemblyImages.ToList().ForEach(im => {

                    AssemblyImages.Add(new ImageItemViewModel(
                        NavigationService,
                        Guid.NewGuid(),
                        GetImageSource(MediaFiles, im.Image),
                        im
                        ));
                });
            }
        }

        #endregion

        #region Methods

        private async void Add()
        {
            var elem = new InspectionSubAssemblyImage();

            await NavigationService.NavigateAsync("WorkOrders.Tool.Assembly.ImagePage", new NavigationParameters {
                { "guidTemp", Guid.NewGuid() },
                { "original", elem },
                { "copy", JsonConvert.DeserializeObject<InspectionSubAssemblyImage>(JsonConvert.SerializeObject(elem))},
                { "mediaFile",  new MediaFileModel() },
                { "option", "Agregar" }
            });
            return;
        }

        #endregion
    }
}
