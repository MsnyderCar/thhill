﻿using Newtonsoft.Json;
using Prism.Commands;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using THHill.Prism.Models;
using Transversal.Common.Entities.DataBase.THHill;
using Transversal.Common.Entities.Models;
using Xamarin.Forms;

namespace THHill.Prism.ViewModels.WorkOrders
{
    public class ToolItemViewModel
    {
        #region Members

        private readonly INavigationService _navigationService;

        public List<KeyValuePair<string, List<Combo>>> Combos { get; set; }

        public Guid GuidTemp { get; set; }

        public List<MediaFileModel> MediaFiles { get; set; }

        public ImageSource ImageSource { get; set; }

        public InspectionTool Original { get; set; }

        public InspectionTool Copy { get; set; }

        #endregion

        #region Delegates

        public DelegateCommand SelectCommand { get; set; }

        #endregion

        #region Constructor

        public ToolItemViewModel(INavigationService navigationService, List<KeyValuePair<string, List<Combo>>> combos, Guid guidTemp, List<MediaFileModel> mediaFiles, ImageSource imageSource, InspectionTool inspectionTool)
        {
            _navigationService = navigationService;
            Combos = combos;
            GuidTemp = guidTemp;
            MediaFiles = mediaFiles;
            ImageSource = imageSource;
            Original = inspectionTool;
            Copy = JsonConvert.DeserializeObject<InspectionTool>(JsonConvert.SerializeObject(inspectionTool));
            
            SelectCommand = new DelegateCommand(Select);
        }

        #endregion

        #region Methods

        private async void Select()
        {
            await _navigationService.NavigateAsync("WorkOrders.Tool.MainPage", new NavigationParameters
            {
                { "combos", Combos },
                { "guidTemp", GuidTemp },
                { "original", Original },
                { "copy", Copy },
                { "imageSource", ImageSource },
                { "mediaFile",  new MediaFileModel() },
                { "mediaFiles", MediaFiles },
                { "option", "Editar" }
            });
        }

        #endregion
    }
}
