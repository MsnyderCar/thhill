﻿using DistributedServices.Controller.Api;
using Prism.Commands;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using THHill.Prism.Models;
using Transversal.Common.Entities.DataBase.Authentication;
using Transversal.Common.Entities.DataBase.THHill;
using Transversal.Common.Entities.Models;

namespace THHill.Prism.ViewModels
{
    public class WorkOrderItemViewModel
    {
        private User User { get; set; }

        private List<KeyValuePair<string, List<Combo>>> Combos { get; set; }

        public Guid DataId { get; set; }

        public WorkOrder WorkOrder { get; set; }

        private readonly INavigationService _navigationService;

        public DelegateCommand SelectCommand { get; set; }

        public WorkOrderItemViewModel(INavigationService navigationService, User user, List<KeyValuePair<string, List<Combo>>> combos, Guid dataId, WorkOrder workOrder)
        {
            _navigationService = navigationService;
            User = user;
            Combos = combos;
            DataId = dataId;
            WorkOrder = workOrder;
            SelectCommand = new DelegateCommand(Select);
        }

        private async void Select()
        {
            await _navigationService.NavigateAsync("WorkOrders.WorkOrderPage", new NavigationParameters
            {
                { "user", User },
                { "combos", Combos },
                { "dataId", DataId },
                { "workOrder", WorkOrder },
                { "mediaFiles", new List<MediaFileModel>() }
            });
        }
    }
}
