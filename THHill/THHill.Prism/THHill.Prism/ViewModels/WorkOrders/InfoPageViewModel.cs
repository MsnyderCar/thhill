﻿using DistributedServices.Controller.DataAccess.Mobile;
using Prism.Commands;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using THHill.Prism.Helpers;
using Transversal.Common.Entities.DataBase.THHill;
using Transversal.Common.Entities.Models;

namespace THHill.Prism.ViewModels.WorkOrders
{
    public class InfoPageViewModel : WorkOrderViewModelBase
    {
        #region Members

        private List<Combo> _operators;
        private Combo _selectedOperator;

        private List<Combo> _oilWells;
        private Combo _selectedOilWell;

        private DateTime _beginDate;
        private DateTime _endDate;

        private List<Combo> _monitoringTypes;
        private Combo _selectedMonitoringType;

        private List<Combo> _inspectionCategories;
        private Combo _selectedInspectionCategory;

        #region Encapsulation

        public List<Combo> Operators
        {
            get => _operators;
            set => SetProperty(ref _operators, value);
        }
        public Combo SelectedOperator
        {
            get => _selectedOperator;
            //set => SetProperty(ref _selectedOperator, value);
            set
            {
                SetProperty(ref _selectedOperator, value);
                WorkOrder.OperatorId = value != null && value.Id != null && value.Id.ToString().Length > 0 ? new Guid(value.Id.ToString()) : Guid.Empty;
            }
        }

        public List<Combo> OilWells
        {
            get => _oilWells;
            set => SetProperty(ref _oilWells, value);
        }
        public Combo SelectedOilWell
        {
            get => _selectedOilWell;
            //set => SetProperty(ref _selectedOilWell, value);
            set {
                SetProperty(ref _selectedOilWell, value);
                WorkOrder.OilWellId = value != null && value.Id != null && value.Id.ToString().Length > 0 ? new Guid(value.Id.ToString()) : Guid.Empty;
            }
        }

        public DateTime BeginDate
        {
            get => _beginDate;
            //set => SetProperty(ref _beginDate, value);
            set
            {
                SetProperty(ref _beginDate, value);
                if (WorkOrder != null) WorkOrder.BeginDate = value.ToString("u", DateTimeFormatInfo.InvariantInfo).Substring(0, 10);
            }
        }

        public DateTime EndDate
        {
            get => _endDate;
            //set => SetProperty(ref _endDate, value);
            set
            {
                SetProperty(ref _endDate, value);
                if (WorkOrder != null) WorkOrder.EndDate = value.ToString("u", DateTimeFormatInfo.InvariantInfo).Substring(0, 10);
            }
        }

        public List<Combo> MonitoringTypes
        {
            get => _monitoringTypes;
            set => SetProperty(ref _monitoringTypes, value);
        }
        public Combo SelectedMonitoringType
        {
            get => _selectedMonitoringType;
            //set => SetProperty(ref _selectedMonitoringType, value);
            set
            {
                SetProperty(ref _selectedMonitoringType, value);
                WorkOrder.MonitoringTypeId = value != null && value.Id != null && value.Id.ToString().Length > 0 ? new Guid(value.Id.ToString()) : Guid.Empty;
            }
        }

        public List<Combo> InspectionCategories
        {
            get => _inspectionCategories;
            set => SetProperty(ref _inspectionCategories, value);
        }
        public Combo SelectedInspectionCategory
        {
            get => _selectedInspectionCategory;
            //set => SetProperty(ref _selectedInspectionCategory, value);
            set
            {
                SetProperty(ref _selectedInspectionCategory, value);
                WorkOrder.InspectionCategoryId = value != null && value.Id != null && value.Id.ToString().Length > 0 ? new Guid(value.Id.ToString()) : Guid.Empty;
            }
        }

        #endregion

        #region Delegates

        public DelegateCommand GoToInspectionCommand { get; set; }

        #endregion

        #endregion

        #region Constructor

        public InfoPageViewModel(INavigationService navigationService, RemoteManager remote) : base(navigationService, remote)
        {
            GoToInspectionCommand = new DelegateCommand(GoToInspection);
        }

        #endregion

        #region Overrides

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            BeginDate = !string.IsNullOrEmpty(WorkOrder.BeginDate) ? DateTime.Parse(WorkOrder.BeginDate) : DateTime.Now;
            EndDate = !string.IsNullOrEmpty(WorkOrder.EndDate) ? DateTime.Parse(WorkOrder.EndDate) : DateTime.Now;

            if (Operators == null)
            {
                Operators = new List<Combo>(Combos.Where(c => c.Key.Equals("Operators")).FirstOrDefault().Value);
                if (WorkOrder.OperatorId != null && WorkOrder.OperatorId != Guid.Empty) SelectedOperator = Operators.Where(o => new Guid(o.Id.ToString()) == WorkOrder.OperatorId).FirstOrDefault();
            }

            if (OilWells == null)
            {
                OilWells = new List<Combo>(Combos.Where(c => c.Key.Equals("OilWells")).FirstOrDefault().Value);
                if (WorkOrder.OilWellId != null && WorkOrder.OilWellId != Guid.Empty) SelectedOilWell = OilWells.Where(o => new Guid(o.Id.ToString()) == WorkOrder.OilWellId).FirstOrDefault();
            }

            if (MonitoringTypes == null)
            {
                MonitoringTypes = new List<Combo>(Combos.Where(c => c.Key.Equals("MonitoringTypes")).FirstOrDefault().Value);
                if (WorkOrder.MonitoringTypeId != null && WorkOrder.MonitoringTypeId != Guid.Empty) SelectedMonitoringType = MonitoringTypes.Where(o => new Guid(o.Id.ToString()) == WorkOrder.MonitoringTypeId).FirstOrDefault();
            }

            if (InspectionCategories == null)
            {
                InspectionCategories = new List<Combo>(Combos.Where(c => c.Key.Equals("InspectionCategories")).FirstOrDefault().Value);
                if (WorkOrder.InspectionCategoryId != null && WorkOrder.InspectionCategoryId != Guid.Empty) SelectedInspectionCategory = InspectionCategories.Where(o => new Guid(o.Id.ToString()) == WorkOrder.InspectionCategoryId).FirstOrDefault();
            }
        }

        #endregion

        #region Methods

        private async void GoToInspection()
        {
            await NavigationService.NavigateAsync("WorkOrders.Inspection.MainPage", new NavigationParameters {
                    { "combos", _combos },
                    { "workOrder", _workOrder },
                    { "mediaFiles", _mediaFiles }
                });

            return;
        }

        #endregion
    }

}