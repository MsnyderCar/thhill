﻿using DistributedServices.Controller.DataAccess.Mobile;
using Newtonsoft.Json;
using Prism.Commands;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using THHill.Prism.Helpers;
using THHill.Prism.Models;
using Transversal.Common.Entities.DataBase.THHill;

namespace THHill.Prism.ViewModels.WorkOrders
{
    public class ToolsPageViewModel : WorkOrderViewModelBase
    {
        #region Members

        private ObservableCollection<ToolItemViewModel> _inspectionTools;
        
        #region Encapsulation

        public ObservableCollection<ToolItemViewModel> InspectionTools
        {
            get => _inspectionTools;
            set => SetProperty(ref _inspectionTools, value);
        }

        #endregion

        #region Delegates

        public DelegateCommand AddCommand { get; set; }

        #endregion

        #endregion

        #region Constructor

        public ToolsPageViewModel(INavigationService navigationService, RemoteManager remote) : base(navigationService, remote)
        {
            AddCommand = new DelegateCommand(Add);
        }

        #endregion

        #region Overrides

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            if (parameters.ContainsKey("optionTool"))
            {
                MediaFiles = UpdateMediaFiles(parameters, MediaFiles);

                var gt = parameters.GetValue<Guid>("guidTemp");

                var itevm = InspectionTools.Where(ic => ic.GuidTemp == gt).FirstOrDefault();

                if (itevm != null) WorkOrder.InspectionTools.Remove(itevm.Original);

                if (WorkOrder.InspectionTools == null) WorkOrder.InspectionTools = new List<InspectionTool>();

                WorkOrder.InspectionTools.Add(parameters.GetValue<InspectionTool>("copy"));
            }

            InspectionTools = new ObservableCollection<ToolItemViewModel>();

            if (WorkOrder.InspectionTools != null && WorkOrder.InspectionTools.Count > 0)
            {
                WorkOrder.InspectionTools.ToList().ForEach(it => {
                    if (it.ToolTypeId > 0) it.ToolType = Combos.Where(c => c.Key.Equals("ToolTypes")).FirstOrDefault().Value.Where(o => Convert.ToInt32(o.Id) == it.ToolTypeId).Select(o => new Transversal.Common.Entities.DataBase.THHill.ToolType { Id = Convert.ToInt32(o.Id), Name = o.Value }).FirstOrDefault();
                    if (it.ToolTypeId == 0 && it.ToolType != null) it.ToolTypeId = it.ToolType.Id;

                    InspectionTools.Add(new ToolItemViewModel(
                        NavigationService,
                        Combos,
                        Guid.NewGuid(),
                        new List<MediaFileModel>(MediaFiles),
                        GetImageSource(MediaFiles, it.Image),
                        it
                        ));
                });
            }
        }

        #endregion

        #region Methods

        private async void Add()
        {
            var elem = new InspectionTool
            {
                ToolTypeId = 0,
                InspectionComponents = new List<InspectionComponent>(),
                InspectionToolDiscrepancies = new List<InspectionToolDiscrepancy>(),
                InspectionSubAssemblyImages = new List<InspectionSubAssemblyImage>(),
                InspectionTestRunImages = new List<InspectionTestRunImage>(),
                InspectionTestRunDiscrepancies = new List<InspectionTestRunDiscrepancy>()
            };

            await NavigationService.NavigateAsync("WorkOrders.Tool.MainPage", new NavigationParameters {
                { "combos", Combos },
                { "guidTemp", Guid.NewGuid() },
                { "original", elem },
                { "copy", JsonConvert.DeserializeObject<InspectionTool>(JsonConvert.SerializeObject(elem))},
                { "mediaFile",  new MediaFileModel() },
                { "mediaFiles", new List<MediaFileModel>(MediaFiles) },
                { "option", "Agregar" }
            });
            return;
        }

        #endregion
    }
}
