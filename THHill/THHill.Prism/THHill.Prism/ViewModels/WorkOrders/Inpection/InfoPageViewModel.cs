﻿using DistributedServices.Controller.DataAccess.Mobile;
using Prism.Navigation;
using System;
using System.Globalization;
using THHill.Prism.Helpers;
using Transversal.Common.Entities.DataBase.THHill;

namespace THHill.Prism.ViewModels.WorkOrders.Inspection
{
    public class InfoPageViewModel : InspectionViewModelBase
    {
        #region Members

        private DateTime _inspectionToolDate;
        private DateTime _penetratingLiquidDate;
        private DateTime _developerDate;

        #region Encapsulation

        public DateTime InspectionToolDate
        {
            get => _inspectionToolDate;
            //set => SetProperty(ref _inspectionToolDate, value);
            set
            {
                SetProperty(ref _inspectionToolDate, value);
                if (WorkOrder != null) WorkOrder.InspectionToolDate = value.ToString("u", DateTimeFormatInfo.InvariantInfo).Substring(0, 10);
            }
        }
        public DateTime PenetratingLiquidDate
        {
            get => _penetratingLiquidDate;
            //set => SetProperty(ref _penetratingLiquidDate, value);
            set
            {
                SetProperty(ref _penetratingLiquidDate, value);
                if (WorkOrder != null) WorkOrder.PenetratingLiquidDate = value.ToString("u", DateTimeFormatInfo.InvariantInfo).Substring(0, 10);
            }
        }
        public DateTime DeveloperDate
        {
            get => _developerDate;
            //set => SetProperty(ref _developerDate, value);
            set
            {
                SetProperty(ref _developerDate, value);
                if (WorkOrder != null) WorkOrder.DeveloperDate = value.ToString("u", DateTimeFormatInfo.InvariantInfo).Substring(0, 10);
            }
        }

        #endregion

        #endregion

        #region Constructor

        public InfoPageViewModel(INavigationService navigationService, RemoteManager remote) : base(navigationService, remote)
        {
            
        }

        #endregion

        #region Overrides

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            InspectionToolDate = !string.IsNullOrEmpty(WorkOrder.InspectionToolDate) ? DateTime.Parse(WorkOrder.InspectionToolDate) : DateTime.Now;
            PenetratingLiquidDate = !string.IsNullOrEmpty(WorkOrder.PenetratingLiquidDate) ? DateTime.Parse(WorkOrder.PenetratingLiquidDate) : DateTime.Now;
            DeveloperDate = !string.IsNullOrEmpty(WorkOrder.DeveloperDate) ? DateTime.Parse(WorkOrder.DeveloperDate) : DateTime.Now;
        }

        #endregion
    }
}
