﻿using DistributedServices.Controller.DataAccess.Mobile;
using Newtonsoft.Json;
using Prism.Commands;
using Prism.Navigation;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using THHill.Prism.Helpers;
using THHill.Prism.Models;
using Transversal.Common.Entities.DataBase.THHill;

namespace THHill.Prism.ViewModels.WorkOrders.Inspection
{
    public class ImagesPageViewModel : InspectionViewModelBase
    {
        #region Members

        private ObservableCollection<ImageItemViewModel> _inspectionImages;
        
        #region Encapsulation

        public ObservableCollection<ImageItemViewModel> InspectionImages
        {
            get => _inspectionImages;
            set => SetProperty(ref _inspectionImages, value);
        }

        #endregion

        #region Delegates

        public DelegateCommand AddCommand { get; set; }

        #endregion

        #endregion

        #region Constructor

        public ImagesPageViewModel(INavigationService navigationService, RemoteManager remote) : base(navigationService, remote)
        {
            AddCommand = new DelegateCommand(Add);
        }

        #endregion

        #region Overrides

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            if (parameters.ContainsKey("optionImage"))
            {
                MediaFiles = UpdateMediaFiles(parameters, MediaFiles);

                var gt = parameters.GetValue<Guid>("guidTemp");

                var iievm = InspectionImages.Where(ii => ii.GuidTemp == gt).FirstOrDefault();

                if (iievm != null) WorkOrder.InspectionImages.Remove(iievm.Original);

                WorkOrder.InspectionImages.Add(parameters.GetValue<InspectionImage>("copy"));
            }

            InspectionImages = new ObservableCollection<ImageItemViewModel>();

            if (WorkOrder.InspectionImages != null && WorkOrder.InspectionImages.Count > 0)
            {
                WorkOrder.InspectionImages.ToList().ForEach(im => {

                    InspectionImages.Add(new ImageItemViewModel(
                        NavigationService,
                        Guid.NewGuid(),
                        GetImageSource(MediaFiles, im.Image),
                        im
                        ));
                });
            }
        }

        #endregion

        #region Methods

        private async void Add()
        {
            var elem = new InspectionImage();

            await NavigationService.NavigateAsync("WorkOrders.Inspection.ImagePage", new NavigationParameters {
                { "guidTemp", Guid.NewGuid() },
                { "original", elem },
                { "copy", JsonConvert.DeserializeObject<InspectionImage>(JsonConvert.SerializeObject(elem))},
                { "mediaFile",  new MediaFileModel() },
                { "option", "Agregar" }
            });
            return;
        }

        #endregion
    }
}
