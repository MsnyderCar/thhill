﻿using Newtonsoft.Json;
using Prism.Commands;
using Prism.Navigation;
using System;
using THHill.Prism.Models;
using Transversal.Common.Entities.DataBase.THHill;
using Xamarin.Forms;

namespace THHill.Prism.ViewModels.WorkOrders.Inspection
{
    public class ImageItemViewModel
    {
        #region Members

        private readonly INavigationService _navigationService;
        public Guid GuidTemp { get; set; }
        public ImageSource ImageSource { get; set; }
        public InspectionImage Original { get; set; }
        public InspectionImage Copy { get; set; }

        #endregion

        #region Delegates

        public DelegateCommand SelectCommand { get; set; }

        #endregion

        #region Constructor

        public ImageItemViewModel(INavigationService navigationService, Guid guidTemp, ImageSource imageSource, InspectionImage inspectionImage)
        {
            _navigationService = navigationService;
            GuidTemp = guidTemp;
            Original = inspectionImage;
            Copy = JsonConvert.DeserializeObject<InspectionImage>(JsonConvert.SerializeObject(inspectionImage));
            ImageSource = imageSource;
            SelectCommand = new DelegateCommand(Select);
        }

        #endregion

        private async void Select()
        {
            await _navigationService.NavigateAsync("WorkOrders.Inspection.ImagePage", new NavigationParameters
            {
                { "guidTemp", GuidTemp },
                { "original", Original },
                { "copy", Copy },
                { "imageSource", ImageSource },
                { "mediaFile",  new MediaFileModel() },
                { "option", "Editar" }
            });
        }
    }
}
