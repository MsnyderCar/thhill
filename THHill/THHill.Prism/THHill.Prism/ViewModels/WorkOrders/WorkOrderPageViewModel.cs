﻿using DistributedServices.Controller.DataAccess.Mobile;
using Prism.Navigation;
using THHill.Prism.Helpers;

namespace THHill.Prism.ViewModels.WorkOrders
{
    public class WorkOrderPageViewModel : WorkOrderViewModelBase
    {
        #region Constructor

        public WorkOrderPageViewModel(INavigationService navigationService, RemoteManager remote) : base(navigationService, remote)
        {
            
        }

        #endregion
    }
}
