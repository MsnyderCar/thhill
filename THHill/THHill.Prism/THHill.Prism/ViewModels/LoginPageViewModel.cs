﻿using DistributedServices.Controller.Api;
using DistributedServices.Controller.DataAccess.Mobile;
using Prism.Commands;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using THHill.Prism.Helpers;
using Transversal.Common.Entities.DataBase.Authentication;
using Transversal.Common.Entities.DataBase.Core;
using Transversal.Common.Entities.Models;

namespace THHill.Prism.ViewModels
{
    public class LoginPageViewModel : ViewModelBase
    {
        #region Members

        private string _email;
        private string _password;
        private bool _isEnabled;

        #region Encapsulation
        
        public string Email
        {
            get => _email;
            set => SetProperty(ref _email, value);
        }
        public string Password
        {
            get => _password;
            set => SetProperty(ref _password, value);
        }
        
        public bool IsEnabled
        {
            get => _isEnabled;
            set => SetProperty(ref _isEnabled, value);
        }

        #endregion

        #region Delegates

        public DelegateCommand LoginCommand { get; set; }
        public DelegateCommand ClearCommand { get; set; }

        #endregion

        #endregion

        #region Constructor

        public LoginPageViewModel(INavigationService navigationService, RemoteManager remote) : base(navigationService, remote)
        {
            LoginCommand = new DelegateCommand(Login);
            ClearCommand = new DelegateCommand(Clear);
            Title = "Login";
            IsEnabled = true;
            
            //TODO: Delete this! is justo to probe
            Email = "m.carvajal@9torch.com";
            Password = "123456";
        }

        #endregion

        #region Methods

        private async void Login()
        {
            var errors = new List<string>();
            User user = null;
            var combos = new List<KeyValuePair<string, List<Combo>>>();

            try
            {
                IsRunning = true;

                await Task.Run(() => {
                    if (string.IsNullOrEmpty(Email))
                        errors.Add("You must enter an email.");
                    if (string.IsNullOrEmpty(Password))
                        errors.Add("You must enter a password.");

                    if (errors.Count == 0)
                    {
                        if (!Remote.Connect(Email, Password, string.Empty))
                        {
                            errors.Add(Remote.Message);
                            Password = string.Empty;
                        }
                        else
                        {
                            user = (User)Remote.Execute("Authentication", "GetUserByEmail", Email);

                            if (user == null)
                                errors.Add("This user has trouble to connect, contact support.");
                            else
                            {
                                Password = string.Empty;

                                var listCombos = new List<string> {
                                    "Operators",
                                    "OilWells",
                                    "MonitoringTypes",
                                    "InspectionCategories",
                                    "WorkOrderStates",
                                    "ToolTypes",
                                    "Conditions",
                                    "DiscrepancyTypes",
                                    "DiscrepancyResults",
                                    "DiscrepancyStates",
                                    "DiscrepancyRisks"
                                };

                                listCombos.ForEach(c => {
                                    combos.Add(new KeyValuePair<string, List<Combo>>(c, (List<Combo>)Remote.Execute("Core", "GetCombo", c)));
                                });
                            }
                        }
                    }

                });
            }
            finally
            {
                IsRunning = false;

                if (errors.Count > 0)
                    await App.Current.MainPage.DisplayAlert("Error.", string.Join(" ", errors.ToArray()), "Accept");
                else
                    await NavigationService.NavigateAsync("MainPage", new NavigationParameters {//
                        { "user", user },
                        { "combos", combos }
                    });
            }

            return;
        }

        private async void Clear()
        {
            try 
            {
                IsRunning = true;

                await Task.Run(() => {
                    Remote.Clear();
                });
            }
            finally
            {
                IsRunning = false;

                await App.Current.MainPage.DisplayAlert("Error.", "Finalizo", "Accept");
            }

            return;
        }

        #endregion
    }
}
