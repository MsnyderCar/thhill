﻿using DistributedServices.Controller.Api;
using DistributedServices.Controller.DataAccess.Mobile;
using Prism.Commands;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using THHill.Prism.Helpers;
using Transversal.Common.Entities.DataBase.Authentication;
using Transversal.Common.Entities.DataBase.Core;
using Transversal.Common.Entities.Models;

namespace THHill.Prism.ViewModels
{
    public class SyncPageViewModel : ViewModelBase
    {
        #region Members

        #region Encapsulation
        
        #endregion

        #region Delegates

        public DelegateCommand SyncCommand { get; set; }

        #endregion

        #endregion

        #region Constructor

        public SyncPageViewModel(INavigationService navigationService, RemoteManager remote) : base(navigationService, remote)
        {
            SyncCommand = new DelegateCommand(Sync);
        }

        #endregion

        #region Methods

        private async void Sync()
        {
            try
            {
                IsRunning = true;
                IsBtnEnabled = false;

                await Task.Run(() => {
                    var elementsSync = (int)Remote.Execute("Core", "SyncData", 0);
                });
            }
            finally
            {
                IsRunning = false;
                IsBtnEnabled = true;
                if (Remote.Error)
                    await App.Current.MainPage.DisplayAlert("Error.", string.Join(" ", Remote.Message), "Accept");
            }

            return;
        }

        #endregion
    }
}
