﻿using DistributedServices.Controller.Api;
using DistributedServices.Controller.DataAccess.Mobile;
using Prism.Commands;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using THHill.Prism.Helpers;
using Transversal.Common.Entities.DataBase.Authentication;
using Transversal.Common.Entities.DataBase.Core;
using Transversal.Common.Entities.Models;

namespace THHill.Prism.ViewModels
{
    public class MainPageViewModel : ViewModelBase
    {
        #region Members

        #region Encapsulation
        
        #endregion

        #region Delegates

        #endregion

        #endregion

        #region Constructor

        public MainPageViewModel(INavigationService navigationService, RemoteManager remote) : base(navigationService, remote)
        {
            Title = "THHill";
        }

        #endregion

        #region Methods

        #endregion
    }
}
