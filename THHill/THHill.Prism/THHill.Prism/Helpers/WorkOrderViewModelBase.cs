﻿using DistributedServices.Controller.DataAccess.Mobile;
using Prism.Commands;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using THHill.Prism.Models;
using Transversal.Common.Entities.DataBase.Authentication;
using Transversal.Common.Entities.DataBase.Mobile;
using Transversal.Common.Entities.DataBase.THHill;
using Transversal.Common.Entities.Models;
using Transversal.Common.Utilities;

namespace THHill.Prism.Helpers
{
    public class WorkOrderViewModelBase : ViewModelBase
    {
        #region Members

        protected User _user;
        protected List<KeyValuePair<string, List<Combo>>> _combos;
        protected Guid _dataId;
        protected WorkOrder _workOrder;
        protected List<MediaFileModel> _mediaFiles;

        #region Encapsulation

        public User User
        {
            get => _user;
            set => SetProperty(ref _user, value);
        }

        public List<KeyValuePair<string, List<Combo>>> Combos
        {
            get => _combos;
            set => SetProperty(ref _combos, value);
        }

        public Guid DataId
        {
            get => _dataId;
            set => SetProperty(ref _dataId, value);
        }

        public WorkOrder WorkOrder
        {
            get => _workOrder;
            set => SetProperty(ref _workOrder, value);
        }

        public List<MediaFileModel> MediaFiles
        {
            get => _mediaFiles;
            set => SetProperty(ref _mediaFiles, value);
        }

        #endregion

        #region Delegates

        public DelegateCommand SaveCommand { get; set; }

        #endregion

        #endregion

        #region Constructor

        public WorkOrderViewModelBase(INavigationService navigationService, RemoteManager remote) : base(navigationService, remote)
        {
            SaveCommand = new DelegateCommand(Save);
        }

        #endregion

        #region Methods

        public override void Initialize(INavigationParameters parameters)
        {
            User = parameters.GetValue<User>("user");
            Combos = parameters.GetValue<List<KeyValuePair<string, List<Combo>>>>("combos");
            DataId = parameters.GetValue<Guid>("dataId");
            WorkOrder = parameters.GetValue<WorkOrder>("workOrder");
            MediaFiles = parameters.GetValue<List<MediaFileModel>>("mediaFiles");
        }

        private async void Save()
        {
            var errors = new List<string>();

            try
            {
                IsRunning = true;

                await Task.Run(() => {
                    if (WorkOrder.OperatorId == Guid.Empty)
                        errors.Add("Debe seleccionar un operador.");
                    if (WorkOrder.OilWellId == Guid.Empty)
                        errors.Add("Debe seleccionar un pozo.");
                    if (WorkOrder.MonitoringTypeId == Guid.Empty)
                        errors.Add("Debe seleccionar un tipo de monitoreo.");
                    if (WorkOrder.InspectionCategoryId == Guid.Empty)
                        errors.Add("Debe seleccionar una categoria de inspección.");

                    if (errors.Count == 0)
                    {
                        WorkOrder.UserId = User.Id;

                        WorkOrder.Operator = null;
                        WorkOrder.OilWell = null;
                        WorkOrder.MonitoringType = null;
                        WorkOrder.InspectionCategory = null;
                        WorkOrder.Vendor = null;

                        if (WorkOrder.InspectionTools != null) WorkOrder.InspectionTools.ToList().ForEach(it => {
                            if (it.ToolType != null && it.ToolType.Id != it.ToolTypeId) { it.ToolTypeId = it.ToolType.Id; it.ToolType = null; }
                            if (it.SubAssemblyCondition != null && it.SubAssemblyCondition.Id != it.SubAssemblyConditionId) { it.SubAssemblyConditionId = it.SubAssemblyCondition.Id; it.SubAssemblyCondition = null; }

                            if (it.InspectionComponents != null) it.InspectionComponents.ToList().ForEach(ic => {
                                if (ic.Condition != null && ic.Condition.Id != ic.ConditionId) { ic.ConditionId = ic.Condition.Id; ic.Condition = null; }

                                if (ic.InspectionComponentDiscrepancies != null) ic.InspectionComponentDiscrepancies.ToList().ForEach(icd => {
                                    if (icd.DiscrepancyType != null && icd.DiscrepancyType.Id != icd.DiscrepancyTypeId) { icd.DiscrepancyTypeId = icd.DiscrepancyType.Id; icd.DiscrepancyType = null; }
                                    if (icd.DiscrepancyResult != null && icd.DiscrepancyResult.Id != icd.DiscrepancyResultId) { icd.DiscrepancyResultId = icd.DiscrepancyResult.Id; icd.DiscrepancyResult = null; }
                                    if (icd.DiscrepancyState != null && icd.DiscrepancyState.Id != icd.DiscrepancyStateId) { icd.DiscrepancyStateId = icd.DiscrepancyState.Id; icd.DiscrepancyState = null; }
                                    if (icd.DiscrepancyRisk != null && icd.DiscrepancyRisk.Id != icd.DiscrepancyRiskId) { icd.DiscrepancyRiskId = icd.DiscrepancyRisk.Id; icd.DiscrepancyRisk = null; }
                                });
                            });

                            if (it.InspectionToolDiscrepancies != null) it.InspectionToolDiscrepancies.ToList().ForEach(itd => {
                                if (itd.DiscrepancyType != null && itd.DiscrepancyType.Id != itd.DiscrepancyTypeId) { itd.DiscrepancyTypeId = itd.DiscrepancyType.Id; itd.DiscrepancyType = null; }
                                if (itd.DiscrepancyResult != null && itd.DiscrepancyResult.Id != itd.DiscrepancyResultId) { itd.DiscrepancyResultId = itd.DiscrepancyResult.Id; itd.DiscrepancyResult = null; }
                                if (itd.DiscrepancyState != null && itd.DiscrepancyState.Id != itd.DiscrepancyStateId) { itd.DiscrepancyStateId = itd.DiscrepancyState.Id; itd.DiscrepancyState = null; }
                                if (itd.DiscrepancyRisk != null && itd.DiscrepancyRisk.Id != itd.DiscrepancyRiskId) { itd.DiscrepancyRiskId = itd.DiscrepancyRisk.Id; itd.DiscrepancyRisk = null; }
                            });

                            if (it.InspectionTestRunDiscrepancies != null) it.InspectionTestRunDiscrepancies.ToList().ForEach(itrd => {
                                if (itrd.DiscrepancyType != null && itrd.DiscrepancyType.Id != itrd.DiscrepancyTypeId) { itrd.DiscrepancyTypeId = itrd.DiscrepancyType.Id; itrd.DiscrepancyType = null; }
                                if (itrd.DiscrepancyResult != null && itrd.DiscrepancyResult.Id != itrd.DiscrepancyResultId) { itrd.DiscrepancyResultId = itrd.DiscrepancyResult.Id; itrd.DiscrepancyResult = null; }
                                if (itrd.DiscrepancyState != null && itrd.DiscrepancyState.Id != itrd.DiscrepancyStateId) { itrd.DiscrepancyStateId = itrd.DiscrepancyState.Id; itrd.DiscrepancyState = null; }
                                if (itrd.DiscrepancyRisk != null && itrd.DiscrepancyRisk.Id != itrd.DiscrepancyRiskId) { itrd.DiscrepancyRiskId = itrd.DiscrepancyRisk.Id; itrd.DiscrepancyRisk = null; }
                            });
                        });

                        var tmpWorkOrder = (KeyValuePair<Guid, WorkOrder>)Remote.Execute(
                            "THHill", 
                            "SaveWorkOrder", 
                            DataId,
                            WorkOrder,
                            MediaFiles.Select(mf =>
                                new Media
                                {
                                    Id = Guid.Empty,
                                    InternalId = new Guid(mf.IdFile),
                                    Name = mf.IdFile + Path.GetExtension(mf.MediaFile.Path),
                                    Description = mf.MediaFile.Path,
                                    Content = Objects.ReadFully(mf.MediaFile.GetStream())
                                }
                            ).ToList()
                            );

                        if (Remote.Error) errors.Add(Remote.Message);
                        if (tmpWorkOrder.Key == Guid.Empty) errors.Add("No se pudo guardar la orden de trabajo");
                    }
                });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                IsRunning = false;

                if (errors.Count > 0)
                    await App.Current.MainPage.DisplayAlert("Error.", string.Join(" ", errors), "Accept");
                else
                    await NavigationService.GoBackAsync(new NavigationParameters {
                        { "option", "WorkOrders" }
                    });
            }

            return;
        }

        #endregion
    }
}
