﻿using DistributedServices.Controller.DataAccess.Mobile;
using Prism.Navigation;
using System.Collections.Generic;
using THHill.Prism.Models;
using Transversal.Common.Entities.DataBase.THHill;
using Transversal.Common.Entities.Models;

namespace THHill.Prism.Helpers
{
    public class TestRunViewModelBase : ViewModelBase
    {
        #region Members

        protected List<KeyValuePair<string, List<Combo>>> _combos;
        protected InspectionTool _inspectionTool;
        protected List<MediaFileModel> _mediaFiles;

        #region Encapsulation

        public List<KeyValuePair<string, List<Combo>>> Combos
        {
            get => _combos;
            set => SetProperty(ref _combos, value);
        }
        public InspectionTool InspectionTool
        {
            get => _inspectionTool;
            set => SetProperty(ref _inspectionTool, value);
        }
        public List<MediaFileModel> MediaFiles
        {
            get => _mediaFiles;
            set => SetProperty(ref _mediaFiles, value);
        }

        #endregion

        #endregion

        #region Constructor

        public TestRunViewModelBase(INavigationService navigationService, RemoteManager remote) : base(navigationService, remote)
        {

        }

        #endregion

        #region Methods

        public override void Initialize(INavigationParameters parameters)
        {
            Combos = parameters.GetValue<List<KeyValuePair<string, List<Combo>>>>("combos");
            InspectionTool = parameters.GetValue<InspectionTool>("inspectionTool");
            MediaFiles = parameters.GetValue<List<MediaFileModel>>("mediaFiles");
        }

        #endregion
    }
}
