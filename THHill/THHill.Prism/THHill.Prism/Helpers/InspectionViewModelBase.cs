﻿using DistributedServices.Controller.DataAccess.Mobile;
using Prism.Navigation;
using System.Collections.Generic;
using THHill.Prism.Models;
using Transversal.Common.Entities.DataBase.THHill;

namespace THHill.Prism.Helpers
{
    public class InspectionViewModelBase : ViewModelBase
    {
        #region Members

        protected WorkOrder _workOrder;
        protected List<MediaFileModel> _mediaFiles;

        #region Encapsulation

        public WorkOrder WorkOrder
        {
            get => _workOrder;
            set => SetProperty(ref _workOrder, value);
        }
        public List<MediaFileModel> MediaFiles
        {
            get => _mediaFiles;
            set => SetProperty(ref _mediaFiles, value);
        }

        #endregion

        #endregion

        #region Constructor

        public InspectionViewModelBase(INavigationService navigationService, RemoteManager remote) : base(navigationService, remote)
        {
            
        }

        #endregion

        #region Methods

        public override void Initialize(INavigationParameters parameters)
        {
            WorkOrder = parameters.GetValue<WorkOrder>("workOrder");
            MediaFiles = parameters.GetValue<List<MediaFileModel>>("mediaFiles");
        }

        #endregion
    }
}
