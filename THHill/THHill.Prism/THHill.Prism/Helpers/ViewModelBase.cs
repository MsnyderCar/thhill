﻿using DistributedServices.Controller.DataAccess.Mobile;
using Newtonsoft.Json;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using THHill.Prism.Models;
using Transversal.Common.Entities.DataBase.Mobile;
using Transversal.Common.Entities.Models;
using Xamarin.Forms;

namespace THHill.Prism.Helpers
{
    public class ViewModelBase : BindableBase, IInitialize, INavigationAware, IDestructible
    {
        #region Members

        protected RemoteManager Remote;

        protected bool _isRunning;
        protected bool _isBtnEnabled;

        protected string _title;

        protected INavigationService NavigationService { get; private set; }

        #region Encapsulation

        public string Title
        {
            get => _title;
            set => SetProperty(ref _title, value);
        }

        public bool IsRunning
        {
            get => _isRunning;
            set => SetProperty(ref _isRunning, value);
        }

        public bool IsBtnEnabled
        {
            get => _isBtnEnabled;
            set => SetProperty(ref _isBtnEnabled, value);
        }

        #endregion

        #endregion

        #region Constructor

        public ViewModelBase(INavigationService navigationService, RemoteManager remote)
        {
            NavigationService = navigationService;
            Remote = remote;

            IsRunning = false;
            IsBtnEnabled = true;
        }

        #endregion

        #region Methods

        protected async Task<ChangeImageModel> ChangeImageEvent(ChangeImageModel cim)
        {
            await CrossMedia.Current.Initialize();

            var cancel = "Cancelar";
            var galeria = "Galería";
            var camara = "Cámara";

            var source = await Application.Current.MainPage.DisplayActionSheet(
                "De donde desea obtener la imagen?",
                cancel,
                null,
                galeria,
                camara);

            if (source == cancel)
            {
                cim.MediaFile = null;
            }
            else
            {
                if (source == camara)
                {
                    cim.MediaFile = await CrossMedia.Current.TakePhotoAsync(
                        new StoreCameraMediaOptions
                        {
                            Directory = "Sample",
                            Name = "test.jpg",
                            PhotoSize = PhotoSize.Small,
                        }
                    );
                }
                else
                {
                    cim.MediaFile = await CrossMedia.Current.PickPhotoAsync();
                }

                if (cim.MediaFile != null)
                {
                    var idFile = Guid.NewGuid().ToString();

                    if (!string.IsNullOrEmpty(cim.Image)) 
                    {
                        idFile = JsonConvert.DeserializeObject<ContentFile>(cim.Image).Id;
                    }
                    

                    if (string.IsNullOrEmpty(cim.MediaFileModel.IdFile))
                    {
                        cim.MediaFileModel.IdFile = idFile;
                        cim.MediaFileModel.Processed = false;
                    }

                    cim.MediaFileModel.MediaFile = cim.MediaFile;

                    cim.Image = JsonConvert.SerializeObject(new ContentFile
                    {
                        Id = cim.MediaFileModel.IdFile,
                        Name = Path.GetFileName(cim.MediaFileModel.MediaFile.Path),
                        State = "Uploaded",
                        Path = string.Empty
                    });

                    cim.ImageSource = ImageSource.FromStream(() =>
                    {
                        var stream = cim.MediaFile.GetStream();
                        return stream;
                    });
                }
            }

            return cim;
        }

        protected ImageSource GetImageSource(List<MediaFileModel> mediaFiles, string contentFile)
        {
            ImageSource imageSource = ImageSource.FromResource("BotonFoto.png");

            if (!string.IsNullOrEmpty(contentFile))
            {
                var cf = JsonConvert.DeserializeObject<ContentFile>(contentFile);

                var img = mediaFiles.FirstOrDefault(fod => fod.IdFile.Equals(cf.Id));
                if (img != null)
                {
                    imageSource = ImageSource.FromStream(() =>
                    {
                        var stream = img.MediaFile.GetStream();
                        return stream;
                    });
                }
                else
                {
                    var tmpMedia = (Media)Remote.Execute("Core", "GetMedia", new Guid(cf.Id));
                    imageSource = ImageSource.FromStream(() => new MemoryStream(tmpMedia.Content));
                }
            }

            return imageSource;
        }

        protected List<MediaFileModel> UpdateMediaFiles(INavigationParameters parameters, List<MediaFileModel> mediaFiles)
        {
            if (parameters.ContainsKey("mediaFiles"))
            {
                var tmfs = parameters.GetValue<List<MediaFileModel>>("mediaFiles");
                if (tmfs != null && tmfs.Count > 0)
                {
                    tmfs.ForEach(t => {
                        var mfe = mediaFiles.Where(mf => mf.IdFile == t.IdFile).FirstOrDefault();
                        if (mfe == null) mediaFiles.Add(t);
                        else mfe = t;
                    });
                }
            }

            if (parameters.ContainsKey("mediaFile"))
            {
                var tmf = parameters.GetValue<MediaFileModel>("mediaFile");
                if (tmf != null && !string.IsNullOrEmpty(tmf.IdFile))
                {
                    var mfe = mediaFiles.Where(mf => mf.IdFile == tmf.IdFile).FirstOrDefault();
                    if (mfe == null)
                        mediaFiles.Add(tmf);
                    else
                        mfe = tmf;
                }
            }

            return mediaFiles;
        }

        public virtual void Initialize(INavigationParameters parameters)
        {

        }

        public virtual void OnNavigatedFrom(INavigationParameters parameters)
        {

        }

        public virtual void OnNavigatedTo(INavigationParameters parameters)
        {

        }

        public virtual void Destroy()
        {

        }

        #endregion
    }
}
