﻿using DistributedServices.Controller.DataAccess.Mobile;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Prism.Mvvm;
using Prism.Navigation;
using System.Threading.Tasks;
using THHill.Prism.Models;
using Xamarin.Forms;

namespace THHill.Prism.Helpers
{
    public class BasicViewModelBase : BindableBase, IInitialize, INavigationAware, IDestructible
    {
        #region Members

        private bool _isRunning;

        private string _title;

        protected RemoteManager Remote;
        protected INavigationService NavigationService { get; private set; }

        #region Encapsulation

        public string Title
        {
            get => _title;
            set => SetProperty(ref _title, value);
        }

        public bool IsRunning
        {
            get => _isRunning;
            set => SetProperty(ref _isRunning, value);
        }

        #endregion

        #endregion

        #region Constructor

        public BasicViewModelBase(INavigationService navigationService, RemoteManager remote)
        {
            NavigationService = navigationService;
            Remote = remote;
        }

        #endregion

        #region Methods

        protected async Task<MediaFileModel> ChangeImage2(MediaFileModel mfvm)
        {
            MediaFile mf = null;

            await CrossMedia.Current.Initialize();

            var source = await Application.Current.MainPage.DisplayActionSheet(
                "De donde desea obtener la imagen?",
                "Cancel",
                null,
                "Galeria",
                "Camara");

            if (source != "Cancel")
            {
                if (source == "From Camera")
                {
                    mf = await CrossMedia.Current.TakePhotoAsync(
                        new StoreCameraMediaOptions
                        {
                            Directory = "Sample",
                            Name = "test.jpg",
                            PhotoSize = PhotoSize.Small,
                        }
                    );
                }
                else
                {
                    mf = await CrossMedia.Current.PickPhotoAsync();
                }

                if (mf != null)
                {

                    if (mfvm == null)
                    {
                        var idFile = (string)Remote.Execute("Core", "GetNewGuid");
                        mfvm = new MediaFileModel
                        {
                            IdFile = idFile,
                            Processed = false
                        };
                    }

                    mfvm.MediaFile = mf;
                }
            }

            return mfvm;
        }

        public virtual void Initialize(INavigationParameters parameters)
        {

        }

        public virtual void OnNavigatedFrom(INavigationParameters parameters)
        {

        }

        public virtual void OnNavigatedTo(INavigationParameters parameters)
        {

        }

        public virtual void Destroy()
        {

        }

        #endregion
    }
}
