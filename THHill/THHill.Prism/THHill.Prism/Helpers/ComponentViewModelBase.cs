﻿using DistributedServices.Controller.DataAccess.Mobile;
using Prism.Commands;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using THHill.Prism.Models;
using Transversal.Common.Entities.DataBase.THHill;
using Transversal.Common.Entities.Models;

namespace THHill.Prism.Helpers
{
    public class ComponentViewModelBase : ViewModelBase
    {
        #region Members

        protected List<KeyValuePair<string, List<Combo>>> _combos;
        protected Guid _guidTemp;
        protected InspectionComponent _original;
        protected InspectionComponent _copy;
        protected List<MediaFileModel> _mediaFiles;
        protected string _option;

        protected MediaFileModel _mediaFileModel;

        #region Encapsulation

        public List<KeyValuePair<string, List<Combo>>> Combos
        {
            get => _combos;
            set => SetProperty(ref _combos, value);
        }
        public Guid GuidTemp
        {
            get => _guidTemp;
            set => SetProperty(ref _guidTemp, value);
        }
        public InspectionComponent Original
        {
            get => _original;
            set => SetProperty(ref _original, value);
        }
        public InspectionComponent Copy
        {
            get => _copy;
            set => SetProperty(ref _copy, value);
        }
        public List<MediaFileModel> MediaFiles
        {
            get => _mediaFiles;
            set => SetProperty(ref _mediaFiles, value);
        }
        public string Option
        {
            get => _option;
            set => SetProperty(ref _option, value);
        }
        protected MediaFileModel MediaFileModel
        {
            get => _mediaFileModel;
            set => SetProperty(ref _mediaFileModel, value);
        }

        #endregion

        #region Delegates

        public DelegateCommand SaveCommand { get; set; }

        #endregion

        #endregion

        #region Constructor

        public ComponentViewModelBase(INavigationService navigationService, RemoteManager remote) : base(navigationService, remote)
        {
            SaveCommand = new DelegateCommand(Save);
        }

        #endregion

        #region Methods

        public override void Initialize(INavigationParameters parameters)
        {
            Combos = parameters.GetValue<List<KeyValuePair<string, List<Combo>>>>("combos");
            GuidTemp = parameters.GetValue<Guid>("guidTemp");
            Original = parameters.GetValue<InspectionComponent>("original");
            Copy = parameters.GetValue<InspectionComponent>("copy");
            MediaFileModel = parameters.GetValue<MediaFileModel>("mediaFile");
            MediaFiles = parameters.GetValue<List<MediaFileModel>>("mediaFiles");
            Option = parameters.GetValue<string>("option");
            Title = $"{Option} Componente";
        }

        private async void Save()
        {
            var errors = new List<string>();
            try
            {
                await Task.Run(() => {
                    if (Copy.ConditionId == 0)
                        errors.Add("Debe seleccionar una condición.");
                });
            }
            finally
            {
                if (errors.Count == 0)
                {
                    await NavigationService.GoBackAsync(new NavigationParameters {
                        { "guidTemp", GuidTemp },
                        { "original", Original },
                        { "copy", Copy },
                        { "optionComponent", Option },
                        { "mediaFile", MediaFileModel },
                        { "mediaFiles", MediaFiles }
                    });
                }
                else
                {
                    await App.Current.MainPage.DisplayAlert("Error.", string.Join(" ", errors.ToArray()), "Accept");
                }
            }

            return;
        }

        #endregion
    }
}
