﻿using System;
using Plugin.Media.Abstractions;

namespace THHill.Prism.Models
{
    public class MediaFileModel
    {
        public bool Processed { get; set; }
        public string IdFile { get; set; }
        public MediaFile MediaFile { get; set; }
    }
}
