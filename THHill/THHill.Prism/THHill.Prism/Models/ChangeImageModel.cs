﻿using System;
using Plugin.Media.Abstractions;
using Xamarin.Forms;

namespace THHill.Prism.Models
{
    public class ChangeImageModel
    {
        public MediaFileModel MediaFileModel { get; set; }
        public MediaFile MediaFile { get; set; }
        public ImageSource ImageSource { get; set; }
        public string Image { get; set; }
    }
}
