﻿using System.Linq;
using System.Net;
using System.Reflection;
using DistributedServices.Controller.DataAccess.Mobile;
using Infrastructure.Data.Mobile;
using Prism;
using Prism.Ioc;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace THHill.Prism
{
    public partial class App
    {
        protected RemoteManager Remote;

        /* 
         * The Xamarin Forms XAML Previewer in Visual Studio uses System.Activator.CreateInstance.
         * This imposes a limitation in which the App class must have a default constructor. 
         * App(IPlatformInitializer initializer = null) cannot be handled by the Activator.
         */
        public App() : this(null) { }

        public App(IPlatformInitializer initializer) : base(initializer) { }

        protected override async void OnInitialized()
        {
            #if DEBUG
                ServicePointManager.ServerCertificateValidationCallback += (o, certificate, chain, errors) => true;
            #endif

            InitializeComponent();

            //Task.Run(() => (new BackgroundManager(Remote)).Run());

            await NavigationService.NavigateAsync("NavigationPage/LoginPage");
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            var dummy = new Dummy();

            //Service = new Service(App.Current.Resources["UrlAPI"].ToString(), true);

            var isAndroid = Device.RuntimePlatform == Device.Android;
            //#if DEBUG
            //Remote = new RemoteManager("https://192.168.0.106:44388/", isAndroid);
            //#else
            Remote = new RemoteManager("https://www.thars.app/", isAndroid);//new Main(isAndroid)
            //#endif
            containerRegistry.RegisterInstance(Remote);
            
            containerRegistry.RegisterForNavigation<NavigationPage>();

            Assembly.GetAssembly(this.GetType()).GetTypes().Where(x => x.Name.EndsWith("Page")).ToList().ForEach(pAssembly => {
                var vmlAssembly = Assembly.GetAssembly(this.GetType()).GetTypes().Where(x => x.Namespace.Equals(pAssembly.Namespace.Replace("Views","ViewModels")) && x.Name.Equals($"{pAssembly.Name}ViewModel")).FirstOrDefault();
                containerRegistry.RegisterForNavigation(pAssembly, pAssembly.FullName.Replace("THHill.Prism.Views.", ""));
                containerRegistry.RegisterForNavigation(vmlAssembly, vmlAssembly.FullName.Replace("THHill.Prism.ViewModels.", ""));
            });
        }
    }
}
