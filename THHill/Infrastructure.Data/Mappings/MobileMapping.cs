﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Transversal.Common.Entities.DataBase.Mobile;

namespace Infrastructure.Data.Mappings
{
    public class RecordStatesMapping : IEntityTypeConfiguration<RecordState>
    {
        public void Configure(EntityTypeBuilder<RecordState> builder)
        {
            builder.ToTable("RecordStates", "Mobile");

            builder.HasKey(hk => hk.Id);
            builder.Property(p => p.Id).ValueGeneratedNever();

            builder.Property(p => p.Name).HasMaxLength(32);
            builder.Ignore(p => p.Data);
            builder.Ignore(p => p.Media);
        }
    }

    public class DataTypesMapping : IEntityTypeConfiguration<DataType>
    {
        public void Configure(EntityTypeBuilder<DataType> builder)
        {
            builder.ToTable("DataTypes", "Mobile");

            builder.HasKey(hk => hk.Id);
            builder.Property(p => p.Id).ValueGeneratedNever();

            builder.Property(p => p.Name).HasMaxLength(32);
            builder.Property(p => p.NameSpace).HasMaxLength(128);
            builder.Ignore(p => p.Data);
        }
    }
}
