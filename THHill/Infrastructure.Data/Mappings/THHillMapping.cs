﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Transversal.Common.Entities.DataBase;
using Transversal.Common.Entities.DataBase.Authentication;
using Transversal.Common.Entities.DataBase.THHill;

namespace Infrastructure.Data.Mappings
{
    public class ToolTypesMapping : IEntityTypeConfiguration<ToolType>
    {
        public void Configure(EntityTypeBuilder<ToolType> builder)
        {
            builder.ToTable("ToolTypes","THHill");

            builder.HasKey(hk => hk.Id);
            builder.Property(p => p.Id).ValueGeneratedNever();

            builder.Property(p => p.Name).HasMaxLength(64);
        }
    }

    public class ToolsMapping : IEntityTypeConfiguration<Tool>
    {
        public void Configure(EntityTypeBuilder<Tool> builder)
        {
            builder.ToTable("Tools", "THHill");

            builder.HasKey(hk => hk.Id);
            builder.Property(p => p.Id).ValueGeneratedOnAdd();

            builder.Property(p => p.Name).HasMaxLength(256);
            builder.Property(p => p.Number).HasMaxLength(64);
            builder.Property(p => p.PartNumber).HasMaxLength(64);
            builder.Property(p => p.Image).HasMaxLength(1024);
            builder.Property(p => p.Remarks).HasMaxLength(2048);

            builder.HasOne<ToolType>(hr => hr.ToolType)
                .WithMany(wm => wm.Tools)
                .HasForeignKey(hf => hf.ToolTypeId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }

    public class ComponentsMapping : IEntityTypeConfiguration<Component>
    {
        public void Configure(EntityTypeBuilder<Component> builder)
        {
            builder.ToTable("Components", "THHill");

            builder.HasKey(hk => hk.Id);
            builder.Property(p => p.Id).ValueGeneratedOnAdd();

            builder.Property(p => p.Name).HasMaxLength(256);
            builder.Property(p => p.Number).HasMaxLength(32);
            builder.Property(p => p.Image).HasMaxLength(1024);
            builder.Property(p => p.Remarks).HasMaxLength(2048);

            builder.HasOne<Tool>(hr => hr.Tool)
                .WithMany(wm => wm.Components)
                .HasForeignKey(hf => hf.ToolId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }

    public class OperatorsMapping : IEntityTypeConfiguration<Operator>
    {
        public void Configure(EntityTypeBuilder<Operator> builder)
        {
            builder.ToTable("Operators", "THHill");

            builder.HasKey(hk => hk.Id);
            builder.Property(p => p.Id).ValueGeneratedOnAdd();

            builder.Property(p => p.Name).HasMaxLength(256);
            builder.Property(p => p.Email).HasMaxLength(256);
            builder.Property(p => p.Phone).HasMaxLength(16);
            builder.Property(p => p.Address).HasMaxLength(1024);
        }
    }

    public class OilWellsMapping : IEntityTypeConfiguration<OilWell>
    {
        public void Configure(EntityTypeBuilder<OilWell> builder)
        {
            builder.ToTable("OilWells", "THHill");

            builder.HasKey(hk => hk.Id);
            builder.Property(p => p.Id).ValueGeneratedOnAdd();

            builder.Property(p => p.Name).HasMaxLength(256);
            builder.Property(p => p.Description).HasMaxLength(1024);
        }
    }

    public class MonitoringTypesMapping : IEntityTypeConfiguration<MonitoringType>
    {
        public void Configure(EntityTypeBuilder<MonitoringType> builder)
        {
            builder.ToTable("MonitoringTypes", "THHill");

            builder.HasKey(hk => hk.Id);
            builder.Property(p => p.Id).ValueGeneratedOnAdd();

            builder.Property(p => p.Name).HasMaxLength(64);
        }
    }

    public class InspectionCategoriesMapping : IEntityTypeConfiguration<InspectionCategory>
    {
        public void Configure(EntityTypeBuilder<InspectionCategory> builder)
        {
            builder.ToTable("InspectionCategories", "THHill");

            builder.HasKey(hk => hk.Id);
            builder.Property(p => p.Id).ValueGeneratedOnAdd();

            builder.Property(p => p.Name).HasMaxLength(64);
        }
    }

    public class WorkOrderStatesMapping : IEntityTypeConfiguration<WorkOrderState>
    {
        public void Configure(EntityTypeBuilder<WorkOrderState> builder)
        {
            builder.ToTable("WorkOrderStates", "THHill");

            builder.HasKey(hk => hk.Id);
            builder.Property(p => p.Id).ValueGeneratedNever();

            builder.Property(p => p.Name).HasMaxLength(64);
        }
    }

    public class VendorsMapping : IEntityTypeConfiguration<Vendor>
    {
        public void Configure(EntityTypeBuilder<Vendor> builder)
        {
            builder.ToTable("Vendors", "THHill");

            builder.HasKey(hk => hk.Id);
            builder.Property(p => p.Id).ValueGeneratedOnAdd();

            builder.Property(p => p.Name).HasMaxLength(64);
        }
    }

    public class WorkOrdersMapping : IEntityTypeConfiguration<WorkOrder>
    {
        public void Configure(EntityTypeBuilder<WorkOrder> builder)
        {
            builder.ToTable("WorkOrders", "THHill");

            builder.HasKey(hk => hk.Id);
            builder.Property(p => p.Id).ValueGeneratedOnAdd();

            builder.Property(p => p.Drill).HasMaxLength(128);
            builder.Property(p => p.ProjectManager).HasMaxLength(128);
            builder.Property(p => p.BeginDate).HasMaxLength(10);
            builder.Property(p => p.EndDate).HasMaxLength(10);
            builder.Property(p => p.LocationWork).HasMaxLength(256);
            builder.Property(p => p.CustomerSpecification).HasMaxLength(2048);
            builder.Property(p => p.ManualsAndProcedures).HasMaxLength(128);
            builder.Property(p => p.ProviderRepresentative).HasMaxLength(64);
            builder.Property(p => p.InformationToClient).HasMaxLength(2048);

            builder.Property(p => p.InspectionCompany).HasMaxLength(256);
            builder.Property(p => p.Inspector).HasMaxLength(128);
            builder.Property(p => p.TrainingCertification).HasMaxLength(128);
            builder.Property(p => p.ParticleConcentration).HasMaxLength(128);
            builder.Property(p => p.ParticleType).HasMaxLength(128);
            builder.Property(p => p.PenetratingLiquidDate).HasMaxLength(10);
            builder.Property(p => p.DeveloperDate).HasMaxLength(10);
            builder.Property(p => p.InspectionInfo).HasMaxLength(2048);

            builder.HasOne<User>(hr => hr.User)
                .WithMany(wm => wm.WorkOrders)
                .HasForeignKey(hf => hf.UserId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne<Operator>(hr => hr.Operator)
                .WithMany(wm => wm.WorkOrders)
                .HasForeignKey(hf => hf.OperatorId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne<OilWell>(hr => hr.OilWell)
                .WithMany(wm => wm.WorkOrders)
                .HasForeignKey(hf => hf.OilWellId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne<MonitoringType>(hr => hr.MonitoringType)
                .WithMany(wm => wm.WorkOrders)
                .HasForeignKey(hf => hf.MonitoringTypeId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne<InspectionCategory>(hr => hr.InspectionCategory)
                .WithMany(wm => wm.WorkOrders)
                .HasForeignKey(hf => hf.InspectionCategoryId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne<Vendor>(hr => hr.Vendor)
                .WithMany(wm => wm.WorkOrders)
                .HasForeignKey(hf => hf.VendorId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne<WorkOrderState>(hr => hr.WorkOrderState)
                .WithMany(wm => wm.WorkOrders)
                .HasForeignKey(hf => hf.WorkOrderStateId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }

    public class InspectionImagesMapping : IEntityTypeConfiguration<InspectionImage>
    {
        public void Configure(EntityTypeBuilder<InspectionImage> builder)
        {
            builder.ToTable("InspectionImages", "THHill");

            builder.HasKey(hk => hk.Id);
            builder.Property(p => p.Id).ValueGeneratedOnAdd();

            builder.Property(p => p.Description).HasMaxLength(2048);
            builder.Property(p => p.Image).HasMaxLength(1024);
            
            builder.HasOne<WorkOrder>(hr => hr.WorkOrder)
                .WithMany(wm => wm.InspectionImages)
                .HasForeignKey(hf => hf.WorkOrderId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }

    public class DiscrepancyTypesMapping : IEntityTypeConfiguration<DiscrepancyType>
    {
        public void Configure(EntityTypeBuilder<DiscrepancyType> builder)
        {
            builder.ToTable("DiscrepancyTypes", "THHill");

            builder.HasKey(hk => hk.Id);
            builder.Property(p => p.Id).ValueGeneratedNever();

            builder.Property(p => p.Name).HasMaxLength(64);
        }
    }

    public class DiscrepancyResultsMapping : IEntityTypeConfiguration<DiscrepancyResult>
    {
        public void Configure(EntityTypeBuilder<DiscrepancyResult> builder)
        {
            builder.ToTable("DiscrepancyResults", "THHill");

            builder.HasKey(hk => hk.Id);
            builder.Property(p => p.Id).ValueGeneratedNever();

            builder.Property(p => p.Name).HasMaxLength(64);
        }
    }

    public class DiscrepancyStatesMapping : IEntityTypeConfiguration<DiscrepancyState>
    {
        public void Configure(EntityTypeBuilder<DiscrepancyState> builder)
        {
            builder.ToTable("DiscrepancyStates", "THHill");

            builder.HasKey(hk => hk.Id);
            builder.Property(p => p.Id).ValueGeneratedNever();

            builder.Property(p => p.Name).HasMaxLength(64);
        }
    }

    public class DiscrepancyRisksMapping : IEntityTypeConfiguration<DiscrepancyRisk>
    {
        public void Configure(EntityTypeBuilder<DiscrepancyRisk> builder)
        {
            builder.ToTable("DiscrepancyRisks", "THHill");

            builder.HasKey(hk => hk.Id);
            builder.Property(p => p.Id).ValueGeneratedNever();

            builder.Property(p => p.Name).HasMaxLength(64);
        }
    }

    public class InspectionToolsMapping : IEntityTypeConfiguration<InspectionTool>
    {
        public void Configure(EntityTypeBuilder<InspectionTool> builder)
        {
            builder.ToTable("InspectionTools", "THHill");

            builder.HasKey(hk => hk.Id);
            builder.Property(p => p.Id).ValueGeneratedOnAdd();
            builder.Property(p => p.Image).HasMaxLength(1024);

            builder.HasOne<WorkOrder>(hr => hr.WorkOrder)
                .WithMany(wm => wm.InspectionTools)
                .HasForeignKey(hf => hf.WorkOrderId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne<ToolType>(hr => hr.ToolType)
                .WithMany(wm => wm.InspectionTools)
                .HasForeignKey(hf => hf.ToolTypeId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne<Condition>(hr => hr.SubAssemblyCondition)
                .WithMany(wm => wm.InspectionToolSubAssemblies)
                .HasForeignKey(hf => hf.SubAssemblyConditionId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }

    public class InspectionToolDiscrepanciesMapping : IEntityTypeConfiguration<InspectionToolDiscrepancy>
    {
        public void Configure(EntityTypeBuilder<InspectionToolDiscrepancy> builder)
        {
            builder.ToTable("InspectionToolDiscrepancies", "THHill");

            builder.HasKey(hk => hk.Id);
            builder.Property(p => p.Id).ValueGeneratedOnAdd();

            builder.Property(p => p.StandardProcedure).HasMaxLength(1024);
            builder.Property(p => p.Clause).HasMaxLength(1024);
            builder.Property(p => p.Detail).HasMaxLength(1024);
            builder.Property(p => p.CorrectiveAction).HasMaxLength(1024);
            builder.Property(p => p.InspectionCompany).HasMaxLength(1024);
            builder.Property(p => p.Evidence).HasMaxLength(1024);

            builder.HasOne<InspectionTool>(hr => hr.InspectionTool)
                .WithMany(wm => wm.InspectionToolDiscrepancies)
                .HasForeignKey(hf => hf.InspectionToolId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne<DiscrepancyType>(hr => hr.DiscrepancyType)
                .WithMany(wm => wm.InspectionToolDiscrepancies)
                .HasForeignKey(hf => hf.DiscrepancyTypeId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne<DiscrepancyResult>(hr => hr.DiscrepancyResult)
                .WithMany(wm => wm.InspectionToolDiscrepancies)
                .HasForeignKey(hf => hf.DiscrepancyResultId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne<DiscrepancyState>(hr => hr.DiscrepancyState)
                .WithMany(wm => wm.InspectionToolDiscrepancies)
                .HasForeignKey(hf => hf.DiscrepancyStateId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne<DiscrepancyRisk>(hr => hr.DiscrepancyRisk)
                .WithMany(wm => wm.InspectionToolDiscrepancies)
                .HasForeignKey(hf => hf.DiscrepancyRiskId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }

    public class InspectionSubAssemblyImagesMapping : IEntityTypeConfiguration<InspectionSubAssemblyImage>
    {
        public void Configure(EntityTypeBuilder<InspectionSubAssemblyImage> builder)
        {
            builder.ToTable("InspectionSubAssemblyImages", "THHill");

            builder.HasKey(hk => hk.Id);
            builder.Property(p => p.Id).ValueGeneratedOnAdd();

            builder.Property(p => p.Description).HasMaxLength(2048);
            builder.Property(p => p.Image).HasMaxLength(1024);

            builder.HasOne<InspectionTool>(hr => hr.InspectionTool)
                .WithMany(wm => wm.InspectionSubAssemblyImages)
                .HasForeignKey(hf => hf.InspectionToolId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }

    public class InspectionTestRunImagesMapping : IEntityTypeConfiguration<InspectionTestRunImage>
    {
        public void Configure(EntityTypeBuilder<InspectionTestRunImage> builder)
        {
            builder.ToTable("InspectionTestRunImages", "THHill");

            builder.HasKey(hk => hk.Id);
            builder.Property(p => p.Id).ValueGeneratedOnAdd();

            builder.Property(p => p.Description).HasMaxLength(2048);
            builder.Property(p => p.Image).HasMaxLength(1024);

            builder.HasOne<InspectionTool>(hr => hr.InspectionTool)
                .WithMany(wm => wm.InspectionTestRunImages)
                .HasForeignKey(hf => hf.InspectionToolId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }

    public class InspectionTestRunDiscrepanciesMapping : IEntityTypeConfiguration<InspectionTestRunDiscrepancy>
    {
        public void Configure(EntityTypeBuilder<InspectionTestRunDiscrepancy> builder)
        {
            builder.ToTable("InspectionTestRunDiscrepancies", "THHill");

            builder.HasKey(hk => hk.Id);
            builder.Property(p => p.Id).ValueGeneratedOnAdd();

            builder.Property(p => p.StandardProcedure).HasMaxLength(1024);
            builder.Property(p => p.Clause).HasMaxLength(1024);
            builder.Property(p => p.Detail).HasMaxLength(1024);
            builder.Property(p => p.CorrectiveAction).HasMaxLength(1024);
            builder.Property(p => p.InspectionCompany).HasMaxLength(1024);
            builder.Property(p => p.Evidence).HasMaxLength(1024);

            builder.HasOne<InspectionTool>(hr => hr.InspectionTool)
                .WithMany(wm => wm.InspectionTestRunDiscrepancies)
                .HasForeignKey(hf => hf.InspectionToolId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne<DiscrepancyType>(hr => hr.DiscrepancyType)
                .WithMany(wm => wm.InspectionTestRunDiscrepancies)
                .HasForeignKey(hf => hf.DiscrepancyTypeId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne<DiscrepancyResult>(hr => hr.DiscrepancyResult)
                .WithMany(wm => wm.InspectionTestRunDiscrepancies)
                .HasForeignKey(hf => hf.DiscrepancyResultId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne<DiscrepancyState>(hr => hr.DiscrepancyState)
                .WithMany(wm => wm.InspectionTestRunDiscrepancies)
                .HasForeignKey(hf => hf.DiscrepancyStateId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne<DiscrepancyRisk>(hr => hr.DiscrepancyRisk)
                .WithMany(wm => wm.InspectionTestRunDiscrepancies)
                .HasForeignKey(hf => hf.DiscrepancyRiskId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }

    public class ConditionsMapping : IEntityTypeConfiguration<Condition>
    {
        public void Configure(EntityTypeBuilder<Condition> builder)
        {
            builder.ToTable("Conditions", "THHill");

            builder.HasKey(hk => hk.Id);
            builder.Property(p => p.Id).ValueGeneratedNever();

            builder.Property(p => p.Name).HasMaxLength(64);
        }
    }

    public class InspectionComponentsMapping : IEntityTypeConfiguration<InspectionComponent>
    {
        public void Configure(EntityTypeBuilder<InspectionComponent> builder)
        {
            builder.ToTable("InspectionComponents", "THHill");

            builder.HasKey(hk => hk.Id);
            builder.Property(p => p.Id).ValueGeneratedOnAdd();

            builder.Property(p => p.Description).HasMaxLength(2048);
            builder.Property(p => p.Image).HasMaxLength(1024);

            builder.HasOne<InspectionTool>(hr => hr.InspectionTool)
                .WithMany(wm => wm.InspectionComponents)
                .HasForeignKey(hf => hf.InspectionToolId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne<Condition>(hr => hr.Condition)
                .WithMany(wm => wm.InspectionComponents)
                .HasForeignKey(hf => hf.ConditionId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }

    public class InspectionComponentDiscrepanciesMapping : IEntityTypeConfiguration<InspectionComponentDiscrepancy>
    {
        public void Configure(EntityTypeBuilder<InspectionComponentDiscrepancy> builder)
        {
            builder.ToTable("InspectionComponentDiscrepancies", "THHill");

            builder.HasKey(hk => hk.Id);
            builder.Property(p => p.Id).ValueGeneratedOnAdd();

            builder.Property(p => p.StandardProcedure).HasMaxLength(1024);
            builder.Property(p => p.Clause).HasMaxLength(1024);
            builder.Property(p => p.Detail).HasMaxLength(1024);
            builder.Property(p => p.CorrectiveAction).HasMaxLength(1024);
            builder.Property(p => p.InspectionCompany).HasMaxLength(1024);
            builder.Property(p => p.Evidence).HasMaxLength(1024);

            builder.HasOne<InspectionComponent>(hr => hr.InspectionComponent)
                .WithMany(wm => wm.InspectionComponentDiscrepancies)
                .HasForeignKey(hf => hf.InspectionComponentId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne<DiscrepancyType>(hr => hr.DiscrepancyType)
                .WithMany(wm => wm.InspectionComponentDiscrepancies)
                .HasForeignKey(hf => hf.DiscrepancyTypeId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne<DiscrepancyResult>(hr => hr.DiscrepancyResult)
                .WithMany(wm => wm.InspectionComponentDiscrepancies)
                .HasForeignKey(hf => hf.DiscrepancyResultId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne<DiscrepancyState>(hr => hr.DiscrepancyState)
                .WithMany(wm => wm.InspectionComponentDiscrepancies)
                .HasForeignKey(hf => hf.DiscrepancyStateId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne<DiscrepancyRisk>(hr => hr.DiscrepancyRisk)
                .WithMany(wm => wm.InspectionComponentDiscrepancies)
                .HasForeignKey(hf => hf.DiscrepancyRiskId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }

    
}
