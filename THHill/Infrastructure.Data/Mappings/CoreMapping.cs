﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Transversal.Common.Entities.DataBase.Core;

namespace Infrastructure.Data.Mappings
{
    public class ParameterTypesMapping : IEntityTypeConfiguration<ParameterType>
    {
        public void Configure(EntityTypeBuilder<ParameterType> builder)
        {
            builder.ToTable("ParameterTypes", "Core");

            builder.HasKey(hk => hk.Id);
            builder.Property(p => p.Id).ValueGeneratedNever();

            builder.Property(p => p.Name).HasMaxLength(32);
        }
    }

    public class ParametersMapping : IEntityTypeConfiguration<Parameter>
    {
        public void Configure(EntityTypeBuilder<Parameter> builder)
        {
            builder.ToTable("Parameters", "Core");

            builder.HasKey(hk => hk.Id);
            builder.Property(p => p.Id).ValueGeneratedOnAdd();

            builder.Property(p => p.Categories).HasMaxLength(256);
            builder.Property(p => p.Description).HasMaxLength(256);
            builder.Property(p => p.Value).HasMaxLength(8000);

            builder.HasOne<ParameterType>(ho => ho.ParameterType)
                .WithMany(wm => wm.Parameters)
                .HasForeignKey(hfk => hfk.ParameterTypeId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
