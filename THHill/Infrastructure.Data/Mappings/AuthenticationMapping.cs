﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Transversal.Common.Entities.DataBase.Authentication;

namespace Infrastructure.Data.Mappings
{
    public class UserTypesMapping : IEntityTypeConfiguration<UserType>
    {
        public void Configure(EntityTypeBuilder<UserType> builder)
        {
            builder.ToTable("UserTypes", "Authentication");

            builder.HasKey(hk => hk.Id);
            builder.Property(p => p.Id).ValueGeneratedNever();

            builder.Property(p => p.Name).HasMaxLength(64);
        }
    }

    public class UsersMapping : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("Users", "Authentication");

            builder.HasKey(hk => hk.Id);
            builder.Property(p => p.Id).ValueGeneratedOnAdd();

            builder.Property(p => p.Email).HasMaxLength(50);
            builder.Property(p => p.Document).HasMaxLength(20);
            builder.Property(p => p.FirstName).HasMaxLength(50);
            builder.Property(p => p.LastName).HasMaxLength(50);
            builder.Property(p => p.Address).HasMaxLength(100);

            builder.HasOne<UserType>(ho => ho.UserType)
                .WithMany(wm => wm.Users)
                .HasForeignKey(hfk => hfk.UserTypeId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Ignore(p => p.FullName);
        }
    }

    public class PasswordsMapping : IEntityTypeConfiguration<Password>
    {
        public void Configure(EntityTypeBuilder<Password> builder)
        {
            builder.ToTable("Passwords", "Authentication");

            builder.HasKey(hk => hk.Id);
            builder.Property(p => p.Id).ValueGeneratedOnAdd();

            builder.Property(p => p.Value).HasMaxLength(64);
            builder.Property(p => p.Salt).HasMaxLength(16);

            builder.HasOne<User>(ho => ho.User)
                .WithMany(wm => wm.Passwords)
                .HasForeignKey(hfk => hfk.UserId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }

    public class TokensMapping : IEntityTypeConfiguration<TokenDB>
    {
        public void Configure(EntityTypeBuilder<TokenDB> builder)
        {
            builder.ToTable("Tokens", "Authentication");

            builder.HasKey(hk => hk.Id);
            builder.Property(p => p.Id).ValueGeneratedOnAdd();

            builder.Property(p => p.AccessToken).HasMaxLength(36);
            builder.Property(p => p.RefreshToken).HasMaxLength(36);
            builder.Property(p => p.Ip).HasMaxLength(15);

            builder.HasOne<User>(ho => ho.User)
                .WithMany(wm => wm.Tokens)
                .HasForeignKey(hfk => hfk.UserId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }

    public class ModulesMapping : IEntityTypeConfiguration<Module>
    {
        public void Configure(EntityTypeBuilder<Module> builder)
        {
            builder.ToTable("Modules", "Authentication");

            builder.HasKey(hk => hk.Id);
            builder.Property(p => p.Id).ValueGeneratedOnAdd();

            builder.Property(p => p.Name).HasMaxLength(64);
            builder.Property(p => p.Icon).HasMaxLength(64);
            builder.Property(p => p.Controller).HasMaxLength(64);
        }
    }
    
    public class PermissionsMapping : IEntityTypeConfiguration<Permission>
    {
        public void Configure(EntityTypeBuilder<Permission> builder)
        {
            builder.ToTable("Permissions", "Authentication");

            builder.HasKey(hk => hk.Id);
            builder.Property(p => p.Id).ValueGeneratedOnAdd();

            builder.HasOne<UserType>(r => r.UserType)
                .WithMany(m => m.Permissions)
                .HasForeignKey(f => f.UserTypeId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne<Module>(r => r.Module)
               .WithMany(m => m.Permissions)
               .HasForeignKey(f => f.ModuleId)
               .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
