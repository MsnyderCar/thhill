﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Data.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "Authentication");

            migrationBuilder.EnsureSchema(
                name: "Core");

            migrationBuilder.EnsureSchema(
                name: "Mobile");

            migrationBuilder.EnsureSchema(
                name: "THHill");

            migrationBuilder.CreateTable(
                name: "Modules",
                schema: "Authentication",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(maxLength: 64, nullable: true),
                    Order = table.Column<int>(nullable: false),
                    Icon = table.Column<string>(maxLength: 64, nullable: true),
                    Controller = table.Column<string>(maxLength: 64, nullable: true),
                    CanCreate = table.Column<bool>(nullable: false),
                    CanUpdate = table.Column<bool>(nullable: false),
                    CanDelete = table.Column<bool>(nullable: false),
                    CanSave = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Modules", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserTypes",
                schema: "Authentication",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 64, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ParameterTypes",
                schema: "Core",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ParameterTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DataTypes",
                schema: "Mobile",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 32, nullable: true),
                    NameSpace = table.Column<string>(maxLength: 128, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DataTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RecordStates",
                schema: "Mobile",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RecordStates", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Conditions",
                schema: "THHill",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 64, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Conditions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DiscrepancyResults",
                schema: "THHill",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 64, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DiscrepancyResults", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DiscrepancyRisks",
                schema: "THHill",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 64, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DiscrepancyRisks", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DiscrepancyStates",
                schema: "THHill",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 64, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DiscrepancyStates", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DiscrepancyTypes",
                schema: "THHill",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 64, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DiscrepancyTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "InspectionCategories",
                schema: "THHill",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(maxLength: 64, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InspectionCategories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MonitoringTypes",
                schema: "THHill",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(maxLength: 64, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MonitoringTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OilWells",
                schema: "THHill",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    Description = table.Column<string>(maxLength: 1024, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OilWells", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Operators",
                schema: "THHill",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    Phone = table.Column<string>(maxLength: 16, nullable: true),
                    Address = table.Column<string>(maxLength: 1024, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Operators", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ToolTypes",
                schema: "THHill",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 64, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ToolTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Vendors",
                schema: "THHill",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(maxLength: 64, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Vendors", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WorkOrderStates",
                schema: "THHill",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 64, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkOrderStates", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Permissions",
                schema: "Authentication",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    UserTypeId = table.Column<int>(nullable: false),
                    ModuleId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Permissions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Permissions_Modules_ModuleId",
                        column: x => x.ModuleId,
                        principalSchema: "Authentication",
                        principalTable: "Modules",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Permissions_UserTypes_UserTypeId",
                        column: x => x.UserTypeId,
                        principalSchema: "Authentication",
                        principalTable: "UserTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                schema: "Authentication",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    Email = table.Column<string>(maxLength: 50, nullable: true),
                    UserTypeId = table.Column<int>(nullable: false),
                    Document = table.Column<string>(maxLength: 20, nullable: true),
                    FirstName = table.Column<string>(maxLength: 50, nullable: true),
                    LastName = table.Column<string>(maxLength: 50, nullable: true),
                    Address = table.Column<string>(maxLength: 100, nullable: true),
                    AuthToken = table.Column<Guid>(nullable: true),
                    AuthTokenDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Users_UserTypes_UserTypeId",
                        column: x => x.UserTypeId,
                        principalSchema: "Authentication",
                        principalTable: "UserTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Parameters",
                schema: "Core",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    ParameterTypeId = table.Column<int>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    Categories = table.Column<string>(maxLength: 256, nullable: true),
                    Description = table.Column<string>(maxLength: 256, nullable: true),
                    Value = table.Column<string>(maxLength: 8000, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Parameters", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Parameters_ParameterTypes_ParameterTypeId",
                        column: x => x.ParameterTypeId,
                        principalSchema: "Core",
                        principalTable: "ParameterTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Tools",
                schema: "THHill",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    ToolTypeId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    Number = table.Column<string>(maxLength: 64, nullable: true),
                    PartNumber = table.Column<string>(maxLength: 64, nullable: true),
                    Image = table.Column<string>(maxLength: 1024, nullable: true),
                    Remarks = table.Column<string>(maxLength: 2048, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tools", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Tools_ToolTypes_ToolTypeId",
                        column: x => x.ToolTypeId,
                        principalSchema: "THHill",
                        principalTable: "ToolTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Passwords",
                schema: "Authentication",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    Value = table.Column<string>(maxLength: 64, nullable: true),
                    Salt = table.Column<string>(maxLength: 16, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Passwords", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Passwords_Users_UserId",
                        column: x => x.UserId,
                        principalSchema: "Authentication",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Tokens",
                schema: "Authentication",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    AccessToken = table.Column<string>(maxLength: 36, nullable: true),
                    ValidityPeriod = table.Column<int>(nullable: false),
                    RefreshToken = table.Column<string>(maxLength: 36, nullable: true),
                    Ip = table.Column<string>(maxLength: 15, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tokens", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Tokens_Users_UserId",
                        column: x => x.UserId,
                        principalSchema: "Authentication",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WorkOrders",
                schema: "THHill",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    OperatorId = table.Column<Guid>(nullable: false),
                    Drill = table.Column<string>(maxLength: 128, nullable: true),
                    ProjectManager = table.Column<string>(maxLength: 128, nullable: true),
                    OilWellId = table.Column<Guid>(nullable: false),
                    Consecutive = table.Column<int>(nullable: false),
                    BeginDate = table.Column<string>(maxLength: 10, nullable: true),
                    EndDate = table.Column<string>(maxLength: 10, nullable: true),
                    Assignment = table.Column<int>(nullable: false),
                    LocationWork = table.Column<string>(maxLength: 256, nullable: true),
                    MonitoringTypeId = table.Column<Guid>(nullable: false),
                    InspectionCategoryId = table.Column<Guid>(nullable: false),
                    CustomerSpecification = table.Column<string>(maxLength: 2048, nullable: true),
                    ManualsAndProcedures = table.Column<string>(maxLength: 128, nullable: true),
                    ProviderRepresentative = table.Column<string>(maxLength: 64, nullable: true),
                    InformationToClient = table.Column<string>(maxLength: 2048, nullable: true),
                    VendorId = table.Column<Guid>(nullable: true),
                    InspectionCompany = table.Column<string>(maxLength: 256, nullable: true),
                    InspectionToolDate = table.Column<string>(nullable: true),
                    Inspector = table.Column<string>(maxLength: 128, nullable: true),
                    TrainingCertification = table.Column<string>(maxLength: 128, nullable: true),
                    ParticleConcentration = table.Column<string>(maxLength: 128, nullable: true),
                    ParticleType = table.Column<string>(maxLength: 128, nullable: true),
                    BlackLightIntensity = table.Column<double>(nullable: false),
                    PenetratingLiquid = table.Column<bool>(nullable: false),
                    PenetratingLiquidDate = table.Column<string>(maxLength: 10, nullable: true),
                    WhiteLightIntensity = table.Column<double>(nullable: false),
                    Developer = table.Column<bool>(nullable: false),
                    DeveloperDate = table.Column<string>(maxLength: 10, nullable: true),
                    InspectionInfo = table.Column<string>(maxLength: 2048, nullable: true),
                    WorkOrderStateId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkOrders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WorkOrders_InspectionCategories_InspectionCategoryId",
                        column: x => x.InspectionCategoryId,
                        principalSchema: "THHill",
                        principalTable: "InspectionCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WorkOrders_MonitoringTypes_MonitoringTypeId",
                        column: x => x.MonitoringTypeId,
                        principalSchema: "THHill",
                        principalTable: "MonitoringTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WorkOrders_OilWells_OilWellId",
                        column: x => x.OilWellId,
                        principalSchema: "THHill",
                        principalTable: "OilWells",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WorkOrders_Operators_OperatorId",
                        column: x => x.OperatorId,
                        principalSchema: "THHill",
                        principalTable: "Operators",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WorkOrders_Users_UserId",
                        column: x => x.UserId,
                        principalSchema: "Authentication",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WorkOrders_Vendors_VendorId",
                        column: x => x.VendorId,
                        principalSchema: "THHill",
                        principalTable: "Vendors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WorkOrders_WorkOrderStates_WorkOrderStateId",
                        column: x => x.WorkOrderStateId,
                        principalSchema: "THHill",
                        principalTable: "WorkOrderStates",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Components",
                schema: "THHill",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    ToolId = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    Number = table.Column<string>(maxLength: 32, nullable: true),
                    Image = table.Column<string>(maxLength: 1024, nullable: true),
                    Remarks = table.Column<string>(maxLength: 2048, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Components", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Components_Tools_ToolId",
                        column: x => x.ToolId,
                        principalSchema: "THHill",
                        principalTable: "Tools",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "InspectionImages",
                schema: "THHill",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    WorkOrderId = table.Column<Guid>(nullable: false),
                    Description = table.Column<string>(maxLength: 2048, nullable: true),
                    Image = table.Column<string>(maxLength: 1024, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InspectionImages", x => x.Id);
                    table.ForeignKey(
                        name: "FK_InspectionImages_WorkOrders_WorkOrderId",
                        column: x => x.WorkOrderId,
                        principalSchema: "THHill",
                        principalTable: "WorkOrders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "InspectionTools",
                schema: "THHill",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    WorkOrderId = table.Column<Guid>(nullable: false),
                    ToolTypeId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Number = table.Column<string>(nullable: true),
                    PartNumber = table.Column<string>(nullable: true),
                    Remarks = table.Column<string>(nullable: true),
                    AdvancePercent = table.Column<int>(nullable: false),
                    Image = table.Column<string>(maxLength: 1024, nullable: true),
                    AssemblyPercent = table.Column<int>(nullable: false),
                    SubAssemblySerial = table.Column<string>(nullable: true),
                    SubAssemblyDescription = table.Column<string>(nullable: true),
                    SubAssemblyConditionId = table.Column<int>(nullable: false),
                    SubAssemblyComments = table.Column<string>(nullable: true),
                    TestRunPercent = table.Column<int>(nullable: false),
                    TestRunPass = table.Column<bool>(nullable: false),
                    TestRunDescription = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InspectionTools", x => x.Id);
                    table.ForeignKey(
                        name: "FK_InspectionTools_Conditions_SubAssemblyConditionId",
                        column: x => x.SubAssemblyConditionId,
                        principalSchema: "THHill",
                        principalTable: "Conditions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_InspectionTools_ToolTypes_ToolTypeId",
                        column: x => x.ToolTypeId,
                        principalSchema: "THHill",
                        principalTable: "ToolTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_InspectionTools_WorkOrders_WorkOrderId",
                        column: x => x.WorkOrderId,
                        principalSchema: "THHill",
                        principalTable: "WorkOrders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "InspectionComponents",
                schema: "THHill",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    InspectionToolId = table.Column<Guid>(nullable: false),
                    ConditionId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Number = table.Column<string>(nullable: true),
                    Image = table.Column<string>(maxLength: 1024, nullable: true),
                    Remarks = table.Column<string>(nullable: true),
                    Description = table.Column<string>(maxLength: 2048, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InspectionComponents", x => x.Id);
                    table.ForeignKey(
                        name: "FK_InspectionComponents_Conditions_ConditionId",
                        column: x => x.ConditionId,
                        principalSchema: "THHill",
                        principalTable: "Conditions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_InspectionComponents_InspectionTools_InspectionToolId",
                        column: x => x.InspectionToolId,
                        principalSchema: "THHill",
                        principalTable: "InspectionTools",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "InspectionSubAssemblyImages",
                schema: "THHill",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    InspectionToolId = table.Column<Guid>(nullable: false),
                    Description = table.Column<string>(maxLength: 2048, nullable: true),
                    Image = table.Column<string>(maxLength: 1024, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InspectionSubAssemblyImages", x => x.Id);
                    table.ForeignKey(
                        name: "FK_InspectionSubAssemblyImages_InspectionTools_InspectionToolId",
                        column: x => x.InspectionToolId,
                        principalSchema: "THHill",
                        principalTable: "InspectionTools",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "InspectionTestRunDiscrepancies",
                schema: "THHill",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    InspectionToolId = table.Column<Guid>(nullable: false),
                    DiscrepancyTypeId = table.Column<int>(nullable: false),
                    StandardProcedure = table.Column<string>(maxLength: 1024, nullable: true),
                    Clause = table.Column<string>(maxLength: 1024, nullable: true),
                    Detail = table.Column<string>(maxLength: 1024, nullable: true),
                    CorrectiveAction = table.Column<string>(maxLength: 1024, nullable: true),
                    InspectionCompany = table.Column<string>(maxLength: 1024, nullable: true),
                    DiscrepancyResultId = table.Column<int>(nullable: false),
                    DiscrepancyStateId = table.Column<int>(nullable: false),
                    DiscrepancyRiskId = table.Column<int>(nullable: false),
                    Evidence = table.Column<string>(maxLength: 1024, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InspectionTestRunDiscrepancies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_InspectionTestRunDiscrepancies_DiscrepancyResults_DiscrepancyResultId",
                        column: x => x.DiscrepancyResultId,
                        principalSchema: "THHill",
                        principalTable: "DiscrepancyResults",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_InspectionTestRunDiscrepancies_DiscrepancyRisks_DiscrepancyRiskId",
                        column: x => x.DiscrepancyRiskId,
                        principalSchema: "THHill",
                        principalTable: "DiscrepancyRisks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_InspectionTestRunDiscrepancies_DiscrepancyStates_DiscrepancyStateId",
                        column: x => x.DiscrepancyStateId,
                        principalSchema: "THHill",
                        principalTable: "DiscrepancyStates",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_InspectionTestRunDiscrepancies_DiscrepancyTypes_DiscrepancyTypeId",
                        column: x => x.DiscrepancyTypeId,
                        principalSchema: "THHill",
                        principalTable: "DiscrepancyTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_InspectionTestRunDiscrepancies_InspectionTools_InspectionToolId",
                        column: x => x.InspectionToolId,
                        principalSchema: "THHill",
                        principalTable: "InspectionTools",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "InspectionTestRunImages",
                schema: "THHill",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    InspectionToolId = table.Column<Guid>(nullable: false),
                    Description = table.Column<string>(maxLength: 2048, nullable: true),
                    Image = table.Column<string>(maxLength: 1024, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InspectionTestRunImages", x => x.Id);
                    table.ForeignKey(
                        name: "FK_InspectionTestRunImages_InspectionTools_InspectionToolId",
                        column: x => x.InspectionToolId,
                        principalSchema: "THHill",
                        principalTable: "InspectionTools",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "InspectionToolDiscrepancies",
                schema: "THHill",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    InspectionToolId = table.Column<Guid>(nullable: false),
                    DiscrepancyTypeId = table.Column<int>(nullable: false),
                    StandardProcedure = table.Column<string>(maxLength: 1024, nullable: true),
                    Clause = table.Column<string>(maxLength: 1024, nullable: true),
                    Detail = table.Column<string>(maxLength: 1024, nullable: true),
                    CorrectiveAction = table.Column<string>(maxLength: 1024, nullable: true),
                    InspectionCompany = table.Column<string>(maxLength: 1024, nullable: true),
                    DiscrepancyResultId = table.Column<int>(nullable: false),
                    DiscrepancyStateId = table.Column<int>(nullable: false),
                    DiscrepancyRiskId = table.Column<int>(nullable: false),
                    Evidence = table.Column<string>(maxLength: 1024, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InspectionToolDiscrepancies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_InspectionToolDiscrepancies_DiscrepancyResults_DiscrepancyResultId",
                        column: x => x.DiscrepancyResultId,
                        principalSchema: "THHill",
                        principalTable: "DiscrepancyResults",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_InspectionToolDiscrepancies_DiscrepancyRisks_DiscrepancyRiskId",
                        column: x => x.DiscrepancyRiskId,
                        principalSchema: "THHill",
                        principalTable: "DiscrepancyRisks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_InspectionToolDiscrepancies_DiscrepancyStates_DiscrepancyStateId",
                        column: x => x.DiscrepancyStateId,
                        principalSchema: "THHill",
                        principalTable: "DiscrepancyStates",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_InspectionToolDiscrepancies_DiscrepancyTypes_DiscrepancyTypeId",
                        column: x => x.DiscrepancyTypeId,
                        principalSchema: "THHill",
                        principalTable: "DiscrepancyTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_InspectionToolDiscrepancies_InspectionTools_InspectionToolId",
                        column: x => x.InspectionToolId,
                        principalSchema: "THHill",
                        principalTable: "InspectionTools",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "InspectionComponentDiscrepancies",
                schema: "THHill",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    InspectionComponentId = table.Column<Guid>(nullable: false),
                    DiscrepancyTypeId = table.Column<int>(nullable: false),
                    StandardProcedure = table.Column<string>(maxLength: 1024, nullable: true),
                    Clause = table.Column<string>(maxLength: 1024, nullable: true),
                    Detail = table.Column<string>(maxLength: 1024, nullable: true),
                    CorrectiveAction = table.Column<string>(maxLength: 1024, nullable: true),
                    InspectionCompany = table.Column<string>(maxLength: 1024, nullable: true),
                    DiscrepancyResultId = table.Column<int>(nullable: false),
                    DiscrepancyStateId = table.Column<int>(nullable: false),
                    DiscrepancyRiskId = table.Column<int>(nullable: false),
                    Evidence = table.Column<string>(maxLength: 1024, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InspectionComponentDiscrepancies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_InspectionComponentDiscrepancies_DiscrepancyResults_DiscrepancyResultId",
                        column: x => x.DiscrepancyResultId,
                        principalSchema: "THHill",
                        principalTable: "DiscrepancyResults",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_InspectionComponentDiscrepancies_DiscrepancyRisks_DiscrepancyRiskId",
                        column: x => x.DiscrepancyRiskId,
                        principalSchema: "THHill",
                        principalTable: "DiscrepancyRisks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_InspectionComponentDiscrepancies_DiscrepancyStates_DiscrepancyStateId",
                        column: x => x.DiscrepancyStateId,
                        principalSchema: "THHill",
                        principalTable: "DiscrepancyStates",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_InspectionComponentDiscrepancies_DiscrepancyTypes_DiscrepancyTypeId",
                        column: x => x.DiscrepancyTypeId,
                        principalSchema: "THHill",
                        principalTable: "DiscrepancyTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_InspectionComponentDiscrepancies_InspectionComponents_InspectionComponentId",
                        column: x => x.InspectionComponentId,
                        principalSchema: "THHill",
                        principalTable: "InspectionComponents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Passwords_UserId",
                schema: "Authentication",
                table: "Passwords",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Permissions_ModuleId",
                schema: "Authentication",
                table: "Permissions",
                column: "ModuleId");

            migrationBuilder.CreateIndex(
                name: "IX_Permissions_UserTypeId",
                schema: "Authentication",
                table: "Permissions",
                column: "UserTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Tokens_UserId",
                schema: "Authentication",
                table: "Tokens",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_UserTypeId",
                schema: "Authentication",
                table: "Users",
                column: "UserTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Parameters_ParameterTypeId",
                schema: "Core",
                table: "Parameters",
                column: "ParameterTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Components_ToolId",
                schema: "THHill",
                table: "Components",
                column: "ToolId");

            migrationBuilder.CreateIndex(
                name: "IX_InspectionComponentDiscrepancies_DiscrepancyResultId",
                schema: "THHill",
                table: "InspectionComponentDiscrepancies",
                column: "DiscrepancyResultId");

            migrationBuilder.CreateIndex(
                name: "IX_InspectionComponentDiscrepancies_DiscrepancyRiskId",
                schema: "THHill",
                table: "InspectionComponentDiscrepancies",
                column: "DiscrepancyRiskId");

            migrationBuilder.CreateIndex(
                name: "IX_InspectionComponentDiscrepancies_DiscrepancyStateId",
                schema: "THHill",
                table: "InspectionComponentDiscrepancies",
                column: "DiscrepancyStateId");

            migrationBuilder.CreateIndex(
                name: "IX_InspectionComponentDiscrepancies_DiscrepancyTypeId",
                schema: "THHill",
                table: "InspectionComponentDiscrepancies",
                column: "DiscrepancyTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_InspectionComponentDiscrepancies_InspectionComponentId",
                schema: "THHill",
                table: "InspectionComponentDiscrepancies",
                column: "InspectionComponentId");

            migrationBuilder.CreateIndex(
                name: "IX_InspectionComponents_ConditionId",
                schema: "THHill",
                table: "InspectionComponents",
                column: "ConditionId");

            migrationBuilder.CreateIndex(
                name: "IX_InspectionComponents_InspectionToolId",
                schema: "THHill",
                table: "InspectionComponents",
                column: "InspectionToolId");

            migrationBuilder.CreateIndex(
                name: "IX_InspectionImages_WorkOrderId",
                schema: "THHill",
                table: "InspectionImages",
                column: "WorkOrderId");

            migrationBuilder.CreateIndex(
                name: "IX_InspectionSubAssemblyImages_InspectionToolId",
                schema: "THHill",
                table: "InspectionSubAssemblyImages",
                column: "InspectionToolId");

            migrationBuilder.CreateIndex(
                name: "IX_InspectionTestRunDiscrepancies_DiscrepancyResultId",
                schema: "THHill",
                table: "InspectionTestRunDiscrepancies",
                column: "DiscrepancyResultId");

            migrationBuilder.CreateIndex(
                name: "IX_InspectionTestRunDiscrepancies_DiscrepancyRiskId",
                schema: "THHill",
                table: "InspectionTestRunDiscrepancies",
                column: "DiscrepancyRiskId");

            migrationBuilder.CreateIndex(
                name: "IX_InspectionTestRunDiscrepancies_DiscrepancyStateId",
                schema: "THHill",
                table: "InspectionTestRunDiscrepancies",
                column: "DiscrepancyStateId");

            migrationBuilder.CreateIndex(
                name: "IX_InspectionTestRunDiscrepancies_DiscrepancyTypeId",
                schema: "THHill",
                table: "InspectionTestRunDiscrepancies",
                column: "DiscrepancyTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_InspectionTestRunDiscrepancies_InspectionToolId",
                schema: "THHill",
                table: "InspectionTestRunDiscrepancies",
                column: "InspectionToolId");

            migrationBuilder.CreateIndex(
                name: "IX_InspectionTestRunImages_InspectionToolId",
                schema: "THHill",
                table: "InspectionTestRunImages",
                column: "InspectionToolId");

            migrationBuilder.CreateIndex(
                name: "IX_InspectionToolDiscrepancies_DiscrepancyResultId",
                schema: "THHill",
                table: "InspectionToolDiscrepancies",
                column: "DiscrepancyResultId");

            migrationBuilder.CreateIndex(
                name: "IX_InspectionToolDiscrepancies_DiscrepancyRiskId",
                schema: "THHill",
                table: "InspectionToolDiscrepancies",
                column: "DiscrepancyRiskId");

            migrationBuilder.CreateIndex(
                name: "IX_InspectionToolDiscrepancies_DiscrepancyStateId",
                schema: "THHill",
                table: "InspectionToolDiscrepancies",
                column: "DiscrepancyStateId");

            migrationBuilder.CreateIndex(
                name: "IX_InspectionToolDiscrepancies_DiscrepancyTypeId",
                schema: "THHill",
                table: "InspectionToolDiscrepancies",
                column: "DiscrepancyTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_InspectionToolDiscrepancies_InspectionToolId",
                schema: "THHill",
                table: "InspectionToolDiscrepancies",
                column: "InspectionToolId");

            migrationBuilder.CreateIndex(
                name: "IX_InspectionTools_SubAssemblyConditionId",
                schema: "THHill",
                table: "InspectionTools",
                column: "SubAssemblyConditionId");

            migrationBuilder.CreateIndex(
                name: "IX_InspectionTools_ToolTypeId",
                schema: "THHill",
                table: "InspectionTools",
                column: "ToolTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_InspectionTools_WorkOrderId",
                schema: "THHill",
                table: "InspectionTools",
                column: "WorkOrderId");

            migrationBuilder.CreateIndex(
                name: "IX_Tools_ToolTypeId",
                schema: "THHill",
                table: "Tools",
                column: "ToolTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_WorkOrders_InspectionCategoryId",
                schema: "THHill",
                table: "WorkOrders",
                column: "InspectionCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_WorkOrders_MonitoringTypeId",
                schema: "THHill",
                table: "WorkOrders",
                column: "MonitoringTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_WorkOrders_OilWellId",
                schema: "THHill",
                table: "WorkOrders",
                column: "OilWellId");

            migrationBuilder.CreateIndex(
                name: "IX_WorkOrders_OperatorId",
                schema: "THHill",
                table: "WorkOrders",
                column: "OperatorId");

            migrationBuilder.CreateIndex(
                name: "IX_WorkOrders_UserId",
                schema: "THHill",
                table: "WorkOrders",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_WorkOrders_VendorId",
                schema: "THHill",
                table: "WorkOrders",
                column: "VendorId");

            migrationBuilder.CreateIndex(
                name: "IX_WorkOrders_WorkOrderStateId",
                schema: "THHill",
                table: "WorkOrders",
                column: "WorkOrderStateId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Passwords",
                schema: "Authentication");

            migrationBuilder.DropTable(
                name: "Permissions",
                schema: "Authentication");

            migrationBuilder.DropTable(
                name: "Tokens",
                schema: "Authentication");

            migrationBuilder.DropTable(
                name: "Parameters",
                schema: "Core");

            migrationBuilder.DropTable(
                name: "DataTypes",
                schema: "Mobile");

            migrationBuilder.DropTable(
                name: "RecordStates",
                schema: "Mobile");

            migrationBuilder.DropTable(
                name: "Components",
                schema: "THHill");

            migrationBuilder.DropTable(
                name: "InspectionComponentDiscrepancies",
                schema: "THHill");

            migrationBuilder.DropTable(
                name: "InspectionImages",
                schema: "THHill");

            migrationBuilder.DropTable(
                name: "InspectionSubAssemblyImages",
                schema: "THHill");

            migrationBuilder.DropTable(
                name: "InspectionTestRunDiscrepancies",
                schema: "THHill");

            migrationBuilder.DropTable(
                name: "InspectionTestRunImages",
                schema: "THHill");

            migrationBuilder.DropTable(
                name: "InspectionToolDiscrepancies",
                schema: "THHill");

            migrationBuilder.DropTable(
                name: "Modules",
                schema: "Authentication");

            migrationBuilder.DropTable(
                name: "ParameterTypes",
                schema: "Core");

            migrationBuilder.DropTable(
                name: "Tools",
                schema: "THHill");

            migrationBuilder.DropTable(
                name: "InspectionComponents",
                schema: "THHill");

            migrationBuilder.DropTable(
                name: "DiscrepancyResults",
                schema: "THHill");

            migrationBuilder.DropTable(
                name: "DiscrepancyRisks",
                schema: "THHill");

            migrationBuilder.DropTable(
                name: "DiscrepancyStates",
                schema: "THHill");

            migrationBuilder.DropTable(
                name: "DiscrepancyTypes",
                schema: "THHill");

            migrationBuilder.DropTable(
                name: "InspectionTools",
                schema: "THHill");

            migrationBuilder.DropTable(
                name: "Conditions",
                schema: "THHill");

            migrationBuilder.DropTable(
                name: "ToolTypes",
                schema: "THHill");

            migrationBuilder.DropTable(
                name: "WorkOrders",
                schema: "THHill");

            migrationBuilder.DropTable(
                name: "InspectionCategories",
                schema: "THHill");

            migrationBuilder.DropTable(
                name: "MonitoringTypes",
                schema: "THHill");

            migrationBuilder.DropTable(
                name: "OilWells",
                schema: "THHill");

            migrationBuilder.DropTable(
                name: "Operators",
                schema: "THHill");

            migrationBuilder.DropTable(
                name: "Users",
                schema: "Authentication");

            migrationBuilder.DropTable(
                name: "Vendors",
                schema: "THHill");

            migrationBuilder.DropTable(
                name: "WorkOrderStates",
                schema: "THHill");

            migrationBuilder.DropTable(
                name: "UserTypes",
                schema: "Authentication");
        }
    }
}
