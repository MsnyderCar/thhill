﻿using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Data
{
    public class Main : Core.AppContext
    {
        #region Constructor

        public Main() : base() { }

        public Main(DbContextOptions options) : base(options) { }

        public Main(string connectionString) : base(connectionString) { }

        #endregion
    }
}
