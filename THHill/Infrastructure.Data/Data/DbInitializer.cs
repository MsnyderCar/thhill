﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using Transversal.Common.Entities.DataBase.Authentication;
using Transversal.Common.Entities.DataBase.Core;
using Transversal.Common.Entities.DataBase.Mobile;
using Transversal.Common.Entities.DataBase.THHill;

namespace Infrastructure.Data.Data
{
    public class DbInitializer
    {
        public DbInitializer(Main context)
        {
            var migrations = context.Database.GetAppliedMigrations().ToList().Count;

            if (migrations == 0)
            {
                context.Database.Migrate();
                context.Database.EnsureCreated();
            }

            using (var ct = context.Database.BeginTransaction())
            {
                try
                {
                    #region RecordState

                    var RecordStates = context.Set<RecordState>();

                    RecordStates.AddRange(
                        new RecordState { Id = 1, Name = "Local" },
                        new RecordState { Id = 2, Name = "Servidor" }
                        );

                    context.SaveChanges();

                    #endregion

                    #region DataType

                    var DataTypes = context.Set<DataType>();

                    DataTypes.AddRange(
                        new DataType { Id = 1, Name = "User", NameSpace = "Transversal.Common.Entities.DataBase.Authentication" },
                        new DataType { Id = 2, Name = "Password", NameSpace = "Transversal.Common.Entities.DataBase.Authentication" },
                        new DataType { Id = 3, Name = "ComboEntity", NameSpace = "Transversal.Common.Entities.Models" },
                        new DataType { Id = 4, Name = "WorkOrder", NameSpace = "Transversal.Common.Entities.DataBase.THHill" }
                        );

                    context.SaveChanges();

                    #endregion

                    #region Core

                    #region ParameterType

                    var PTBool = new ParameterType { Id = 1, Name = "Yes/No" };
                    var PTInt = new ParameterType { Id = 2, Name = "Integer" };
                    var PTString = new ParameterType { Id = 3, Name = "String" };

                    var ParameterTypes = context.Set<ParameterType>();

                    ParameterTypes.AddRange(
                        PTBool,
                        PTInt,
                        PTString
                        );

                    context.SaveChanges();

                    #endregion

                    #region Parameter

                    var Parameters = context.Set<Parameter>();

                    Parameters.AddRange(
                        // Core
                        new Parameter { Code = "Url", ParameterTypeId = PTInt.Id, Categories = "Core", Description = "Url de publicacion", Value = "https://www.thebeastdev.com" },//http://localhost:52033
                                                                                                                                                                                                             // Security
                        new Parameter { Code = "TokenValidityPeriod", ParameterTypeId = PTInt.Id, Categories = "Security", Description = "Tiempo de refresco del token en minutos", Value = "5" },
                        new Parameter { Code = "AuthTokenValidityPeriod", ParameterTypeId = PTInt.Id, Categories = "Security", Description = "Tiempo de vida del token para crear contraseña en horas", Value = "12" },
                        // Folders
                        new Parameter { Code = "TransferBufferSize", ParameterTypeId = PTInt.Id, Categories = "Folders", Description = "Tamaño del buffer de transmision de archivos", Value = "16384" },
                        new Parameter { Code = "MaxUploadFileSize", ParameterTypeId = PTInt.Id, Categories = "Folders", Description = "Tamaño maximo de archivo a subir (MB)", Value = "10" },
                        // Scheduler
                        new Parameter { Code = "Maintenance", ParameterTypeId = PTBool.Id, Categories = "Scheduler", Description = "Habilita o deshabilita el modo de mantenimiento para todo el sistema", Value = "false" },
                        new Parameter { Code = "RefreshTime", ParameterTypeId = PTInt.Id, Categories = "Scheduler", Description = "Cantidad de segundos en que se refresca el servicio", Value = "20" },
                        // Email
                        new Parameter { Code = "ServerConfig", ParameterTypeId = PTInt.Id, Categories = "Email", Description = "Configuracion de servidor y usuario para envio de emails", Value = "smtp-mail.outlook.com;587;ssl;scudo@scolalegal.com;Sl900517262" },
                        // Business
                        new Parameter { Code = "Consecutive", ParameterTypeId = PTInt.Id, Categories = "Business", Description = "Consecutivo de ordenes de trabajo", Value = "1" }/*,
                        new Parameter { Code = "CreateAccount", ParameterTypeId = PTInt.Id, Categories = "Email", Description = "Plantilla cuando se crea una cuenta", Value = "" },
                        new Parameter { Code = "ChangePassword", ParameterTypeId = PTInt.Id, Categories = "Email", Description = "Plantilla para cambiar contraseña", Value = "" },
                        new Parameter { Code = "ForgotPassword", ParameterTypeId = PTInt.Id, Categories = "Email", Description = "Plantilla para recordar contraseña", Value = "" }*/
                    );

                    context.SaveChanges();

                    #endregion

                    #endregion

                    #region Authentication

                    #region UserType

                    var UTAdministrator = new UserType { Id = 1, Name = "Administrator" };
                    var UTTechnician = new UserType { Id = 2, Name = "Technician" };

                    var AccountTypes = context.Set<UserType>();

                    AccountTypes.AddRange(
                        UTAdministrator,
                        UTTechnician
                        );

                    context.SaveChanges();

                    #endregion

                    #region User

                    var AFirstUser = new User
                    {
                        Email = "administrator@domain.com",
                        UserTypeId = UTAdministrator.Id,
                        FirstName = "Usuario",
                        LastName = "Administrador",
                        Active = true
                    };

                    var ASecondtUser = new User
                    {
                        Email = "m.carvajal@9torch.com",
                        UserTypeId = UTTechnician.Id,
                        FirstName = "Mario",
                        LastName = "Carvajal",
                        Active = true
                    };

                    var Users = context.Set<User>();

                    Users.AddRange(
                        AFirstUser,
                        ASecondtUser
                        );

                    context.SaveChanges();

                    #endregion

                    #region Password

                    var TempSalt = Transversal.Security.Common.GenerarPasswd(4);

                    var PFirstUser = new Password
                    {
                        UserId = AFirstUser.Id,
                        Salt = TempSalt,
                        Value = Transversal.Security.Common.SHA256Hash(string.Concat(TempSalt, "Qwerty16")),
                        Active = true
                    };

                    TempSalt = Transversal.Security.Common.GenerarPasswd(4);

                    var PSecondUser = new Password
                    {
                        UserId = ASecondtUser.Id,
                        Salt = TempSalt,
                        Value = Transversal.Security.Common.SHA256Hash(string.Concat(TempSalt, "123456")),
                        Active = true
                    };

                    var Passwords = context.Set<Password>();

                    Passwords.AddRange(
                        PFirstUser,
                        PSecondUser
                        );

                    context.SaveChanges();

                    #endregion

                    #region Module

                    var Modules = context.Set<Module>();

                    var MTools = new Module { Name = "Tools", Order = 1, Icon = "", Controller = "Tools", CanCreate = true, CanUpdate = true, CanDelete = true, CanSave = true };
                    var MTechnicians = new Module { Name = "Technicians", Order = 1, Icon = "", Controller = "Technicians", CanCreate = true, CanUpdate = true, CanDelete = true, CanSave = true };
                    var MOperators = new Module { Name = "Operators", Order = 1, Icon = "", Controller = "Operators", CanCreate = true, CanUpdate = true, CanDelete = true, CanSave = true };
                    var MOilWells = new Module { Name = "OilWells", Order = 1, Icon = "", Controller = "OilWells", CanCreate = true, CanUpdate = true, CanDelete = true, CanSave = true };
                    var MOInspectionCategories = new Module { Name = "Inspection Categories", Order = 1, Icon = "", Controller = "InspectionCategories", CanCreate = true, CanUpdate = true, CanDelete = true, CanSave = true };
                    var MMonitoringTypes = new Module { Name = "Monitoring Types", Order = 1, Icon = "", Controller = "MonitoringTypes", CanCreate = true, CanUpdate = true, CanDelete = true, CanSave = true };
                    var MWorkOrders = new Module { Name = "WorkOrders", Order = 1, Icon = "", Controller = "WorkOrders", CanCreate = false, CanUpdate = true, CanDelete = false, CanSave = false };

                    Modules.AddRange(
                        MTools,
                        MTechnicians,
                        MOperators,
                        MOilWells,
                        MOInspectionCategories,
                        MMonitoringTypes,
                        MWorkOrders
                        );

                    context.SaveChanges();

                    #endregion

                    #region Permission

                    var Permissions = context.Set<Permission>();

                    Permissions.AddRange(
                        new Permission { UserTypeId = UTAdministrator.Id, ModuleId = MTools.Id },
                        new Permission { UserTypeId = UTAdministrator.Id, ModuleId = MTechnicians.Id },
                        new Permission { UserTypeId = UTAdministrator.Id, ModuleId = MOperators.Id },
                        new Permission { UserTypeId = UTAdministrator.Id, ModuleId = MOilWells.Id },
                        new Permission { UserTypeId = UTAdministrator.Id, ModuleId = MOInspectionCategories.Id },
                        new Permission { UserTypeId = UTAdministrator.Id, ModuleId = MMonitoringTypes.Id },
                        new Permission { UserTypeId = UTAdministrator.Id, ModuleId = MWorkOrders.Id }
                        );

                    context.SaveChanges();

                    #endregion

                    #endregion

                    #region THHill

                    #region ToolType

                    var ToolTypes = context.Set<ToolType>();

                    var TT01 = new ToolType { Id = 1, Name = "Accesorios de Casing" };

                    ToolTypes.AddRange(
                        TT01,
                        new ToolType { Id = 2, Name = "Cabeza de Segmentacion" },
                        new ToolType { Id = 3, Name = "Componentes de Motores" },
                        new ToolType { Id = 4, Name = "Componentes de Tijeras" },
                        new ToolType { Id = 5, Name = "Componentes de Válvulas" },
                        new ToolType { Id = 6, Name = "CRT/CDS " },
                        new ToolType { Id = 7, Name = "Drill Collars" },
                        new ToolType { Id = 8, Name = "Dril Pipe" },
                        new ToolType { Id = 9, Name = "Equipo de Circulacion" },
                        new ToolType { Id = 10, Name = "Equipo de Completamiento" },
                        new ToolType { Id = 11, Name = "Equipo de Fracking" },
                        new ToolType { Id = 12, Name = "Equipo de Manejo & Levante" },
                        new ToolType { Id = 13, Name = "Equipo de Pesca" },
                        new ToolType { Id = 14, Name = "Equipo Direccional" },
                        new ToolType { Id = 15, Name = "Equipos de Medición" },
                        new ToolType { Id = 16, Name = "Estabilizadores" },
                        new ToolType { Id = 17, Name = "HWDP" },
                        new ToolType { Id = 18, Name = "LWD/MWD" },
                        new ToolType { Id = 19, Name = "Moneles" },
                        new ToolType { Id = 20, Name = "Motores" },
                        new ToolType { Id = 21, Name = "Reamers" },
                        new ToolType { Id = 22, Name = "Substitutos" },
                        new ToolType { Id = 23, Name = "Tijeras" },
                        new ToolType { Id = 24, Name = "Trépanos" },
                        new ToolType { Id = 25, Name = "Válvulas" },
                        new ToolType { Id = 26, Name = "Vastagos" }
                        );

                    context.SaveChanges();

                    #endregion

                    #region Operator

                    /*var Operators = context.Set<Operator>();

                    var Operator01 = new Operator { Name = "Operador 01" };
                    var Operator02 = new Operator { Name = "Operador 02" };

                    Operators.AddRange(
                        Operator01,
                        Operator02
                        );

                    context.SaveChanges();*/

                    #endregion

                    #region OilWell

                    /*var OilWells = context.Set<OilWell>();

                    var OilWell01 = new OilWell { Name = "Pozo 01" };
                    var OilWell02 = new OilWell { Name = "Pozo 02" };

                    OilWells.AddRange(
                        OilWell01,
                        OilWell02
                        );

                    context.SaveChanges();*/

                    #endregion

                    #region MonitoringType

                    /*var MonitoringTypes = context.Set<MonitoringType>();

                    MonitoringTypes.AddRange(
                        new MonitoringType { Id = 0, Name = "Sin seleccionar" }
                        );

                    context.SaveChanges();*/

                    #endregion

                    #region InspectionCategory

                    /*var InspectionCategories = context.Set<InspectionCategory>();

                    InspectionCategories.AddRange(
                        new InspectionCategory { Id = 0, Name = "Sin seleccionar" }
                        );

                    context.SaveChanges();*/

                    #endregion

                    #region Vendor

                    var Vendors = context.Set<Vendor>();

                    Vendors.AddRange(
                        new Vendor { Name = "BHGE(Bits)" },
                        new Vendor { Name = "BHGE(Fishing)" },
                        new Vendor { Name = "BHGE(Liner Hangers)" },
                        new Vendor { Name = "BHGE(Pressure Pumping)" },
                        new Vendor { Name = "Dimpor" },
                        new Vendor { Name = "Dragon" },
                        new Vendor { Name = "FEPCO" },
                        new Vendor { Name = "Frank's International" },
                        new Vendor { Name = "HAL(Baroid)" },
                        new Vendor { Name = "HAL(Cementing)" },
                        new Vendor { Name = "HAL(Completion Tools)" },
                        new Vendor { Name = "HAL(HCT)" },
                        new Vendor { Name = "HAL(Liner Hangers)" },
                        new Vendor { Name = "HAL(Subsea)" },
                        new Vendor { Name = "HAL(WCT)" },
                        new Vendor { Name = "HJ Stauble" },
                        new Vendor { Name = "IPS" },
                        new Vendor { Name = "NOV(Drilling Tools)" },
                        new Vendor { Name = "NOV(ReedHycalog)" },
                        new Vendor { Name = "NOV(Tuboscope)" },
                        new Vendor { Name = "SLB(BDT)" },
                        new Vendor { Name = "SLB(Cameron)" },
                        new Vendor { Name = "SLB(Cementing)" },
                        new Vendor { Name = "SLB(Completions)" },
                        new Vendor { Name = "SLB(D & M)" },
                        new Vendor { Name = "SLB(Drilco)" },
                        new Vendor { Name = "SLB(DT)" },
                        new Vendor { Name = "SLB(MI - Swaco)" },
                        new Vendor { Name = "SLB(Well Test)" },
                        new Vendor { Name = "SLB(Wireline)" },
                        new Vendor { Name = "Spec" },
                        new Vendor { Name = "Stabil - Drill" },
                        new Vendor { Name = "Star" },
                        new Vendor { Name = "Superior Energy" },
                        new Vendor { Name = "TAM" },
                        new Vendor { Name = "Tecnosupply" },
                        new Vendor { Name = "Tenaris" },
                        new Vendor { Name = "TIW" },
                        new Vendor { Name = "TR & RS" },
                        new Vendor { Name = "Tubodrilling" },
                        new Vendor { Name = "WIS" },
                        new Vendor { Name = "WFD(Cementing)" },
                        new Vendor { Name = "WFD(Fishing)" },
                        new Vendor { Name = "WFD(Labs)" },
                        new Vendor { Name = "WFD(MPD)" },
                        new Vendor { Name = "WFD(Packers)" },
                        new Vendor { Name = "WFD(TRS)" },
                        new Vendor { Name = "WFD(Wireline)" },
                        new Vendor { Name = "WFD(Drilling Tools)" },
                        new Vendor { Name = "HAL(Drilling Tools)" },
                        new Vendor { Name = "BHGE(Wireline)" },
                        new Vendor { Name = "BHGE(Drilling Tools)" }
                        );

                    context.SaveChanges();



                    #endregion

                    #region WorkOrderState

                    var WorkOrderStates = context.Set<WorkOrderState>();

                    WorkOrderStates.AddRange(
                        new WorkOrderState { Id = 0, Name = "Sin seleccionar" },
                        new WorkOrderState { Id = 1, Name = "Nueva" },
                        new WorkOrderState { Id = 2, Name = "Actualizada" },
                        new WorkOrderState { Id = 3, Name = "Cerrada" }
                        );

                    context.SaveChanges();

                    #endregion

                    #region Condition

                    var Conditions = context.Set<Condition>();

                    Conditions.AddRange(
                        new Condition { Id = 0, Name = "Sin seleccionar" },
                        new Condition { Id = 1, Name = "Aceptada" },
                        new Condition { Id = 2, Name = "Rechazada" },
                        new Condition { Id = 3, Name = "Excepción" },
                        new Condition { Id = 4, Name = "Reparación" }
                        );

                    context.SaveChanges();

                    #endregion

                    #region DiscrepancyType

                    var DiscrepancyTypes = context.Set<DiscrepancyType>();

                    DiscrepancyTypes.AddRange(
                        new DiscrepancyType { Id = 0, Name = "Sin seleccionar" },
                        new DiscrepancyType { Id = 1, Name = "Asignado a otro Cliente" },
                        new DiscrepancyType { Id = 2, Name = "Calibración" },
                        new DiscrepancyType { Id = 3, Name = "Calificación Personal" },
                        new DiscrepancyType { Id = 4, Name = "Daño Mecánico Conexión" },
                        new DiscrepancyType { Id = 5, Name = "Daño Mecánico Cuerpo" },
                        new DiscrepancyType { Id = 6, Name = "Desviacion Procedimiento" },
                        new DiscrepancyType { Id = 7, Name = "Dimensional" },
                        new DiscrepancyType { Id = 8, Name = "Equipo Faltante" },
                        new DiscrepancyType { Id = 9, Name = "Error en Documento" },
                        new DiscrepancyType { Id = 10, Name = "HSE" },
                        new DiscrepancyType { Id = 11, Name = "NDE Indication / Crack" },
                        new DiscrepancyType { Id = 12, Name = "NPT" },
                        new DiscrepancyType { Id = 13, Name = "Personnel Qualification" },
                        new DiscrepancyType { Id = 14, Name = "Prueba Fallida" },
                        new DiscrepancyType { Id = 15, Name = "Trazabilidad" }
                        );

                    context.SaveChanges();

                    #endregion

                    #region DiscrepancyResult

                    var DiscrepancyResults = context.Set<DiscrepancyResult>();

                    DiscrepancyResults.AddRange(
                        new DiscrepancyResult { Id = 0, Name = "Sin seleccionar" },
                        new DiscrepancyResult { Id = 1, Name = "Corrección a Procedimiento" },
                        new DiscrepancyResult { Id = 2, Name = "Rechazado y Reemplazado" },
                        new DiscrepancyResult { Id = 3, Name = "Reparado y Reinspeccionado" },
                        new DiscrepancyResult { Id = 4, Name = "Desviacion aceptada por cliente" },
                        new DiscrepancyResult { Id = 5, Name = "Oportunidad de Mejora" },
                        new DiscrepancyResult { Id = 6, Name = "Prueba exitosa luego de reparar" },
                        new DiscrepancyResult { Id = 7, Name = "Documentacion Verificada" }
                        );

                    context.SaveChanges();

                    #endregion

                    #region DiscrepancyResult

                    var DiscrepancyStates = context.Set<DiscrepancyState>();

                    DiscrepancyStates.AddRange(
                        new DiscrepancyState { Id = 0, Name = "Sin seleccionar" },
                        new DiscrepancyState { Id = 1, Name = "Abierto" },
                        new DiscrepancyState { Id = 2, Name = "Cerrado" }
                        );

                    context.SaveChanges();

                    #endregion

                    #region DiscrepancyRisk

                    var DiscrepancyRisks = context.Set<DiscrepancyRisk>();

                    DiscrepancyRisks.AddRange(
                        new DiscrepancyRisk { Id = 0, Name = "Sin seleccionar" },
                        new DiscrepancyRisk { Id = 1, Name = "Alto" },
                        new DiscrepancyRisk { Id = 2, Name = "Medio" },
                        new DiscrepancyRisk { Id = 3, Name = "Bajo" }
                        );

                    context.SaveChanges();

                    #endregion

                    #endregion

                    ct.Commit();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    ct.Rollback();
                }
            }
        }
    }
}
