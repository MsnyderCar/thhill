﻿using System.IO;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace Presentation.Web.Main
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                //#if DEBUG
                //.UseUrls($"https://*:44388/")
                //#else
                .UseUrls($"http://*:5000/")
                //#endif
                .UseStartup<Startup>()
                .Build();
    }
}
