﻿using System;
using System.Collections.Generic;
using Transversal.Common.Entities.Api;
using Transversal.Common.Entities.DataBase.Authentication;
using Transversal.Common.Entities.Models;

namespace Presentation.Web.Main.Models.Authentication
{
    public class SessionViewModel
    {
        public Token Token;
        public Guid UserId { get; set; }
        public bool IsLogged { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string AuthToken { get; set; }

        public SessionViewModel()
        {
            BeginSession();
        }

        private void BeginSession()
        {
            UserId = Guid.Empty;
            IsLogged = false;
            FullName = string.Empty;
            Email = string.Empty;
            AuthToken = string.Empty;
        }
    }
}
