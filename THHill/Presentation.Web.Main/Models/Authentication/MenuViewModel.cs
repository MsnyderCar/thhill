﻿using System.Collections.Generic;

namespace Presentation.Web.Main.Models.Authentication
{
    public class MenuViewModel
    {
        public int Id { get; set; }
        public string Controller { get; set; }
        public string Title { get; set; }
        public string Icon { get; set; }
        public List<MenuViewModel> Items { get; set; }
    }
}
