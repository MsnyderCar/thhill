﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Transversal.Common.Entities.Models;

namespace Presentation.Web.Main.Models.Main
{
    public class RequestListViewModel
    {
        public List<Filter> Filters { get; set; }
        public int PageNum { get; set; }
        public int PageSize { get; set; }
        public string SortBy { get; set; }
        public string SortDir { get; set; }
        public string AnotherData { get; set; }
    }
}
