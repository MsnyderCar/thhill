﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Presentation.Web.Main.Models.Main
{
    public class ComboViewModel
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}
