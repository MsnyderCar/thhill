﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Presentation.Web.Main.Models.Main
{
    public class ColumnViewModel
    {
        public bool Primary { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
        public string PlaceHolder { get; set; }
        public bool Display { get; set; }
        public bool Visibility { get; set; }
        public bool Required { get; set; }
        public bool OrderBy { get; set; }
    }
}
