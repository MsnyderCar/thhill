﻿using System;

namespace Presentation.Web.Main.Models.Main
{
    public class ResponseViewModel
    {
        #region Members

        public string Code { get; set; }
        public string Message { get; set; }
        public Object Data1 { get; set; }
        public Object Data2 { get; set; }
        public Object Data3 { get; set; }
        public Object Data4 { get; set; }
        public Object Data5 { get; set; }
        public Object Data6 { get; set; }
        public Object Data7 { get; set; }
        public Object Data8 { get; set; }

        #endregion

        #region Constructor

        public ResponseViewModel()
        {
            Code = "1";
            Message = "OK";
        }

        #endregion
    }
}
