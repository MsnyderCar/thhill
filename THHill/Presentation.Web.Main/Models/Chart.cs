﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Presentation.Web.Main.Models
{
    public class report
    {
        #region Constructor

        public report()
        {
            data = new data();
            options = new options();
        }

        #endregion

        #region Members

        public string type { get; set; }
        public data data { get; set; }
        public options options { get; set; }

        #endregion
    }

    public class data
    {
        #region Constructor

        public data()
        {
            labels = new List<string>();
            datasets = new List<dataset>();
        }

        #endregion

        #region Members

        public List<string> labels { get; set; }
        public List<dataset> datasets { get; set; }

        #endregion
    }

    public class dataset
    {
        #region Constructor

        public dataset()
        {
            data = new List<double>();
            fill = true;
            fillColor = "white";
        }

        #endregion

        #region Members

        public bool fill { get; set; }
        public string label { get; set; }
        public string fillColor { get; set; }
        public List<string> pointBackgroundColor { get; set; }
        public List<double> data { get; set; }
        public List<string> backgroundColor { get; set; }
        public List<string> borderColor { get; set; }
        public List<string> pointBorderColor { get; set; }

        #endregion
    }

    public class options
    {
        #region Constructor

        public options()
        {
            bezierCurve = false;
            animation = new animation();
        }

        #endregion

        #region Members

        public bool bezierCurve { get; set; }
        public animation animation { get; set; }
        public legend legend { get; set; }

        #endregion
    }

    public class animation
    {
        #region Constructor

        public animation()
        {
            onComplete = "done()";
        }

        #endregion

        #region Members

        public string onComplete { get; set; }

        #endregion
    }

    public class legend
    {
        #region Constructor

        public legend()
        {
            fontColor = "black";
        }

        #endregion

        #region Members

        public string fontColor { get; set; }

        #endregion
    }

    public class tag
    {
        #region Members

        public string Value { get; set; }
        public float Score { get; set; }

        #endregion
    }
}
