﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using Presentation.Web.Main.Helpers;
using Presentation.Web.Main.Models.Main;
using System;
using System.Collections.Generic;
using Transversal.Common.Entities.DataBase.Core;
using Transversal.Common.Entities.Models;
using Transversal.Common.Utilities;

namespace Presentation.Web.Main.Controllers
{
    public class CoreController : BaseController
    {
        #region Constructor

        public CoreController(string connectionString, IHttpContextAccessor httpContextAccessor) : base(connectionString, httpContextAccessor) { }

        #endregion

        #region JSON Services

        [HttpPost]
        public ResponseViewModel GetNewEntity([FromBody]JObject request)
        {
            var response = new ResponseViewModel();

            try
            {
                var entity = request["Entity"].ToString();
                response.Data1 = ((object)Execute("Core", "GetNewEntity", entity));
            }
            catch (Exception ex)
            {
                response = ProcessException(ex);
            }

            return response;
        }

        [HttpPost]
        public ResponseViewModel GetNewGuid()
        {
            var response = new ResponseViewModel();

            try
            {
                response.Data1 = (string)Execute(
                    "Core",
                    "GetNewGuid"
                );
            }
            catch (Exception ex)
            {
                response = ProcessException(ex);
            }

            return response;
        }

        [HttpPost]
        public ResponseViewModel GetCombo([FromBody]JObject request)
        {
            var response = new ResponseViewModel();

            try
            {
                var entity = request["Entity"].ToString();
                var value = request["Value"] != null ? request["Value"].ToString() : (object)("");

                var options = ((List<Combo>)Execute(
                    "Core",
                    "GetCombo",
                    entity,
                    value
                ));

                if (!options.Exists(o => o.Id.ToString().Equals("0"))) options.Add(new Combo { Id = "0", Value = "Sin seleccionar" });

                response.Data1 = options;
            }
            catch (Exception ex)
            {
                response = ProcessException(ex);
            }

            return response;
        }

        [HttpPost]
        public ResponseViewModel GetDepartments()
        {
            var response = new ResponseViewModel();

            try
            {
                /*response.Data1 = ((List<Department>)Execute(
                    "Core.Core",
                    "GetDepartmentsByFilter",
                    new Department()
                )).OrderBy(o => o.Name).Select(s => new ComboViewModel { Id = s.Id, Value = s.Name }).ToList();*/
            }
            catch (Exception ex)
            {
                response = ProcessException(ex);
            }

            return response;
        }

        [HttpPost]
        public ResponseViewModel GetCities([FromBody]JObject request)
        {
            var response = new ResponseViewModel();

            var departmentId = Convert.ToInt32(request["departmentId"].ToString());

            try
            {
                /*response.Data1 = ((List<City>)Execute(
                    "Core.Core",
                    "GetCitiesByFilter",
                    new City { DepartmentId = departmentId }
                )).OrderBy(o => o.Name).Select(s => new ComboViewModel { Id = s.Id, Value = s.Name }).ToList();*/
            }
            catch (Exception ex)
            {
                response = ProcessException(ex);
            }

            response.Data2 = request["selectedCombo"].ToString();

            return response;
        }

        [HttpPost]
        public List<Parameter> GetParameters([FromBody]JObject request)
        {
            var response = new List<Parameter>();

            try
            {
                response = (List<Parameter>)Execute(
                    "Core",
                    "GetParametersByFilter",
                    new Parameter
                    {
                        Code = request["code"].ToString(),
                        Categories = request["categories"].ToString()
                    }
                );
            }
            catch (Exception ex)
            {
                ProcessException(ex);
            }

            return response;
        }

        public List<Parameter> GetParameters(string code, string categories)
        {
            var response = new List<Parameter>();

            try
            {
                response = (List<Parameter>)Execute(
                    "Core",
                    "GetParametersByFilter",
                    new Parameter
                    {
                        Code = code,
                        Categories = categories
                    }
                );
            }
            catch (Exception ex)
            {
                ProcessException(ex);
            }

            return response;
        }

        [HttpPost]
        public ResponseViewModel GetTransferBufferSize()
        {
            var response = new ResponseViewModel();

            try
            {
                var parameters = (List<Parameter>)Execute(
                    "Core",
                    "GetParametersByFilter",
                    new Parameter
                    {
                        Code = "TransferBufferSize"
                    }
                );

                response.Data1 = Convert.ToInt32(parameters[0].Value);
            }
            catch (Exception ex)
            {
                response = ProcessException(ex);
            }

            return response;
        }

        #endregion

        #region Another Methods

        #endregion
    }
}
