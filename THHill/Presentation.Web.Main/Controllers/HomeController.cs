﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using Presentation.Web.Main.Helpers;
using Presentation.Web.Main.Models.Authentication;
using Presentation.Web.Main.Models.Main;
using Transversal.Common.Entities.Api;
using Transversal.Common.Entities.DataBase.Authentication;
using Transversal.Common.Entities.Models;
using Newtonsoft.Json;
using System.Text;
using Transversal.Common.Utilities;

namespace Presentation.Web.Main.Controllers
{
    public class HomeController : BaseController
    {
        #region Constructor

        public HomeController(string connectionString, IHttpContextAccessor httpContextAccessor) : base(connectionString, httpContextAccessor) { }

        #endregion

        #region JSON Services

        [HttpPost]
        public ResponseViewModel IsLogged()
        {
            var response = new ResponseViewModel();

            try
            {
                var data = GetDataSesion();

                if (!string.IsNullOrEmpty(data.AuthToken))
                {
                    var result = Remote.ValidateAuthToken(data.AuthToken);
                    if (!string.IsNullOrEmpty(result.AnotherData))
                    {
                        var parts = result.AnotherData.Split(";");
                        data.FullName = parts[0];
                        data.Email = parts[1];
                    }
                    else
                    {
                        data.AuthToken = string.Empty;
                    }
                }

                response.Data1 = data.FullName;
                response.Data2 = data.Email;
                response.Data3 = data.IsLogged;
                response.Data6 = data.AuthToken;
                response.Data8 = $"http{(Request.IsHttps ? "s" : "")}://{Request.Host.Value}";

                SetDataSesion(data);
            }
            catch (Exception ex)
            {
                response = ProcessException(ex);
                //response.Message = "Can't connect to service";
            }
            /*
            var encode = Encoding.GetEncoding("iso-8859-1");
            var filePath = "wwwroot/modules/WorkOrders/mainform.json";

            System.IO.File.WriteAllText(filePath, JsonRawData.GuidReplacer(System.IO.File.ReadAllText(filePath, encode)), encode);
            */
            return response;
        }

        [HttpPost]
        public ResponseViewModel LogIn([FromBody]JObject request)
        {
            var response = new ResponseViewModel();

            try
            {
                Remote.Connect(request["Email"].ToString(), request["Password"].ToString(), Request.HttpContext.Connection.RemoteIpAddress.ToString());

                if (!Remote.Error && Remote.Token.Access != Guid.Empty.ToString())
                {
                    var data = GetDataSesion();

                    data.Token = Remote.Token;
                    data.IsLogged = true;

                    SetDataSesion(data);

                    var user = (User)Execute("Authentication", "GetUserById", data.Token.AccountId);

                    if (Remote.Token.Access != data.Token.Access)
                    {
                        data.Token = Remote.Token;
                    }

                    if (user != null && user.Id != Guid.Empty)
                    {
                        data.UserId = user.Id;
                        data.FullName = $"{user.FirstName} {user.LastName}";
                        data.Email = user.Email;
                    }
                    else
                    {
                        data = new SessionViewModel { IsLogged = false, Token = new Token() };

                        response.Code = "2";
                        response.Message = "Email y/o contraseña incorrectos";
                    }

                    SetDataSesion(data);
                }
                else
                {
                    var message = Remote.Message;
                    if (message == null)
                    {
                        response = ProcessException(new Exception(Remote.Message));
                        response.Message = "Error en el sistema";
                    }
                    else
                    {
                        response = ProcessException(new Exception($"{request["Email"].ToString()} - {message}"));
                        response.Message = message;
                    }
                }
            }
            catch (Exception ex)
            {
                var message = Remote.Message;
                if (message == null)
                {
                    response = ProcessException(ex);
                    response.Message = "Error en el sistema";
                }
                else
                {
                    response = ProcessException(new Exception($"{request["Email"].ToString()} - {message}"));
                    response.Message = message;
                }
            }

            return response;
        }

        [HttpPost]
        public ResponseViewModel LogOff()
        {
            var response = new ResponseViewModel();

            try
            {
                SetToken();
                var data = GetDataSesion();
                if (data != null && data.IsLogged)
                {
                    Remote.Disconnect();
                    if (Remote.Error)
                        response = ProcessException(new Exception(Remote.Message));
                }

                SetDataSesion(new SessionViewModel { IsLogged = false, Token = new Token() });
            }
            catch (Exception ex)
            {
                response = ProcessException(ex);
                response.Message = "Can't connect";
            }

            return response;
        }

        [HttpPost]
        public ResponseViewModel GetMenu()
        {
            var response = new ResponseViewModel();

            try
            {
                var data = GetDataSesion();
                response.Data1 = (Menu)Execute("Authentication", "GetMenu", data.UserId);
            }
            catch (Exception ex)
            {
                response = ProcessException(ex);
            }

            return response;
        }

        [HttpPost]
        public ResponseViewModel CreatePassword([FromBody]JObject request)
        {
            var response = new ResponseViewModel();

            try
            {
                var data = GetDataSesion();
                var token = request["Token"].ToString();
                if (string.IsNullOrEmpty(data.AuthToken) || string.IsNullOrEmpty(token) || !data.AuthToken.Equals(token)) throw new Exception("Datos invalidos");

                if (!Remote.CreatePassword(request["Email"].ToString(), request["Password"].ToString(), token))
                {
                    response.Code = "2";
                    response.Message = "Error en el sistema";
                }
            }
            catch (Exception ex)
            {
                response = ProcessException(ex);
            }

            return response;
        }

        [HttpPost]
        public ResponseViewModel ForgotSend([FromBody]JObject request)
        {
            var response = new ResponseViewModel();

            try
            {
                if (!Remote.Forgot(request["Email"].ToString()))
                {
                    response.Code = "2";
                    response.Message = "Error en el sistema";
                }
            }
            catch (Exception ex)
            {
                response = ProcessException(ex);
            }

            return response;
        }

        [HttpPost]
        public ResponseViewModel ChangeSend([FromBody]JObject request)
        {
            var response = new ResponseViewModel();

            try
            {
                var result = (ErrorContainer)Execute(
                    "Authentication",
                    "Change",
                    new User
                    {
                        Email = request["email"].ToString()
                    }
                );

                if (result.InError)
                {
                    response.Code = "2";
                    response.Message = "Error en el sistema";
                }
                else
                {
                    //response.Message = "";
                }
            }
            catch (Exception ex)
            {
                response = ProcessException(ex);
            }

            return response;
        }

        [HttpPost]
        public ResponseViewModel LoadModule([FromBody]JObject request)
        {
            var response = new ResponseViewModel();

            try
            {
                var module = (Transversal.Common.Entities.DataBase.Authentication.Module)Execute("Authentication", "GetModule", new Guid(request["Id"].ToString()));
                response.Data1 = new Transversal.Common.Entities.Models.Module {
                    Id = module.Id,
                    Name = module.Name,
                    Controller = module.Controller + "/",
                    MainForm = JsonConvert.DeserializeObject(System.IO.File.ReadAllText($"wwwroot/modules/{module.Controller}/mainform.json", Encoding.GetEncoding("iso-8859-1"))),
                    Columns = JsonConvert.DeserializeObject(System.IO.File.ReadAllText($"wwwroot/modules/{module.Controller}/columns.json", Encoding.GetEncoding("iso-8859-1"))),
                    Filters = JsonConvert.DeserializeObject(System.IO.File.ReadAllText($"wwwroot/modules/{module.Controller}/filters.json", Encoding.GetEncoding("iso-8859-1"))),
                    CanCreate = module.CanCreate,
                    CanUpdate = module.CanUpdate,
                    CanDelete = module.CanDelete,
                    Value = request["Value"].ToString()
                };
            }
            catch (Exception ex)
            {
                response = ProcessException(ex);
            }

            return response;
        }

        #endregion

        #region Views

        public IActionResult LoginForm()
        {
            return PartialView();
        }

        public IActionResult LandingPage()
        {
            return PartialView();
        }

        public IActionResult Admin()
        {
            return PartialView();
        }

        public IActionResult Dashboard()
        {
            return PartialView();
        }

        public IActionResult ForgotForm()
        {
            return PartialView();
        }

        public IActionResult Change()
        {
            return PartialView();
        }

        #endregion

        #region Privatre Methods

        #endregion
    }
}
