﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using Presentation.Web.Main.Helpers;
using Presentation.Web.Main.Models.Main;
using System;
using System.Collections.Generic;
using Transversal.Common.Entities.DataBase.THHill;

namespace Presentation.Web.Main.Controllers
{
    public class OperatorsController : BaseController
    {
        #region Constructor

        public OperatorsController(string connectionString, IHttpContextAccessor httpContextAccessor) : base(connectionString, httpContextAccessor) { }

        #endregion

        #region JSON Services

        [HttpPost]
        public ResponseViewModel Get([FromBody]JObject request)
        {
            var response = new ResponseViewModel();

            try
            {
                //validatePermissions(ControllerAction.List);

                var id = GetGuidId(request);

                if (id != Guid.Empty)
                {
                    var element = (Operator)Execute("THHill", "GetOperatorById", id);

                    if (element != null)
                    {
                        response.Data1 = element;
                    }
                    else
                    {
                        response.Code = "2";
                        response.Message = "Get error";
                    }
                }
                else
                {
                    response.Data1 = new Operator();
                }
            }
            catch (Exception ex)
            {
                response = ProcessException(ex);
            }

            return response;
        }

        [HttpPost]
        public ResponseViewModel List([FromBody]RequestListViewModel request)
        {
            var response = new ResponseViewModel();

            try
            {
                //validatePermissions(ControllerAction.Delete);

                if (string.IsNullOrEmpty(request.SortBy)) request.SortBy = "Name";

                if (request.SortDir == null) request.SortDir = "A";
                if (request.Filters == null) request.Filters = new List<Transversal.Common.Entities.Models.Filter>();

                var elements = (KeyValuePair<List<Operator>, int>)Execute(
                    "THHill",
                    "GetOperatorsPaged",
                    request.Filters,
                    new List<KeyValuePair<string, bool>> {
                        new KeyValuePair<string, bool>(
                            request.SortBy,
                            !(request.SortDir == "A")
                        )
                    },
                    request.PageSize,
                    request.PageNum
                );

                response.Data1 = elements.Key;
                response.Data2 = elements.Value;

                if (elements.Value == 0)
                {
                    response.Code = "2";
                    response.Message = "No results found";
                }
            }
            catch (Exception ex)
            {
                response = ProcessException(ex);
            }

            return response;
        }

        [HttpPost]
        public ResponseViewModel Save([FromBody]Operator request)
        {
            var response = new ResponseViewModel();

            try
            {
                //validatePermissions(ControllerAction.List);

                if ((Operator)Execute("THHill", "SaveOperator", request) != null)
                {
                    response.Message = "Information stored correctly";
                }
                else
                {
                    response.Code = "2";
                    response.Message = "Could not store information";
                }
            }
            catch (Exception ex)
            {
                response = ProcessException(ex);
            }

            return response;
        }

        [HttpPost]
        public ResponseViewModel Delete([FromBody]JObject request)
        {
            var response = new ResponseViewModel();

            try
            {
                var id = GetGuidId(request);

                if (id != Guid.Empty)
                {
                    if (!(bool)Execute("THHill", "DeleteOperator", id))
                    {
                        response.Code = "2";
                        response.Message = "Error deleting";
                    }
                }
                else
                {
                    response.Code = "2";
                    response.Message = "You have not selected an item to delete";
                }

            }
            catch (Exception ex)
            {
                response = ProcessException(ex);
            }

            return response;
        }

        #endregion

        #region Views

        #endregion
    }
}
