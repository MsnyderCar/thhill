﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using Presentation.Web.Main.Helpers;
using Presentation.Web.Main.Models.Main;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Transversal.Common.Entities.Api;
using Transversal.Common.Entities.DataBase.Authentication;
using Transversal.Common.Entities.DataBase.THHill;

namespace Presentation.Web.Main.Controllers
{
    public class ApiController : BaseController
    {
        #region Constructor

        public ApiController(string connectionString, IHttpContextAccessor httpContextAccessor) : base(connectionString, httpContextAccessor) { }

        #endregion

        #region JSON Services

        [HttpPost]
        public AuthResponse Auth([FromBody]AuthRequest request)
        {
            GetAccessToken();
            return Remote.GenericController.Auth(request);
        }

        [HttpPost]
        public Packet Exec([FromBody]Packet request)
        {
            GetAccessToken();
            return Remote.GenericController.Exec(request, true);
        }

        #endregion

        #region Views

        #endregion

        void GetAccessToken()
        {
            var authorization = HttpContext.Request.Headers.Where(h => h.Key.Equals("Authorization")).Select(h => h.Value.First()).FirstOrDefault();

            if (!string.IsNullOrEmpty(authorization))
            {
                var authBits = authorization.Split(' ');
                if (authBits.Length == 2 && authBits[0].ToLowerInvariant().Equals("bearer"))
                {
                    AccessToken = authBits[1];
                }
            }
        }
    }
}
