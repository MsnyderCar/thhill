﻿using DistributedServices.Controller.DataAccess;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Presentation.Web.Main.Models.Authentication;
using Presentation.Web.Main.Models.Main;
using System;
using System.IO;
using Transversal.Common.Entities.Api;
using Transversal.Common.Entities.Models;

namespace Presentation.Web.Main.Helpers
{
    public class BaseController : Controller
    {
        #region Members

        protected RemoteManager Remote;
        protected string ConnectionString;
        protected string AccessToken;

        private readonly IHttpContextAccessor HttpContextAccessor;
        private new HttpContext HttpContext;

        #endregion

        #region Constructor

        public BaseController(string connectionString, IHttpContextAccessor httpContextAccessor)
        {
            try
            {
                ConnectionString = connectionString;
                HttpContextAccessor = httpContextAccessor;
                HttpContext = httpContextAccessor.HttpContext;
                Remote = new RemoteManager(ConnectionString);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Startup error: {ex.Message} {ex.StackTrace}");
            }
        }

        #endregion

        #region JSON Services

        [HttpPost]
        public ResponseViewModel UploadFilePart([FromBody]StreamFile request)
        {
            var response = new ResponseViewModel();

            try
            {
                if (!(bool)Execute("Core", "UploadFilePart", request))
                    throw new Exception("Can't Upload file part");
            }
            catch (Exception ex)
            {
                response = ProcessException(ex);
            }

            return response;
        }

        [HttpPost]
        public ResponseViewModel DownloadFilePart([FromBody]RequestFile request)
        {
            var response = new ResponseViewModel();

            try
            {
                var basePath = $"wwwroot/repository/core/{request.Id}{Path.GetExtension(request.Name)}";

                if(request.Parts == 0 && System.IO.File.Exists(basePath))
                {
                    var fi = new FileInfo(basePath);
                    double partsQuantity = (double)fi.Length / (double)request.TransferBufferSize;
                    request.Parts = (partsQuantity - (int)partsQuantity > 0) ? (int)partsQuantity + 1 : (int)partsQuantity;
                }

                if(request.Parts == 0) throw new Exception($"Archivo {request.Name} no existe");

                using (Stream source = System.IO.File.OpenRead(basePath))
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        byte[] buffer = new byte[request.TransferBufferSize];

                        int read = 0;

                        source.Seek(request.Part * request.TransferBufferSize, SeekOrigin.Begin);

                        if ((read = source.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            ms.Write(buffer, 0, read);
                        }

                        response.Data1 = new StreamFile
                        {
                            StoredFileName = request.Name,
                            Part = request.Part,
                            Parts = request.Parts,
                            Data = Convert.ToBase64String(ms.ToArray())
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                response = ProcessException(ex);
            }

            return response;
        }

        #endregion

        #region Views

        public IActionResult Index(string id)
        {
            IActionResult view = null;

            var data = GetDataSesion();

            if (data == null)
                data = new SessionViewModel { IsLogged = false, Token = new Token() };

            view = View();

            if (!string.IsNullOrEmpty(id) && IsGuid(id))
            {
                var result = Remote.ValidateAuthToken(id);
                if (!string.IsNullOrEmpty(result.AnotherData))
                {
                    data.AuthToken = id;
                }
                else
                {
                    data.AuthToken = string.Empty;
                    view = Redirect($"http{(Request.IsHttps?"s":"")}://{Request.Host.Value}");
                }
            }

            SetDataSesion(data);

            return view;
        }

        public IActionResult Main()
        {
            return PartialView();
        }

        public IActionResult Actions()
        {
            return PartialView();
        }

        public IActionResult Filters()
        {
            return PartialView();
        }

        public IActionResult Form()
        {
            return PartialView();
        }

        #endregion

        #region Another methods

        public Guid GetGuidId(JObject request)
        {
            var tmpId = request["Id"].ToString();
            return tmpId.Equals("0") ? Guid.Empty : new Guid(request["Id"].ToString());
        }

        protected bool IsGuid(string value)
        {
            Guid x;
            return Guid.TryParse(value, out x);
        }

        protected void SetToken()
        {
            try
            {
                HttpContext.Session.SetString("LastUpdate", DateTime.Now.ToString());
                var data = GetDataSesion();
                if (data != null) Remote.Token = data.Token;
                else
                {
                    data = new SessionViewModel
                    {
                        IsLogged = false,
                        Token = new Token()
                    };
                }
            }
            catch (Exception ex)
            {
                WriteLog(ex);
            }
        }

        protected object Execute(string service, string action, params object[] args)
        {
            SetToken();

            var response = Remote.Execute(service, action, args);

            var data = GetDataSesion();
            if (data != null && Remote.Token.Access != data.Token.Access)
            {
                data.Token = Remote.Token;
                SetDataSesion(data);
            }

            var error = Remote.GetError(Guid.Empty);

            if (error != null && error.InError) throw new Exception(string.Join(", ",error.Message));

            return response;
        }

        #region Session and Globalization

        protected SessionViewModel GetDataSesion()
        {
            SessionViewModel result = new SessionViewModel();
            //var result = new SessionViewModel(Request.HttpContext.Features.Get<IRequestCultureFeature>().RequestCulture.Culture.Name);

            try
            {
                result = new SessionViewModel();
                var data = HttpContext.Session.GetString("Data");
                if (!string.IsNullOrEmpty(data)) result = (SessionViewModel)JsonConvert.DeserializeObject(data, typeof(SessionViewModel));
            }
            catch (Exception ex)
            {
                WriteLog(ex);
            }

            return result;
        }

        protected void SetDataSesion(SessionViewModel data)
        {
            try
            {
                var json = JsonConvert.SerializeObject(data, Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                HttpContext.Session.SetString("Data", json);
            }
            catch (Exception ex)
            {
                WriteLog(ex);
            }
        }

        #endregion

        #region Exception

        protected ResponseViewModel ProcessException(Exception ex)
        {
            WriteLog(ex);

            return new ResponseViewModel
            {
                Code = "2",
                Message = ex.Message //ex.ToString()
            };
        }

        protected void WriteLog(Exception ex)
        {
            var date = DateTime.Now;
            using (StreamWriter sw = System.IO.File.AppendText($"log_{date.ToString("yyyyMMdd")}.txt"))
            {
                sw.WriteLine($"{date.ToString("yyyy-MM-dd HH:mm:ss")}|Exception:{ex.Message}|StackTrace:{ex.StackTrace}");
            }
        }

        #endregion

        #endregion
    }
}
