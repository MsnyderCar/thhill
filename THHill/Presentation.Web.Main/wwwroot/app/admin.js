﻿app.controller('adminCtrl', function ($scope, $document, $compile, service, $timeout) {
    $scope.metaData = {
        components: {},
        cache: {
            grid: '',
            filters: '',
            mainform: '',
            sideform: ''
        },
        controls: [],
        combos: [],
        grids: [],
        charts: {},
        transferBufferSize: 16384,
        files: {
            processIndex: 0,
            isInProcess: false,
            items: [],
            toDownload: {}
        },
        element: {},
        elements: {
            items: []
        },
        filters: { items: [{}] },
        iterator: parseInt('a000', 16)
    };

    $scope.crud = {
        scope: $scope,
        gridId: '',
        gridFilterId: '',
        primary: '',
        internalModal: '',
        currentFile: {},
        renderGrid: function (columns) {
            this.gridId = 'grid_g_' + (Math.round(+new Date() / 1000));
            var component = {
                category: "controls",
                type: "table",
                properties: {
                    id: this.gridId,
                    displayName: $scope.module.metaData.Name,
                    binding: "items",
                    size: "sm",
                    newItem: {},
                    editControls: false,
                    style: "font-size: 0.8rem;",
                    paging: true,
                    order: true,
                    grid: true
                },
                elements: []
            };

            var index = 1;
            for (column in columns) {
                var columnElement = columns[column];
                if (columnElement.display === true && columnElement.visibility === true)
                    component.elements.push({
                        category: "controls",
                        type: "label",
                        properties: {
                            id: this.gridId + '_col_' + index,
                            displayName: columnElement.displayName,
                            binding: columnElement.binding,
                            useHtml: columnElement.useHtml !== undefined && columnElement.useHtml === true ? true : false
                        }
                    });
                else if (columnElement.primary) this.primary = columnElement.binding;
                index++;
            }

            return component;
        },
        renderFilters: function (columns) {
            this.gridFilterId = 'grid_f_' + (Math.round(+new Date() / 1000));
            var component = {
                category: "controls",
                type: "table",
                properties: {
                    id: this.gridFilterId,
                    displayName: "Filtros",
                    binding: "items",
                    size: "sm",
                    newItem: {},
                    editControls: false,
                    style: "font-size: 0.8rem;",
                    paging: false,
                    order: false,
                    grid: true,
                    showIndex: false,
                    enableSelect: false,
                    hideFindButton: true
                },
                elements: []
            };

            $scope.metaData.filters = { items: [{}] };

            for (column in columns) {
                var columnElement = columns[column];
                if (columnElement.properties.binding.length > 0) eval("$scope.metaData.filters.items[0]." + columnElement.properties.binding + " = '0'");
                component.elements.push(columnElement);
            }

            return component;
        },
        changeView: function (edit) {
            // List
            //$('#sideFilter').collapse(edit === true ? 'hide' : 'show');
            $('#mainGrid').collapse(edit === true ? 'hide' : 'show');
            if ($scope.module.metaData.Filters.length > 0) $('#mainFilter').collapse(edit === true ? 'hide' : 'show');
            // Edit
            //$('#sideForm').collapse(edit === true ? 'show' : 'hide');
            $('#mainForm').collapse(edit === true ? 'show' : 'hide');
            // TopMenu

            $scope.layout.topMenu = [];

            if (edit === true) {
                if ($scope.module.metaData.CanCreate === true || $scope.module.metaData.CanUpdate === true) $scope.layout.topMenu.push({ value: 'Save', action: $scope.crud.save });
                if ($scope.metaData.cache.grid !== '') $scope.layout.topMenu.push({ value: 'Cancel', action: $scope.crud.cancel });
            }
            else {
                if ($scope.module.metaData.CanCreate === true) $scope.layout.topMenu.push({ value: 'New', action: $scope.crud.new });
                if ($scope.module.metaData.CanUpdate === true) $scope.layout.topMenu.push({ value: 'Edit', action: $scope.crud.edit });
                if ($scope.module.metaData.CanDelete === true) $scope.layout.topMenu.push({ value: 'Delete', action: $scope.crud.deleteConfirmation });
            }
        },
        new: function () {
            $scope.crud.get(true);
        },
        edit: function () {
            $scope.crud.get(false);
        },
        get: function (isNew, noGridValue) {
            var grid = $scope.metaData.grids[this.gridId];
            var id = isNew ? 0 : (noGridValue !== undefined ? noGridValue : $scope.metaData.elements.items[grid.itemSelected - 1][this.primary]);
            service.request(
                $scope.module.metaData.Controller + 'get',// cambiar por ruta del modulo
                {
                    Id: id
                },
                function (response) {
                    if (response.Code === '1') {
                        $scope.metaData.files = {
                            processIndex: 0,
                            items: []
                        };
                        $scope.metaData.element = response.Data1;
                        $scope.render.methods.general.renderFromHtml('mainForm', $scope.metaData.cache.mainform);

                        $timeout(function () {
                            $scope.crud.changeView(true);
                        }, 1);
                    }
                    else app.messages.show(response.Message, 'warning');
                }
            );
        },
        list: function () {
            var grid = $scope.metaData.grids[this.gridId];

            var filters = [];

            if ($scope.module.metaData.Filters.length > 0) {
                for (filter in $scope.module.metaData.Filters) {
                    var element = $scope.module.metaData.Filters[filter];
                    if (element.properties.binding.length) filters.push({ Member: element.properties.binding, Value: $scope.metaData.filters.items[0][element.properties.binding] });
                }
            }
            service.request(
                $scope.module.metaData.Controller + 'list',
                {
                    PageNum: grid.pageSelected - 1,
                    PageSize: grid.pageSize,
                    SortBy: grid.sortBy,
                    SortDir: grid.sortDirection,
                    Filters: filters
                },
                function (response) {
                    $scope.metaData.elements.items = response.Data1;
                    $scope.crud.changeView(false);
                    $scope.render.methods.tables.updatePagination($scope.crud.gridId, response.Data2);
                    if (response.Code !== '1') app.messages.show(response.Message, 'warning');
                }
            );
        },
        closeInternalModal: function () {
            $scope.layout.modal.close($scope.crud.internalModal);
            $scope.crud.internalModal = '';
        },
        save: function () {
            if ($scope.metaData.files.items.filter(obj => { return obj.processed === false; }).length > 0) $scope.crud.processFiles();
            else {
                if ($scope.crud.internalModal !== '') { $scope.crud.closeInternalModal(); }
                service.request(
                    $scope.module.metaData.Controller + 'save',// cambiar por ruta del modulo
                    $scope.metaData.element,
                    function (response) {
                        if (response.Code === '1') {
                            app.messages.show(response.Message, 'success');
                            $scope.crud.list();
                            $scope.crud.changeView(false);
                        }
                        else app.messages.show(response.Message, 'warning');
                    }
                );
            }
        },
        processFiles: function () {
            $scope.crud.currentFile = $scope.metaData.files.items.filter(obj => { return obj.processed === false; })[0];
            if ($scope.crud.internalModal === '') $scope.crud.internalModal = $scope.layout.modal.add('Subiendo archivos', 'lg', false, { content: '{{metaData.realFileName}} parte {{metaData.part}} de {{metaData.parts}}' }, undefined, $scope.crud.currentFile);
            var size = $scope.crud.currentFile.data.length;
            var begin = $scope.crud.currentFile.part * $scope.metaData.transferBufferSize;
            var end = begin + $scope.metaData.transferBufferSize < size ? begin + $scope.metaData.transferBufferSize : size;
            service.request(
                'core/uploadFilePart',
                {
                    StoredFileName: $scope.crud.currentFile.storedFileName,
                    Part: $scope.crud.currentFile.part,
                    Parts: $scope.crud.currentFile.parts,
                    Data: $scope.crud.currentFile.data.substring(begin, end)
                },
                function (response) {
                    if (response.Code === '1') {
                        $scope.crud.currentFile.part++;
                        if ($scope.crud.currentFile.part >= $scope.crud.currentFile.parts) {
                            $scope.crud.currentFile.processed = true;
                            if ($scope.crud.internalModal !== '') { $scope.crud.closeInternalModal(); }
                        }
                        $scope.crud.save();
                    }
                    else {
                        app.messages.show(response.Message, 'warning');
                        if ($scope.crud.internalModal !== '') { $scope.crud.closeInternalModal(); }
                    }
                }
            );
        },
        deleteConfirmation: function () {
            $scope.crud.internalModal = $scope.layout.modal.add(
                'Eliminar registro',//Title
                'lg',//Size
                false,//Closeable
                { content: 'Desea eliminar el registro?' },//Body
                { content: '<button type="button" class="btn btn-danger" ng-click="methods.delete()">Delete</button><button type="button" class="btn btn-secondary" data-dismiss="modal" ng-click="methods.close()">Close</button>' },//Footer
                $scope.metaData.element,
                {
                    close: function () {
                        $scope.crud.closeInternalModal();
                    },
                    delete: function () {
                        $scope.crud.delete();
                    }
                }
            );
        },
        delete: function () {
            var grid = $scope.metaData.grids[this.gridId];
            service.request(
                $scope.module.metaData.Controller + 'delete',// cambiar por ruta del modulo
                {
                    Id: $scope.metaData.elements.items[grid.itemSelected - 1][$scope.crud.primary]
                },
                function (response) {
                    if (response.Code === '1') {
                        $scope.crud.closeInternalModal();
                        app.messages.show(response.Message, 'success');
                        $scope.crud.list();
                        $scope.crud.changeView(false);
                    }
                    else app.messages.show(response.Message, 'warning');
                }
            );
        },
        cancel: function () {
            $scope.crud.changeView(false);
        },
        print: function (id) {
            $timeout(function () {
                var outerHTML = document.getElementById($scope.render.methods.general.idParser(id)).outerHTML.trim();
                var canvas = outerHTML.match(/<canvas [^>]*><\/canvas>/g);
                for (element in canvas) {
                    var canElement = canvas[element];
                    var tmpId = canElement.match(/id=\s*\\*"(.+?)\\*"/g)[0].replace('id="', '').replace('"', '');
                    
                    var imageData = $scope.metaData.charts[tmpId.substring(0, tmpId.length - 2)].canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");
                    //var imageData = $('#' + tmpId)[0].toDataURL("image/png").replace("image/png", "image/octet-stream");
                    htmlImg = '<img src="' + imageData + '" style="width: 100%;">';
                    outerHTML = outerHTML.replace(canElement, htmlImg);
                }

                newWin = window.open("");

                var template = '<html><head><title></title>';
                template += '<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">';
                template += '</head><body onload="window.print();">'; //window.onfocus = function () { window.close(); }
                template += outerHTML;
                template += '<script src="vendor/jquery/jquery.min.js"></script>';
                template += '<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>';
                template += '</body></html>';
                newWin.document.write(template);
                newWin.document.close(); // necessary for IE >= 10
                newWin.focus();
                //newWin.print();
                //newWin.close();
            }, 1);
        },
        beginDownload: function (file) {
            file = JSON.parse(file);
            if (file.state === 'Uploaded') {
                $scope.crud.induceDownload($scope.crud.b64ToArray($scope.metaData.files.items[$scope.metaData.files.items.findIndex(item => item.id === file.id)].data),file.name);
            }
            else {
                $scope.metaData.files.toDownload = {
                    id: file.id,
                    name: file.name,
                    part: 0,
                    parts: 0,
                    byteArrays: []
                };

                if ($scope.crud.internalModal === '') $scope.crud.internalModal = $scope.layout.modal.add('Subiendo archivos', 'lg', false, { content: '{{metaData.name}} parte {{metaData.part}} de {{metaData.parts}}' }, undefined, $scope.metaData.files.toDownload);

                $scope.crud.download();
            }
            
        },
        download: function () {
            var file = $scope.metaData.files.toDownload;
            service.request(
                'core/downloadFilePart',
                {
                    Id: file.id,
                    Name: file.name,
                    Part: file.part,
                    Parts: file.parts,
                    TransferBufferSize: $scope.metaData.transferBufferSize
                },
                function (result) {
                    var file = $scope.metaData.files.toDownload;
                    if (result.Code === "1") {
                        $scope.metaData.files.toDownload.byteArrays = $scope.metaData.files.toDownload.byteArrays.concat($scope.crud.b64ToArray(result.Data1.Data));
                        if (file.parts === 0) file.parts = result.Data1.Parts;
                        file.part++;

                        if (file.part >= file.parts) {
                            if ($scope.crud.internalModal !== '') $scope.crud.closeInternalModal();
                            $scope.crud.induceDownload($scope.metaData.files.toDownload.byteArrays, file.name);
                        }
                        else $scope.crud.download();
                    }
                    else {
                        app.messages.show(response.Message, 'warning');
                        if ($scope.crud.internalModal !== '') $scope.crud.closeInternalModal();
                    }
                }
            );
        },
        induceDownload: function (byteArrays, fileName) {
            var blobFinal = $scope.crud.arrayToBlob(byteArrays);

            if (typeof navigator.msSaveOrOpenBlob !== 'undefined') {
                return navigator.msSaveOrOpenBlob(blobFinal, fileName);
            } else if (typeof navigator.msSaveBlob !== 'undefined') {
                return navigator.msSaveBlob(blobFinal, fileName);
            }

            var hyperlink = document.createElement('a');
            hyperlink.href = URL.createObjectURL(blobFinal);
            hyperlink.target = '_blank';
            hyperlink.download = fileName;

            if (!!navigator.mozGetUserMedia) {
                hyperlink.onclick = function () {
                    (document.body || document.documentElement).removeChild(hyperlink);
                };
                (document.body || document.documentElement).appendChild(hyperlink);
            }

            var evt = new MouseEvent('click', {
                view: window,
                bubbles: true,
                cancelable: true
            });

            hyperlink.dispatchEvent(evt);

            if (!navigator.mozGetUserMedia) {
                URL.revokeObjectURL(hyperlink.href);
            }
        },
        b64ToArray: function (b64Data, sliceSize) {
            sliceSize = sliceSize || 512;
            var byteCharacters = atob(b64Data);
            var byteArrays = [];

            for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                var slice = byteCharacters.slice(offset, offset + sliceSize);

                var byteNumbers = new Array(slice.length);
                for (var i = 0; i < slice.length; i++) {
                    byteNumbers[i] = slice.charCodeAt(i);
                }

                var byteArray = new Uint8Array(byteNumbers);

                byteArrays.push(byteArray);
            }

            return byteArrays;
        },
        arrayToBlob: function (byteArrays, contentType) {
            return new Blob(byteArrays, { type: contentType || '' });
        }
        // TO DO: Agregar botones de accion en la parte inferior derecha con sus respectivos iconos y tooltips
    };

    $scope.module = {
        metaData: {
            Name: ''
        },
        methods: {
            run: function (value) {
                if ($scope.metaData.cache.filters === '') $scope.render.methods.general.renderFromHtml('mainFilter', $scope.metaData.cache.filters);

                if ((value !== undefined && value !== 0 && value !== '0') || ($scope.metaData.cache.grid === '' && $scope.metaData.cache.filters === '')) {
                    if ($scope.metaData.cache.grid === '' && $scope.metaData.cache.filters === '') value = 1;
                    $scope.crud.get(false, value);
                }
                else {
                    $scope.render.methods.general.renderFromHtml('mainGrid', $scope.metaData.cache.grid);
                    $scope.render.methods.general.renderFromHtml('mainFilter', $scope.metaData.cache.filters);
                    $timeout(function () {
                        $scope.crud.list();
                        $scope.crud.changeView(false);
                    }, 1);
                }
            },
            load: function (id, value) {
                if ($scope.module.metaData.Id === undefined || $scope.module.metaData.Id !== id) {
                    service.request(
                        'home/LoadModule',
                        {
                            Id: id !== undefined ? id : 1,
                            Value: value !== undefined ? value : 0
                        },
                        function (response) {
                            var grids = $scope.metaData.grids;
                            if (grids.length > 1) {
                                for (grid in grids) {
                                    delete $scope.metaData.grids[grids[grid]];
                                }
                            }
                            var combos = $scope.metaData.combos;
                            if (combos.length > 1) {
                                for (combo in combos) {
                                    delete $scope.metaData.combos[combos[combo]];
                                }
                            }

                            $scope.module.metaData = response.Data1;
                            $scope.metaData.cache.grid = $scope.module.metaData.Columns.length > 0 ? $scope.render.methods.general.getHtml($scope.crud.renderGrid($scope.module.metaData.Columns), 'metaData.elements') : '';
                            $scope.metaData.cache.filters = $scope.module.metaData.Filters.length > 0 ? $scope.render.methods.general.getHtml($scope.crud.renderFilters($scope.module.metaData.Filters), 'metaData.filters') : '';
                            $scope.metaData.cache.mainform = $scope.render.methods.general.getHtml($scope.module.metaData.MainForm);
                            //$scope.metaData.cache.sideform = $scope.render.methods.general.getHtml($scope.module.metaData.SideForm);
                            
                            $scope.module.methods.run($scope.module.metaData.Value);
                        }
                    );
                }
                else $scope.module.methods.run(value);
            }
        }
    };

    /* 
        Falta:
        * Agregar validacion de editabilidad o eliminacion de registros en componente repeat y table
    */
    $scope.render = {
        components: {
            layout: {
                row: function (id, obj, isRepeat, isColumn, binding, size) {
                    return '<div id="' + id + '" class="row clearfix"' + $scope.render.methods.general.showCondition(obj, binding) + ' style="margin-bottom:10px;">' + $scope.render.methods.general.renderComponents(obj, isRepeat, isColumn, binding) + '</div>';
                },
                col: function (id, obj, isRepeat, isColumn, binding, size) {
                    return '<div id="' + id + '" class="col-md-' + obj.properties.size + '"' + $scope.render.methods.general.showCondition(obj, binding) + '>' + $scope.render.methods.general.renderComponents(obj, isRepeat, isColumn, binding) + '</div>';
                }
            },
            containers: {
                group: function (id, obj, isRepeat, isColumn, binding, size) {
                    return '<div id="' + id + '" class="card" style="margin-bottom:10px;">'
                        + '<div id="' + id + '_h" class="card-header">'
                        + '<label id="' + id + '_hl" class="form-label"' + (obj.properties.collapse ? ' data-toggle="collapse" data-target="#' + id + '_b"' : '') + ' style="color:#252525;">' + (obj.properties.binding !== undefined && obj.properties.binding.length > 0 ? '{{metaData.element.' + obj.properties.binding + '}}' : $scope.render.methods.general.replace(obj.properties.displayName, '[binding].', binding)) + '</label>'
                        + '</div>'
                        + '<div id="' + id + '_b"' + (obj.properties.collapse ? ' class="collapse' + (!obj.properties.collapsed ? ' show' : '') + '"' : '') + '><div class="card-body">' + $scope.render.methods.general.renderComponents(obj, isRepeat, isColumn, binding) + '</div></div>'
                        + '</div>';
                },
                panel: function (id, obj, isRepeat, isColumn, binding, size) {
                    return '<div id="' + id + '" class="card">'
                        + '<div class="card-body">' + $scope.render.methods.general.renderComponents(obj, isRepeat, isColumn, binding) + '</div>'
                        + '</div>';
                },
                tab: function (id, obj, isRepeat, isColumn, binding, size) {
                    var tabs = '';
                    var containers = '';
                    var index = 0;
                    for (element in obj.elements) {
                        var tabElement = obj.elements[element];//class="active"
                        tabElement.parent = obj.properties.id;
                        tabs += '<li class="nav-item"><a class="nav-link' + (index === 0 ? ' active' : '') + '" data-toggle="tab" href="#' + tabElement.properties.id + '">' + tabElement.properties.displayName + '</a></li>';
                        containers += '<div id="' + id + '" class="tab-pane container ' + (index === 0 ? 'active' : 'fade') + '">' + $scope.render.methods.general.renderComponents(tabElement, isRepeat, isColumn, binding) + '</div>';
                        index++;
                    }
                    return '<ul class="nav nav-tabs">' + tabs + '</ul><div class="tab-content">' + containers + '</div>';
                },
                // To depreciate
                card: function (id, obj, isRepeat, isColumn, binding, size) {
                    return '<div id="' + id + '" class="card">' + $scope.render.methods.general.renderComponents(obj, isRepeat, isColumn, binding) + '</div>';
                },
                cardHeader: function (id, obj, isRepeat, isColumn, binding, size) {
                    return '<div id="' + id + '" class="card-header">' + $scope.render.methods.general.renderComponents(obj, isRepeat, isColumn, binding) + '</div>';
                },
                cardBody: function (id, obj, isRepeat, isColumn, binding, size) {
                    return '<div id="' + id + '" class="card-body">' + $scope.render.methods.general.renderComponents(obj, isRepeat, isColumn, binding) + '</div>';
                },
                cardFooter: function (id, obj, isRepeat, isColumn, binding, size) {
                    return '<div id="' + id + '" class="card-footer">' + $scope.render.methods.general.renderComponents(obj, isRepeat, isColumn, binding) + '</div>';
                }
            },
            controls: {
                button: function (id, obj, isRepeat, isColumn, binding, size) {
                    var control = '<button id="' + id + '" type="button"' + (obj.properties.style !== undefined && obj.properties.style.length > 0 ? ' class="btn btn-' + obj.properties.style + '"' : '') + (obj.properties.action !== undefined && obj.properties.action.length > 0 ? ' ng-click="' + $scope.render.methods.general.replace(obj.properties.action, '[binding]', binding) + '"' : '') + ' ' + $scope.render.methods.general.canUpdateWhenUpdate(obj, binding) + '>' + (binding !== undefined && binding.length > 0 && obj.properties.binding !== undefined && obj.properties.binding.length > 0 ? '{{' + binding + '}}' : obj.properties.displayName) + '</button>';
                    return isColumn ? control : $scope.render.components.controls.labelinternal(id, control, obj.properties, size);
                },
                chart: function (id, obj, isRepeat, isColumn, binding, size) {
                    return '<div id="' + id + '" ng-init="render.methods.chart.init(\'' + id + '\',' + binding + ')"></div>';
                },
                check: function (id, obj, isRepeat, isColumn, binding, size) {
                    var control = '<input id="' + id + '" type="checkbox" class="form-check-input' + (size !== undefined && size.length > 0 ? ' form-control-' + size : '') + '" ng-model="' + binding + '" ng-true-value="true" ng-false-value="false" ' + $scope.render.methods.general.canUpdateWhenUpdate(obj, binding) + '>';
                    return isColumn ? control : $scope.render.components.controls.labelinternal(id, control, obj.properties, size);
                },
                comboclassic: function (id, obj, isRepeat, isColumn, binding, size) {
                    var onchange = obj.properties.child !== undefined && obj.properties.child.length > 0 ? 'render.methods.combos.onChange(' + $scope.render.methods.general.replace(binding, '.' + obj.properties.binding, '') + ',\'' + obj.properties.id.substr(0, 36) + '\',\'' + obj.properties.child.substr(0, 36) + '\')' : '';
                    if (onchange.length === 0 && obj.properties.onchange !== undefined && obj.properties.onchange.length > 0) onchange = 'render.methods.' + obj.properties.onchange.replace('[bindingbase]', $scope.render.methods.general.replace(binding, '.' + obj.properties.binding, ''));
                    onchange = onchange.length > 0 ? ' ng-change="' + onchange + ';"' : '';

                    var control = '<select id="' + id + '" class="form-control show-tick' + (size !== undefined && size.length > 0 ? ' form-control-' + size : '') + '" '
                        + 'ng-model="' + binding + '" '
                        + 'ng-init="render.methods.combos.getData(\'' + obj.properties.service + '\'' + (obj.properties.parent !== undefined && obj.properties.parent.length > 0 ? ',' + $scope.render.methods.general.replace(binding, '.' + obj.properties.binding, '') + ',\'' + obj.properties.parent.substr(0, 36) + '\'' : '') + ')" '
                        + 'ng-options="combo.Id as combo.Value for combo in render.methods.combos.getElements(\'' + obj.properties.service + '\'' + (obj.properties.parent !== undefined && obj.properties.parent.length > 0 ? ',' + $scope.render.methods.general.replace(binding, '.' + obj.properties.binding, '') + ',\'' + obj.properties.parent.substr(0, 36) + '\'' : '') + ')"'
                        + onchange
                        + $scope.render.methods.general.canUpdateWhenUpdate(obj, binding)
                        + '>'
                        + '<option>' + obj.properties.placeHolder + '</option>'
                        + '</select>';
                    return isColumn ? control : $scope.render.components.controls.labelinternal(id, control, obj.properties, size);
                },
                // Aplicar mismas correcciones que comboclassic
                combo: function (id, obj, isRepeat, isColumn, binding, size) {
                    var repeatId = 'e_' + obj.properties.id;
                    var control = '<div class="dropdown" ng-init="render.methods.combos.getData(\'' + obj.properties.service + '\'' + (obj.properties.parent !== undefined && obj.properties.parent.length > 0 ? ',' + $scope.render.methods.general.replace(binding, '.' + obj.properties.binding, '') + '[render.methods.general.getControlById(\'' + obj.properties.parent + '\').properties.binding]' : '') + ')">'
                        + '<button id="' + id + '" class="btn btn-secondary dropdown-toggle' + (size !== undefined && size.length > 0 ? ' btn-' + size : '') + '" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{render.methods.combos.getValue(\'' + obj.properties.service + '\',' + binding + ',\'' + obj.properties.placeHolder + '\')}}</button>'
                        + '<div class="dropdown-menu" aria-labelledby="' + id + '">'
                        + '<a class="dropdown-item" href="javascript:void(0)" ng-repeat="' + repeatId + ' in metaData.combos[\'' + obj.properties.service + '\']" ng-click="' + binding + ' = ' + repeatId + '.id;' + (obj.properties.child !== undefined && obj.properties.child.length > 0 ? $scope.render.methods.general.replace(binding, '.' + obj.properties.binding, '') + '[render.methods.general.getControlById(\'' + obj.properties.child + '\').properties.binding] = \'0\';render.methods.combos.getData(render.methods.general.getControlById(\'' + obj.properties.child + '\').properties.service,' + binding + ');' : '') + '">{{' + repeatId + '.value}}</a>'
                        + '</div>'
                        + '</div>';
                    return isColumn ? control : $scope.render.components.controls.labelinternal(id, control, obj.properties, size);
                },
                date: function (id, obj, isRepeat, isColumn, binding, size) {
                    var control = '<input id="' + id + '" type="text" readonly autocomplete="false" class="form-control' + (size !== undefined && size.length > 0 ? ' form-control-' + size : '') + '" datepicker="" ng-model="' + binding + '" dp-format="yyyy-mm-dd" />';
                    return isColumn ? control : $scope.render.components.controls.labelinternal(id, control, obj.properties, size);
                },
                image: function (id, obj, isRepeat, isColumn, binding, size) {
                    var control = '<img id="' + id + '" ng-src="{{' + (obj.properties.bindingupload === undefined ? binding : 'render.methods.upload.getDataFile(' + binding + ',\'' + obj.properties.bindingupload + '\')') + '}}" class="rounded float-left" alt="{{' + obj.properties.displayName + '}}"' + (obj.properties.style !== undefined ? 'style="' + obj.properties.style + '"' : '') + '>';
                    return isColumn ? control : $scope.render.components.controls.labelinternal(id, control, obj.properties, size);
                },
                label: function (id, obj, isRepeat, isColumn, binding, size) {
                    var usesBinding = obj.properties.binding !== undefined && obj.properties.binding.length > 0;
                    var usesHtml = obj.properties.useHtml !== undefined && obj.properties.useHtml === true;
                    return '<div class="form-group"><label id="' + id + '" class="form-label' + (size !== undefined && size.length > 0 ? ' form-control-' + size : '') + '" ' + (usesBinding && !usesHtml ? ' ng-bind-html="' + (obj.properties.method !== undefined ? obj.properties.method.replace('binding', binding) : binding) + '"' : '') + (obj.properties.style !== null && obj.properties.style !== undefined ? ' style ="' + obj.properties.style + '"': '') + '>' + (!usesBinding ? obj.properties.displayName : (usesBinding && usesHtml ? '{{' + binding + '}}':'')) + '</label></div>';
                },
                labelinternal: function (id, control, properties, size) {
                    if (properties.labelshow === undefined || properties.labelshow === true) {
                        var newControl = '';
                        if (properties.labelposition === undefined || properties.labelposition === '') properties.labelposition = 'left';
                        var label = '<label for="' + id + '" class="col-form-label' + (size !== undefined && size.length > 0 ? ' form-control-' + size : '') + (properties.labelposition === 'left' ? ' col-sm-6' : '') + '">' + properties.displayName + '</label>';
                        
                        switch (properties.labelposition) {
                            case "up":
                                newControl = '<div class="form-group">' + label + control + '</div>';
                                break;

                            case "down":
                                newControl = '<div class="form-group">' + control + label + '</label></div>';
                                break;
                            case "left":
                                newControl = '<div class="form-group row">' + label + '<div class="col-sm-6">' + control + '</div></div>';
                                break;
                        }
                        return newControl;
                    }
                    else return control;
                },
                richtextbox: function (id, obj, isRepeat, isColumn, binding, size) {
                    var parts = binding.split('.');
                    var attr = parts.pop();
                    var bindingTmp = parts.join('.');
                    var control = '<textarea id="rt_' + id + '" ng-init="render.methods.richtext.activate(' + (isColumn === true ? '\'rt_' + $scope.render.methods.general.idParser(obj.properties.id) + '_\' + $index' : '\'rt_' + $scope.render.methods.general.idParser(obj.properties.id) + '\'') + ',' + bindingTmp + ',\'' + attr + '\')"></textarea>';
                    return isColumn ? control : $scope.render.components.controls.labelinternal(id, control, obj.properties, size);
                },
                // TO DO: Habilitar campo para restringir actualizacion o eliminacion de elementos
                // TO DO: Poner filtro en modulo responder y evaluacion para solo traer preguntas activas
                table: function (id, obj, isRepeat, isColumn, binding, size) {
                    var columns = '';
                    var containers = '';
                    var repeatId = $scope.render.methods.general.getRepeatId();

                    var idTemp = id;
                    if (idTemp.includes('{{$index}}', 0)) idTemp = idTemp.replace('{{$index}}', '\' + $index + \'');

                    for (element in obj.elements) {
                        var columnElement = obj.elements[element];//class="active"
                        if (obj.properties.canUpdateWhenUpdate !== undefined && obj.properties.canUpdateWhenUpdate === false) columnElement.properties.canUpdateWhenUpdate = false;
                        var style = 'white-space:nowrap;'
                            + (obj.properties.order !== undefined && obj.properties.order === true ? 'cursor:pointer;' : '')
                            + (obj.properties.style !== undefined ? obj.properties.style : '');
                        columns += '<th scope="col" ' + (obj.properties.orderBy !== undefined && obj.properties.orderBy === true ? ' ng-click="render.methods.tables.changeOrder(\'' + idTemp + '\',' + binding + ',\'' + columnElement.properties.binding + '\')"' : '') + ' style="' + style + '">' + columnElement.properties.displayName + '</th>';
                        containers += '<td style="white-space:nowrap;">' + $scope.render.methods.general.renderComponent(columnElement, true, true, repeatId, size) + '</td>';
                    }
                    var editControls = obj.properties.editControls;

                    editControls = editControls === undefined || editControls === false ? '' : ''
                        + '<button type="button" title="Adicionar" class="btn btn-outline-secondary btn-sm" ng-click="render.methods.tables.newItem(\'' + idTemp + '\',' + binding + ')"><i class="fas fa-plus"></i></button>&nbsp;'
                        + '<button type="button" title="Eliminar" class="btn btn-outline-secondary btn-sm" ng-click="render.methods.tables.getSelect(\'' + idTemp + '\') !== 0 ? render.methods.tables.delItem(\'' + idTemp + '\',\'' + binding + '\') : null" ng-disabled="render.methods.tables.getSelect(\'' + idTemp + '\') === 0" ><i class="fas fa-trash"></i></button>';

                    var paging = obj.properties.paging;

                    paging = paging === undefined || paging === false ? '' : ''
                        + '<span class="badge badge-pill " ng-repeat="page in render.methods.tables.getPages(\'' + id + '\')" ng-class="page.num !== render.methods.tables.getPageSelected(\'' + idTemp + '\') ? \'badge-light\' : \'badge-secondary\'" ng-style="{\'cursor\' : (page.num !== render.methods.tables.getPageSelected(\'' + idTemp + '\') ? \'pointer\' : \'\')}" ng-click="page.num !== render.methods.tables.getPageSelected(\'' + idTemp + '\') ? render.methods.tables.setPageSelected(\'' + idTemp + '\', page.num) : null">{{page.num}}</span>';

                    var showIndex = obj.properties.showIndex === undefined || obj.properties.showIndex === true;

                    return '<div class="card">'
                        + '<div class="card-header"><div class="row clearfix"><div class="col-md-11"><label style="color:#252525;">' + (obj.properties.displayName !== undefined && obj.properties.displayName.length > 0 ? obj.properties.displayName : '') + '</label></div><div class="col-md-1">' + (obj.properties.hideFindButton !== undefined && obj.properties.hideFindButton === true ? '' : '<button type="button" class="btn btn-sm btn-primary" ng-click="crud.list()">Buscar</button>') + '</div></div ></div>'
                        + '<div class="card-body">'
                        + '<div class="table-responsive' + (size !== undefined && size === 'sm' ? ' table-sm' : '') + '" ' + (obj.properties.style !== undefined ? 'style="' + obj.properties.style + '"' : '') + '>'
                        + '<table id="' + id + '" class="table table-hover' + (size !== undefined && size === 'sm' ? ' table-sm' : '') + '" ng-init="render.methods.tables.init(\'' + idTemp + '\',' + binding + ',' + (obj.properties.paging !== undefined && obj.properties.paging === true) + ')">'
                        + '<thead><tr>'
                        + '<th scope="col">' + (!showIndex ? '&nbsp;' : '#') + '</th>'
                        + columns
                        + '</tr></thead>'
                        + '<tbody><tr ng-repeat="' + repeatId + ' in render.methods.tables.getElements(\'' + idTemp + '\',' + binding + ')" ng-class="$index+1 === render.methods.tables.getSelect(\'' + idTemp + '\') ? \'table-primary\' : \'\'" ' + (obj.properties.enableSelect !== undefined && obj.properties.enableSelect === false ? '' : 'ng-click="render.methods.tables.setSelect(\'' + idTemp + '\',$index+1)" ng-dblclick="crud.edit()"') + '>'
                        + '<th scope="row">' + (!showIndex ? '&nbsp;' : '{{' + (paging.length === 0 ? '' : 'render.methods.tables.getPagePivot(\'' + id + '\')') + ' + $index + 1}}') + '</th>'
                        + containers
                        + '</tr></tbody>'
                        + '</table>'
                        + '</div>'
                        + '</div>'
                        + (editControls.length === 0 && paging.length === 0 ? '' : '<div class="card-footer"><div class="row clearfix"><div class="col-md-2">' + editControls + '</div><div class="col-md-8 text-center">' + paging + '</div><div class="col-md-2"></div></div></div>')
                        + '</div>';
                },
                // TO DO: Habilitar campo para restringir actualizacion o eliminacion de elementos
                repeat: function (id, obj, isRepeat, isColumn, binding, size) {
                    //var repeatId = 'e_' + id;
                    var repeatId = $scope.render.methods.general.getRepeatId();

                    var idTemp = id;
                    if (idTemp.includes('{{$index}}', 0)) idTemp = idTemp.replace('{{$index}}', '\' + $index + \'');

                    var editControls = obj.properties.editControls;

                    editControls = editControls === undefined || editControls === false ? '' : ''
                        + '<button type="button" title="Adicionar" class="btn btn-outline-secondary btn-sm" ng-click="render.methods.tables.newItem(\'' + idTemp + '\',' + binding + ')"><i class="fas fa-plus"></i></button>&nbsp;'
                        + '<button type="button" title="Eliminar" class="btn btn-outline-secondary btn-sm" ng-click="render.methods.tables.getSelect(\'' + idTemp + '\') !== 0 ? render.methods.tables.delItem(\'' + idTemp + '\',\'' + binding + '\') : null" ng-disabled="render.methods.tables.getSelect(\'' + idTemp + '\') === 0" ><i class="fas fa-trash"></i></button>';

                    var paging = obj.properties.paging;

                    paging = paging === undefined || paging === false ? '' : ''
                        + '<span class="badge badge-pill " ng-repeat="page in render.methods.tables.getPages(\'' + id + '\')" ng-class="page.num !== render.methods.tables.getPageSelected(\'' + idTemp + '\') ? \'badge-light\' : \'badge-secondary\'" ng-style="{\'cursor\' : (page.num !== render.methods.tables.getPageSelected(\'' + idTemp + '\') ? \'pointer\' : \'\')}" ng-click="page.num !== render.methods.tables.getPageSelected(\'' + idTemp + '\') ? render.methods.tables.setPageSelected(\'' + idTemp + '\', page.num) : null">{{page.num}}</span>';

                    var showIndex = obj.properties.showIndex === undefined || obj.properties.showIndex === true;

                    var whenUpdate = {};

                    if (obj.properties.canUpdateWhenUpdate !== undefined && obj.properties.canUpdateWhenUpdate === false) whenUpdate.canUpdateWhenUpdate = false;

                    return '<div id="' + id + '" class="card" ng-init="render.methods.tables.init(\'' + idTemp + '\',' + binding + ',' + (obj.properties.paging !== undefined && obj.properties.paging === true) + ')">'
                        + (obj.properties.displayName !== undefined && obj.properties.displayName.length > 0 ? '<div class="card-footer">' + obj.properties.displayName + '</div>' : '')
                        + '<div class="card-body">'
                        + '<div class="row clearfix" ng-repeat="' + repeatId + ' in render.methods.tables.getElements(\'' + idTemp + '\',' + binding + ')" ng-class="$index+1 === render.methods.tables.getSelect(\'' + idTemp + '\') ? \'alert alert-primary\' : \'\'">'
                        + '<div class="col-md-1" ng-click="render.methods.tables.setSelect(\'' + idTemp + '\',$index+1)">' + (!showIndex ? '*' : '{{' + (paging.length === 0 ? '' : 'render.methods.tables.getPagePivot(\'' + id + '\')') + ' + $index + 1}}') + '</div>'
                        + '<div class="col-md-11">'
                        + $scope.render.methods.general.renderComponents(obj, true, false, repeatId, whenUpdate)
                        + '<hr></div>'
                        + '</div>'
                        + '</div>'
                        + (editControls.length === 0 && paging.length === 0 ? '' : '<div class="card-footer"><div class="row clearfix"><div class="col-md-2">' + editControls + '</div><div class="col-md-8 text-center">' + paging + '</div><div class="col-md-2"></div></div></div>')
                        + '</div>';
                },
                upload: function (id, obj, isRepeat, isColumn, binding, size) {
                    var parts = binding.split(".");
                    var attr = parts.pop();
                    var bindingTmp = parts.join(".");
                    var paramsClick = (isRepeat === true ? '\'' + $scope.render.methods.general.idParser(obj.properties.id) + '_\' + $index' : '\'' + $scope.render.methods.general.idParser(obj.properties.id) + '\'') + ',' + bindingTmp + ',\'' + bindingTmp + '\',\'' + attr + '\'';
                    var paramsData = bindingTmp + ',\'' + attr + '\',' + (isRepeat === true ? '\'' + $scope.render.methods.general.idParser(obj.properties.id) + '_\' + $index' : '\'' + $scope.render.methods.general.idParser(obj.properties.id) + '\'') + ',\'' + obj.properties.defaultImage + '\'';
                    var ng_click = ' ng-click="render.methods.upload.selectFile(' + paramsClick + ')"';
                    var control =
                        (obj.properties.bindingshow === undefined || obj.properties.bindingshow === true
                            ? '<div class="d-flex justify-content-between">'
                            + '<div>'
                            + '<a href="javascript:void(0)" ng-click="crud.beginDownload(' + binding + ')" data-toggle="tooltip" data-placement="bottom" title="{{render.methods.upload.getFileName(' + binding + ')}}">{{render.methods.upload.getFileName(' + binding + ') | lengthFiltr:16}}</a>'
                            + '</div>'
                            + '<div>'
                            + (obj.properties.disableClear !== undefined && obj.properties.disableClear === true ? '' : '&nbsp;<a title="Limpiar archivo" class="btn btn-danger btn-circle btn-sm" href="javascript:void(0)" ng-show="' + binding + ' !== null" ng-click="render.methods.upload.clearFile(' + paramsClick + ',' + isRepeat + ')"><i class="fas fa-trash"></i></a>')
                            + (obj.properties.disableDownload !== undefined && obj.properties.disableDownload === true ? '' : '&nbsp;<a title="Descargar archivo" class="btn btn-primary btn-circle btn-sm" href="javascript:void(0)" ng-show="' + binding + ' !== null" ng-click="crud.beginDownload(' + binding + ')"><i class="fas fa-download"></i></a>')
                            + (obj.properties.disableUpload !== undefined && obj.properties.disableUpload === true ? '' : '&nbsp;<a title="Subir archivo" id="' + id + '" class="btn btn-success btn-circle btn-sm" href="javascript:void(0)" ' + ng_click + '><i class="fas fa-upload"></i></a>')
                            + '</div>'
                            + '</div>'
                            : '')
                            + (obj.properties.previewimage !== undefined && obj.properties.previewimage === true
                            ? '<div class="previewimage">'
                            + '<img id="' + id + '_img" ' + ng_click + ' src="{{' + 'render.methods.upload.getDataFile(' + paramsData + ')' + '}}" ' + (obj.properties.style !== undefined ? 'style="' + obj.properties.style + '"' : '') + '>'
                            + '</div>'
                            : (!(obj.properties.bindingshow === undefined || obj.properties.bindingshow === true) ? '&nbsp;<a id="' + id + '" href="javascript:void(0)" ' + ng_click + '><i class="material-icons">cloud_upload</i></a>' : ''))
                            + '<input id="if_' + id + '" type="file" style="visibility: hidden; display:none;" />';
                    return isColumn ? control : $scope.render.components.controls.labelinternal(id, control, obj.properties, size);
                },
                textarea: function (id, obj, isRepeat, isColumn, binding, size) {
                    var control = '<textarea id="' + id + '" class="form-control' + (size !== undefined && size.length > 0 ? ' form-control-' + size : '') + '" rows="' + obj.properties.rows + '" ng-model="' + binding + '" ' + $scope.render.methods.general.canUpdateWhenUpdate(obj, binding) + '></textarea>';
                    return isColumn ? control : $scope.render.components.controls.labelinternal(id, control, obj.properties, size);
                },
                textbox: function (id, obj, isRepeat, isColumn, binding, size) {
                    var control = '<input id="' + id + '" type="text" class="form-control' + (size !== undefined && size.length > 0 ? ' form-control-' + size : '') + '" ng-model="' + binding + '" ' + $scope.render.methods.general.canUpdateWhenUpdate(obj, binding) + '>';
                    return isColumn ? control : $scope.render.components.controls.labelinternal(id, control, obj.properties, size);
                }
            }
        },
        methods: {
            binding: 'metaData.element',
            general: {
                getRepeatId: function () {
                    $scope.metaData.iterator++;
                    return 'r_' + $scope.metaData.iterator.toString(16);
                },
                replace: function (str, search, replacement) {
                    return str.split(search).join(replacement);
                },
                getPriorityValue: function (left, right) {
                    return left !== undefined ? left : right;
                },
                idParser: function (id) {
                    return $scope.render.methods.general.replace(id, '-', '_');
                },
                getControlById: function (id) {
                    return $scope.metaData.controls.filter(obj => { return obj.id === id; })[0];
                },
                renderComponents: function (obj, isRepeat, isColumn, repeatId, whenUpdate) {
                    var html = '';
                    if (obj.elements !== undefined) {
                        for (element in obj.elements) {
                            var elementToRender = obj.elements[element];
                            elementToRender.parent = obj.properties.id;
                            if (whenUpdate !== undefined) {
                                if (whenUpdate !== undefined && whenUpdate.canUpdateWhenUpdate !== undefined && whenUpdate.canUpdateWhenUpdate === false) elementToRender.properties.canUpdateWhenUpdate = false;
                            }
                            html += $scope.render.methods.general.renderComponent(elementToRender, isRepeat, isColumn, repeatId);
                        }
                    }
                    return html;
                },
                renderComponent: function (obj, isRepeat, isColumn, repeatId, size) {
                    $scope.metaData.controls.push(obj);
                    isColumn = isColumn !== undefined && isColumn === true;
                    obj.properties.id = $scope.render.methods.general.idParser(obj.properties.id);
                    obj.id = obj.properties.id;
                    if (obj.properties.parent !== undefined && obj.properties.parent.length > 0) obj.properties.parent = $scope.render.methods.general.idParser(obj.properties.parent) + (isColumn ? '_{{$index}}' : '');
                    if (obj.properties.child !== undefined && obj.properties.child.length > 0) obj.properties.child = $scope.render.methods.general.idParser(obj.properties.child) + (isColumn ? '_{{$index}}' : '');
                    if (obj.properties.bindingupload !== undefined && obj.properties.bindingupload.length > 0) obj.properties.bindingupload = $scope.render.methods.general.idParser(obj.properties.bindingupload) + (isColumn ? '_{{$index}}' : '');
                    // Pilas al obtener el binding
                    var binding = (isRepeat ? repeatId : $scope.render.methods.binding) + (obj.properties.binding !== undefined ? '.' + obj.properties.binding : '');
                    var id = obj.properties.id + (isRepeat ? '_{{$index}}' : '');
                    size = $scope.render.methods.general.getPriorityValue(obj.properties.size, size);
                    return $scope.render.components[obj.category][obj.type](id, obj, isRepeat, isColumn, binding, size);
                },
                render: function (target, form, binding) {
                    $scope.render.methods.binding = binding !== undefined && binding.length > 0 ? binding : 'metaData.element';
                    $('#' + target).append($compile($scope.render.methods.general.renderComponent(form))($scope));
                    //$scope.performActions(actions);
                },
                renderFromHtml: function (target, form) {
                    $('#' + target).empty();
                    //angular.element(document.getElementById(target)).append($compile(form)($scope));
                    $('#' + target).append($compile(form)($scope));
                },
                getHtml: function (form, binding) {
                    $scope.render.methods.binding = binding !== undefined && binding.length > 0 ? binding : 'metaData.element';
                    return $scope.render.methods.general.renderComponent(form);
                },
                recursiveBinding: function (bindingbase, bindingpart, setValue) {
                    parts = bindingpart.split('.');
                    binding = bindingbase;
                    for (part in parts) {
                        if (setValue !== undefined && part === (parts.length - 1) + '') {
                            binding[parts[part]] = setValue;
                        }
                        binding = binding[parts[part]];
                    }
                    return binding;
                },
                showCondition: function (obj, binding) {
                    return obj.properties.showCondition !== undefined && obj.properties.showCondition.length > 0 ? ' ng-show="' + obj.properties.showCondition.replace('[binding]', binding) + '"' : '';
                },
                changeLastBindingPart: function (binding, part) {
                    var baseBinding = binding.split('.');
                    baseBinding.pop();
                    baseBinding.push(part);
                    return baseBinding.join('.');
                },
                canUpdateWhenUpdate: function (obj, binding) {
                    return obj.properties.canUpdateWhenUpdate !== undefined && obj.properties.canUpdateWhenUpdate === false ? ' ng-disabled="' + $scope.render.methods.general.changeLastBindingPart(binding, 'Id') + ' > 0" ' : '';
                },
                totalize: function (obj, prop) {
                    return obj.reduce((a, b) => a + parseFloat(b[prop]), 0);
                },
                getServiceData: function (bindingbase, serviceUrl, parameterFilter, parameterTarget) {
                    service.request(
                        serviceUrl,
                        {
                            Value: bindingbase[parameterFilter]
                        },
                        function (response) {
                            bindingbase[parameterTarget] = response.Data1;
                        }
                    );
                },
                // render.methods.general.isJson
                isJson: function (str) {
                    try {
                        JSON.parse(str);
                    } catch (e) {
                        return false;
                    }
                    return true;
                },
                // render.methods.general.getJson
                getJson: function (str) {
                    try {
                        JSON.parse(str);
                    } catch (e) {
                        return false;
                    }
                    return true;
                }
            },
            chart: {
                init: function (id, binding) {
                    $timeout(function () {
                        var control = $('#' + id);
                        control.html('');
                        var el = document.createElement('canvas');
                        el.id = id + '_c';
                        control[0].appendChild(el);
                        var ctx = el.getContext('2d');
                        binding.options.scales = {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }],
                            xAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                        };
                        $scope.metaData.charts[id] = new Chart(ctx, binding);
                    }, 1);
                }
            },
            combos: {
                getData: function (url, bindingbase, parent) {
                    var value = '0';
                    if (parent !== undefined) {
                        var binding = $scope.render.methods.general.recursiveBinding(bindingbase, $scope.render.methods.general.getControlById(parent).properties.binding);
                        if (binding !== null) value = binding;
                    }
                    var urlTmp = url + (value !== '0' ? '_' + value : '');
                    if ($scope.metaData.combos[urlTmp] === undefined) {
                        $scope.metaData.combos[urlTmp] = [];
                        var urlParts = url.split("/");
                        var entity = urlParts.pop();
                        url = urlParts.join("/");
                        service.request(
                            url,
                            {
                                Entity: entity,
                                Value: value
                            },
                            function (response) {
                                $scope.metaData.combos[urlTmp] = response.Data1;
                            }
                        );
                    }
                },
                getValue: function (url, id, placeHolder) {
                    var value = placeHolder;
                    if ($scope.metaData.combos[url].length > 0) {
                        var filter = $scope.metaData.combos[url].filter(obj => { return obj.id === id; });
                        if (filter.length === 1) return filter[0].value;
                    }
                    return value;
                },
                getElements: function (service, bindingbase, target) {
                    if (bindingbase !== undefined) {
                        var binding = $scope.render.methods.general.recursiveBinding(bindingbase, $scope.render.methods.general.getControlById(target).properties.binding);
                        service += binding !== null ? '_' + binding : '';
                    }
                    return $scope.metaData.combos[service];
                },
                onChange: function (bindingbase, parent, child) {
                    var controlChild = $scope.render.methods.general.getControlById(child);
                    $scope.render.methods.general.recursiveBinding(bindingbase, controlChild.properties.binding, null);
                    $scope.render.methods.combos.getData(controlChild.properties.service, bindingbase, parent);
                }
            },
            tables: {
                init: function (id, binding, paging) {
                    $scope.metaData.grids[id] = {
                        paging: paging,
                        itemSelected: 0,
                        // Page Properties
                        pageSelected: 1,
                        pageSize: 5,
                        pages: 0,
                        pagesVisible: [{ num: 1 }],
                        // Sort properties
                        sortBy: '',
                        sortDirection: 'A'
                    };
                    $scope.render.methods.tables.updatePagination(id, binding !== null && binding !== undefined ? binding.length : 0);
                },
                setSelect: function (id, index) {
                    $scope.metaData.grids[id].itemSelected = $scope.metaData.grids[id].itemSelected === index ? 0 : index;
                },
                getSelect: function (id) {
                    return $scope.metaData.grids[id].itemSelected;
                },
                newItem: function (id, binding) {
                    var control = $scope.render.methods.general.getControlById(id);
                    //if (control.properties !== undefined && (typeof control.properties.newItem === 'string' || control.properties.newItem instanceof String)) {
                    if (typeof control.properties.newItem === 'string' || control.properties.newItem instanceof String) {
                        service.request(
                            'core/GetNewEntity',
                            {
                                Entity: control.properties.newItem
                            },
                            function (response) {
                                control.properties.newItem = response.Data1;
                                $scope.render.methods.tables.newItem(id, binding);
                            }
                        );
                    }
                    else {

                        binding.push(JSON.parse(JSON.stringify(control.properties.newItem)));
                        if ($scope.metaData.grids[id].paging) {
                            $scope.render.methods.tables.updatePagination(id, binding !== undefined ? binding.length : 0);
                            $scope.render.methods.tables.setPageLast(id);
                        }
                    }
                },
                delItem: function (id, binding) {
                    binding = '$scope.' + binding;
                    $scope.render.methods.upload.validateFiles(eval(binding + '.splice($scope.metaData.grids[id].itemSelected - 1,1)')[0], binding);
                    var grid = $scope.metaData.grids[id];
                    grid.itemSelected = 0;
                    if (grid.paging) {
                        var tmpBinding = eval(binding);
                        $scope.render.methods.tables.updatePagination(id, tmpBinding !== undefined ? tmpBinding.length : 0);
                        if (grid.pagesVisible.length < grid.pageSelected) $scope.render.methods.tables.setPageLast(id);
                    }
                },
                getElements: function (id, binding) {
                    var elements = [];
                    if ($scope.metaData.grids[id] !== undefined) {
                        if ($scope.metaData.grids[id].paging && binding.length > 0) {
                            var grid = $scope.metaData.grids[id];
                            var begin = ((id === $scope.crud.gridId ? 1 : grid.pageSelected) - 1) * grid.pageSize;
                            elements = binding.slice(begin, begin + grid.pageSize);
                        }
                        else elements = binding;
                    }
                    return elements;
                },
                getPagePivot: function (id) {
                    var grid = $scope.metaData.grids[id];
                    return (grid.pageSelected - 1) * grid.pageSize;
                },
                getPages: function (id) {
                    var pagesVisible = [];
                    if ($scope.metaData.grids[id] !== undefined) pagesVisible = $scope.metaData.grids[id].pagesVisible;
                    return pagesVisible;
                },
                setPageLast: function (id) {
                    var grid = $scope.metaData.grids[id];
                    $scope.render.methods.tables.setPageSelected(id, grid.pagesVisible[grid.pagesVisible.length - 1].num);
                },
                setPageSelected: function (id, index) {
                    $scope.metaData.grids[id].pageSelected = index;
                    if (id === $scope.crud.gridId) $scope.crud.list();
                },
                getPageSelected: function (id) {
                    return $scope.metaData.grids[id].pageSelected;
                },
                updatePagination: function (id, size) {
                    var grid = $scope.metaData.grids[id];
                    var pages = size / grid.pageSize;
                    var intPages = Math.trunc(pages);
                    pages = intPages + (pages - intPages > 0 ? 1 : 0);
                    grid.pagesVisible = Array.apply(null, Array(pages > 0 ? pages : 1)).map(function (x, i) { return { num: i + 1 }; });
                },
                dynamicSort: function (property) {
                    var sortOrder = 1;
                    if (property[0] === '-') {
                        sortOrder = -1;
                        property = property.substr(1);
                    }
                    return function (a, b) {
                        var result = a[property] < b[property] ? -1 : (a[property] > b[property] ? 1 : 0);
                        return result * sortOrder;
                    };
                },
                changeOrder: function (id, binding, column) {
                    var grid = $scope.metaData.grids[id];
                    if (grid.sortBy !== column) {
                        grid.sortBy = column;
                        grid.sortDirection = 'A';
                    }
                    else grid.sortDirection = grid.sortDirection === 'A' ? 'D' : 'A';

                    if (id === $scope.crud.gridId) $scope.crud.list();
                    else binding.sort($scope.render.methods.tables.dynamicSort((grid.sortDirection === 'A' ? '' : '-') + grid.sortBy));
                }
            },
            upload: {
                loadFile: function (id, bindingbase, binding, attr, file, control, index) {
                    var blob = file.slice(0, file.size);
                    var fr = new FileReader();
                    fr.onload = function (event) {
                        var idParts = id.split(';');
                        id = idParts[0];
                        
                        var idFile = $scope.render.methods.general.idParser(idParts[1]);
                        
                        var jsonFile = { id: idFile, name: file.name, state: 'uploaded', path: '' };

                        bindingbase[attr] = JSON.stringify(jsonFile);
                        
                        var data = event.target.result.split(',')[1];
                        var partsQuantity = data.length / $scope.metaData.transferBufferSize;
                        partsQuantity = partsQuantity - parseInt(partsQuantity) > 0 ? parseInt(partsQuantity) + 1 : parseInt(partsQuantity);

                        var tempFile = {
                            id: idFile,
                            realFileName: file.name,
                            storedFileName: idFile + '.' + file.name.split('.').pop(),
                            data: data,
                            binding: binding,
                            attr: attr,
                            processed: false,
                            part: 0,
                            parts: partsQuantity
                        };

                        if (index < 0) $scope.metaData.files.items.push(tempFile);
                        else $scope.metaData.files.items[index] = tempFile;

                        control.wrap('<form>').closest('form').get(0).reset();
                        control.unwrap();
                        document.getElementById('btnDummy').click();
                    };
                    fr.readAsDataURL(blob);
                },
                selectFile: function (id, bindingbase, binding, attr) {
                    var control = $('#if_' + id);
                    control.change(function (event) {
                        event.stopImmediatePropagation();
                        if (event.isImmediatePropagationStopped()) {
                            if (control[0].files.length === 1) {
                                var file = control[0].files[0];
                                if (file.size < 5242880 && $scope.metaData.files[id] === undefined) {
                                    var index = $scope.metaData.files.items.findIndex(item => item.binding === (binding.split(".")[0] !== 'metaData' ? binding : '$scope.' + binding) && item.attr === attr);
                                    if (index >= 0) {
                                        debugger;
                                        $scope.render.methods.upload.loadFile(id, bindingbase, binding, attr, file, control, index);
                                    }
                                    else {
                                        service.request(
                                            'core/getnewguid',
                                            {},
                                            function (response) {
                                                id += ';' + response.Data1;
                                                $scope.render.methods.upload.loadFile(id, bindingbase, binding, attr, file, control, index);
                                            }
                                        );
                                    }
                                }
                                else app.messages.show('El tamaño del archivo supera los 5242880 bytes', 'warning');
                            }
                            else app.messages.show('Solo puede elejir un archivo', 'warning');
                        }
                    });
                    $timeout(function () {
                        control.trigger("click");
                    }, 1);
                    //control.trigger("click");
                },
                getFileName: function (binding) {
                    return binding !== undefined && binding !== null ? binding.name : binding;
                },
                getDataFile: function (bindingbase, attr, bindingupload, defaultImage) {
                    var file = $scope.render.methods.general.recursiveBinding(bindingbase, attr);
                    var response = defaultImage;
                    if (file !== null) {
                        file = JSON.parse(file);
                        var extFile = file.Name.split(".").slice(-1)[0];
                        if (file.State === 'Uploaded') {
                            var index = $scope.metaData.files.items.findIndex(item => item.Id === file.Id);
                            response = "data:[type];base64," + $scope.metaData.files.items[index].data;
                            switch (extFile) {
                                case "jpg":
                                case "jpeg":
                                    response = response.replace("[type]", "image/jpeg"); break;
                                case "gif":
                                    response = response.replace("[type]", "image/gif"); break;
                                case "png":
                                    response = response.replace("[type]", "image/x-png"); break;
                            }
                            
                        }
                        else response = file.Path + '/' + file.Id + '.' + extFile;
                    }
                    if (response === undefined || response === 'undefined' || response.length === 0) response = 'images/no_preview.jpg';

                    return response;
                },
                getAttrByBinding: function (binding) {
                    return $scope.metaData.files.items.filter(obj => { return obj.binding === binding; }).map(function (item) { return item.attr; }).filter(function (value, index, self) { return self.indexOf(value) === index; });
                },
                removeById: function (id) {
                    var index = $scope.metaData.files.items.findIndex(item => item.id === id);
                    if (index >= 0) $scope.metaData.files.items.splice(index, 1);
                },
                validateFiles: function (obj, binding) {
                    var attrs = $scope.render.methods.upload.getAttrByBinding(binding);
                    if (attrs.length > 0) {
                        for (attr in attrs) {
                            var value = obj[attrs[attr]];
                            if (value !== undefined && value !== null && value.length > 0 && value.indexOf(';') > 0) {
                                $scope.render.methods.upload.removeById(value.split(';')[0]);
                            }
                        }
                    }
                },
                clearFile: function (id, bindingbase, binding, attr, isColumn) {
                    $scope.render.methods.upload.removeById(JSON.parse(bindingbase[attr]).id);
                    bindingbase[attr] = null;
                }
            },
            richtext: {
                activate: function (id, binding, attr) {
                    $timeout(function () {
                        CKEDITOR.replace(id);
                        CKEDITOR.config.height = 300;
                        var editor = CKEDITOR.instances[id];
                        editor.setData(binding[attr]);
                        editor.on('change', function () {
                            binding[attr] = this.getData();
                        });
                    }, 1);
                }
            },
            actions: {
                perform: function (actions) {
                    for (action in actions) {
                        $scope.render.methods.actions[actions[action].type](actions[action]);
                    }
                },
                collapse: function (action) {
                    $('#' + action.properties.origin).attr("target", action.properties.target).click(function (event) { event.stopPropagation(); $('#' + $(this).attr("target")).collapse("toggle"); });
                    $('#' + action.properties.target).addClass("collapse" + (action.properties.collapsed ? "" : " show"));
                }
            }
        }
    };

    this.$onInit = function () {
        $scope.layout = $scope.$parent.$parent;
        $scope.layout.admin = $scope;

        service.request(
            'core/getTransferBufferSize',
            {},
            function (response) {
                if (response.Code === '1') {
                    $scope.metaData.transferBufferSize = response.Data1;
                }
                else app.messages.show(response.Message, 'warning');
            }
        );
    };
});

app.registerController('adminCtrl');