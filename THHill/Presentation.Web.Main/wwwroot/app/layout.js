﻿app.controller('layoutCtrl',
    function ($scope, $compile, service, $timeout) {
        $scope.MVCController = 'home';
        // **********************************************************************
        // Members

        $scope.metaData = {
            template: {
                loginPageClass: 'bg-login',
                adminPageClass: ''
            },
            layout: {
                url: ''
            },
            viewMain: '',
            user: {
                Token: '',
                FullName: '',
                Email: '',
                Password: '',
                IsLogged: false,
                SelectedComerceId: 0,
                Comerces: []
            },
            remember: {},
            menu: [],
            modals: {},
            state: {
                isFinish: true,
                isForgot: false
            },
            password: {
                First: '',
                Confirm: ''
            }
        };

        $scope.topMenu = [

        ];

        // **********************************************************************
        // Methods

        // TO DO: Crear tic para actualizar session

        $scope.DoNothing = function () {

        };

        $scope.LoadLogin = function () {
            $scope.metaData.viewMain = $scope.MVCController + "/loginform/" + (new Date()).getTime();
        };

        $scope.IsLogged = function () {
            service.request(
                'home/isLogged',
                {},
                function (result) {
                    if (result.Code === '1') {
                        $scope.metaData.user.FullName = result.Data1;
                        $scope.metaData.user.Email = result.Data2;
                        $scope.metaData.user.IsLogged = result.Data3;
                        $scope.metaData.user.Token = result.Data6;
                        
                        if ($scope.metaData.user.IsLogged) {
                            $scope.metaData.user.IsLogged = true;
                            $('body').removeClass($scope.metaData.template.loginPageClass).addClass($scope.metaData.template.adminPageClass);
                            $scope.GetMenu();
                            $.getScript('app/admin.js')
                                .done(function (script, textStatus) {
                                    $timeout(function () {
                                        $scope.metaData.viewMain = $scope.MVCController + "/admin/" + (new Date()).getTime();
                                        document.getElementById('btnDummy').click();
                                    }, 1);
                                });
                        }
                        else {
                            $scope.metaData.user.IsLogged = false;
                            $('body').removeClass($scope.metaData.template.adminPageClass).addClass($scope.metaData.template.loginPageClass);
                            $scope.metaData.viewMain = $scope.MVCController + "/landingPage/" + (new Date()).getTime();
                            //$scope.metaData.viewMain = $scope.MVCController + "/loginform/" + (new Date()).getTime();
                        }
                    }
                    else app.messages.show(result.Message, 'warning'); 
                    //else app.ShowPopup('Ocurrio un error: ' + result.Message);
                }
            );
        };

        $scope.Login = function () {
            service.request(
                'home/logIn',
                {
                    Email: $scope.metaData.user.Email,
                    Password: $scope.metaData.user.Password
                },
                function (result) {
                    if (result.Code === '1') location.reload();
                    else app.messages.show(result.Message, 'warning'); 
                }
            );
        };

        $scope.Forgot = function () {
            $scope.metaData.remember = {
                Email: ''
            };
            $scope.metaData.state.isForgot = true;
        };

        $scope.Return = function () {
            $scope.metaData.remember = {
                Email: ''
            };
            $scope.metaData.state.isForgot = false;
        };

        $scope.ForgotSend = function () {
            service.request(
                'home/forgotSend',
                {
                    Email: $scope.metaData.remember.Email
                },
                function (result) {
                    if (result.Code === '1') location.reload();
                    else app.messages.show(result.Message, 'warning');
                }
            );
        };

        $scope.CreatePassword = function () {
            service.request(
                'home/createPassword',
                {
                    Email: $scope.metaData.user.Email,
                    Token: $scope.metaData.user.Token,
                    Password: $scope.metaData.password.First
                },
                function (result) {
                    if (result.Code === '1') location.reload();
                    else app.messages.show(result.Message, 'warning');
                    //else app.ShowPopup('Ocurrio un error: ' + result.Message);
                }
            );
        };

        $scope.GetComerceById = function (comerceId) {
            return $scope.metaData.user.Comerces.filter(obj => { return obj.Id === comerceId; })[0];
        };

        $scope.LogOff = function () {
            service.request(
                'home/logOff',
                {},
                function (result) {
                    if (result.Code === '1') location.reload();
                    else app.messages.show(result.Message, 'warning'); 
                }
            );
        };

        $scope.GetMenu = function () {
            service.request(
                'home/getMenu',
                {},
                function (result) {
                    if (result.Code === '1') {
                        $scope.metaData.menu = result.Data1.Items;
                    }
                    //else app.ShowPopup('Ocurrio un error: ' + result.Message);
                }
            );
        };

        $scope.LoadModule = function (id) {
            $scope.admin.module.methods.load(id);
        };

        $scope.Dashboard = function () {
            /*$scope.metaData.module = {
                url: $scope.MVCController + "/dashboard/" + (new Date()).getTime(),
                title: 'Dashboard'
            };*/
        };

        $scope.modal = {
            replaceBinding: function (id, content) {
                return content.split('metaData').join('metaData.modals.' + id + '.metaData').split('methods').join('metaData.modals.' + id + '.methods');
            },
            render: function (id, title, size, closeable, body, footer) { // size: sm, lg
                return '<div id="' + id + '" class="modal fade" tabindex="-1" role="dialog">'
                    + '<div class="modal-dialog' + (size !== undefined || size.length > 0 ? size = ' modal-' + size : '') + '" role="document">'
                    + '<div class="modal-content">'
                    + '<div class="modal-header">'
                    + '<h5 class="modal-title">' + title + '</h5>'
                    + (closeable ? '<button id="' + id + '_close" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>' : '')
                    + '</div>'
                    + '<div id="' + id + '_body" class="modal-body" ' + (body !== undefined && body.url !== undefined && body.url.length > 0 ? 'ng-include="' + body.url + '"' : '') + '>'
                    + (body !== undefined && body.content !== undefined && body.content.length > 0 ? $scope.modal.replaceBinding(id, body.content) : '')
                    + '</div>'
                    + (footer !== undefined ? '<div class="modal-footer"' + (footer.url !== undefined && footer.url.length > 0 ? 'ng-include="' + footer.url + '"' : '') + '>' + (footer.content !== undefined && footer.content.length > 0 ? $scope.modal.replaceBinding(id, footer.content) : '') + '</div>' : '')
                    + '</div>'
                    + '</div>'
                    + '</div>';
            },
            add: function (title, size, closeable, body, footer, binding, methods) { //
                var id = 'modal_' + Math.round(+new Date() / 1000) + '_' + Math.floor(Math.random() * 9 + 1);
                $scope.metaData.modals[id] = { metaData: binding, methods: methods };
                var render = $scope.modal.render(id, title, size, closeable, body, footer);
                $('#modalContainer').append($compile(render)($scope));
                $('#' + id).modal({
                    backdrop: 'static',
                    keyboard: false
                });
                $('#' + id).on('hidden.bs.modal', function (e) {
                    $('#' + id).remove();
                });
                return id;
            },
            close: function (id) {
                $('#' + id).removeClass("in");
                $('.modal-backdrop').remove();
                $('#' + id).hide();
                //document.getElementById('btnDummy').click();
            }
        };

        // **********************************************************************
        // Events

        this.$onInit = function () {
            $scope.IsLogged();
        };

        onLoadLayout = function () {
            
        };

    }
);