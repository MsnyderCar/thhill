﻿angular.module('app').factory('service', ["$http", function ($http) {
    return {
        request: function (url, data, callBack) {
            app.loader.methods.add();
            $http.post(
                url, data
            ).success(function (result) {
                app.loader.methods.del();
                callBack(result);
            }).error(function (result, status) {
                app.loader.methods.del();
                callBack({ Code: '2', Message: result });
            });
        }
    };
}]);