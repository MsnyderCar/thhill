﻿var app = angular.module('app', ['ngSanitize'], function ($controllerProvider, $filterProvider) {
    // Constants
    app.onlyNumbers = /^\d+$/;
    // Fields validators
    app.isNumberKey = function (evt) {
        return app.OnlyNumbers.test(evt.key);
    };
    // **********************************************************************
    app.getNewDate = function () { return (new Date()).getTime(); };
    // Register controllers in execution time
    app.registerController = function (controllerName) {
        var queue = this._invokeQueue;
        for (var i = 0; i < queue.length; i++) {
            var call = queue[i];
            if (call[0] === '$controllerProvider' && call[1] === 'register' && call[2][0] === controllerName && !$controllerProvider.has(controllerName)) {
                $controllerProvider.register(controllerName, call[2][1]);
            }
        }
    };
    // **********************************************************************
    // Messages
    app.messages = {
        show: function (text, color, forever) {
            realColor = 'bg-black';
            if (color === 'warning') realColor = 'alert-warning';
            else if (color === 'danger') realColor = 'alert-danger';
            else if (color === 'success') realColor = 'alert-success';
            else if (color === 'info') realColor = 'alert-info';
            return this.create(realColor, text, 'bottom', 'right', '', '', forever);
        },
        create: function (colorName, text, placementFrom, placementAlign, animateEnter, animateExit, forever) {
            if (colorName === null || colorName === '') { colorName = 'bg-black'; }
            if (text === null || text === '') { text = 'Turning standard Bootstrap alerts'; }
            if (animateEnter === null || animateEnter === '') { animateEnter = 'animated fadeInDown'; }
            if (animateExit === null || animateExit === '') { animateExit = 'animated fadeOutUp'; }
            var allowDismiss = true;

            forever = forever !== undefined && forever === true;

            var delay = 2000;
            var timer = 2000;

            if (forever) {
                delay = 2000;
                timer = 0;
            }

            var not = $.notify({
                message: text
            }, {
                    type: colorName,
                    allow_dismiss: allowDismiss,
                    newest_on_top: true,
                    delay: delay,
                    timer: timer,
                    placement: {
                        from: placementFrom,
                        align: placementAlign
                    },
                    animate: {
                        enter: animateEnter,
                        exit: animateExit
                    },
                    template: '<div data-notify="container" class="bootstrap-notify-container alert alert-dismissible {0} ' + (allowDismiss ? "p-r-35" : "") + '" role="alert">' +
                        '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
                        '<span data-notify="icon"></span> ' +
                        '<span data-notify="title">{1}</span> ' +
                        '<span data-notify="message">{2}</span>' +
                        '<div class="progress" data-notify="progressbar">' +
                        '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                        '</div>' +
                        '<a href="{3}" target="{4}" data-notify="url"></a>' +
                        '</div>'
                });

            return forever ? not : undefined;
        },
        update: function (not, message) {
            not.update('message', message);
        },
        close: function (not) {
            not.close();
        }
    };

    app.loader = {
        metaData: {
            intervalId: undefined,
            items: [],
            begin: new Date()
        },
        methods: {
            add: function (message) {
                app.loader.metaData.begin = new Date();
                app.loader.metaData.items.push(0);
                var launchMonitor = !app.loader.methods.opened();
                app.loader.methods.open(message);
                if (launchMonitor) {
                    app.loader.methods.monitor();
                }
            },
            open: function (message) {
                $('#LoaderMessage').empty();
                $('#LoaderMessage').append('<span class="mist-0">Espere por favor</span>');
                setTimeout(function () { $('.page-loader-wrapper').fadeTo(50, 0.5); }, 50);
            },
            opened: function () {
                return !($(".mist-0").length === 0);
            },
            monitor: function () {
                app.loader.metaData.intervalId = setInterval(function () {
                    if ((new Date() - app.loader.metaData.begin) / 1000 > 30 && app.loader.metaData.items === undefined && app.loader.metaData.items.length > 0)
                        app.loader.metaData.items.pop();
                    if (app.loader.metaData.items === undefined || app.loader.metaData.items.length === 0) {
                        app.loader.methods.close();
                        clearInterval(app.loader.metaData.intervalId);
                        app.loader.metaData.intervalId = undefined;
                        app.loader.metaData.items = [];
                        document.getElementById('btnDummy').click();
                    }
                }, 500);
            },
            del: function () {
                if (app.loader.methods.opened()) app.loader.metaData.items.pop();
            },
            close: function () {
                if (app.loader.methods.opened()) {
                    setTimeout(function () { $('.page-loader-wrapper').fadeOut(); }, 50);
                    $('#LoaderMessage').empty();
                }
            }
        }
    };
});

// **********************************************************************
// Filters

app.filter("lengthFiltr", function ($filter) {
    return function (value, size) {
        return typeof value === 'string' && value.length > size ? value.substring(0, size) : value;
    };
});

// **********************************************************************
// Directives

app.directive('datepicker', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        compile: function () {
            return {
                pre: function (scope, element, attrs, ngModelCtrl) {
                    var format, dateObj;
                    format = !attrs.dpFormat ? 'd/m/yyyy' : attrs.dpFormat;
                    if (!attrs.initDate && !attrs.dpFormat) {
                        // If there is no initDate attribute than we will get todays date as the default
                        dateObj = new Date();
                        scope[attrs.ngModel] = dateObj.getDate() + '/' + (dateObj.getMonth() + 1) + '/' + dateObj.getFullYear();
                    } else if (!attrs.initDate) {
                        // Otherwise set as the init date
                        scope[attrs.ngModel] = attrs.initDate;
                    } else {
                        // I could put some complex logic that changes the order of the date string I
                        // create from the dateObj based on the format, but I'll leave that for now
                        // Or I could switch case and limit the types of formats...
                    }
                    // Initialize the date-picker
                    $(element).datepicker({
                        format: format,
                        autoclose: true
                    }).on('changeDate', function (ev) {
                        // To me this looks cleaner than adding $apply(); after everything.
                        scope.$apply(function () {
                            ngModelCtrl.$setViewValue(ev.format(format));
                        });
                    });
                }
            };
        }
    };
});

app.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});
