﻿using Domain.Services;
using Infrastructure.Core.UnityOfWork;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Transversal.Common.Entities.Api;

namespace DistributedServices.Controller.DataAccess
{
    public class GenericController : IDisposable
    {
        #region Members

        protected IUnityOfWork UOW;
        protected Authentication AuthenticationManager;

        public string AccessToken { get; set; }

        #endregion

        #region Constructor

        public GenericController(string connectionString)
        {
            try
            {
                UOW = new UnityOfWork(connectionString);
                AuthenticationManager = new Authentication(UOW);

                /*var res = Initialize();
                if (!res.Contains("OK")) throw new Exception("No se pudo inicializar la base de datos");*/
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Startup error: {ex.Message} {ex.StackTrace}");
            }
        }

        #endregion

        #region Public methods

        public string Initialize()
        {
            var response = string.Empty;

            try
            {
                UOW.Initialize();
                response = "OK";
            }
            catch (Exception ex)
            {
                WriteLog(ex);
                response = $"{ex.Message} | {ex.StackTrace} | {ex.InnerException.Message} | {ex.InnerException.StackTrace}";
            }

            return response;
        }

        public AuthResponse Auth(AuthRequest request)
        {
            var response = new AuthResponse();
            var result = new ErrorContainer();

            try
            {
                if (request.Type == "login")
                {
                    var token = AuthenticationManager.Connect(request.Username, request.Password, request.Ip);
                    response = new AuthResponse
                    {
                        StatusCode = 200,
                        StatusMessage = new List<string> { "OK" },
                        Token = new Token
                        {
                            AccountId = token.UserId,
                            Create = token.CreatedDate,
                            Access = token.AccessToken.ToString(),
                            Period = token.ValidityPeriod,
                            Refresh = token.RefreshToken.ToString(),
                            Ip = token.Ip
                        }
                    };
                }
                else if (request.Type == "refresh")
                {
                    var token = AuthenticationManager.Refresh(request.RefreshToken, request.Ip);
                    response = new AuthResponse
                    {
                        StatusCode = 200,
                        StatusMessage = new List<string> { "OK" },
                        Token = new Token
                        {
                            AccountId = token.UserId,
                            Create = token.CreatedDate,
                            Access = token.AccessToken.ToString(),
                            Period = token.ValidityPeriod,
                            Refresh = token.RefreshToken.ToString(),
                            Ip = token.Ip
                        }
                    };
                }
                else if (request.Type == "logout")
                {
                    response = new AuthResponse
                    {
                        StatusCode = 200,
                        StatusMessage = new List<string> { "OK" },
                        Token = AuthenticationManager.Disconnect(AccessToken, request.Ip) ? new Token() : null
                    };
                }
                else if (request.Type == "forgot")
                {
                    result = AuthenticationManager.Forgot(request.Username);

                    response = new AuthResponse
                    {
                        StatusCode = 200,
                        StatusMessage = new List<string> { "OK" },
                        Token = new Token()
                    };
                }
                else if (request.Type == "create")
                {
                    result = AuthenticationManager.CreatePassword(request.Username, request.Password, request.AuthToken);

                    response = new AuthResponse
                    {
                        StatusCode = 200,
                        StatusMessage = new List<string> { "OK" },
                        Token = new Token()
                    };
                }
                else if (request.Type == "validate")
                {
                    var resultTmp = AuthenticationManager.ValidateAuthToken(request.AuthToken);

                    if (!string.IsNullOrEmpty(resultTmp))
                    {
                        response = new AuthResponse
                        {
                            AnotherData = resultTmp,
                            StatusCode = 200,
                            StatusMessage = new List<string> { "OK" },
                            Token = new Token()
                        };
                    }
                    else
                        throw new Exception("Token Invalido");
                }

                if (result.InError)
                    throw new Exception(string.Join(", ", result.Message));
            }
            catch(Exception ex)
            {
                WriteLog(ex);
                response = new AuthResponse
                {
                    StatusCode = 500,
                    StatusMessage = new List<string> { $"{ex.Message}" },
                    Token = new Token()
                };
            }

            return response;
        }

        public Packet Exec(Packet request, bool serialize = false)
        {
            Packet response = null;
            BaseService service = null;

            try
            {
                CheckSecurity(request);
                service = GetCustomService(request.Service, serialize);
                response = service.Execute(request);
                response.StatusCode = 200;
                response.StatusMessage = new List<string>();
            }
            catch (Exception ex)
            {
                Exception realException = null;

                if (ex is TargetInvocationException)
                {
                    if (ex.InnerException != null)
                        realException = ex.InnerException;
                    else
                        realException = ex;
                }
                else
                    realException = ex;

                WriteLog(ex);

                response = new Packet
                {
                    StatusCode = 500,
                    StatusMessage = new List<string> { realException.Message }
                };
            }

            return response;
        }

        #endregion

        #region Private Methods

        void CheckSecurity(Packet request)
        {
            if (!AuthenticationManager.HasPermissions(AccessToken, request.Service, request.Action))
            {
                throw new Exception("No posee permisos para ejecutar la accion");
            }
        }

        BaseService GetCustomService(string service, bool serialize = false)
        {
            BaseService oService = null;

            try
            {
                oService = (dynamic)Activator.CreateInstance(Assembly.GetAssembly(typeof(Authentication)).GetTypes().Where(x => x.FullName.EndsWith(service)).ToList().First(), UOW, serialize);
            }
            catch (Exception ex)
            {
                WriteLog(ex);
            }

            return oService;
        }

        protected void WriteLog(Exception ex)
        {
            var date = DateTime.Now;
            using (StreamWriter sw = File.AppendText($"log_{date.ToString("yyyyMMdd")}.txt"))
            {
                sw.WriteLine($"{date.ToString("yyyy-MM-dd HH:mm:ss")}|Exception:{ex.Message}|StackTrace:{ex.StackTrace}");
            }
        }

        #endregion

        #region IDisposable

        public void Dispose()
        {
            UOW.Dispose();
        }

        #endregion
    }
}
