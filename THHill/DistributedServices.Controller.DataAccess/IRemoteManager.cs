﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DistributedServices.Controller.DataAccess
{
    public interface IRemoteManager
    {
        bool Connect(string userName, string password, string ip);

        bool Disconnect();

        object Execute(string service, string action, params object[] args);
    }
}
