﻿using DistributedServices.Controller.Api;
using DistributedServices.Controller.DataAccess.Mobile;
using System;
using System.Timers;

namespace Domain.Daemons
{
    public class DaemonManager : IDisposable
    {
        #region Members

        public volatile bool Started;

        protected RemoteManager Remote;
        protected Timer Timer;
        protected int RefreshTime { get; set; }
        protected bool OnMaintenance { get; set; }
        protected int TransferBufferSize { get; set; }
        protected string Ip { get; set; }

        #endregion

        #region Handlers

        public event EventHandler OnExecute;
        public event EventHandler OnStop;

        #endregion

        #region Constructor

        public DaemonManager(RemoteManager remote)
        {
            Remote = remote;
            OnMaintenance = false;
        }

        #endregion

        #region Methods

        public void Run()
        {
            try
            {
                Start();
                while (Started) System.Threading.Thread.Sleep(500);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void Stop()
        {
            Timer.Enabled = false;
            Started = false;
            OnStop?.Invoke(this, EventArgs.Empty);
        }

        private void Start()
        {
            Timer = new Timer();
            Timer.Elapsed += new ElapsedEventHandler(Execute);
            Timer.AutoReset = true;
            Timer.Interval = RefreshTime * 1000;
            Timer.Enabled = true;
            Started = true;
        }

        private void Execute(object sender, ElapsedEventArgs e)
        {
            Timer.Enabled = false;
            OnExecute?.Invoke(this, EventArgs.Empty);
            Timer.Enabled = true;
        }

        #endregion

        #region IDisposable

        public void Dispose()
        {
            Timer.Dispose();
        }

        #endregion
    }

}
