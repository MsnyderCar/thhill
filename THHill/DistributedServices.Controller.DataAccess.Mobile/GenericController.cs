﻿using Domain.Services.Mobile;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Transversal.Common.Entities.Api;

namespace DistributedServices.Controller.DataAccess.Mobile
{
    public class GenericController : IDisposable
    {
        #region Members

        public string WebApiBaseUrl;
        protected readonly bool IsAndroid;

        #endregion

        #region Constructor

        public GenericController(string webApiBaseUrl, bool isAndroid)
        {
            try
            {
                WebApiBaseUrl = webApiBaseUrl;
                IsAndroid = isAndroid;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Startup error: {ex.Message} {ex.StackTrace}");
            }
        }

        #endregion

        #region Public methods

        public string Initialize()
        {
            var response = string.Empty;

            try
            {
                response = "OK";
            }
            catch (Exception ex)
            {
                WriteLog(ex);
                response = $"{ex.Message} | {ex.StackTrace} | {ex.InnerException.Message} | {ex.InnerException.StackTrace}";
            }

            return response;
        }

        public AuthResponse Auth(AuthRequest request)
        {
            var response = new AuthResponse
            {
                StatusCode = 200,
                StatusMessage = new List<string> { "OK" },
                Token = null
            };

            var result = new ErrorContainer();

            try
            {
                if (request.Type == "login") result = (new Authentication(WebApiBaseUrl, IsAndroid)).Connect(request.Username, request.Password, request.Ip);
                
                if (result.InError)
                    throw new Exception(string.Join(", ", result.Message));
            }
            catch(Exception ex)
            {
                WriteLog(ex);
                response = new AuthResponse
                {
                    StatusCode = 500,
                    StatusMessage = new List<string> { $"{ex.Message}" },
                    Token = new Token()
                };
            }

            return response;
        }

        public Packet Exec(Packet request)
        {
            Packet response = null;
            BaseService service = null;

            try
            {
                CheckSecurity(request);
                service = GetCustomService(request.Service);
                response = service.Execute(request);
                response.StatusCode = 200;
                response.StatusMessage = new List<string>();
            }
            catch (Exception ex)
            {
                Exception realException = null;

                if (ex is TargetInvocationException)
                {
                    if (ex.InnerException != null)
                        realException = ex.InnerException;
                    else
                        realException = ex;
                }
                else
                    realException = ex;

                WriteLog(ex);

                response = new Packet
                {
                    StatusCode = 500,
                    StatusMessage = new List<string> { realException.Message }
                };
            }

            return response;
        }

        public void Clear()
        {
            var auth = (new Authentication(WebApiBaseUrl, true, IsAndroid, false));
        }

        #endregion

        #region Private Methods

        void CheckSecurity(Packet request)
        {
            /*if (!AuthenticationManager.HasPermissions(AccessToken, request.Service, request.Action))
            {
                throw new Exception("No posee permisos para ejecutar la accion");
            }*/
        }

        BaseService GetCustomService(string service)
        {
            BaseService oService = null;

            try
            {
                var baseType = typeof(Authentication);
                var types = Assembly.GetAssembly(baseType).GetTypes();
                var selectedType = types.Where(x => x.Name.Equals(service) && x.Namespace.Equals(baseType.Namespace)).ToList().First();
                oService = (dynamic)Activator.CreateInstance(selectedType, WebApiBaseUrl, IsAndroid, false);
            }
            catch (Exception ex)
            {
                WriteLog(ex);
            }

            return oService;
        }

        protected void WriteLog(Exception ex)
        {
            var date = DateTime.Now;
            Console.WriteLine($"{date.ToString("yyyy-MM-dd HH:mm:ss")}|Exception:{ex.ToString()}");
        }

        #endregion

        #region IDisposable

        public void Dispose()
        {
            
        }

        #endregion
    }
}
