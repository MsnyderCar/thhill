﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transversal.Common.Entities.Api;

namespace DistributedServices.Controller.DataAccess.Mobile
{
    public class RemoteManager : IRemoteManager, IDisposable
    {
        #region Members

        string Domain { get; set; }
        public GenericController GenericController { get; set; }

        public string UserName { get; set; }
        string Ip { get; set; }

        static object IsUpdateToken = new object();

        public bool Error
        {
            get
            {
                var error = ErrorContainer.Where(e => e.ExecutionId == Guid.Empty).FirstOrDefault();
                return error.InError;
            }
        }

        public string Message
        {
            get
            {
                var error = ErrorContainer.Where(e => e.ExecutionId == Guid.Empty).FirstOrDefault();
                return string.Join(", ", error.Message);
            }
        }

        List<ErrorContainer> ErrorContainer;

        #endregion

        #region Constructor

        public RemoteManager(string webApiBaseUrl, bool isAndroid)
        {
            GenericController = new GenericController(webApiBaseUrl, isAndroid);

            ErrorContainer = new List<ErrorContainer>
            {
                new ErrorContainer()
            };
        }

        #endregion

        #region IRemoteManager Methods

        public string WebApiBaseUrl {
            set
            {
                GenericController.WebApiBaseUrl = value;
            }
            get
            {
                return GenericController.WebApiBaseUrl;
            }
        }

        public bool Connect(string username, string password, string ip)
        {
            UserName = username;
            Ip = ip.Length <= 15 ? ip : "0.0.0.0";

            var result = ProcessAuth(
                GenericController.Auth(
                    new AuthRequest { Type = "login", Username = username, Password = password, Ip = Ip }
                )
            );

            if (result) GenericController.WebApiBaseUrl += $";{username};{password};";

            return result;
        }

        public bool Disconnect()
        {
            return ProcessAuth(
                GenericController.Auth(
                    new AuthRequest { Type = "logout", Ip = Ip }
                )
            );
        }

        public bool Forgot(string username)
        {
            return ProcessAuth(
                GenericController.Auth(
                    new AuthRequest { Type = "forgot", Username = username }
                )
            );
        }

        public bool CreatePassword(string username, string password, string token)
        {
            return ProcessAuth(
                GenericController.Auth(
                    new AuthRequest { Type = "create", Username = username, Password = password, AuthToken = token }
                )
            );
        }

        public AuthResponse ValidateAuthToken(string token)
        {
            return GenericController.Auth(new AuthRequest { Type = "validate", AuthToken = token });
        }

        public object Execute(string service, string action, params object[] args)
        {
            object response = null;

            var error = ErrorContainer.Where(e => e.ExecutionId == Guid.Empty).FirstOrDefault();

            var responseExecute = ExecuteInternal(service, action, args.Select(o => new RawData { Assembly = GetAssembly(o.GetType()), Data = o }).ToList());

            if (responseExecute.StatusCode == 200)
            {
                if (error == null) error = new ErrorContainer();
                error.Message = new List<string>();

                if (responseExecute.RawData.Count == 0)
                    response = null;
                else if (responseExecute.RawData.Count == 1)
                    response = responseExecute.RawData[0].Data;
                else
                    response = responseExecute.RawData.Select(r => r.Data).ToList();
            }
            else
            {
                error.Message = responseExecute.StatusMessage;
            }

            return response;
        }

        public ErrorContainer GetError(Guid executionId)
        {
            ErrorContainer response = null;

            var error = ErrorContainer.Where(e => e.ExecutionId == executionId).FirstOrDefault();

            if (error != null)
            {
                response = new ErrorContainer
                {
                    ExecutionId = error.ExecutionId,
                    Message = error.Message
                };

                ErrorContainer.Remove(error);
            }

            return response;
        }

        public void Clear()
        {
            GenericController.Clear();
        }

        #endregion

        #region Private Methods

        bool ProcessAuth(AuthResponse responseExecute)
        {
            var response = true;

            var error = ErrorContainer.Where(e => e.ExecutionId == Guid.Empty).FirstOrDefault();

            if (responseExecute.StatusCode == 200)
            {
                if (error == null) error = new ErrorContainer();
                error.Message = new List<string>();
            }
            else
            {
                error.Message = responseExecute.StatusMessage;
                response = false;
            }

            return response;
        }

        Packet ExecuteInternal(string service, string action, List<RawData> rawdata)
        {
            var packet = new Packet { Service = service, Action = action, RawData = rawdata };
            return GenericController.Exec(packet);
        }

        string GetAssembly(Type target)
        {
            var assembly = string.Empty;
            if (target.Name.Contains("List") && target.GenericTypeArguments.Length == 1)
            {
                assembly = $"{target.GenericTypeArguments[0].FullName} List";
            }
            else assembly = $"{target.FullName}";

            return assembly;
        }

        #endregion

        #region IDisposable

        public void Dispose()
        {
            GenericController.Dispose();
        }

        #endregion
    }
}
